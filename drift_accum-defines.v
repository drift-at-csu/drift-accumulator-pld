///////////////////////////////
//  Mandatory Defines       //
/////////////////////////////

`define XILINX
`define XILINX_PLL
`define SYSTEM_CLOCK_PERIOD 20 // in nsec 20nsec = 50 MHz

////////////////////////////////////
// Define a Feature to Enable it //
//////////////////////////////////
`define JTAG_DEBUG
// `define CAN0
// `define UART0
// `define SPI0
// `define ADC0
// `define ADC1
// `define ADC2
// `define ADC3
// `define ADC4
// `define MEM0
// `define ETH0
//===================================================================//
// Include OpenRisc1200 core defines
`include "cores/or1200/or1200_defines.v"
`ifdef JTAG_DEBUG
//	`include "cores/dbg_if/dbg_defines.v"
`endif // JTAG_DEBUG
