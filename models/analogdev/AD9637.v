/* This is self-made model for AD9637 It is made based on Data Specs
 * and have nothing to do with Analog Devices, except the fact that this
 * nice ADC itself exists. If you somehow got this model feel free to do
 * whatever you wnat with it. I don't care, AVD.
 */
`timescale 1ns/1ps
module AD9637 #(
	parameter CHAN1_INPUT_FILE = "chan1_input.txt",
	parameter CHAN2_INPUT_FILE = "chan2_input.txt",
	parameter CHAN3_INPUT_FILE = "chan3_input.txt",
	parameter CHAN4_INPUT_FILE = "chan4_input.txt",
	parameter CHAN5_INPUT_FILE = "chan5_input.txt",
	parameter CHAN6_INPUT_FILE = "chan6_input.txt",
	parameter CHAN7_INPUT_FILE = "chan7_input.txt",
	parameter CHAN8_INPUT_FILE = "chan8_input.txt",
	parameter SAMPLING_SPEED = 40,	// sampling freq in MBPS can be 40 or 80
	parameter ADC_BIT_WIDTH = 12,

	parameter tEH = 50.0, 
	parameter tEL = 50.0,

	parameter tCPD = 0.0,
	parameter tDCO = 1.000,

	parameter tFRAME = 0.0,
	parameter tPD = 0.0,

	parameter tDATA = 0.100
)(	//---> Slow Control Signals
	input wire PDWN,	// Active High Device Power-Down
	input wire SYNC,	// Active High Sync clocks for multiple ADCs
	//---> SPI Signals
	input wire CSB,
	inout wire SDIO_DFS,
	input wire SCLK_DTP,
	//---> ADCs Data output (lvds)
	output reg  D1_P,
	output wire D1_N,
	output reg D2_P,
	output wire D2_N,
	output reg D3_P,
	output wire D3_N,
	output reg D4_P,
	output wire D4_N,
	output reg D5_P,
	output wire D5_N,
	output reg D6_P,
	output wire D6_N,
	output reg D7_P,
	output wire D7_N,
	output reg D8_P,
	output wire D8_N,
	//---> Return clocks Frame Clock Output (FCO) and Data Clock Output (DCO)
	output reg FCO_P = 1'b0,
	output wire FCO_N,
	output reg DCO_P = 1'b0,
	output wire DCO_N,
	//---> Input Sampling Clock
	input wire CLK_P,
	input wire CLK_N
);
///////////////////////////////////////////////////
// Parameters, based on the Input Clock         //
/////////////////////////////////////////////////
wire clk_i;
// for now let's do this
assign clk_i = CLK_P;

reg internal_clk = 1'b0;

real period_clkin = 1000.0 / SAMPLING_SPEED; // In nsec
real period_data_clk = 1000.0 / SAMPLING_SPEED / ADC_BIT_WIDTH / 2.0;
time clkin_edge_time = 0;
integer clkin_period[4:0]; // in nsec

reg [7:0] clk_edges_sampled = 8'h0A; // 10 clocks are enough
wire clk_is_locked; assign clk_is_locked = (clk_edges_sampled == 8'h0);
always @(posedge clk_i) begin
	if (clk_edges_sampled != 8'h0)
		clk_edges_sampled <=  clk_edges_sampled - 1'b1;
end

always @(posedge clk_i) begin
  clkin_edge_time  <= $time;
  if (clkin_edge_time != 0) begin
	clkin_period[4] <= clkin_period[3];
	clkin_period[3] <= clkin_period[2];
	clkin_period[2] <= clkin_period[1];
	clkin_period[1] <= clkin_period[0];
	clkin_period[0] <= $time - clkin_edge_time;
	// $display("...Setting  data_clk_period to %f ns", period_data_clk);
  end
end

always @(clkin_period[0] or clkin_period[1] or clkin_period[2] or
	 clkin_period[3] or clkin_period[4] or posedge clk_i ) begin
	if ( clk_is_locked ) begin
		period_clkin = (	clkin_period[0] + clkin_period[1] + 
					clkin_period[2] + clkin_period[3] + 
					clkin_period[4] )/5.0;

		period_data_clk = (	clkin_period[0] + clkin_period[1] +
					clkin_period[2] + clkin_period[3] +
					clkin_period[4] ) / 10.0 / ADC_BIT_WIDTH;
		// $display("...Setting  data_clk_period to %f ns", period_data_clk);
	end

end

always begin
  #period_data_clk internal_clk <= clk_is_locked ? ~internal_clk : 1'b0;
end
//===================================================================//
// Now the internal_clk should have the frequency that we want for sampling

// Address of the bit, that we're accessing
reg [3:0] current_bit_addr_reg = ADC_BIT_WIDTH - 1;


always  @(posedge internal_clk) begin
	if (current_bit_addr_reg == 4'h0)
		current_bit_addr_reg <= ADC_BIT_WIDTH - 1;
	else
		current_bit_addr_reg <= current_bit_addr_reg - 1;
end

// Frame Clock
reg fco_reg = 1'b0;
always  @(posedge internal_clk) begin
	if (current_bit_addr_reg == (ADC_BIT_WIDTH - 1))
		fco_reg <= 1'b1;
	else if (current_bit_addr_reg == (ADC_BIT_WIDTH/2 - 1))
		fco_reg <= 1'b0;
end

// Data Clock (Double rate)
reg dco_reg = 1'b0;
always  @(posedge internal_clk) begin
	dco_reg <= ~dco_reg;
end

// Data
reg [7:0] adc_data_reg = 8'bXXXXXXXX;
reg [ADC_BIT_WIDTH-1:0] data1_input_reg = 'h000;
reg [ADC_BIT_WIDTH-1:0] data2_input_reg = 'h001;
reg [ADC_BIT_WIDTH-1:0] data3_input_reg = 'h002;
reg [ADC_BIT_WIDTH-1:0] data4_input_reg = 'h003;
reg [ADC_BIT_WIDTH-1:0] data5_input_reg = 'h004;
reg [ADC_BIT_WIDTH-1:0] data6_input_reg = 'h005;
reg [ADC_BIT_WIDTH-1:0] data7_input_reg = 'h006;
reg [ADC_BIT_WIDTH-1:0] data8_input_reg = 'h007;

// For now, let's just increment counter by one
always  @(posedge internal_clk) begin
	if (current_bit_addr_reg == (ADC_BIT_WIDTH - 1)) begin
		data1_input_reg <= data1_input_reg + 1'b1;
		data2_input_reg <= data2_input_reg + 1'b1;
		data3_input_reg <= data3_input_reg + 1'b1;
		data4_input_reg <= data4_input_reg + 1'b1;
		data5_input_reg <= data5_input_reg + 1'b1;
		data6_input_reg <= data6_input_reg + 1'b1;
		data7_input_reg <= data7_input_reg + 1'b1;
		data8_input_reg <= data8_input_reg + 1'b1;
	end	
end
always  @(posedge internal_clk) begin
	adc_data_reg[0] <= data1_input_reg[current_bit_addr_reg];
	adc_data_reg[1] <= data2_input_reg[current_bit_addr_reg];
	adc_data_reg[2] <= data3_input_reg[current_bit_addr_reg];
	adc_data_reg[3] <= data4_input_reg[current_bit_addr_reg];
	adc_data_reg[4] <= data5_input_reg[current_bit_addr_reg];
	adc_data_reg[5] <= data6_input_reg[current_bit_addr_reg];
	adc_data_reg[6] <= data7_input_reg[current_bit_addr_reg];
	adc_data_reg[7] <= data8_input_reg[current_bit_addr_reg];
end

// Assign this data with some delay
always @(posedge internal_clk) begin
	DCO_P <= #(tDATA + tDCO) dco_reg;
	FCO_P <= #(tDATA ) fco_reg;
	D1_P  <= #(tDATA ) adc_data_reg[0];
	D2_P  <= #(tDATA ) adc_data_reg[1];
	D3_P  <= #(tDATA ) adc_data_reg[2];
	D4_P  <= #(tDATA ) adc_data_reg[3];
	D5_P  <= #(tDATA ) adc_data_reg[4];
	D6_P  <= #(tDATA ) adc_data_reg[5];
	D7_P  <= #(tDATA ) adc_data_reg[6];
	D8_P  <= #(tDATA ) adc_data_reg[7];
end
assign DCO_N = !DCO_P;
assign FCO_N = !FCO_P;
assign D1_N = !D1_P;
assign D2_N = !D2_P;
assign D3_N = !D3_P;
assign D4_N = !D4_P;
assign D5_N = !D5_P;
assign D6_N = !D6_P;
assign D7_N = !D7_P;
assign D8_N = !D8_P;





endmodule
