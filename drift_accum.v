`include "drift_accum-defines.v"
`timescale 1ns/1ps
module drift_accum(

////////////////////////////////////
// ADC1                          //
//////////////////////////////////
input wire [8:1] adc1_d_i_p,
input wire [8:1] adc1_d_i_n,
output wire adc1_clk_o_p,
output wire adc1_clk_o_n,
input wire adc1_fco_i_p,
input wire adc1_fco_i_n,
input wire adc1_dco_i_p,
input wire adc1_dco_i_n,

output wire adc1_spi_clk_o,
inout wire adc1_spi_dat_io,
output wire adc1_spi_csb_o,

///////////////////////////////////
// ADC2                         //
/////////////////////////////////
input wire [8:1] adc2_d_i_p,
input wire [8:1] adc2_d_i_n,
output wire adc2_clk_o_p,
output wire adc2_clk_o_n,
input wire adc2_fco_i_p,
input wire adc2_fco_i_n,
input wire adc2_dco_i_p,
input wire adc2_dco_i_n,

output wire adc2_spi_clk_o,
inout wire adc2_spi_dat_io,
output wire adc2_spi_csb_o,

/////////////////////////////////////
// ADC3                           //
///////////////////////////////////
input wire [8:1] adc3_d_i_p,
input wire [8:1] adc3_d_i_n,
output wire adc3_clk_o_p,
output wire adc3_clk_o_n,
input wire adc3_fco_i_p,
input wire adc3_fco_i_n,
input wire adc3_dco_i_p,
input wire adc3_dco_i_n,

output wire adc3_spi_clk_o,
inout wire adc3_spi_dat_io,
output wire adc3_spi_csb_o,

////////////////////////////////////
// ADC4                          //
//////////////////////////////////
input wire [8:1] adc4_d_i_p,
input wire [8:1] adc4_d_i_n,
output wire adc4_clk_o_p,
output wire adc4_clk_o_n,
input wire adc4_fco_i_p,
input wire adc4_fco_i_n,
input wire adc4_dco_i_p,
input wire adc4_dco_i_n,

output wire adc4_spi_clk_o,
inout wire adc4_spi_dat_io,
output wire adc4_spi_csb_o,

///////////////////////////////////
// ADC5                         //
/////////////////////////////////
input wire [8:1] adc5_d_i_p,
input wire [8:1] adc5_d_i_n,
output wire adc5_clk_o_p,
output wire adc5_clk_o_n,
input wire adc5_fco_i_p,
input wire adc5_fco_i_n,
input wire adc5_dco_i_p,
input wire adc5_dco_i_n,

output wire adc5_spi_clk_o,
inout wire adc5_spi_dat_io,
output wire adc5_spi_csb_o,

/////////////////////////////////
// ADC6                       //
///////////////////////////////
input wire [8:1] adc6_d_i_p,
input wire [8:1] adc6_d_i_n,
output wire adc6_clk_o_p,
output wire adc6_clk_o_n,
input wire adc6_fco_i_p,
input wire adc6_fco_i_n,
input wire adc6_dco_i_p,
input wire adc6_dco_i_n,

output wire adc6_spi_clk_o,
inout wire adc6_spi_dat_io,
output wire adc6_spi_csb_o,

///////////////////////////
// MEM1                 //
/////////////////////////
input wire [8:0] mem1_data_i,
output wire [8:0] mem1_data_o,
output wire [21:0] mem1_addr_o,
output wire mem1_bws_o,
output wire mem1_wps_o,
output wire mem1_rps_o,
output wire mem1_doff_o,

output wire mem1_wclk_o_p,
output wire mem1_wclk_o_n,
output wire mem1_rclk_o_p,
output wire mem1_rclk_o_n,
input wire mem1_rclk_i_p,
input wire mem1_rclk_i_n,

////////////////////////////
// MEM2                  //
//////////////////////////
input wire [8:0] mem2_data_i,
output wire [8:0] mem2_data_o,
output wire [21:0] mem2_addr_o,
output wire mem2_bws_o,
output wire mem2_wps_o,
output wire mem2_rps_o,
output wire mem2_doff_o,

output wire mem2_wclk_o_p,
output wire mem2_wclk_o_n,
output wire mem2_rclk_o_p,
output wire mem2_rclk_o_n,
input wire mem2_rclk_i_p,
input wire mem2_rclk_i_n,

//////////////////////////////////////
// Ethernet 10/100/1000            //
////////////////////////////////////
output wire [7:0] eth1_tx_d_o,
output wire eth1_tx_error_o,
output wire eth1_gtx_clk_o,
output wire eth1_tx_enable_o,
input wire eth1_tx_ref_clk_i,

input wire [7:0] eth1_rx_d_i,
input wire eth1_rx_error_i,
input wire eth1_rx_data_valid_i,
input wire eth1_rx_ref_clk_i,

inout wire eth1_md_data_io,
output wire eth1_md_clk_o,

input wire eth1_crs_i,
input wire eth1_col_i,
input wire eth1_int_i,
input wire eth1_125MHz_clk_i,
output wire eth1_reset_o,

////////////////////////////////////////////////////////////////////////////
// SPI0                                                                  //
// The acces to spi0_clk_o(dedicated) is coming from JTAG user register //
/////////////////////////////////////////////////////////////////////////
output wire spi0_cs_o,
output wire spi0_data0_o,
input wire spi0_data1_i,
inout wire spi0_data2_io,
inout wire spi0_data3_io,
output wire spi0_reset_o,

///////////////
// SPI 1    //
/////////////
output wire spi1_cs_o,
output wire spi1_clk_o,
inout wire spi1_data0_io,
inout wire spi1_data1_io,
inout wire spi1_data2_io,
inout wire spi1_data3_io,
output wire spi1_reset_o,

///////////////////
//  SPI 2       //
/////////////////
output wire [7:0] spi2_cs_o,
output wire spi2_clk_o,
input wire spi2_sclk_i,
inout wire spi2_data0_io,
input wire spi2_data1_i,

/////////////////
// UART 0     //
///////////////
output wire uart0_tx_o,
input wire uart0_rx_i,

/////////////////
//  TWI 0     //
///////////////
inout wire twi0_sda_io,
inout wire twi0_scl_io,

//////////////////////////
// Power Controller    //
////////////////////////
output wire pow_adc1_vdd_enable_o,
output wire pow_adc1_g1_enable_o,
output wire pow_adc1_g2_enable_o,
output wire pow_adc1_g3_enable_o,
output wire pow_adc1_g4_enable_o,

output wire pow_adc2_vdd_enable_o,
output wire pow_adc2_g1_enable_o,
output wire pow_adc2_g2_enable_o,
output wire pow_adc2_g3_enable_o,
output wire pow_adc2_g4_enable_o,

output wire pow_adc3_vdd_enable_o,
output wire pow_adc3_g1_enable_o,
output wire pow_adc3_g2_enable_o,
output wire pow_adc3_g3_enable_o,
output wire pow_adc3_g4_enable_o,

output wire pow_adc4_vdd_enable_o,
output wire pow_adc4_g1_enable_o,
output wire pow_adc4_g2_enable_o,
output wire pow_adc4_g3_enable_o,
output wire pow_adc4_g4_enable_o,

output wire pow_adc5_vdd_enable_o,
output wire pow_adc5_g1_enable_o,
output wire pow_adc5_g2_enable_o,
output wire pow_adc5_g3_enable_o,
output wire pow_adc5_g4_enable_o,

output wire pow_adc6_vdd_enable_o,
output wire pow_adc6_g1_enable_o,
output wire pow_adc6_g2_enable_o,
output wire pow_adc6_g3_enable_o,
output wire pow_adc6_g4_enable_o,

output wire mem1_cntrl_s3_o,
output wire mem1_cntrl_s5_o,

output wire mem2_cntrl_s3_o,
output wire mem2_cntrl_s5_o,

output wire pow_qsfp1_clkresetn_o,
output wire pow_qsfp1_clken_o,

output wire pow_qsfp2_clkresetn_o,
output wire pow_qsfp2_clken_o,

/////////////////////////////////
//  QSFP 1                    //
///////////////////////////////
/*
input wire qsfp1_rx1_i_p,
input wire qsfp1_rx1_i_n,
input wire qsfp1_rx2_i_p,
input wire qsfp1_rx2_i_n,
input wire qsfp1_rx3_i_p,
input wire qsfp1_rx3_i_n,
input wire qsfp1_rx4_i_p,
input wire qsfp1_rx4_i_n,

output wire qsfp1_tx1_o_p,
output wire qsfp1_tx1_o_n,
output wire qsfp1_tx2_o_p,
output wire qsfp1_tx2_o_n,
output wire qsfp1_tx3_o_p,
output wire qsfp1_tx3_o_n,
output wire qsfp1_tx4_o_p,
output wire qsfp1_tx4_o_n,

input wire qsfp1_clk_i_p,
input wire qsfp1_clk_i_n,
*/

output wire qsfp1_cs_o,
inout wire qsfp1_sda_io,
output wire qsfp1_scl_o,
output wire qsfp1_mode_o,
input wire qsfp1_present_i,
output wire qsfp1_reset_o,
input wire qsfp1_interrupt_i,

/////////////////////////////////
//  QSFP 2                    //
///////////////////////////////

/*
input wire qsfp2_rx1_i_p,
input wire qsfp2_rx1_i_n,
input wire qsfp2_rx2_i_p,
input wire qsfp2_rx2_i_n,
input wire qsfp2_rx3_i_p,
input wire qsfp2_rx3_i_n,
input wire qsfp2_rx4_i_p,
input wire qsfp2_rx4_i_n,

output wire qsfp2_tx1_o_p,
output wire qsfp2_tx1_o_n,
output wire qsfp2_tx2_o_p,
output wire qsfp2_tx2_o_n,
output wire qsfp2_tx3_o_p,
output wire qsfp2_tx3_o_n,
output wire qsfp2_tx4_o_p,
output wire qsfp2_tx4_o_n,

input wire qsfp2_clk_i_p,
input wire qsfp2_clk_i_n,
*/

output wire qsfp2_cs_o,
inout wire qsfp2_sda_io,
output wire qsfp2_scl_o,
output wire qsfp2_mode_o,
input wire qsfp2_present_i,
output wire qsfp2_reset_o,
input wire qsfp2_interrupt_i,

/////////////////////////
//  System Clock      //
///////////////////////
input wire sys_clk_i_p,
input wire sys_clk_i_n
);
//===================================================================//
// There is a trick on how to use 2.5LVDS in 3.3V Banks (see ucf file also):
// Clock generator
wire sys_clk_i;
IBUFGDS #(
	.DIFF_TERM("FALSE"), // Differential Termination
        .IBUF_LOW_PWR("TRUE"), // Low power="TRUE", Highest performance="FALSE" 
	.IOSTANDARD("LVDS_25") // Specifies the I/O standard for this buffer
) input_sysclk_buffer_inst (
	.O(sys_clk_i),  // Clock buffer output
	.I(sys_clk_i_p),  // Diff_p clock buffer input
	.IB(sys_clk_i_n) // Diff_n clock buffer input
);
////////////////////////////////
// Clock Generator           //
//////////////////////////////
wire adcs_clk;

wire mems_clk;
wire mems_rst;

wire wb_clk;
wire wb_rst;

wire wbfast_clk;
wire wbfast_rst;

wire spi0_clk;
clkgen clkgen (
	// ADCs sampling clock
	.adcs_clk_o(adcs_clk),
	// Memories clock and reset
	.mems_clk_o( mems_clk ),
	.mems_rst_o( mems_rst ),
	// Normal wishbone clock and reset (CPU + Peripherials)
	.wb_clk_o(wb_clk),
	.wb_rst_o(wb_rst),
	// Fast wishbone bus clock and reset (ADCs + QSFPs)
	.wbfast_clk_o( wbfast_clk ),
	.wbfast_rst_o( wbfast_rst ),
	// Input system clock
	.sys_clk_i(sys_clk_i),
	// User Configuration Clock input (connected to SPI0 on the board)
	.cclock_i (spi0_clk)
);
//===================================================================//
//  INSTANCES:
//===================================================================//
// Real time Clock (should be generated at OR1200)
wire		one_pulse_per_sec_clk;
/////////////////////////////////////////////
// ADC1                                  ///
///////////////////////////////////////////
wire		adc1_interrupt;
// Wishbone slave signals
wire [31:0]	wbs_c_adc1_adr_i;
wire [3:0]	wbs_c_adc1_sel_i;
wire		wbs_c_adc1_we_i;
wire [31:0]	wbs_c_adc1_dat_i;
wire		wbs_c_adc1_cyc_i;
wire		wbs_c_adc1_stb_i;
wire [2:0]	wbs_c_adc1_cti_i;
wire [1:0]	wbs_c_adc1_bte_i;

wire [31:0]	wbs_c_adc1_dat_o;
wire		wbs_c_adc1_ack_o;
wire		wbs_c_adc1_err_o;
wire		wbs_c_adc1_rty_o;

// Wishbone Master signals
wire [31:0]	wbm_a_adc1_adr_o;
wire [3:0]	wbm_a_adc1_sel_o;
wire		wbm_a_adc1_we_o;
wire [31:0]	wbm_a_adc1_dat_o;
wire		wbm_a_adc1_cyc_o;
wire		wbm_a_adc1_stb_o;
wire [2:0]	wbm_a_adc1_cti_o;
wire [1:0]	wbm_a_adc1_bte_o;

wire [31:0]	wbm_a_adc1_dat_i;
wire		wbm_a_adc1_ack_i;
wire		wbm_a_adc1_err_i;
wire		wbm_a_adc1_rty_i;

adc_lvds #(
	.D1_IDELAY(15),
	.D2_IDELAY(15),
	.D3_IDELAY(15),
	.D4_IDELAY(15),
	.D5_IDELAY(15),
	.D6_IDELAY(15),
	.D7_IDELAY(15),
	.D8_IDELAY(15),
	.FC_IDELAY(15),
	.DC_IDELAY(15)
) adc1 (
//---> Master Wishbone connections
  .wbm_adr_o( wbm_a_adc1_adr_o ),
  .wbm_bte_o( wbm_a_adc1_bte_o ),
  .wbm_cti_o( wbm_a_adc1_cti_o ),
  .wbm_cyc_o( wbm_a_adc1_cyc_o ),
  .wbm_dat_o( wbm_a_adc1_dat_o ),
  .wbm_sel_o( wbm_a_adc1_sel_o ),
  .wbm_stb_o( wbm_a_adc1_stb_o ),
  .wbm_we_o ( wbm_a_adc1_we_o  ),
  .wbm_ack_i( wbm_a_adc1_ack_i ),
  .wbm_err_i( wbm_a_adc1_err_i ),
  .wbm_rty_i( wbm_a_adc1_rty_i ),
  .wbm_dat_i( wbm_a_adc1_dat_i ),
  .wbm_clk_i( wbfast_clk ),
  .wbm_rst_i( wbfast_rst ),
//---> Slave Wishbone connections
  .wbs_adr_i( wbs_c_adc1_adr_i ),
  .wbs_bte_i( wbs_c_adc1_bte_i ),
  .wbs_cti_i( wbs_c_adc1_cti_i ),
  .wbs_cyc_i( wbs_c_adc1_cyc_i ),
  .wbs_dat_i( wbs_c_adc1_dat_i ),
  .wbs_sel_i( wbs_c_adc1_sel_i ),
  .wbs_stb_i( wbs_c_adc1_stb_i ),
  .wbs_we_i ( wbs_c_adc1_we_i  ),
  .wbs_ack_o( wbs_c_adc1_ack_o ),
  .wbs_err_o( wbs_c_adc1_err_o ),
  .wbs_rty_o( wbs_c_adc1_rty_o ),
  .wbs_dat_o( wbs_c_adc1_dat_o ),
  .wbs_clk_i( wb_clk ),
  .wbs_rst_i( wb_rst ),
//---> Connection to the ADC Chip
  .data_i_p		(adc1_d_i_p),
  .data_i_n		(adc1_d_i_n),
  .frame_clk_i_p	(adc1_fco_i_p),
  .frame_clk_i_n	(adc1_fco_i_n),
  .data_clk_i_p		(adc1_dco_i_p),
  .data_clk_i_n		(adc1_dco_i_n),
  .sample_clk_o_p	(adc1_clk_o_p),
  .sample_clk_o_n	(adc1_clk_o_n),
  .spi_cs_o		(adc1_spi_csb_o),
  .spi_dat_io		(adc1_spi_dat_io),
  .spi_clk_o		(adc1_spi_clk_o),
//---> Power Management
  .pow_adc_en_o	( pow_adc1_vdd_enable_o ),
  .pow_gr1_en_o	( pow_adc1_g1_enable_o  ),
  .pow_gr2_en_o	( pow_adc1_g2_enable_o  ),
  .pow_gr3_en_o	( pow_adc1_g3_enable_o  ),
  .pow_gr4_en_o	( pow_adc1_g4_enable_o  ),
//---> Interrupt
  .interrupt_o	( adc1_interrupt ),
//---> For Timestamp:
  .time_pulse_i ( one_pulse_per_sec_clk ),
//---> Input Clock for Sample Frequency
  .adc_clk_i	( adcs_clk )
);
//===================================================================//
///////////////////////////////////////////
// ADC2                                 //
/////////////////////////////////////////
wire		adc2_interrupt;
// Wishbone slave signals
wire [31:0]	wbs_c_adc2_adr_i;
wire [3:0]	wbs_c_adc2_sel_i;
wire		wbs_c_adc2_we_i;
wire [31:0]	wbs_c_adc2_dat_i;
wire		wbs_c_adc2_cyc_i;
wire		wbs_c_adc2_stb_i;
wire [2:0]	wbs_c_adc2_cti_i;
wire [1:0]	wbs_c_adc2_bte_i;

wire [31:0]	wbs_c_adc2_dat_o;
wire		wbs_c_adc2_ack_o;
wire		wbs_c_adc2_err_o;
wire		wbs_c_adc2_rty_o;

// Wishbone Master signals
wire [31:0]	wbm_a_adc2_adr_o;
wire [3:0]	wbm_a_adc2_sel_o;
wire		wbm_a_adc2_we_o;
wire [31:0]	wbm_a_adc2_dat_o;
wire		wbm_a_adc2_cyc_o;
wire		wbm_a_adc2_stb_o;
wire [2:0]	wbm_a_adc2_cti_o;
wire [1:0]	wbm_a_adc2_bte_o;

wire [31:0]	wbm_a_adc2_dat_i;
wire		wbm_a_adc2_ack_i;
wire		wbm_a_adc2_err_i;
wire		wbm_a_adc2_rty_i;

adc_lvds #(
	.D1_IDELAY(15),
	.D2_IDELAY(15),
	.D3_IDELAY(15),
	.D4_IDELAY(15),
	.D5_IDELAY(15),
	.D6_IDELAY(15),
	.D7_IDELAY(15),
	.D8_IDELAY(15),
	.FC_IDELAY(15),
	.DC_IDELAY(15)
) adc2 (
//---> Master Wishbone connections
  .wbm_adr_o( wbm_a_adc2_adr_o ),
  .wbm_bte_o( wbm_a_adc2_bte_o ),
  .wbm_cti_o( wbm_a_adc2_cti_o ),
  .wbm_cyc_o( wbm_a_adc2_cyc_o ),
  .wbm_dat_o( wbm_a_adc2_dat_o ),
  .wbm_sel_o( wbm_a_adc2_sel_o ),
  .wbm_stb_o( wbm_a_adc2_stb_o ),
  .wbm_we_o ( wbm_a_adc2_we_o  ),
  .wbm_ack_i( wbm_a_adc2_ack_i ),
  .wbm_err_i( wbm_a_adc2_err_i ),
  .wbm_rty_i( wbm_a_adc2_rty_i ),
  .wbm_dat_i( wbm_a_adc2_dat_i ),
  .wbm_clk_i( wbfast_clk ),
  .wbm_rst_i( wbfast_rst ),
//---> Slave Wishbone connections
  .wbs_adr_i( wbs_c_adc2_adr_i ),
  .wbs_bte_i( wbs_c_adc2_bte_i ),
  .wbs_cti_i( wbs_c_adc2_cti_i ),
  .wbs_cyc_i( wbs_c_adc2_cyc_i ),
  .wbs_dat_i( wbs_c_adc2_dat_i ),
  .wbs_sel_i( wbs_c_adc2_sel_i ),
  .wbs_stb_i( wbs_c_adc2_stb_i ),
  .wbs_we_i ( wbs_c_adc2_we_i  ),
  .wbs_ack_o( wbs_c_adc2_ack_o ),
  .wbs_err_o( wbs_c_adc2_err_o ),
  .wbs_rty_o( wbs_c_adc2_rty_o ),
  .wbs_dat_o( wbs_c_adc2_dat_o ),
  .wbs_clk_i( wb_clk ),
  .wbs_rst_i( wb_rst ),
//---> Connection to the ADC Chip
  .data_i_p		(adc2_d_i_p),
  .data_i_n		(adc2_d_i_n),
  .frame_clk_i_p	(adc2_fco_i_p),
  .frame_clk_i_n	(adc2_fco_i_n),
  .data_clk_i_p		(adc2_dco_i_p),
  .data_clk_i_n		(adc2_dco_i_n),
  .sample_clk_o_p	(adc2_clk_o_p),
  .sample_clk_o_n	(adc2_clk_o_n),
  .spi_cs_o		(adc2_spi_csb_o),
  .spi_dat_io		(adc2_spi_dat_io),
  .spi_clk_o		(adc2_spi_clk_o),
//---> Power Management
  .pow_adc_en_o ( pow_adc2_vdd_enable_o ),
  .pow_gr1_en_o ( pow_adc2_g1_enable_o  ),
  .pow_gr2_en_o ( pow_adc2_g2_enable_o  ),
  .pow_gr3_en_o ( pow_adc2_g3_enable_o  ),
  .pow_gr4_en_o ( pow_adc2_g4_enable_o  ),
//---> Interrupt
  .interrupt_o	( adc2_interrupt ),
//---> For Timestamp:
  .time_pulse_i ( one_pulse_per_sec_clk ),
//---> Input Clock for Sample Frequency
  .adc_clk_i	( adcs_clk )
);
//===================================================================//
///////////////////////////////////////////
// ADC3                                 //
/////////////////////////////////////////
wire		adc3_interrupt;
// Wishbone slave signals
wire [31:0]	wbs_c_adc3_adr_i;
wire [3:0]	wbs_c_adc3_sel_i;
wire		wbs_c_adc3_we_i;
wire [31:0]	wbs_c_adc3_dat_i;
wire		wbs_c_adc3_cyc_i;
wire		wbs_c_adc3_stb_i;
wire [2:0]	wbs_c_adc3_cti_i;
wire [1:0]	wbs_c_adc3_bte_i;

wire [31:0]	wbs_c_adc3_dat_o;
wire		wbs_c_adc3_ack_o;
wire		wbs_c_adc3_err_o;
wire		wbs_c_adc3_rty_o;

// Wishbone Master signals
wire [31:0]	wbm_a_adc3_adr_o;
wire [3:0]	wbm_a_adc3_sel_o;
wire		wbm_a_adc3_we_o;
wire [31:0]	wbm_a_adc3_dat_o;
wire		wbm_a_adc3_cyc_o;
wire		wbm_a_adc3_stb_o;
wire [2:0]	wbm_a_adc3_cti_o;
wire [1:0]	wbm_a_adc3_bte_o;

wire [31:0]	wbm_a_adc3_dat_i;
wire		wbm_a_adc3_ack_i;
wire		wbm_a_adc3_err_i;
wire		wbm_a_adc3_rty_i;

adc_lvds #( 
	.D1_IDELAY(15),
	.D2_IDELAY(15),
	.D3_IDELAY(15),
	.D4_IDELAY(15),
	.D5_IDELAY(15),
	.D6_IDELAY(15),
	.D7_IDELAY(15),
	.D8_IDELAY(15),
	.FC_IDELAY(15),
	.DC_IDELAY(15)
) adc3 (
//---> Master Wishbone connections
  .wbm_adr_o( wbm_a_adc3_adr_o ),
  .wbm_bte_o( wbm_a_adc3_bte_o ),
  .wbm_cti_o( wbm_a_adc3_cti_o ),
  .wbm_cyc_o( wbm_a_adc3_cyc_o ),
  .wbm_dat_o( wbm_a_adc3_dat_o ),
  .wbm_sel_o( wbm_a_adc3_sel_o ),
  .wbm_stb_o( wbm_a_adc3_stb_o ),
  .wbm_we_o ( wbm_a_adc3_we_o  ),
  .wbm_ack_i( wbm_a_adc3_ack_i ),
  .wbm_err_i( wbm_a_adc3_err_i ),
  .wbm_rty_i( wbm_a_adc3_rty_i ),
  .wbm_dat_i( wbm_a_adc3_dat_i ),
  .wbm_clk_i( wbfast_clk ),
  .wbm_rst_i( wbfast_rst ),
//---> Slave Wishbone connections
  .wbs_adr_i( wbs_c_adc3_adr_i ),
  .wbs_bte_i( wbs_c_adc3_bte_i ),
  .wbs_cti_i( wbs_c_adc3_cti_i ),
  .wbs_cyc_i( wbs_c_adc3_cyc_i ),
  .wbs_dat_i( wbs_c_adc3_dat_i ),
  .wbs_sel_i( wbs_c_adc3_sel_i ),
  .wbs_stb_i( wbs_c_adc3_stb_i ),
  .wbs_we_i ( wbs_c_adc3_we_i  ),
  .wbs_ack_o( wbs_c_adc3_ack_o ),
  .wbs_err_o( wbs_c_adc3_err_o ),
  .wbs_rty_o( wbs_c_adc3_rty_o ),
  .wbs_dat_o( wbs_c_adc3_dat_o ),
  .wbs_clk_i( wb_clk ),
  .wbs_rst_i( wb_rst ),
//---> Connection to the ADC Chip
  .data_i_p		(adc3_d_i_p),
  .data_i_n		(adc3_d_i_n),
  .frame_clk_i_p	(adc3_fco_i_p),
  .frame_clk_i_n	(adc3_fco_i_n),
  .data_clk_i_p		(adc3_dco_i_p),
  .data_clk_i_n		(adc3_dco_i_n),
  .sample_clk_o_p	(adc3_clk_o_p),
  .sample_clk_o_n	(adc3_clk_o_n),
  .spi_cs_o		(adc3_spi_csb_o),
  .spi_dat_io		(adc3_spi_dat_io),
  .spi_clk_o		(adc3_spi_clk_o),
//---> Power Management
  .pow_adc_en_o ( pow_adc3_vdd_enable_o ),
  .pow_gr1_en_o ( pow_adc3_g1_enable_o  ),
  .pow_gr2_en_o ( pow_adc3_g2_enable_o  ),
  .pow_gr3_en_o ( pow_adc3_g3_enable_o  ),
  .pow_gr4_en_o ( pow_adc3_g4_enable_o  ),
//---> Interrupt
  .interrupt_o	( adc3_interrupt ),
//---> For Timestamp:
  .time_pulse_i ( one_pulse_per_sec_clk ),
//---> Input Clock for Sample Frequency
  .adc_clk_i	( adcs_clk )
);
//===================================================================//
///////////////////////////////////////////
// ADC4                                 //
/////////////////////////////////////////
wire		adc4_interrupt;
// Wishbone slave signals
wire [31:0]	wbs_c_adc4_adr_i;
wire [3:0]	wbs_c_adc4_sel_i;
wire		wbs_c_adc4_we_i;
wire [31:0]	wbs_c_adc4_dat_i;
wire		wbs_c_adc4_cyc_i;
wire		wbs_c_adc4_stb_i;
wire [2:0]	wbs_c_adc4_cti_i;
wire [1:0]	wbs_c_adc4_bte_i;

wire [31:0]	wbs_c_adc4_dat_o;
wire		wbs_c_adc4_ack_o;
wire		wbs_c_adc4_err_o;
wire		wbs_c_adc4_rty_o;

// Wishbone Master signals
wire [31:0]	wbm_a_adc4_adr_o;
wire [3:0]	wbm_a_adc4_sel_o;
wire		wbm_a_adc4_we_o;
wire [31:0]	wbm_a_adc4_dat_o;
wire		wbm_a_adc4_cyc_o;
wire		wbm_a_adc4_stb_o;
wire [2:0]	wbm_a_adc4_cti_o;
wire [1:0]	wbm_a_adc4_bte_o;

wire [31:0]	wbm_a_adc4_dat_i;
wire		wbm_a_adc4_ack_i;
wire		wbm_a_adc4_err_i;
wire		wbm_a_adc4_rty_i;

adc_lvds #( 
	.D1_IDELAY(15),
	.D2_IDELAY(15),
	.D3_IDELAY(15),
	.D4_IDELAY(15),
	.D5_IDELAY(15),
	.D6_IDELAY(15),
	.D7_IDELAY(15),
	.D8_IDELAY(15),
	.FC_IDELAY(15),
	.DC_IDELAY(15)
) adc4 (
//---> Master Wishbone connections
  .wbm_adr_o( wbm_a_adc4_adr_o ),
  .wbm_bte_o( wbm_a_adc4_bte_o ),
  .wbm_cti_o( wbm_a_adc4_cti_o ),
  .wbm_cyc_o( wbm_a_adc4_cyc_o ),
  .wbm_dat_o( wbm_a_adc4_dat_o ),
  .wbm_sel_o( wbm_a_adc4_sel_o ),
  .wbm_stb_o( wbm_a_adc4_stb_o ),
  .wbm_we_o ( wbm_a_adc4_we_o  ),
  .wbm_ack_i( wbm_a_adc4_ack_i ),
  .wbm_err_i( wbm_a_adc4_err_i ),
  .wbm_rty_i( wbm_a_adc4_rty_i ),
  .wbm_dat_i( wbm_a_adc4_dat_i ),
  .wbm_clk_i( wbfast_clk ),
  .wbm_rst_i( wbfast_rst ),
//---> Slave Wishbone connections
  .wbs_adr_i( wbs_c_adc4_adr_i ),
  .wbs_bte_i( wbs_c_adc4_bte_i ),
  .wbs_cti_i( wbs_c_adc4_cti_i ),
  .wbs_cyc_i( wbs_c_adc4_cyc_i ),
  .wbs_dat_i( wbs_c_adc4_dat_i ),
  .wbs_sel_i( wbs_c_adc4_sel_i ),
  .wbs_stb_i( wbs_c_adc4_stb_i ),
  .wbs_we_i ( wbs_c_adc4_we_i  ),
  .wbs_ack_o( wbs_c_adc4_ack_o ),
  .wbs_err_o( wbs_c_adc4_err_o ),
  .wbs_rty_o( wbs_c_adc4_rty_o ),
  .wbs_dat_o( wbs_c_adc4_dat_o ),
  .wbs_clk_i( wb_clk ),
  .wbs_rst_i( wb_rst ),
//---> Connection to the ADC Chip
  .data_i_p		(adc4_d_i_p),
  .data_i_n		(adc4_d_i_n),
  .frame_clk_i_p	(adc4_fco_i_p),
  .frame_clk_i_n	(adc4_fco_i_n),
  .data_clk_i_p		(adc4_dco_i_p),
  .data_clk_i_n		(adc4_dco_i_n),
  .sample_clk_o_p	(adc4_clk_o_p),
  .sample_clk_o_n	(adc4_clk_o_n),
  .spi_cs_o		(adc4_spi_csb_o),
  .spi_dat_io		(adc4_spi_dat_io),
  .spi_clk_o		(adc4_spi_clk_o),
//---> Power Management
  .pow_adc_en_o ( pow_adc4_vdd_enable_o ),
  .pow_gr1_en_o ( pow_adc4_g1_enable_o  ),
  .pow_gr2_en_o ( pow_adc4_g2_enable_o  ),
  .pow_gr3_en_o ( pow_adc4_g3_enable_o  ),
  .pow_gr4_en_o ( pow_adc4_g4_enable_o  ),
//---> Interrupt
  .interrupt_o	( adc4_interrupt ),
//---> For Timestamp:
  .time_pulse_i ( one_pulse_per_sec_clk ),
//---> Input Clock for Sample Frequency
  .adc_clk_i	( adcs_clk )
);
//===================================================================//
///////////////////////////////////////////
// ADC5                                 //
/////////////////////////////////////////
wire            adc5_interrupt;
// Wishbone slave signals
wire [31:0]     wbs_c_adc5_adr_i;
wire [3:0]      wbs_c_adc5_sel_i;
wire            wbs_c_adc5_we_i;
wire [31:0]     wbs_c_adc5_dat_i;
wire            wbs_c_adc5_cyc_i;
wire            wbs_c_adc5_stb_i;
wire [2:0]      wbs_c_adc5_cti_i;
wire [1:0]      wbs_c_adc5_bte_i;

wire [31:0]     wbs_c_adc5_dat_o;
wire            wbs_c_adc5_ack_o;
wire            wbs_c_adc5_err_o;
wire            wbs_c_adc5_rty_o;

// Wishbone Master signals
wire [31:0]     wbm_a_adc5_adr_o;
wire [3:0]      wbm_a_adc5_sel_o;
wire            wbm_a_adc5_we_o;
wire [31:0]     wbm_a_adc5_dat_o;
wire            wbm_a_adc5_cyc_o;
wire            wbm_a_adc5_stb_o;
wire [2:0]      wbm_a_adc5_cti_o;
wire [1:0]      wbm_a_adc5_bte_o;

wire [31:0]     wbm_a_adc5_dat_i;
wire            wbm_a_adc5_ack_i;
wire            wbm_a_adc5_err_i;
wire            wbm_a_adc5_rty_i;

adc_lvds #( 
	.D1_IDELAY(15),
	.D2_IDELAY(15),
	.D3_IDELAY(15),
	.D4_IDELAY(15),
	.D5_IDELAY(15),
	.D6_IDELAY(15),
	.D7_IDELAY(15),
	.D8_IDELAY(15),
	.FC_IDELAY(15),
	.DC_IDELAY(15)
) adc5 (
//---> Master Wishbone connections
  .wbm_adr_o( wbm_a_adc5_adr_o ),
  .wbm_bte_o( wbm_a_adc5_bte_o ),
  .wbm_cti_o( wbm_a_adc5_cti_o ),
  .wbm_cyc_o( wbm_a_adc5_cyc_o ),
  .wbm_dat_o( wbm_a_adc5_dat_o ),
  .wbm_sel_o( wbm_a_adc5_sel_o ),
  .wbm_stb_o( wbm_a_adc5_stb_o ),
  .wbm_we_o ( wbm_a_adc5_we_o  ),
  .wbm_ack_i( wbm_a_adc5_ack_i ),
  .wbm_err_i( wbm_a_adc5_err_i ),
  .wbm_rty_i( wbm_a_adc5_rty_i ),
  .wbm_dat_i( wbm_a_adc5_dat_i ),
  .wbm_clk_i( wbfast_clk ),
  .wbm_rst_i( wbfast_rst ),
//---> Slave Wishbone connections
  .wbs_adr_i( wbs_c_adc5_adr_i ),
  .wbs_bte_i( wbs_c_adc5_bte_i ),
  .wbs_cti_i( wbs_c_adc5_cti_i ),
  .wbs_cyc_i( wbs_c_adc5_cyc_i ),
  .wbs_dat_i( wbs_c_adc5_dat_i ),
  .wbs_sel_i( wbs_c_adc5_sel_i ),
  .wbs_stb_i( wbs_c_adc5_stb_i ),
  .wbs_we_i ( wbs_c_adc5_we_i  ),
  .wbs_ack_o( wbs_c_adc5_ack_o ),
  .wbs_err_o( wbs_c_adc5_err_o ),
  .wbs_rty_o( wbs_c_adc5_rty_o ),
  .wbs_dat_o( wbs_c_adc5_dat_o ),
  .wbs_clk_i( wb_clk ),
  .wbs_rst_i( wb_rst ),
//---> Connection to the ADC Chip
  .data_i_p		(adc5_d_i_p),
  .data_i_n		(adc5_d_i_n),
  .frame_clk_i_p	(adc5_fco_i_p),
  .frame_clk_i_n	(adc5_fco_i_n),
  .data_clk_i_p		(adc5_dco_i_p),
  .data_clk_i_n		(adc5_dco_i_n),
  .sample_clk_o_p	(adc5_clk_o_p),
  .sample_clk_o_n	(adc5_clk_o_n),
  .spi_cs_o		(adc5_spi_csb_o),
  .spi_dat_io		(adc5_spi_dat_io),
  .spi_clk_o		(adc5_spi_clk_o),
//---> Power Management
  .pow_adc_en_o ( pow_adc5_vdd_enable_o ),
  .pow_gr1_en_o ( pow_adc5_g1_enable_o  ),
  .pow_gr2_en_o ( pow_adc5_g2_enable_o  ),
  .pow_gr3_en_o ( pow_adc5_g3_enable_o  ),
  .pow_gr4_en_o ( pow_adc5_g4_enable_o  ),
//---> Interrupt
  .interrupt_o	( adc5_interrupt ),
//---> For Timestamp:
  .time_pulse_i ( one_pulse_per_sec_clk ),
//---> Input Clock for Sample Frequency
  .adc_clk_i	( adcs_clk )
);
//===================================================================//
///////////////////////////////////////////
// ADC6                                 //
/////////////////////////////////////////
wire		adc6_interrupt;
// Wishbone slave signals
wire [31:0]	wbs_c_adc6_adr_i;
wire [3:0]	wbs_c_adc6_sel_i;
wire		wbs_c_adc6_we_i;
wire [31:0]	wbs_c_adc6_dat_i;
wire		wbs_c_adc6_cyc_i;
wire		wbs_c_adc6_stb_i;
wire [2:0]	wbs_c_adc6_cti_i;
wire [1:0]	wbs_c_adc6_bte_i;

wire [31:0]	wbs_c_adc6_dat_o;
wire		wbs_c_adc6_ack_o;
wire		wbs_c_adc6_err_o;
wire		wbs_c_adc6_rty_o;

// Wishbone Master signals
wire [31:0]	wbm_a_adc6_adr_o;
wire [3:0]	wbm_a_adc6_sel_o;
wire		wbm_a_adc6_we_o;
wire [31:0]	wbm_a_adc6_dat_o;
wire		wbm_a_adc6_cyc_o;
wire		wbm_a_adc6_stb_o;
wire [2:0]	wbm_a_adc6_cti_o;
wire [1:0]	wbm_a_adc6_bte_o;

wire [31:0]	wbm_a_adc6_dat_i;
wire		wbm_a_adc6_ack_i;
wire		wbm_a_adc6_err_i;
wire		wbm_a_adc6_rty_i;

adc_lvds #( 
        .D1_IDELAY(15),
        .D2_IDELAY(15),
        .D3_IDELAY(15),
        .D4_IDELAY(15),
        .D5_IDELAY(15),
        .D6_IDELAY(15),
        .D7_IDELAY(15),
        .D8_IDELAY(15),
        .FC_IDELAY(15),
        .DC_IDELAY(15)
) adc6 (
//---> Master Wishbone connections
  .wbm_adr_o( wbm_a_adc6_adr_o ),
  .wbm_bte_o( wbm_a_adc6_bte_o ),
  .wbm_cti_o( wbm_a_adc6_cti_o ),
  .wbm_cyc_o( wbm_a_adc6_cyc_o ),
  .wbm_dat_o( wbm_a_adc6_dat_o ),
  .wbm_sel_o( wbm_a_adc6_sel_o ),
  .wbm_stb_o( wbm_a_adc6_stb_o ),
  .wbm_we_o ( wbm_a_adc6_we_o  ),
  .wbm_ack_i( wbm_a_adc6_ack_i ),
  .wbm_err_i( wbm_a_adc6_err_i ),
  .wbm_rty_i( wbm_a_adc6_rty_i ),
  .wbm_dat_i( wbm_a_adc6_dat_i ),
  .wbm_clk_i( wbfast_clk ),
  .wbm_rst_i( wbfast_rst ),
//---> Slave Wishbone connections
  .wbs_adr_i( wbs_c_adc6_adr_i ),
  .wbs_bte_i( wbs_c_adc6_bte_i ),
  .wbs_cti_i( wbs_c_adc6_cti_i ),
  .wbs_cyc_i( wbs_c_adc6_cyc_i ),
  .wbs_dat_i( wbs_c_adc6_dat_i ),
  .wbs_sel_i( wbs_c_adc6_sel_i ),
  .wbs_stb_i( wbs_c_adc6_stb_i ),
  .wbs_we_i ( wbs_c_adc6_we_i  ),
  .wbs_ack_o( wbs_c_adc6_ack_o ),
  .wbs_err_o( wbs_c_adc6_err_o ),
  .wbs_rty_o( wbs_c_adc6_rty_o ),
  .wbs_dat_o( wbs_c_adc6_dat_o ),
  .wbs_clk_i( wb_clk ),
  .wbs_rst_i( wb_rst ),
//---> Connection to the ADC Chip
  .data_i_p		(adc6_d_i_p),
  .data_i_n		(adc6_d_i_n),
  .frame_clk_i_p	(adc6_fco_i_p),
  .frame_clk_i_n	(adc6_fco_i_n),
  .data_clk_i_p		(adc6_dco_i_p),
  .data_clk_i_n		(adc6_dco_i_n),
  .sample_clk_o_p	(adc6_clk_o_p),
  .sample_clk_o_n	(adc6_clk_o_n),
  .spi_cs_o		(adc6_spi_csb_o),
  .spi_dat_io		(adc6_spi_dat_io),
  .spi_clk_o		(adc6_spi_clk_o),
//---> Power Management
  .pow_adc_en_o ( pow_adc6_vdd_enable_o ),
  .pow_gr1_en_o ( pow_adc6_g1_enable_o  ),
  .pow_gr2_en_o ( pow_adc6_g2_enable_o  ),
  .pow_gr3_en_o ( pow_adc6_g3_enable_o  ),
  .pow_gr4_en_o ( pow_adc6_g4_enable_o  ),
//---> Interrupt
  .interrupt_o  ( adc6_interrupt ),
//---> For Timestamp:
  .time_pulse_i ( one_pulse_per_sec_clk ),
//---> Input Clock for Sample Frequency
  .adc_clk_i    ( adcs_clk )
);
//===================================================================//
//////////////////////////////////
// MEM0                       ///
////////////////////////////////
// Wishbone Slave Port0 wires 
wire		wbs_i_mem0_ack_o;
wire		wbs_i_mem0_err_o;
wire		wbs_i_mem0_rty_o;
wire [31:0]	wbs_i_mem0_dat_o;
wire		wbs_i_mem0_cyc_i;
wire [31:0]	wbs_i_mem0_adr_i;
wire		wbs_i_mem0_stb_i;
wire		wbs_i_mem0_we_i;
wire [3:0]	wbs_i_mem0_sel_i;
wire [31:0]	wbs_i_mem0_dat_i;
wire		wbs_i_mem0_cab_i; // Consecutive Address Burst Indicator
wire [2:0]	wbs_i_mem0_cti_i;
wire [1:0]	wbs_i_mem0_bte_i;
// Wishbone Slave Port1 wires
wire		wbs_d_mem0_ack_o;
wire		wbs_d_mem0_err_o;
wire		wbs_d_mem0_rty_o;
wire [31:0]	wbs_d_mem0_dat_o;
wire		wbs_d_mem0_cyc_i;
wire [31:0]	wbs_d_mem0_adr_i;
wire		wbs_d_mem0_stb_i;
wire		wbs_d_mem0_we_i;
wire [3:0]	wbs_d_mem0_sel_i;
wire [31:0]	wbs_d_mem0_dat_i;
wire		wbs_d_mem0_cab_i; // Consecutive Address Burst Indicator
wire [2:0]	wbs_d_mem0_cti_i;
wire [1:0]	wbs_d_mem0_bte_i;
qdrii_wb #( 
	.iodelay_group_name ("mem0_delay_group"),

	.data0_delay 	 ( 12 ), // 15
	.data1_delay 	 ( 12 ), // 15
	.data2_delay 	 ( 10 ), // 15 <--Monitoring this
	.data3_delay 	 ( 12 ), // 15
	.data4_delay 	 ( 12 ), // 10
	.data5_delay 	 ( 12 ), // 10
	.data6_delay 	 ( 12 ), // 15
	.data7_delay 	 ( 14 ), // 08

	.data8_delay 	 ( 15 ), // 15
	.echo_clk_p_delay( 15 ), // 15
	.echo_clk_n_delay( 15 )  // 15
) mem0 (
  //---> Port 0
  .wbs0_adr_i( wbs_i_mem0_adr_i ),
  .wbs0_bte_i( wbs_i_mem0_bte_i ),
  .wbs0_cti_i( wbs_i_mem0_cti_i ),
  .wbs0_cyc_i( wbs_i_mem0_cyc_i ),
  .wbs0_dat_i( wbs_i_mem0_dat_i ),
  .wbs0_sel_i( wbs_i_mem0_sel_i ),
  .wbs0_stb_i( wbs_i_mem0_stb_i ),
  .wbs0_we_i( wbs_i_mem0_we_i ),
  .wbs0_cab_i( wbs_i_mem0_cab_i ),
  .wbs0_ack_o( wbs_i_mem0_ack_o ),
  .wbs0_err_o( wbs_i_mem0_err_o ),
  .wbs0_rty_o( wbs_i_mem0_rty_o ),
  .wbs0_dat_o( wbs_i_mem0_dat_o ),

  .wbs0_clk_i( wb_clk ),
  .wbs0_rst_i( wb_rst ),
  //---> Port 1
  .wbs1_adr_i( wbs_d_mem0_adr_i ),
  .wbs1_bte_i( wbs_d_mem0_bte_i ),
  .wbs1_cti_i( wbs_d_mem0_cti_i ),
  .wbs1_cyc_i( wbs_d_mem0_cyc_i ),
  .wbs1_dat_i( wbs_d_mem0_dat_i ),
  .wbs1_sel_i( wbs_d_mem0_sel_i ),
  .wbs1_stb_i( wbs_d_mem0_stb_i ),
  .wbs1_we_i ( wbs_d_mem0_we_i ),
  .wbs1_cab_i( wbs_d_mem0_cab_i ),
  .wbs1_ack_o( wbs_d_mem0_ack_o ),
  .wbs1_err_o( wbs_d_mem0_err_o ),
  .wbs1_rty_o( wbs_d_mem0_rty_o ),
  .wbs1_dat_o( wbs_d_mem0_dat_o ),

  .wbs1_clk_i( wb_clk ),
  .wbs1_rst_i( wb_rst ),
  //---> QDR Interface
  .qdr_rps_o ( mem1_rps_o ),
  .qdr_wps_o ( mem1_wps_o ),
  .qdr_bws_o ( mem1_bws_o ),
  .qdr_addr_o( mem1_addr_o ),
  .qdr_data_o( mem1_data_o ),
  .qdr_data_i( mem1_data_i ),

  .qdr_read_clk_p_o     ( mem1_rclk_o_p ),
  .qdr_read_clk_n_o     ( mem1_rclk_o_n ),

  .qdr_write_clk_p_o    ( mem1_wclk_o_p ),
  .qdr_write_clk_n_o    ( mem1_wclk_o_n ),

  .qdr_echo_clk_p_i     ( mem1_rclk_i_p ),
  .qdr_echo_clk_n_i     ( mem1_rclk_i_n ),

  .qdr_clk_i( mems_clk ),
  .qdr_rst_i( mems_rst ),

  //---> Power Management
  .pow_doff_o( mem1_doff_o )
);

assign spi2_cs_o[0] = 1'bZ;
assign spi2_cs_o[1] = 1'bZ;
assign spi2_cs_o[2] = 1'bZ;
assign spi2_cs_o[3] = 1'bZ;
assign spi2_cs_o[4] = 1'bZ;
assign spi2_cs_o[5] = 1'bZ;
assign spi2_cs_o[6] = 1'bZ;
assign spi2_cs_o[7] = 1'bZ;

//=======================================//
//////////////////////////////////
// MEM1                       ///
////////////////////////////////
// Wishbone Slave Port0 wires
wire		wbs_d_mem1_ack_o;
wire		wbs_d_mem1_err_o;
wire		wbs_d_mem1_rty_o;
wire [31:0]	wbs_d_mem1_dat_o;
wire		wbs_d_mem1_cyc_i;
wire [31:0]	wbs_d_mem1_adr_i;
wire		wbs_d_mem1_stb_i;
wire		wbs_d_mem1_we_i;
wire [3:0]	wbs_d_mem1_sel_i;
wire [31:0]	wbs_d_mem1_dat_i;
wire		wbs_d_mem1_cab_i; // Consecutive Address Burst Indicator
wire [2:0]	wbs_d_mem1_cti_i;
wire [1:0]	wbs_d_mem1_bte_i;
// Wishbone Slave Port1 wires
wire		wbs_a_mem1_ack_o;
wire		wbs_a_mem1_err_o;
wire		wbs_a_mem1_rty_o;
wire [31:0]	wbs_a_mem1_dat_o;
wire		wbs_a_mem1_cyc_i;
wire [31:0]	wbs_a_mem1_adr_i;
wire		wbs_a_mem1_stb_i;
wire		wbs_a_mem1_we_i;
wire [3:0]	wbs_a_mem1_sel_i;
wire [31:0]	wbs_a_mem1_dat_i;
wire		wbs_a_mem1_cab_i; // Consecutive Address Burst Indicator
wire [2:0]	wbs_a_mem1_cti_i;
wire [1:0]	wbs_a_mem1_bte_i;
//---------- The instance of top level memory
// 20 delay -> some bits are not set
qdrii_wb #( 
	.iodelay_group_name ("mem1_delay_group"),
	.data0_delay (15),	
	.data1_delay (15),
	.data2_delay (15),
	.data3_delay (15),
	.data4_delay (15),
	.data5_delay (15),
	.data6_delay (15),
	.data7_delay (18),

	.data8_delay (15),
	.echo_clk_p_delay ( 15 ),
	.echo_clk_n_delay ( 15 )
)mem1(
  //---> Port 0
  .wbs0_adr_i( wbs_d_mem1_adr_i ),
  .wbs0_bte_i( wbs_d_mem1_bte_i ),
  .wbs0_cti_i( wbs_d_mem1_cti_i ),
  .wbs0_cyc_i( wbs_d_mem1_cyc_i ),
  .wbs0_dat_i( wbs_d_mem1_dat_i ),
  .wbs0_sel_i( wbs_d_mem1_sel_i ),
  .wbs0_stb_i( wbs_d_mem1_stb_i ),
  .wbs0_we_i ( wbs_d_mem1_we_i ),
  .wbs0_cab_i( wbs_d_mem1_cab_i ),
  .wbs0_ack_o( wbs_d_mem1_ack_o ),
  .wbs0_err_o( wbs_d_mem1_err_o ),
  .wbs0_rty_o( wbs_d_mem1_rty_o ),
  .wbs0_dat_o( wbs_d_mem1_dat_o ),
 
  .wbs0_clk_i( wb_clk ),
  .wbs0_rst_i( wb_rst ),
  //---> Port 1
  .wbs1_adr_i( wbs_a_mem1_adr_i ),
  .wbs1_bte_i( wbs_a_mem1_bte_i ),
  .wbs1_cti_i( wbs_a_mem1_cti_i ),
  .wbs1_cyc_i( wbs_a_mem1_cyc_i ),
  .wbs1_dat_i( wbs_a_mem1_dat_i ),
  .wbs1_sel_i( wbs_a_mem1_sel_i ),
  .wbs1_stb_i( wbs_a_mem1_stb_i ),
  .wbs1_we_i ( wbs_a_mem1_we_i ),
  .wbs1_cab_i( wbs_a_mem1_cab_i ),
  .wbs1_ack_o( wbs_a_mem1_ack_o ),
  .wbs1_err_o( wbs_a_mem1_err_o ),
  .wbs1_rty_o( wbs_a_mem1_rty_o ),
  .wbs1_dat_o( wbs_a_mem1_dat_o ),

  .wbs1_clk_i( wbfast_clk ),
  .wbs1_rst_i( wbfast_rst ),

  //---> QDR Interface
  .qdr_rps_o ( mem2_rps_o ),
  .qdr_wps_o ( mem2_wps_o ),
  .qdr_bws_o ( mem2_bws_o ),
  .qdr_addr_o( mem2_addr_o ),
  .qdr_data_o( mem2_data_o ),
  .qdr_data_i( mem2_data_i ),

  .qdr_read_clk_p_o	( mem2_rclk_o_p ),
  .qdr_read_clk_n_o	( mem2_rclk_o_n ),

  .qdr_write_clk_p_o 	( mem2_wclk_o_p ),
  .qdr_write_clk_n_o	( mem2_wclk_o_n ),

  .qdr_echo_clk_p_i	( mem2_rclk_i_p ),
  .qdr_echo_clk_n_i	( mem2_rclk_i_n ),

  .qdr_clk_i( mems_clk ),
  .qdr_rst_i( mems_rst ),
  //---> Power Management
  .pow_doff_o( mem2_doff_o )
);
`ifndef OR1200_WB_CAB
  assign wbs_d_mem1_cab_i = 1'b0;
  assign wbs_a_mem1_cab_i = 1'b0;
`endif

///////////////////////////////////////////////////////
//  ROM0 -> CPU0 Boot instruction from it on reset  //
/////////////////////////////////////////////////////
wire [31:0]	wbs_i_rom0_dat_o;
wire		wbs_i_rom0_ack_o;
wire [23:0]	wbs_i_rom0_adr_i;
wire		wbs_i_rom0_stb_i;
wire		wbs_i_rom0_cyc_i;
wire [2:0]	wbs_i_rom0_cti_i;
wire [1:0]	wbs_i_rom0_bte_i;
wire		wbs_i_rom0_err_o;
wire		wbs_i_rom0_rty_o;
rom_wb # (
	.addr_offset (32'h0000_0100)	// The OR1200 boots from 0xF000_0100, so if the bootrom.v
				// doesn't include this 0x100 shift, we have to
				// include it here
) rom0 (
  .wb_dat_o ( wbs_i_rom0_dat_o ),
  .wb_ack_o ( wbs_i_rom0_ack_o ),
  .wb_adr_i ( wbs_i_rom0_adr_i ),
  .wb_stb_i ( wbs_i_rom0_stb_i ),
  .wb_cyc_i ( wbs_i_rom0_cyc_i ),
  .wb_cti_i ( wbs_i_rom0_cti_i ),
  .wb_bte_i ( wbs_i_rom0_bte_i ),
  .wb_err_o ( wbs_i_rom0_err_o ),
  .wb_rty_o ( wbs_i_rom0_rty_o ),

  .wb_clk_i (wb_clk),
  .wb_rst_i (wb_rst)
);
//////////////////////////////////////////
//  ETH0: 10/100 Ethernet  MAC         //
////////////////////////////////////////
// TODO: add a 1000 Core.
wire eth0_interrupt;
// Creating 3-state buffers for mdios
wire	eth0_md_data_i;
wire	eth0_md_data_o;
wire	eth0_md_oe;
IOBUF #(
  .DRIVE(12),	// Specify the output drive strength
  .IBUF_LOW_PWR("TRUE"),	// Low Power - "TRUE", High Performance = "FALSE" 
  .IOSTANDARD("DEFAULT"),	// Specify the I/O standard
  .SLEW("SLOW") 	// Specify the output slew rate
) eth0_mdio_buffer (
  .O ( eth0_md_data_i ),	// Buffer output
  .IO( eth1_md_data_io ),	// Buffer inout port (connect directly to top-level port)
  .I ( eth0_md_data_o ),	// Buffer input
  .T (!eth0_md_oe )	// 3-state enable input, high=input, low=output
);
// Wishbone slave signals
wire [31:0] 	wbs_d_eth0_dat_i;
wire [31:0] 	wbs_d_eth0_dat_o;
wire [31:0] 	wbs_d_eth0_adr_i;
wire [3:0]	wbs_d_eth0_sel_i;
wire		wbs_d_eth0_we_i;
wire		wbs_d_eth0_cyc_i;
wire		wbs_d_eth0_stb_i;
wire		wbs_d_eth0_ack_o;
wire		wbs_d_eth0_err_o;
// Wishbone Master signals
wire [31:0]	wbm_d_eth0_adr_o;
wire [3:0]	wbm_d_eth0_sel_o;
wire		wbm_d_eth0_we_o;
wire [31:0]	wbm_d_eth0_dat_o;
wire [31:0]	wbm_d_eth0_dat_i;
wire		wbm_d_eth0_cyc_o;
wire		wbm_d_eth0_stb_o;
wire		wbm_d_eth0_ack_i;
wire		wbm_d_eth0_err_i;
wire [2:0]	wbm_d_eth0_cti_o;
wire [1:0]	wbm_d_eth0_bte_o;

ethmac eth0 (
  .wb_clk_i( wb_clk ), 
  .wb_rst_i( wb_rst ), 
  //---> Wishbone Slave Interface
  .wb_dat_i( wbs_d_eth0_dat_i ),
  .wb_dat_o( wbs_d_eth0_dat_o ),
  .wb_adr_i( wbs_d_eth0_adr_i[31:2] ),
  .wb_sel_i( wbs_d_eth0_sel_i ), 
  .wb_we_i ( wbs_d_eth0_we_i  ), 
  .wb_cyc_i( wbs_d_eth0_cyc_i ), 
  .wb_stb_i( wbs_d_eth0_stb_i ), 
  .wb_ack_o( wbs_d_eth0_ack_o ), 
  .wb_err_o( wbs_d_eth0_err_o ),
  //---> Wishbone Master Interface
  .m_wb_adr_o( wbm_d_eth0_adr_o ), 
  .m_wb_sel_o( wbm_d_eth0_sel_o ), 
  .m_wb_we_o ( wbm_d_eth0_we_o ),
  .m_wb_dat_o( wbm_d_eth0_dat_o ), 
  .m_wb_dat_i( wbm_d_eth0_dat_i ), 
  .m_wb_cyc_o( wbm_d_eth0_cyc_o ),
  .m_wb_stb_o( wbm_d_eth0_stb_o ), 
  .m_wb_ack_i( wbm_d_eth0_ack_i ), 
  .m_wb_err_i( wbm_d_eth0_err_i ),
  .m_wb_cti_o( wbm_d_eth0_cti_o ), 
  .m_wb_bte_o( wbm_d_eth0_bte_o ),
  //---> PHY TX signals
  .mtx_clk_pad_i( eth1_tx_ref_clk_i ),	// Transmit clock (from PHY)
  .mtxd_pad_o	( eth1_tx_d_o[3:0] ),	// Transmit nibble (to PHY)
  .mtxen_pad_o	( eth1_tx_enable_o ),	// Transmit enable (to PHY)
  .mtxerr_pad_o	( eth1_tx_error_o ),	// Transmit error (to PHY)
  //---> PHY RX signals
  .mrx_clk_pad_i( eth1_rx_ref_clk_i ),	// Receive clock (from PHY)
  .mrxd_pad_i	( eth1_rx_d_i[3:0] ),	// Receive nibble (from PHY)
  .mrxdv_pad_i	( eth1_rx_data_valid_i ), // Receive data valid (from PHY) 
  .mrxerr_pad_i	( eth1_rx_error_i ),	// Receive data error (from PHY)
  .mcoll_pad_i	( eth1_col_i ),		// Collision (from PHY)
  .mcrs_pad_i	( eth1_crs_i ),		// Carrier sense (from PHY)
  //---> PHY MIIM signals

  .mdc_pad_o	( eth1_md_clk_o ), 	// MII Management data clock (to PHY)
  .md_pad_i	( eth0_md_data_i ), 	// MII data input (from I/O cell)
  .md_pad_o	( eth0_md_data_o ), 	// MII data output (to I/O cell)
  .md_padoe_o	( eth0_md_oe ), 	// MII data output enable (to I/O cell)
  //---> Interrupt
  .int_o ( eth0_interrupt )
);
// No parameters. Everything defined in "ethmac_defines.v"
assign eth1_tx_d_o[7:4] = 4'bZZZZ;
assign eth1_gtx_clk_o = 1'bZ;
assign eth1_reset_o = 1'b1; // So that we should see 125MHz clock
/* Not used signals 
 *
 * input wire [7:4] eth1_rx_d_i,
 * input wire eth1_int_i,
 * input wire eth1_125MHz_clk_i,
 */

//////////////////////////////
// SPI 0 - Config EEPROM   //
////////////////////////////
bscan_spi spi0 (
	.data0_o(spi0_data0_o), 	// MOSI
	.data1_i(spi0_data1_i),		// MISO
	.data2_io(spi0_data2_io),	// WP
	.data3_io(spi0_data3_io),	// HOLD
	.cs_o(spi0_cs_o),		// Select
	
	.clk_o( spi0_clk )
);
assign spi0_reset_o = 1'b1; // So that SPI is not held in reset mode

///////////////////////
//  SPI 1 ////////////
/////////////////////
wire spi1_interrupt; assign spi1_interrupt = 1'b0;
// Wishbone Slave Ports
wire 		wbs_b_spi1_cyc_i;
wire 		wbs_b_spi1_stb_i;
wire [31:0]	wbs_b_spi1_adr_i;
wire		wbs_b_spi1_we_i;
wire [7:0]	wbs_b_spi1_dat_i;
wire [7:0]	wbs_b_spi1_dat_o;
wire		wbs_b_spi1_ack_o;

// Wishbone ROM Data Access
wire            wbs_d_spi1_ack_o;
wire            wbs_d_spi1_err_o;
wire            wbs_d_spi1_rty_o;
wire [31:0]     wbs_d_spi1_dat_o;
wire            wbs_d_spi1_cyc_i;
wire [31:0]     wbs_d_spi1_adr_i;
wire            wbs_d_spi1_stb_i;
wire            wbs_d_spi1_we_i;
wire [3:0]      wbs_d_spi1_sel_i;
wire [31:0]     wbs_d_spi1_dat_i;
wire [2:0]      wbs_d_spi1_cti_i;
wire [1:0]      wbs_d_spi1_bte_i;

// Wishbone ROM Instruction
wire            wbs_i_spi1_ack_o;
wire            wbs_i_spi1_err_o;
wire            wbs_i_spi1_rty_o;
wire [31:0]     wbs_i_spi1_dat_o;
wire            wbs_i_spi1_cyc_i;
wire [31:0]     wbs_i_spi1_adr_i;
wire            wbs_i_spi1_stb_i;
wire            wbs_i_spi1_we_i;
wire [3:0]      wbs_i_spi1_sel_i;
wire [31:0]     wbs_i_spi1_dat_i;
wire [2:0]      wbs_i_spi1_cti_i;
wire [1:0]      wbs_i_spi1_bte_i;


quad_spi spi1 (
  .wbs0_adr_i( wbs_i_spi1_adr_i ),
  .wbs0_bte_i( wbs_i_spi1_bte_i ),
  .wbs0_cti_i( wbs_i_spi1_cti_i ),
  .wbs0_cyc_i( wbs_i_spi1_cyc_i ),
  .wbs0_dat_i( wbs_i_spi1_dat_i ),
  .wbs0_sel_i( wbs_i_spi1_sel_i ),
  .wbs0_stb_i( wbs_i_spi1_stb_i ),
  .wbs0_we_i ( wbs_i_spi1_we_i ),
  .wbs0_ack_o( wbs_i_spi1_ack_o ),
  .wbs0_err_o( wbs_i_spi1_err_o ),
  .wbs0_rty_o( wbs_i_spi1_rty_o ),
  .wbs0_dat_o( wbs_i_spi1_dat_o ),

  .wbs1_adr_i( wbs_d_spi1_adr_i ),
  .wbs1_bte_i( wbs_d_spi1_bte_i ),
  .wbs1_cti_i( wbs_d_spi1_cti_i ),
  .wbs1_cyc_i( wbs_d_spi1_cyc_i ),
  .wbs1_dat_i( wbs_d_spi1_dat_i ),
  .wbs1_sel_i( wbs_d_spi1_sel_i ),
  .wbs1_stb_i( wbs_d_spi1_stb_i ),
  .wbs1_we_i ( wbs_d_spi1_we_i ),
  .wbs1_ack_o( wbs_d_spi1_ack_o ),
  .wbs1_err_o( wbs_d_spi1_err_o ),
  .wbs1_rty_o( wbs_d_spi1_rty_o ),
  .wbs1_dat_o( wbs_d_spi1_dat_o ),

  .wbsc_adr_i( wbs_b_spi1_adr_i ),
  .wbsc_cyc_i( wbs_b_spi1_cyc_i ),
  .wbsc_dat_i( wbs_b_spi1_dat_i ),
  .wbsc_stb_i( wbs_b_spi1_stb_i ),
  .wbsc_we_i ( wbs_b_spi1_we_i ),
  .wbsc_ack_o( wbs_b_spi1_ack_o ),
  .wbsc_dat_o( wbs_b_spi1_dat_o ),

  .wbs_clk_i ( wb_clk ),
  .wbs_rst_i ( wb_rst ),

  .spi_clk_o	( spi1_clk_o ),
  .spi_cs_o	( spi1_cs_o ),
  .spi_data0_io	( spi1_data0_io ),
  .spi_data1_io	( spi1_data1_io ),
  .spi_data2_io	( spi1_data2_io ),
  .spi_data3_io	( spi1_data3_io ),
  .spi_rst_o 	( spi1_reset_o )
);
/////////////////////////
//  SPI 2             //
///////////////////////
// Since these are on the connector, let's put them in Zs
// assign spi2_cs_o = 8'bZZZZZZZZ;
assign spi2_clk_o = 1'bZ;

////////////////////////
// UART 0            //
//////////////////////
wire uart0_interrupt;
// Wishbone Slave wires
wire [31:0]	wbs_b_uart0_adr_i;
wire [7:0]	wbs_b_uart0_dat_i;
wire		wbs_b_uart0_we_i;
wire [7:0]	wbs_b_uart0_dat_o;
wire		wbs_b_uart0_stb_i;
wire		wbs_b_uart0_cyc_i;
wire		wbs_b_uart0_ack_o;

uart16550 uart0(
  .wb_clk_i( wb_clk ),
  .wb_rst_i( wb_rst ),

  .wb_adr_i( wbs_b_uart0_adr_i ),
  .wb_dat_i( wbs_b_uart0_dat_i ), 
  .wb_we_i ( wbs_b_uart0_we_i  ), 
  .wb_dat_o( wbs_b_uart0_dat_o ),
  .wb_stb_i( wbs_b_uart0_stb_i ), 
  .wb_cyc_i( wbs_b_uart0_cyc_i ), 
  .wb_ack_o( wbs_b_uart0_ack_o ),

  .int_o ( uart0_interrupt ),

  .stx_pad_o( uart0_tx_o ),
  .srx_pad_i( uart0_rx_i ),

  .rts_pad_o(  ), 
  .cts_pad_i( 1'b0 ), 
  .dtr_pad_o(  ), 
  .dsr_pad_i( 1'b0 ), 
  .ri_pad_i ( 1'b0 ),
  .dcd_pad_i( 1'b0 )
);
// These are defined in "uart_defines.v" file
// defparam uart0.uart_data_width = 32;
// defparam uart0.uart_addr_width = 5;

/////////////////////////
// TWI 0              //
///////////////////////
assign twi0_sda_io = 1'bZ;
assign twi0_scl_io = 1'bZ;

////////////////////////////
// QSFP Power Controller //
//////////////////////////
// We would like to see the clocks output probably
/*
assign pow_qsfp1_clkresetn_o = 1'b1;
assign pow_qsfp1_clken_o = 1'b1;
// But not for the second clock
assign pow_qsfp2_clkresetn_o = 1'b1;
assign pow_qsfp2_clken_o = 1'b0;
*/
// I'm turning them off for now
assign pow_qsfp1_clkresetn_o = 1'b1;
assign pow_qsfp1_clken_o = 1'b0;

assign pow_qsfp2_clkresetn_o = 1'b1;
assign pow_qsfp2_clken_o = 1'b0;

// We want to turn the Memory controls inteligently. 
// But for now, let's just turn it on (Works)
assign mem1_cntrl_s3_o = 1'b1;
assign mem1_cntrl_s5_o = 1'b1;

assign mem2_cntrl_s3_o = 1'b1;
assign mem2_cntrl_s5_o = 1'b1;

////////////////////////////////
// QSFPs                   ////
//////////////////////////////
assign qsfp1_cs_o = 1'bZ;
assign qsfp1_sda_io = 1'bZ;
assign qsfp1_scl_o = 1'bZ;
assign qsfp1_mode_o = 1'bZ;
assign qsfp1_reset_o = 1'bZ;

assign qsfp2_cs_o = 1'bZ;
assign qsfp2_sda_io = 1'bZ;
assign qsfp2_scl_o = 1'bZ;
assign qsfp2_mode_o = 1'bZ;
assign qsfp2_reset_o = 1'bZ;

//===================================================================//
/* Now, that all perepherials are defined let's put an OR1200 minimal
   connections here */
//===================================================================//
//-------------------------------------------------------------------//
/////////////////////////////////////////////
//      OR1200 JTAG Debug Unit            //
///////////////////////////////////////////
wire cpu0_clk; // CPU clock can be slower than wb_clk
		// if we would ever choose different 
		// than 1:1 option
wire cpu0_rst;

// Jtag Debug Wishbone Master interface (BUS d):
wire [31:0]	wbm_d_dbg0_dat_i;
wire [31:0]	wbm_d_dbg0_adr_o;
wire [31:0]	wbm_d_dbg0_dat_o;
wire		wbm_d_dbg0_cyc_o;
wire		wbm_d_dbg0_stb_o;
wire [3:0]	wbm_d_dbg0_sel_o;
wire		wbm_d_dbg0_we_o;
wire		wbm_d_dbg0_ack_i;
wire		wbm_d_dbg0_err_i;
wire [2:0]	wbm_d_dbg0_cti_o;
wire [1:0]	wbm_d_dbg0_bte_o;

// Direct Connections from Debug Unit to CPU
wire [31:0]	dbg0_cpu0_addr;
wire [31:0]	dbg0_cpu0_data;
wire		dbg0_cpu0_stb;
wire		dbg0_cpu0_we;
wire [31:0]	cpu0_dbg0_data;
wire		cpu0_dbg0_ack;
wire		dbg0_cpu0_stall;
wire		cpu0_dbg0_bp;
`ifdef JTAG_DEBUG 

drift_jtag # (
	.JTAG_REG_WBM( 2 ),
	.JTAG_REG_CPU( 3 )
) cpu0_jtag_debug (
  //---> Wishbone Master Signals
  .wbm_adr_o( wbm_d_dbg0_adr_o ),
  .wbm_bte_o( wbm_d_dbg0_bte_o ),
  .wbm_cti_o( wbm_d_dbg0_cti_o ),
  .wbm_cyc_o( wbm_d_dbg0_cyc_o ),
  .wbm_dat_o( wbm_d_dbg0_dat_o ),
  .wbm_sel_o( wbm_d_dbg0_sel_o ),
  .wbm_stb_o( wbm_d_dbg0_stb_o ),
  .wbm_we_o ( wbm_d_dbg0_we_o ),
  .wbm_ack_i( wbm_d_dbg0_ack_i ),
  .wbm_err_i( wbm_d_dbg0_err_i ),
  .wbm_rty_i( 1'b0 ),
  .wbm_dat_i( wbm_d_dbg0_dat_i ),
 
  .wbm_clk_i( wb_clk ),
  .wbm_rst_i( wb_rst ),

  //---> OR1200 control Signals
  .cpu_bp_i     ( cpu0_dbg0_bp ),
  .cpu_stall_o  ( dbg0_cpu0_stall ),

  .cpu_adr_o ( dbg0_cpu0_addr ),
  .cpu_dat_o ( dbg0_cpu0_data ),
  .cpu_stb_o ( dbg0_cpu0_stb  ),
  .cpu_we_o  ( dbg0_cpu0_we   ),
  .cpu_dat_i ( cpu0_dbg0_data ),
  .cpu_ack_i ( cpu0_dbg0_ack ),

  .cpu_clk_i ( cpu0_clk ),
  .cpu_rst_i ( cpu0_rst )
);

`else // if JTAG_DEBUG
// If JTAG is not used we want to set all output signals
// of the debug unit
// Wishbone Master Signals
assign wbm_d_dbg0_adr_o = 32'h0000_0000;
assign wbm_d_dbg0_dat_o = 32'h0000_0000;
assign wbm_d_dbg0_cyc_o = 1'b0;
assign wbm_d_dbg0_stb_o = 1'b0;
assign wbm_d_dbg0_sel_o = 4'b0000;
assign wbm_d_dbg0_we_o  = 1'b0;
assign wbm_d_dbg0_cab_o = 1'b0;
assign wbm_d_dbg0_cti_o = 3'b000;
assign wbm_d_dbg0_bte_o = 2'b00;
// And CPU Signals
assign dbg0_cpu0_addr = 32'h0000_0000;
assign dbg0_cpu0_data = 32'h0000_0000;
assign dbg0_cpu0_stb = 1'b0;
assign dbg0_cpu0_we = 1'b0;
assign dbg0_cpu0_stall = 1'b0;
`endif // if JTAG_DEBUG
//-------------------------------------------------------------------//
/////////////////////////////////////////
//  OR1200  CPU                       //
///////////////////////////////////////
//---> Interrupts !!!
// Number is defined in `OR1200_PIC_INTS. Default=20, Range: [2 to 31]
wire [19:0]	cpu0_interrupts;
assign cpu0_interrupts[ 0] = 0; // Non-maskable inside OR1200
assign cpu0_interrupts[ 1] = 0; // Non-maskable inside OR1200
assign cpu0_interrupts[ 2] = uart0_interrupt; // UART
assign cpu0_interrupts[ 3] = 0; // GPIO
assign cpu0_interrupts[ 4] = eth0_interrupt; // ETH0
assign cpu0_interrupts[ 5] = 0;
assign cpu0_interrupts[ 6] = spi1_interrupt; // SPI0
assign cpu0_interrupts[ 7] = 0;
assign cpu0_interrupts[ 8] = 0;
assign cpu0_interrupts[ 9] = 0;
assign cpu0_interrupts[10] = 0; // I2C
assign cpu0_interrupts[11] = 0; // DMA
assign cpu0_interrupts[12] = 0; //<-- QSFP0
assign cpu0_interrupts[13] = 0; //<-- QSFP1
assign cpu0_interrupts[14] = adc1_interrupt; //<-- ADC1
assign cpu0_interrupts[15] = adc2_interrupt; //<-- ADC2
assign cpu0_interrupts[16] = adc3_interrupt; //<-- ADC3
assign cpu0_interrupts[17] = adc4_interrupt; //<-- ADC4
assign cpu0_interrupts[18] = adc5_interrupt; //<-- ADC5
assign cpu0_interrupts[19] = adc6_interrupt; //<-- ADC6

// This is one pulse per second for ADCs
// In principle we do want to generate it from
// CPU, but right now lets just generate it from
// wishbone clock
reg [31:0] wb_cycles_counter = 32'h0000_0000;
always@(posedge wb_clk) begin
  if ( wb_cycles_counter == 32'h02FA_F080 ) // 50MhZ
  	wb_cycles_counter <= 32'h0000_0000;
  else
  	wb_cycles_counter <= wb_cycles_counter + 1'b1;
end

reg one_pulse_per_sec_reg = 1'b0;
always@(posedge wb_clk) begin
  if (wb_cycles_counter == 32'h017D_7840) // Half of 1sec
	one_pulse_per_sec_reg <= 1'b0;
  else if ( wb_cycles_counter == 32'h0000_0000 )
	one_pulse_per_sec_reg <= 1'b1;
end
assign one_pulse_per_sec_clk = one_pulse_per_sec_reg;

// Master interface to Wishbone Instructions (i) BUS
wire		wbm_i_cpu0_ack_i;
wire		wbm_i_cpu0_err_i;
wire		wbm_i_cpu0_rty_i;
wire [31:0]	wbm_i_cpu0_dat_i;
wire		wbm_i_cpu0_cyc_o;
wire [31:0]	wbm_i_cpu0_adr_o;
wire 		wbm_i_cpu0_stb_o;
wire		wbm_i_cpu0_we_o;
wire [3:0]	wbm_i_cpu0_sel_o;
wire [31:0]	wbm_i_cpu0_dat_o;
wire		wbm_i_cpu0_cab_o; // Consecutive Address Burst Indicator (Default Undefined)
wire [2:0]	wbm_i_cpu0_cti_o;
wire [1:0]	wbm_i_cpu0_bte_o;

// Master interface to Wishbone Data (d) BUS
wire		wbm_d_cpu0_ack_i;
wire		wbm_d_cpu0_err_i;
wire		wbm_d_cpu0_rty_i;
wire [31:0]	wbm_d_cpu0_dat_i;
wire		wbm_d_cpu0_cyc_o;
wire [31:0]	wbm_d_cpu0_adr_o;
wire		wbm_d_cpu0_stb_o;
wire		wbm_d_cpu0_we_o;
wire [3:0]	wbm_d_cpu0_sel_o;
wire [31:0]	wbm_d_cpu0_dat_o;
wire		wbm_d_cpu0_cab_o;
wire [2:0]	wbm_d_cpu0_cti_o;
wire [1:0]	wbm_d_cpu0_bte_o;

or1200_top cpu0 (
  // CPU Clock
  .clk_i 	( cpu0_clk ), 
  // CPU Reset
  .rst_i 	( cpu0_rst ), 
  // Interrupts
  .pic_ints_i 	( cpu0_interrupts ),
  // Clocking Mode
`ifdef OR1200_CLMODE_1TO2
  .clmode_i	( 2'b01 ),
`else
`ifdef OR1200_CLMODE_1TO4
  .clmode_i	( 2'b11 ),
`else
  .clmode_i	( 2'b00 ),
`endif
`endif
  // WISHBONE Instruction Master
  .iwb_clk_i ( wb_clk ),
  .iwb_rst_i ( wb_rst ),

  .iwb_ack_i ( wbm_i_cpu0_ack_i ),
  .iwb_err_i ( wbm_i_cpu0_err_i ),
  .iwb_rty_i ( wbm_i_cpu0_rty_i ),
  .iwb_dat_i ( wbm_i_cpu0_dat_i ),
  .iwb_cyc_o ( wbm_i_cpu0_cyc_o ),
  .iwb_adr_o ( wbm_i_cpu0_adr_o ),
  .iwb_stb_o ( wbm_i_cpu0_stb_o ),
  .iwb_we_o  ( wbm_i_cpu0_we_o ),
  .iwb_sel_o ( wbm_i_cpu0_sel_o ),
  .iwb_dat_o ( wbm_i_cpu0_dat_o ),
`ifdef OR1200_WB_CAB
  .iwb_cab_o ( wbm_i_cpu0_cab_o ),
`endif
`ifdef OR1200_WB_B3
  .iwb_cti_o ( wbm_i_cpu0_cti_o ),
  .iwb_bte_o ( wbm_i_cpu0_bte_o ),
`endif
	
  // WISHBONE Data Master
  .dwb_clk_i ( wb_clk ),
  .dwb_rst_i ( wb_rst ),

  .dwb_ack_i ( wbm_d_cpu0_ack_i ),
  .dwb_err_i ( wbm_d_cpu0_err_i ),
  .dwb_rty_i ( wbm_d_cpu0_rty_i ),
  .dwb_dat_i ( wbm_d_cpu0_dat_i ),
  .dwb_cyc_o ( wbm_d_cpu0_cyc_o ),
  .dwb_adr_o ( wbm_d_cpu0_adr_o ),
  .dwb_stb_o ( wbm_d_cpu0_stb_o ),
  .dwb_we_o  ( wbm_d_cpu0_we_o ),
  .dwb_sel_o ( wbm_d_cpu0_sel_o ),
  .dwb_dat_o ( wbm_d_cpu0_dat_o ),
`ifdef OR1200_WB_CAB
  .dwb_cab_o ( wbm_d_cpu0_cab_o ),
`endif
`ifdef OR1200_WB_B3
  .dwb_cti_o ( wbm_d_cpu0_cti_o ),
  .dwb_bte_o ( wbm_d_cpu0_bte_o ),
`endif

  // Debug
  .dbg_stall_i ( dbg0_cpu0_stall ),
  .dbg_ewt_i   (1'b0),	/* External Watchpoint Trigger Input, 	Not Connected */
  .dbg_lss_o   (  ),	/* External Load/Store Unit Status, 	Not Connected */
  .dbg_is_o    (  ),	/* External Insn Fetch Status, 		Not Connected */
  .dbg_wp_o    (  ),	/* Watchpoints Outputs, 		Not Connected */
  .dbg_bp_o    ( cpu0_dbg0_bp ),
 
  .dbg_adr_i   ( dbg0_cpu0_addr ),
  .dbg_we_i    ( dbg0_cpu0_we ),
  .dbg_stb_i   ( dbg0_cpu0_stb ),
  .dbg_dat_i   ( dbg0_cpu0_data ),
  .dbg_dat_o   ( cpu0_dbg0_data ),
  .dbg_ack_o   ( cpu0_dbg0_ack ),

  // RAM BIST ( Not used )
`ifdef OR1200_BIST
  .mbist_si_i 	( 1'b0 ), 
  .mbist_so_o	(  ), 
  .mbist_ctrl_i	( 1'b0 ),
`endif
  // Power Management ( Not used )
  .pm_cpustall_i 	( 1'b0 ),

  .pm_clksd_o		(  ), 
  .pm_dc_gate_o		(  ), 
  .pm_ic_gate_o		(  ), 
  .pm_dmmu_gate_o	(  ),
  .pm_immu_gate_o	(  ), 
  .pm_tt_gate_o		(  ), 
  .pm_cpu_gate_o	(  ), 
  .pm_wakeup_o		(  ), 
  .pm_lvolt_o		(  ),

  // Exception??? ( Not Used )
  .sig_tick (  ) /* Output, Not Connected */
);
//--->Processor Clock: Bus Clock == 1:1
assign cpu0_clk = wb_clk;
//--->we want to reset processor either from wishbone or from jtag debug interface
//---> NOPE, wb_rst only
assign cpu0_rst = wb_rst /* | dbg0_cpu0_rst */;
// In case some features are undefined, let's assign default values to the
// output:
`ifndef OR1200_WB_CAB
assign wbm_i_cpu0_cab_o = 1'b0;
assign wbm_d_cpu0_cab_o = 1'b0;
`endif
`ifndef OR1200_WB_B3
assign wbm_i_cpu0_cti_o = 3'b000;
assign wbm_d_cpu0_cti_o = 3'b000;
assign wbm_i_cpu0_bte_o = 2'b00;
assign wbm_d_cpu0_bte_o = 2'b00;
`endif

//-------------------------------------------------------------------//
/////////////////////////////////////////////
//      BUSes                             //
///////////////////////////////////////////

///////////////////////////////////////////////
// Wishbone instruction bus arbiter (IBUS)  //
/////////////////////////////////////////////
// This is the simplest Arbiter: 1 master, 3 slaves
// Bus is 32bit wide
arbiter_ibus #(
	.wb_addr_match_width ( 4 ),
	.slave0_adr(4'h0),	// MEM0: 0x0000_0000
	.slave1_adr(4'hf),	// ROM0: 0xF000_0000
	.slave2_adr(4'h1)	// SPI1: 0x1000_0000
)ibus(
  //----------- Masters ----------->
  // Master 0 == CPU0 (OR1200)
  .wbm0_adr_i( wbm_i_cpu0_adr_o ),
  .wbm0_dat_i( wbm_i_cpu0_dat_o ),
  .wbm0_sel_i( wbm_i_cpu0_sel_o ),
  .wbm0_we_i ( wbm_i_cpu0_we_o ),
  .wbm0_cyc_i( wbm_i_cpu0_cyc_o ),
  .wbm0_stb_i( wbm_i_cpu0_stb_o ),
  .wbm0_cti_i( wbm_i_cpu0_cti_o ),
  .wbm0_bte_i( wbm_i_cpu0_bte_o ),
  .wbm0_dat_o( wbm_i_cpu0_dat_i ),
  .wbm0_ack_o( wbm_i_cpu0_ack_i ),
  .wbm0_err_o( wbm_i_cpu0_err_i ),
  .wbm0_rty_o( wbm_i_cpu0_rty_i ),
  // NOT Connected:  wbm_i_cpu0_cab_o;
  //------------ Slaves ----------->
  // Slave 0 == MEM0, Port 0
  .wbs0_adr_o( wbs_i_mem0_adr_i ),
  .wbs0_dat_o( wbs_i_mem0_dat_i ),
  .wbs0_sel_o( wbs_i_mem0_sel_i ),
  .wbs0_we_o ( wbs_i_mem0_we_i ),
  .wbs0_cyc_o( wbs_i_mem0_cyc_i ),
  .wbs0_stb_o( wbs_i_mem0_stb_i ),
  .wbs0_cti_o( wbs_i_mem0_cti_i ),
  .wbs0_bte_o( wbs_i_mem0_bte_i ),
  .wbs0_dat_i( wbs_i_mem0_dat_o ),
  .wbs0_ack_i( wbs_i_mem0_ack_o ),
  .wbs0_err_i( wbs_i_mem0_err_o ),
  .wbs0_rty_i( wbs_i_mem0_rty_o ),
  // NOT Connected: wbs_i_mem0_cab_i;
  // Slave 1 == ROM0
  .wbs1_adr_o( wbs_i_rom0_adr_i ),
  .wbs1_dat_o(  ), // Read-only
  .wbs1_sel_o(  ), // Read-only
  .wbs1_we_o (  ), // Read-only
  .wbs1_cyc_o( wbs_i_rom0_cyc_i ),
  .wbs1_stb_o( wbs_i_rom0_stb_i ),
  .wbs1_cti_o( wbs_i_rom0_cti_i ),
  .wbs1_bte_o( wbs_i_rom0_bte_i ),
  .wbs1_dat_i( wbs_i_rom0_dat_o ),
  .wbs1_ack_i( wbs_i_rom0_ack_o ),
  .wbs1_err_i( wbs_i_rom0_err_o ),
  .wbs1_rty_i( wbs_i_rom0_rty_o ),
  // Slave 2 == SPI1
  .wbs2_adr_o( wbs_i_spi1_adr_i ),
  .wbs2_dat_o( wbs_i_spi1_dat_i ),
  .wbs2_sel_o( wbs_i_spi1_sel_i ),
  .wbs2_we_o ( wbs_i_spi1_we_i ),
  .wbs2_cyc_o( wbs_i_spi1_cyc_i ),
  .wbs2_stb_o( wbs_i_spi1_stb_i ),
  .wbs2_cti_o( wbs_i_spi1_cti_i ),
  .wbs2_bte_o( wbs_i_spi1_bte_i ),
  .wbs2_dat_i( wbs_i_spi1_dat_o ),
  .wbs2_ack_i( wbs_i_spi1_ack_o ),
  .wbs2_err_i( wbs_i_spi1_err_o ),
  .wbs2_rty_i( wbs_i_spi1_rty_o ),
  //------------ Clock, reset ----->
  .wb_clk_i ( wb_clk ),
  .wb_rst_i ( wb_rst )
);
//-------------------------------------------------------------------//
// Bridges wires
// From dbus to bbus
wire		wbs_d_bbus_ack_o;
wire		wbs_d_bbus_err_o;
wire		wbs_d_bbus_rty_o;
wire [31:0]	wbs_d_bbus_dat_o;
wire		wbs_d_bbus_cyc_i;
wire [31:0]	wbs_d_bbus_adr_i;
wire		wbs_d_bbus_stb_i;
wire		wbs_d_bbus_we_i;
wire [3:0]	wbs_d_bbus_sel_i;
wire [31:0]	wbs_d_bbus_dat_i;
wire [2:0]	wbs_d_bbus_cti_i;
wire [1:0]	wbs_d_bbus_bte_i;
// From dbus to cbus
wire		wbs_d_cbus_ack_o;
wire		wbs_d_cbus_err_o;
wire		wbs_d_cbus_rty_o;
wire [31:0]	wbs_d_cbus_dat_o;
wire		wbs_d_cbus_cyc_i;
wire [31:0]	wbs_d_cbus_adr_i;
wire		wbs_d_cbus_stb_i;
wire 		wbs_d_cbus_we_i;
wire [3:0]	wbs_d_cbus_sel_i;
wire [31:0]	wbs_d_cbus_dat_i;
wire [2:0]	wbs_d_cbus_cti_i;
wire [1:0]	wbs_d_cbus_bte_i;

////////////////////////////////////////
// Wishbone data bus arbiter (DBUS)  //
//////////////////////////////////////
// 3 Masters, 6 Slaves
// Bus is 32bit wide
wb_switch_b3 # (
        .dw ( 32 ),
	.aw ( 32 ),
	.slave0_sel_width( 8 ),     // Width of selection bits 8 == [31:24]
	.slave0_sel_addr ( 8'h00 ), // 0x0000_0000 to 0x00FF_FFFF
	.slave0_sel_mask ( 8'hFF ), 
	.slave1_sel_width( 8 ),	
	.slave1_sel_addr ( 8'h01 ), // 0x0100_0000 to 0x01FF_FFFF
	.slave1_sel_mask ( 8'hFF ),
	.slave2_sel_width( 8 ),
	.slave2_sel_addr ( 8'b1000_0000), // 0x8000_0000 to 0x81FF_FFFF
					  // 0x9000_0000 to 0x91FF_FFFF
					  // 0xA000_0000 to 0xA1FF_FFFF
					  // 0xB000_0000 to 0xB1FF_FFFF
	.slave2_sel_mask ( 8'b1100_1110 ),
	.slave3_sel_width( 4 ),
	.slave3_sel_addr ( 4'h7 ),	// 0x7000_0000 to 0x7FFF_FFFF
	.slave3_sel_mask ( 4'hF ),
	.slave4_sel_width( 8 ),
	.slave4_sel_addr ( 8'h92 ),	// 0x9200_0000 to 0x92FF_FFFF
	.slave4_sel_mask ( 8'hFF ),
	.slave5_sel_width( 4 ),
	.slave5_sel_addr ( 4'h1 ), 	// 0x1000_0000 to 0x1FFF_FFFF (128MBytes)
	.slave5_sel_mask ( 4'hF )
) dbus (
  //----------- Masters ----------->
  // Master 0 == CPU0
  .wbm0_adr_i( wbm_d_cpu0_adr_o ),
  .wbm0_bte_i( wbm_d_cpu0_bte_o ),
  .wbm0_cti_i( wbm_d_cpu0_cti_o ),
  .wbm0_cyc_i( wbm_d_cpu0_cyc_o ),
  .wbm0_dat_i( wbm_d_cpu0_dat_o ),
  .wbm0_sel_i( wbm_d_cpu0_sel_o ),
  .wbm0_stb_i( wbm_d_cpu0_stb_o ),
  .wbm0_we_i ( wbm_d_cpu0_we_o ),
  .wbm0_ack_o( wbm_d_cpu0_ack_i ),
  .wbm0_err_o( wbm_d_cpu0_err_i ),
  .wbm0_rty_o( wbm_d_cpu0_rty_i ),
  .wbm0_dat_o( wbm_d_cpu0_dat_i ),
  // NOT Connected: wbm_d_cpu0_cab_o
  // Master 1 == DBG0
  .wbm1_adr_i( wbm_d_dbg0_adr_o ),
  .wbm1_bte_i( wbm_d_dbg0_bte_o ),
  .wbm1_cti_i( wbm_d_dbg0_cti_o ),
  .wbm1_cyc_i( wbm_d_dbg0_cyc_o ),
  .wbm1_dat_i( wbm_d_dbg0_dat_o ),
  .wbm1_sel_i( wbm_d_dbg0_sel_o ), 
  .wbm1_stb_i( wbm_d_dbg0_stb_o ), 
  .wbm1_we_i ( wbm_d_dbg0_we_o ), 
  .wbm1_ack_o( wbm_d_dbg0_ack_i ), 
  .wbm1_err_o( wbm_d_dbg0_err_i ), 
  .wbm1_rty_o(  ), 
  .wbm1_dat_o( wbm_d_dbg0_dat_i ),
  // NOT Connected: wbm_d_dbg0_cab_o
  // Master 2 == ETH0, Master
  .wbm2_adr_i( wbm_d_eth0_adr_o ),
  .wbm2_bte_i( wbm_d_eth0_bte_o ),
  .wbm2_cti_i( wbm_d_eth0_cti_o ),
  .wbm2_cyc_i( wbm_d_eth0_cyc_o ),
  .wbm2_dat_i( wbm_d_eth0_dat_o ),
  .wbm2_sel_i( wbm_d_eth0_sel_o ),
  .wbm2_stb_i( wbm_d_eth0_stb_o ),
  .wbm2_we_i ( wbm_d_eth0_we_o ),
  .wbm2_ack_o( wbm_d_eth0_ack_i ),
  .wbm2_err_o( wbm_d_eth0_err_i ),
  .wbm2_rty_o(  ),                 
  .wbm2_dat_o( wbm_d_eth0_dat_i ),
  //------------ Slaves ----------->
  // Slave 0 == MEM0, Port 1
  .wbs0_adr_o( wbs_d_mem0_adr_i ), 
  .wbs0_bte_o( wbs_d_mem0_bte_i ), 
  .wbs0_cti_o( wbs_d_mem0_cti_i ), 
  .wbs0_cyc_o( wbs_d_mem0_cyc_i ), 
  .wbs0_dat_o( wbs_d_mem0_dat_i ), 
  .wbs0_sel_o( wbs_d_mem0_sel_i ),
  .wbs0_stb_o( wbs_d_mem0_stb_i ), 
  .wbs0_we_o ( wbs_d_mem0_we_i ), 
  .wbs0_ack_i( wbs_d_mem0_ack_o ), 
  .wbs0_err_i( wbs_d_mem0_err_o ), 
  .wbs0_rty_i( wbs_d_mem0_rty_o ), 
  .wbs0_dat_i( wbs_d_mem0_dat_o ),
  // NOT CONNECTED:  wbs_d_mem0_cab_i
  // Slave 1 == MEM1, Port 0
  .wbs1_adr_o( wbs_d_mem1_adr_i ),
  .wbs1_bte_o( wbs_d_mem1_bte_i ),
  .wbs1_cti_o( wbs_d_mem1_cti_i ),
  .wbs1_cyc_o( wbs_d_mem1_cyc_i ),
  .wbs1_dat_o( wbs_d_mem1_dat_i ),
  .wbs1_sel_o( wbs_d_mem1_sel_i ),
  .wbs1_stb_o( wbs_d_mem1_stb_i ),
  .wbs1_we_o ( wbs_d_mem1_we_i ), 
  .wbs1_ack_i( wbs_d_mem1_ack_o ),
  .wbs1_err_i( wbs_d_mem1_err_o ),
  .wbs1_rty_i( wbs_d_mem1_rty_o ),
  .wbs1_dat_i( wbs_d_mem1_dat_o ),
  // NOT CONNECTED: wbs_d_mem1_cab_i
  // Slave 2 == Bridge to BBUS0
  .wbs2_adr_o( wbs_d_bbus_adr_i ),
  .wbs2_bte_o( wbs_d_bbus_bte_i ),
  .wbs2_cti_o( wbs_d_bbus_cti_i ),
  .wbs2_cyc_o( wbs_d_bbus_cyc_i ),
  .wbs2_dat_o( wbs_d_bbus_dat_i ),
  .wbs2_sel_o( wbs_d_bbus_sel_i ),
  .wbs2_stb_o( wbs_d_bbus_stb_i ),
  .wbs2_we_o ( wbs_d_bbus_we_i ),
  .wbs2_ack_i( wbs_d_bbus_ack_o ),
  .wbs2_err_i( wbs_d_bbus_err_o ),
  .wbs2_rty_i( wbs_d_bbus_rty_o ),
  .wbs2_dat_i( wbs_d_bbus_dat_o ),
  // Slave 3 == Bridge to CBUS
  .wbs3_adr_o( wbs_d_cbus_adr_i ),
  .wbs3_bte_o( wbs_d_cbus_bte_i ),
  .wbs3_cti_o( wbs_d_cbus_cti_i ),
  .wbs3_cyc_o( wbs_d_cbus_cyc_i ),
  .wbs3_dat_o( wbs_d_cbus_dat_i ),
  .wbs3_sel_o( wbs_d_cbus_sel_i ),
  .wbs3_stb_o( wbs_d_cbus_stb_i ),
  .wbs3_we_o ( wbs_d_cbus_we_i ),
  .wbs3_ack_i( wbs_d_cbus_ack_o ),
  .wbs3_err_i( wbs_d_cbus_err_o ),
  .wbs3_rty_i( wbs_d_cbus_rty_o ),
  .wbs3_dat_i( wbs_d_cbus_dat_o ),
  // Slave 4 == ETH0, Slave
  .wbs4_adr_o( wbs_d_eth0_adr_i ),
  .wbs4_bte_o(  ),
  .wbs4_cti_o(  ),
  .wbs4_cyc_o( wbs_d_eth0_cyc_i ),
  .wbs4_dat_o( wbs_d_eth0_dat_i ),
  .wbs4_sel_o( wbs_d_eth0_sel_i ),
  .wbs4_stb_o( wbs_d_eth0_stb_i ),
  .wbs4_we_o ( wbs_d_eth0_we_i ),
  .wbs4_ack_i( wbs_d_eth0_ack_o ),
  .wbs4_err_i( wbs_d_eth0_err_o ),
  .wbs4_rty_i( 1'b0 ),
  .wbs4_dat_i( wbs_d_eth0_dat_o ),
  // Slave 5 == SPI1 Flash
  .wbs5_adr_o( wbs_d_spi1_adr_i ),
  .wbs5_bte_o( wbs_d_spi1_bte_i ),
  .wbs5_cti_o( wbs_d_spi1_cti_i ),
  .wbs5_cyc_o( wbs_d_spi1_cyc_i ),
  .wbs5_dat_o( wbs_d_spi1_dat_i ),
  .wbs5_sel_o( wbs_d_spi1_sel_i ),
  .wbs5_stb_o( wbs_d_spi1_stb_i ),
  .wbs5_we_o ( wbs_d_spi1_we_i ),
  .wbs5_ack_i( wbs_d_spi1_ack_o ),
  .wbs5_err_i( wbs_d_spi1_err_o ),
  .wbs5_rty_i( wbs_d_spi1_rty_o ),
  .wbs5_dat_i( wbs_d_spi1_dat_o ),
  //------------ Clock, reset ----->
  .wb_clk ( wb_clk ),
  .wb_rst ( wb_rst )
);
//-------------------------------------------------------------------//
////////////////////////////////////////////////////////
// Wishbone byte bus arbitrer for Perepherials BBUS  //
//////////////////////////////////////////////////////
// 1 Master, 8 Slaves
// Bus is 8bit wide
arbiter_bbus #(
	.wb_addr_match_width (8),
	.slave0_adr(8'h90), //<-- UART0
	.slave1_adr(8'h91),
	.slave2_adr(8'h80),
	.slave3_adr(8'h81),
	.slave4_adr(8'hB0), // <-- SPI1
	.slave5_adr(8'hB1),
	.slave6_adr(8'hA0),
	.slave7_adr(8'hA1)
) bbus (//----------- Masters ----------->
  // Master 0 == Bridge from DBUS, Slave 2
  .wbm0_adr_i( wbs_d_bbus_adr_i ),
  .wbm0_dat_i( wbs_d_bbus_dat_i ),
  .wbm0_sel_i( wbs_d_bbus_sel_i ),
  .wbm0_we_i ( wbs_d_bbus_we_i ),
  .wbm0_cyc_i( wbs_d_bbus_cyc_i ),
  .wbm0_stb_i( wbs_d_bbus_stb_i ),
  .wbm0_cti_i( wbs_d_bbus_cti_i ),
  .wbm0_bte_i( wbs_d_bbus_bte_i ),
  .wbm0_dat_o( wbs_d_bbus_dat_o ),
  .wbm0_ack_o( wbs_d_bbus_ack_o ),
  .wbm0_err_o( wbs_d_bbus_err_o ),
  .wbm0_rty_o( wbs_d_bbus_rty_o ),
  //------------ Slaves ----------->
  // Slave 0 == UART0
  .wbs0_adr_o( wbs_b_uart0_adr_i ),
  .wbs0_dat_o( wbs_b_uart0_dat_i ),
  .wbs0_we_o ( wbs_b_uart0_we_i ),
  .wbs0_cyc_o( wbs_b_uart0_cyc_i ),
  .wbs0_stb_o( wbs_b_uart0_stb_i ),
  .wbs0_cti_o(  ),
  .wbs0_bte_o(  ),
  .wbs0_dat_i( wbs_b_uart0_dat_o ),
  .wbs0_ack_i( wbs_b_uart0_ack_o ),
  .wbs0_err_i( 1'b0 ),
  .wbs0_rty_i( 1'b0 ),
  // Slave 1 == POW0
  .wbs1_adr_o(  ),
  .wbs1_dat_o(  ),
  .wbs1_we_o (  ),
  .wbs1_cyc_o(  ),
  .wbs1_stb_o(  ),
  .wbs1_cti_o(  ),
  .wbs1_bte_o(  ),
  .wbs1_dat_i( 8'h00 ),
  .wbs1_ack_i( 1'b0 ),
  .wbs1_err_i( 1'b0 ),
  .wbs1_rty_i( 1'b0 ),
  // Slave 2 == TWI0
  .wbs2_adr_o(  ),
  .wbs2_dat_o(  ),
  .wbs2_we_o (  ),
  .wbs2_cyc_o(  ),
  .wbs2_stb_o(  ),
  .wbs2_cti_o(  ),
  .wbs2_bte_o(  ),
  .wbs2_dat_i( 8'h00 ),
  .wbs2_ack_i( 1'b0 ),
  .wbs2_err_i( 1'b0 ),
  .wbs2_rty_i( 1'b0 ),
  // Slave 3 == SPI2
  .wbs3_adr_o(  ),
  .wbs3_dat_o(  ),
  .wbs3_we_o (  ),
  .wbs3_cyc_o(  ),
  .wbs3_stb_o(  ),
  .wbs3_cti_o(  ),
  .wbs3_bte_o(  ),
  .wbs3_dat_i( 8'h00 ),
  .wbs3_ack_i( 1'b0 ),
  .wbs3_err_i( 1'b0 ),
  .wbs3_rty_i( 1'b0 ),
  // Slave 4 == SPI1
  .wbs4_adr_o( wbs_b_spi1_adr_i ),
  .wbs4_dat_o( wbs_b_spi1_dat_i ),
  .wbs4_we_o ( wbs_b_spi1_we_i ),
  .wbs4_cyc_o( wbs_b_spi1_cyc_i ),
  .wbs4_stb_o( wbs_b_spi1_stb_i ),
  .wbs4_cti_o(  ),
  .wbs4_bte_o(  ),
  .wbs4_dat_i( wbs_b_spi1_dat_o ),
  .wbs4_ack_i( wbs_b_spi1_ack_o ),
  .wbs4_err_i( 1'b0 ),
  .wbs4_rty_i( 1'b0 ),
  // Slave 5 == ( Not connected )
  .wbs5_adr_o(  ),
  .wbs5_dat_o(  ),
  .wbs5_we_o (  ),
  .wbs5_cyc_o(  ),
  .wbs5_stb_o(  ),
  .wbs5_cti_o(  ),
  .wbs5_bte_o(  ),
  .wbs5_dat_i( 8'h00 ),
  .wbs5_ack_i( 1'b0 ),
  .wbs5_err_i( 1'b0 ),
  .wbs5_rty_i( 1'b0 ),
  // Slave 6 == ( Not connected )
  .wbs6_adr_o(  ),
  .wbs6_dat_o(  ),
  .wbs6_we_o (  ),
  .wbs6_cyc_o(  ),
  .wbs6_stb_o(  ),
  .wbs6_cti_o(  ),
  .wbs6_bte_o(  ),
  .wbs6_dat_i( 8'h00 ),
  .wbs6_ack_i( 1'b0 ),
  .wbs6_err_i( 1'b0 ),
  .wbs6_rty_i( 1'b0 ),
  // Slave 7 == ( Not connected )
  .wbs7_adr_o(  ),
  .wbs7_dat_o(  ),
  .wbs7_we_o (  ),
  .wbs7_cyc_o(  ),
  .wbs7_stb_o(  ),
  .wbs7_cti_o(  ),
  .wbs7_bte_o(  ),
  .wbs7_dat_i( 8'h00 ),
  .wbs7_ack_i( 1'b0 ),
  .wbs7_err_i( 1'b0 ),
  .wbs7_rty_i( 1'b0 ),
  //------------ Clock, reset ----->
  .wb_clk_i ( wb_clk ),
  .wb_rst_i ( wb_rst )
);
//-------------------------------------------------------------------//
//////////////////////////////////////////////////////////////
// Wishbone communication bus arbitrer for ADCs CBUS       //
////////////////////////////////////////////////////////////
// 1 Master, 8 Slaves
// Bus is 32bit wide
arbiter_cbus cbus(
  //----------- Masters ----------->
  // Master 0 == Bridge from DBUS, Slave 3
  .wbm0_adr_i( wbs_d_cbus_adr_i ),
  .wbm0_dat_i( wbs_d_cbus_dat_i ),
  .wbm0_sel_i( wbs_d_cbus_sel_i ),
  .wbm0_we_i ( wbs_d_cbus_we_i ),
  .wbm0_cyc_i( wbs_d_cbus_cyc_i ),
  .wbm0_stb_i( wbs_d_cbus_stb_i ),
  .wbm0_cti_i( wbs_d_cbus_cti_i ),
  .wbm0_bte_i( wbs_d_cbus_bte_i ),
  .wbm0_dat_o( wbs_d_cbus_dat_o ),
  .wbm0_ack_o( wbs_d_cbus_ack_o ),
  .wbm0_err_o( wbs_d_cbus_err_o ),
  .wbm0_rty_o( wbs_d_cbus_rty_o ),
  //------------ Slaves ----------->
  // Slave 0 == QSFP0, Slave
  .wbs0_adr_o(  ),
  .wbs0_sel_o(  ),
  .wbs0_dat_o(  ),
  .wbs0_we_o (  ),
  .wbs0_cyc_o(  ),
  .wbs0_stb_o(  ),
  .wbs0_cti_o(  ),
  .wbs0_bte_o(  ),
  .wbs0_dat_i( 32'h0000_0000 ),
  .wbs0_ack_i( 1'b0 ),
  .wbs0_err_i( 1'b0 ),
  .wbs0_rty_i( 1'b0 ),
  // Slave 1 == QSFP1, Slave
  .wbs1_adr_o(  ),
  .wbs1_sel_o(  ),
  .wbs1_dat_o(  ),
  .wbs1_we_o (  ),
  .wbs1_cyc_o(  ),
  .wbs1_stb_o(  ),
  .wbs1_cti_o(  ),
  .wbs1_bte_o(  ),
  .wbs1_dat_i( 32'h0000_0000 ),
  .wbs1_ack_i( 1'b0 ),
  .wbs1_err_i( 1'b0 ),
  .wbs1_rty_i( 1'b0 ),
  // Slave 2 == ADC1, Slave
  .wbs2_adr_o( wbs_c_adc1_adr_i ),
  .wbs2_sel_o( wbs_c_adc1_sel_i ),
  .wbs2_dat_o( wbs_c_adc1_dat_i ),
  .wbs2_we_o ( wbs_c_adc1_we_i  ),
  .wbs2_cyc_o( wbs_c_adc1_cyc_i ),
  .wbs2_stb_o( wbs_c_adc1_stb_i ),
  .wbs2_cti_o( wbs_c_adc1_cti_i ),
  .wbs2_bte_o( wbs_c_adc1_bte_i ),
  .wbs2_dat_i( wbs_c_adc1_dat_o ),
  .wbs2_ack_i( wbs_c_adc1_ack_o ),
  .wbs2_err_i( wbs_c_adc1_err_o ),
  .wbs2_rty_i( wbs_c_adc1_rty_o ),
  // Slave 3 == ADC2, Slave
  .wbs3_adr_o( wbs_c_adc2_adr_i ),
  .wbs3_sel_o( wbs_c_adc2_sel_i ),
  .wbs3_dat_o( wbs_c_adc2_dat_i ),
  .wbs3_we_o ( wbs_c_adc2_we_i ),
  .wbs3_cyc_o( wbs_c_adc2_cyc_i ),
  .wbs3_stb_o( wbs_c_adc2_stb_i ),
  .wbs3_cti_o( wbs_c_adc2_cti_i ),
  .wbs3_bte_o( wbs_c_adc2_bte_i ),
  .wbs3_dat_i( wbs_c_adc2_dat_o ),
  .wbs3_ack_i( wbs_c_adc2_ack_o ),
  .wbs3_err_i( wbs_c_adc2_err_o ),
  .wbs3_rty_i( wbs_c_adc2_rty_o ),
  // Slave 4 == ADC3, Slave
  .wbs4_adr_o( wbs_c_adc3_adr_i ),
  .wbs4_sel_o( wbs_c_adc3_sel_i ),
  .wbs4_dat_o( wbs_c_adc3_dat_i ),
  .wbs4_we_o ( wbs_c_adc3_we_i ),
  .wbs4_cyc_o( wbs_c_adc3_cyc_i ),
  .wbs4_stb_o( wbs_c_adc3_stb_i ),
  .wbs4_cti_o( wbs_c_adc3_cti_i ),
  .wbs4_bte_o( wbs_c_adc3_bte_i ),
  .wbs4_dat_i( wbs_c_adc3_dat_o ),
  .wbs4_ack_i( wbs_c_adc3_ack_o ),
  .wbs4_err_i( wbs_c_adc3_err_o ),
  .wbs4_rty_i( wbs_c_adc3_rty_o ),
  // Slave 5 == ADC4, Slave
  .wbs5_adr_o( wbs_c_adc4_adr_i ),
  .wbs5_sel_o( wbs_c_adc4_sel_i ),
  .wbs5_dat_o( wbs_c_adc4_dat_i ),
  .wbs5_we_o ( wbs_c_adc4_we_i  ),
  .wbs5_cyc_o( wbs_c_adc4_cyc_i ),
  .wbs5_stb_o( wbs_c_adc4_stb_i ),
  .wbs5_cti_o( wbs_c_adc4_cti_i ),
  .wbs5_bte_o( wbs_c_adc4_bte_i ),
  .wbs5_dat_i( wbs_c_adc4_dat_o ),
  .wbs5_ack_i( wbs_c_adc4_ack_o ),
  .wbs5_err_i( wbs_c_adc4_err_o ),
  .wbs5_rty_i( wbs_c_adc4_rty_o ),
  // Slave 6 == ADC5, Slave
  .wbs6_adr_o( wbs_c_adc5_adr_i ),
  .wbs6_sel_o( wbs_c_adc5_sel_i ),
  .wbs6_dat_o( wbs_c_adc5_dat_i ),
  .wbs6_we_o ( wbs_c_adc5_we_i  ),
  .wbs6_cyc_o( wbs_c_adc5_cyc_i ),
  .wbs6_stb_o( wbs_c_adc5_stb_i ),
  .wbs6_cti_o( wbs_c_adc5_cti_i ),
  .wbs6_bte_o( wbs_c_adc5_bte_i ),
  .wbs6_dat_i( wbs_c_adc5_dat_o ),
  .wbs6_ack_i( wbs_c_adc5_ack_o ),
  .wbs6_err_i( wbs_c_adc5_err_o ),
  .wbs6_rty_i( wbs_c_adc5_rty_o ),
  // Slave 7 == ADC6, Slave
  .wbs7_adr_o( wbs_c_adc6_adr_i ),
  .wbs7_sel_o( wbs_c_adc6_sel_i ),
  .wbs7_dat_o( wbs_c_adc6_dat_i ),
  .wbs7_we_o ( wbs_c_adc6_we_i  ),
  .wbs7_cyc_o( wbs_c_adc6_cyc_i ),
  .wbs7_stb_o( wbs_c_adc6_stb_i ),
  .wbs7_cti_o( wbs_c_adc6_cti_i ),
  .wbs7_bte_o( wbs_c_adc6_bte_i ),
  .wbs7_dat_i( wbs_c_adc6_dat_o ),
  .wbs7_ack_i( wbs_c_adc6_ack_o ),
  .wbs7_err_i( wbs_c_adc6_err_o ),
  .wbs7_rty_i( wbs_c_adc6_rty_o ),
  //------------ Clock, reset ----->
  .wb_clk_i ( wb_clk ),
  .wb_rst_i ( wb_rst )
);
  // See also comments on DBUS if you want to change numbers here
  defparam cbus.wb_addr_match_width = 8;
  defparam cbus.slave0_adr = 8'h71; // QSFP0
  defparam cbus.slave1_adr = 8'h72; // QSFP1
  defparam cbus.slave2_adr = 8'h7A; // ADC1
  defparam cbus.slave3_adr = 8'h7B; // ADC2
  defparam cbus.slave4_adr = 8'h7C; // ADC3
  defparam cbus.slave5_adr = 8'h7D; // ADC4
  defparam cbus.slave6_adr = 8'h7E; // ADC5
  defparam cbus.slave7_adr = 8'h7F; // ADC6
//-------------------------------------------------------------------//
//////////////////////////////////////////////////////////////////
// Wishbone bus arbitrer for ADCs to MEM connectiob ABUS       //
////////////////////////////////////////////////////////////////
// This one is special, it can be a separate clock region
// 8 Masters, 1 Slave
// Bus is 32bit wide
arbiter_abus abus(
  //----------- Masters ----------->
  // Master 0 == QSFP0, Master
  .wbm0_adr_i( 32'h0000_0000 ),
  .wbm0_dat_i( 32'h0000_0000 ),
  .wbm0_sel_i( 4'h0 ),
  .wbm0_we_i ( 1'b0 ),
  .wbm0_cyc_i( 1'b0 ),
  .wbm0_stb_i( 1'b0 ),
  .wbm0_cti_i( 3'b000 ),
  .wbm0_bte_i( 2'b00 ),
  .wbm0_dat_o(  ),
  .wbm0_ack_o(  ),
  .wbm0_err_o(  ),
  .wbm0_rty_o(  ),
  // Master 1 == QSFP1, Master
  .wbm1_adr_i( 32'h0000_0000 ),
  .wbm1_dat_i( 32'h0000_0000 ),
  .wbm1_sel_i( 4'h0 ),
  .wbm1_we_i ( 1'b0 ),
  .wbm1_cyc_i( 1'b0 ),
  .wbm1_stb_i( 1'b0 ),
  .wbm1_cti_i( 3'b000 ),
  .wbm1_bte_i( 2'b00 ),
  .wbm1_dat_o(  ),
  .wbm1_ack_o(  ),
  .wbm1_err_o(  ),
  .wbm1_rty_o(  ),
  // Master 2 == ADC1, Master
  .wbm2_adr_i( wbm_a_adc1_adr_o ),
  .wbm2_dat_i( wbm_a_adc1_dat_o ),
  .wbm2_sel_i( wbm_a_adc1_sel_o ),
  .wbm2_we_i ( wbm_a_adc1_we_o  ),
  .wbm2_cyc_i( wbm_a_adc1_cyc_o ),
  .wbm2_stb_i( wbm_a_adc1_stb_o ),
  .wbm2_cti_i( wbm_a_adc1_cti_o ),
  .wbm2_bte_i( wbm_a_adc1_bte_o ),
  .wbm2_dat_o( wbm_a_adc1_dat_i ),
  .wbm2_ack_o( wbm_a_adc1_ack_i ),
  .wbm2_err_o( wbm_a_adc1_err_i ),
  .wbm2_rty_o( wbm_a_adc1_rty_i ),
  // Master 3 == ADC2, Master
  .wbm3_adr_i( wbm_a_adc2_adr_o ),
  .wbm3_dat_i( wbm_a_adc2_dat_o ),
  .wbm3_sel_i( wbm_a_adc2_sel_o ),
  .wbm3_we_i ( wbm_a_adc2_we_o  ),
  .wbm3_cyc_i( wbm_a_adc2_cyc_o ),
  .wbm3_stb_i( wbm_a_adc2_stb_o ),
  .wbm3_cti_i( wbm_a_adc2_cti_o ),
  .wbm3_bte_i( wbm_a_adc2_bte_o ),
  .wbm3_dat_o( wbm_a_adc2_dat_i ),
  .wbm3_ack_o( wbm_a_adc2_ack_i ),
  .wbm3_err_o( wbm_a_adc2_err_i ),
  .wbm3_rty_o( wbm_a_adc2_rty_i ),
  // Master 4 == ADC3, Master
  .wbm4_adr_i( wbm_a_adc3_adr_o ),
  .wbm4_dat_i( wbm_a_adc3_dat_o ),
  .wbm4_sel_i( wbm_a_adc3_sel_o ),
  .wbm4_we_i ( wbm_a_adc3_we_o  ),
  .wbm4_cyc_i( wbm_a_adc3_cyc_o ),
  .wbm4_stb_i( wbm_a_adc3_stb_o ),
  .wbm4_cti_i( wbm_a_adc3_cti_o ),
  .wbm4_bte_i( wbm_a_adc3_bte_o ),
  .wbm4_dat_o( wbm_a_adc3_dat_i ),
  .wbm4_ack_o( wbm_a_adc3_ack_i ),
  .wbm4_err_o( wbm_a_adc3_err_i ),
  .wbm4_rty_o( wbm_a_adc3_rty_i ),
  // Master 5 == ADC4, Master
  .wbm5_adr_i( wbm_a_adc4_adr_o ),
  .wbm5_dat_i( wbm_a_adc4_dat_o ),
  .wbm5_sel_i( wbm_a_adc4_sel_o ),
  .wbm5_we_i ( wbm_a_adc4_we_o  ),
  .wbm5_cyc_i( wbm_a_adc4_cyc_o ),
  .wbm5_stb_i( wbm_a_adc4_stb_o ),
  .wbm5_cti_i( wbm_a_adc4_cti_o ),
  .wbm5_bte_i( wbm_a_adc4_bte_o ),
  .wbm5_dat_o( wbm_a_adc4_dat_i ),
  .wbm5_ack_o( wbm_a_adc4_ack_i ),
  .wbm5_err_o( wbm_a_adc4_err_i ),
  .wbm5_rty_o( wbm_a_adc4_rty_i ),
  // Master 6 == ADC5, Master
  .wbm6_adr_i( wbm_a_adc5_adr_o ),
  .wbm6_dat_i( wbm_a_adc5_dat_o ),
  .wbm6_sel_i( wbm_a_adc5_sel_o ),
  .wbm6_we_i ( wbm_a_adc5_we_o ),
  .wbm6_cyc_i( wbm_a_adc5_cyc_o ),
  .wbm6_stb_i( wbm_a_adc5_stb_o ),
  .wbm6_cti_i( wbm_a_adc5_cti_o ),
  .wbm6_bte_i( wbm_a_adc5_bte_o ),
  .wbm6_dat_o( wbm_a_adc5_dat_i ),
  .wbm6_ack_o( wbm_a_adc5_ack_i ),
  .wbm6_err_o( wbm_a_adc5_err_i ),
  .wbm6_rty_o( wbm_a_adc5_rty_i ),
  // Master 7 == ADC6, Master
  .wbm7_adr_i( wbm_a_adc6_adr_o ),
  .wbm7_dat_i( wbm_a_adc6_dat_o ),
  .wbm7_sel_i( wbm_a_adc6_sel_o ),
  .wbm7_we_i ( wbm_a_adc6_we_o  ),
  .wbm7_cyc_i( wbm_a_adc6_cyc_o ),
  .wbm7_stb_i( wbm_a_adc6_stb_o ),
  .wbm7_cti_i( wbm_a_adc6_cti_o ),
  .wbm7_bte_i( wbm_a_adc6_bte_o ),
  .wbm7_dat_o( wbm_a_adc6_dat_i ),
  .wbm7_ack_o( wbm_a_adc6_ack_i ),
  .wbm7_err_o( wbm_a_adc6_err_i ),
  .wbm7_rty_o( wbm_a_adc6_rty_i ),
  //------------ Slaves ----------->
  // Slave 0 == MEM1, Port 1
  .wbs0_adr_o( wbs_a_mem1_adr_i ),
  .wbs0_dat_o( wbs_a_mem1_dat_i ),
  .wbs0_sel_o( wbs_a_mem1_sel_i ),
  .wbs0_we_o ( wbs_a_mem1_we_i ),
  .wbs0_cyc_o( wbs_a_mem1_cyc_i ),
  .wbs0_stb_o( wbs_a_mem1_stb_i ),
  .wbs0_cti_o( wbs_a_mem1_cti_i ),
  .wbs0_bte_o( wbs_a_mem1_bte_i ),
  .wbs0_dat_i( wbs_a_mem1_dat_o ),
  .wbs0_ack_i( wbs_a_mem1_ack_o ),
  .wbs0_err_i( wbs_a_mem1_err_o ),
  .wbs0_rty_i( wbs_a_mem1_rty_o ),
  // NOT CONNECTED: wbs_a_mem1_cab_i
  //------------ Clock, reset ----->
  .wb_clk_i ( wbfast_clk ),
  .wb_rst_i ( wbfast_rst )
);
  // With one slave the address assignment is useless
  // So these parameters are not even defined for abus
  // defparam abus.wb_addr_match_width = 1;
  // defparam abus.slave0_adr = XXX;
//===================================================================//
endmodule
