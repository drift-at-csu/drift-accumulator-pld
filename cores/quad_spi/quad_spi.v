// synopsys translate_off
// `include "timescale.v"
// synopsys translate_on

module quad_spi (
	// Wishbone Slave Bus, used for Instruction ROM access
	input wire [31:0]	wbs0_adr_i,
	input wire [1:0]	wbs0_bte_i,
	input wire [2:0]	wbs0_cti_i,
	input wire		wbs0_cyc_i,
	input wire [31:0]	wbs0_dat_i,
	input wire [3:0]	wbs0_sel_i,
	input wire		wbs0_stb_i,
	input wire		wbs0_we_i,
	input wire		wbs0_cab_i,
	output wire		wbs0_ack_o,
	output wire		wbs0_err_o,
	output wire		wbs0_rty_o,
	output wire [31:0]	wbs0_dat_o,
	// Wishbone Slave Bus, used for Data ROM access
	input wire [31:0]       wbs1_adr_i,
	input wire [1:0]        wbs1_bte_i,
	input wire [2:0]        wbs1_cti_i,
	input wire              wbs1_cyc_i,
	input wire [31:0]       wbs1_dat_i,
	input wire [3:0]        wbs1_sel_i,
	input wire              wbs1_stb_i,
	input wire              wbs1_we_i,
	input wire              wbs1_cab_i,
	output wire             wbs1_ack_o,
	output wire             wbs1_err_o,
	output wire             wbs1_rty_o,
	output wire [31:0]      wbs1_dat_o,

	// Wishbone Slave Bus, used for control ( byte wide )
	input wire [31:0]	wbsc_adr_i,
	input wire 		wbsc_cyc_i,
	input wire [7:0]	wbsc_dat_i,
	input wire 		wbsc_stb_i,
	input wire		wbsc_we_i,
	output wire		wbsc_ack_o,
	output wire [7:0]	wbsc_dat_o,

	input wire		wbs_clk_i,
	input wire		wbs_rst_i,
		    
	output wire	spi_clk_o,
	output wire	spi_cs_o,
	inout wire	spi_data0_io,
	inout wire	spi_data1_io,
	inout wire	spi_data2_io,
	inout wire	spi_data3_io,

	output wire	spi_rst_o
);

//////////////////////////////////////////////
// Arbiter for all 3 wishbone interfaces   //
////////////////////////////////////////////
  reg [2:0]	input_select = 3'b000, last_selected = 3'b001; // Number of input ports is 3
  wire		arb_for_wbs0, arb_for_wbs1, arb_for_wbsc;

  // Wires allowing selection of new input
  assign arb_for_wbs0 = (last_selected[1] | last_selected[2]
  	| !(wbs1_cyc_i | wbsc_cyc_i) ) & !(|input_select);
  assign arb_for_wbs1 = (last_selected[0] | last_selected[2]
  	| !(wbs0_cyc_i | wbsc_cyc_i) ) & !(|input_select);
  assign arb_for_wbsc = (last_selected[0] | last_selected[1]
	| !(wbs0_cyc_i | wbs1_cyc_i) ) & !(|input_select);
  
  // Master select logic
  always @(posedge wbs_clk_i) begin
	if (  	(input_select[0] & !wbs0_cyc_i) | 
		(input_select[1] & !wbs1_cyc_i) | 
		(input_select[2] & !wbsc_cyc_i) )
		input_select <= 0;
	else if (!(&input_select) & wbs0_cyc_i & arb_for_wbs0)
		input_select <= 3'b001;
	else if (!(&input_select) & wbs1_cyc_i & arb_for_wbs1)
		input_select <= 3'b010;
	else if (!(&input_select) & wbsc_cyc_i & arb_for_wbsc)
		input_select <= 3'b100;

  end
  // Last Selected logic
  always @(posedge wbs_clk_i) begin
	if (!(&input_select) & wbs0_cyc_i & arb_for_wbs0)
		last_selected <= 3'b001;
	else if (!(&input_select) & wbs1_cyc_i & arb_for_wbs1)
		last_selected <= 3'b010;
	else if (!(&input_select) & wbsc_cyc_i & arb_for_wbsc)
		last_selected <= 3'b100;
  end
  //------------------------------>
  // Mux input signals to SPI
  wire [31:0] wbs_adr_i; assign wbs_adr_i = 
  	(input_select[2]) ? wbsc_adr_i :
	(input_select[1]) ? wbs1_adr_i :
	(input_select[0]) ? wbs0_adr_i :
	32'h0000_0000;

  wire [1:0] wbs_bte_i; assign wbs_bte_i =
  	(input_select[2]) ? 2'b00 :
	(input_select[1]) ? wbs1_bte_i :
	(input_select[0]) ? wbs0_bte_i :
	2'b00;

  wire [2:0] wbs_cti_i; assign wbs_cti_i =
  	(input_select[2]) ? 3'b000 :
	(input_select[1]) ? wbs1_cti_i :
	(input_select[0]) ? wbs0_cti_i :
	3'b000;
 
  wire wbs_cyc_i; assign wbs_cyc_i =
  	(input_select[2]) ? wbsc_cyc_i :
	(input_select[1]) ? wbs1_cyc_i :
	(input_select[0]) ? wbs0_cyc_i :
	1'b0;

  wire [31:0] wbs_dat_i; assign wbs_dat_i =
  	(input_select[2]) ? {24'h00_0000, wbsc_dat_i}:
	(input_select[1]) ? wbs1_dat_i :
	(input_select[0]) ? wbs0_dat_i :
	32'h0000_0000;

  wire [3:0] wbs_sel_i; assign wbs_sel_i =
  	(input_select[2]) ? 4'b0001:
	(input_select[1]) ? wbs1_sel_i :
	(input_select[0]) ? wbs0_sel_i :
	4'b0000;

  wire wbs_stb_i; assign wbs_stb_i = 
	(input_select[2]) ? wbsc_stb_i :
	(input_select[1]) ? wbs1_stb_i :
	(input_select[0]) ? wbs0_stb_i :
	1'b0;

  wire wbs_we_i; assign wbs_we_i = 
	(input_select[2]) ? wbsc_we_i :
	(input_select[1]) ? wbs1_we_i :
	(input_select[0]) ? wbs0_we_i :
	1'b0;

  //------------------------------->
  reg [31:0] wbs_dat_o;
  reg wbs_ack_o = 1'b0;

  // Mux output signals from SPI
  assign wbs0_dat_o = wbs_dat_o;
  assign wbs0_ack_o = wbs_ack_o & input_select[0];
  assign wbs0_err_o = wbs_err_o & input_select[0];
  assign wbs0_rty_o = 0;

  assign wbs1_dat_o = wbs_dat_o;
  assign wbs1_ack_o = wbs_ack_o & input_select[1];
  assign wbs1_err_o = wbs_err_o & input_select[1];
  assign wbs1_rty_o = 0;

  assign wbsc_dat_o = wbs_dat_o[7:0];
  assign wbsc_ack_o = wbs_ack_o & input_select[2];




  reg [29:0]	adr_reg;
  wire		new_access;
  reg		new_access_reg;
  wire		burst;
  reg		burst_reg;
  wire		new_burst;


  assign wbs_err_o = 1'b0;
  assign wbs_rty_o = 1'b0;

  // Assign wb_dat_o


  // Internal Registers:

  reg wbs_stb_i_reg;
  always @(posedge wbs_clk_i)
	wbs_stb_i_reg <= wbs_stb_i;

  assign new_access = (wbs_stb_i & !wbs_stb_i_reg);
  always @(posedge wbs_clk_i)
	new_access_reg <= new_access;

  always @(posedge wbs_clk_i)
	burst_reg <= burst;
  assign new_burst = (burst & !burst_reg);
  assign burst = wbs_cyc_i & (!(wbs_cti_i == 3'b000)) & (!(wbs_cti_i == 3'b111));

  always @(posedge wbs_clk_i)
  if (wbs_rst_i)
	adr_reg <= 30'h0000_0000;
  else if (new_access)
	adr_reg <= wbs_adr_i[31:2];

  else if (burst) begin
	if (wbs_cti_i == 3'b010)
		case (wbs_bte_i)
			2'b00: adr_reg <= adr_reg + 1'b1;
			2'b01: adr_reg[1:0] <= adr_reg[1:0] + 1'b1;
			2'b10: adr_reg[2:0] <= adr_reg[2:0] + 1'b1;
			2'b11: adr_reg[3:0] <= adr_reg[3:0] + 1'b1;
		endcase
	else
		adr_reg <= wbs_adr_i[31:2];
  end
//////////////////////////////////////////////////
// State Machine for acnowledge and SPI data   //
////////////////////////////////////////////////

  reg [1:0] spiphy_mode = 2'b01;

  reg [7:0] spiphy_input_data = 8'h00;
  wire [7:0] spiphy_output_data;

  reg [7:0] spiphy_dummy_cycles = 8'h00;
  reg spiphy_input_data_valid = 1'b0;
  reg spiphy_output_data_valid = 1'b0;

  reg spiphy_last_instruction = 1'b0;
  reg [7:0] spiphy_bytes_counter = 8'h00;
  wire spiphy_data_ready;

  reg [1:0] spi_fsm_request = 2'b00;
  	// 2'b01 => fast read
	// 2'b10 => single instruction request


  (* FSM_ENCODING="SEQUENTIAL", SAFE_IMPLEMENTATION="NO" *) reg [1:0] spi_fsm_state = 2'b00;
  always@(posedge wbs_clk_i)
  if (wbs_rst_i) begin
	spi_fsm_state <= 2'b00; // Idle state
  end else case (spi_fsm_state)
  2'b00 : begin // Idle state
	if (new_access | burst) begin
		// The Begining of cycle access

		spi_fsm_state <= 2'b01;
		spiphy_input_data_valid <= 1'b1;
		if ( input_select[2] ) begin
			// We're executing command for wbsc
			spi_fsm_request <= 2'b10; // Execute Single Instruction
			spiphy_last_instruction <= 1'b1; // This will be the last instruction
			spiphy_input_data <= wbs_adr_i[7:0];
		end else begin
			// We're executing Read for wbs0 or wbs1
			spi_fsm_request <= 2'b01; // Fast Read
			spiphy_last_instruction <= 1'b0; // This is not last instruction
			spiphy_input_data <= 8'hEC; // 0x0B/0xOC is FAST Read
		end
	end else begin
		// We're in the idle state
		// wbs_ack_o <= 1'b0;
	end
	wbs_ack_o <= 1'b0;
	spiphy_dummy_cycles <= 8'h00;
	spiphy_mode <= 2'b01;
  end
  2'b01 : begin // Execute instruction
	// Our Instruction is Stored in spiphy_input_data
	// So let's wait until it's executed first
	if  (spiphy_data_ready) begin
		// Based on what was executed, let's switch back to idle
		// In case of Single instruction or to the fast read
		if ( spi_fsm_request == 2'b01 ) begin
			// We're doing the fast read, let's supply the address
			spiphy_mode <= 2'b11;
			spi_fsm_state <= 2'b10; // Supplying the address
			spiphy_input_data_valid <= 1'b1;
			spiphy_last_instruction <= 1'b0; // This is not last instruction
			// We'll assume 3Address mode here?
			spiphy_input_data <= wbs_adr_i[24:16];
			spiphy_bytes_counter <= 8'h02; // And 2 more bytes to transfer
		end else begin
			spi_fsm_state <= 2'b00;
			wbs_ack_o <= 1'b1;
		end
	end else begin
		spiphy_input_data_valid <= 1'b0;
	end
	
  end
  2'b10 : begin // Address ( Supply the address to the SPI )
  	if  (spiphy_data_ready) begin
		if (spiphy_bytes_counter == 8'h00 ) begin
			// We supplied the address, now let's supply the data
			spiphy_input_data_valid <= 1'b0;
			spiphy_output_data_valid <= 1'b1;
			spi_fsm_state <= 2'b11;
			spiphy_bytes_counter <= 8'h03;
			spiphy_dummy_cycles <= 8'h00;
		end else begin
			spiphy_input_data <= (spiphy_bytes_counter == 8'h02) ?
				wbs_adr_i[15:8] : wbs_adr_i[7:0];
			spiphy_input_data_valid <= 1'b1;
			spiphy_bytes_counter <= spiphy_bytes_counter - 8'h01;
			// We need to wait for 1 byte after the submitting
			// the address and switching to data
			spiphy_dummy_cycles <= (spiphy_bytes_counter == 8'h01) ?
				8'h08 : 8'h00;
		end
	end else begin
		spiphy_input_data_valid <= 1'b0;
	end
  end
  2'b11 : begin // Read 
	if (spiphy_data_ready) begin
		if (spiphy_bytes_counter == 8'h00 ) begin
			// The Data is read, let's go to idle
			spi_fsm_state <= 2'b00;
			wbs_ack_o <= 1'b1;
		end else begin
			spiphy_output_data_valid <= 1'b1;
			spiphy_bytes_counter <= spiphy_bytes_counter - 8'h01;
			// We need to tell when there will be a last byte
			spiphy_last_instruction <= (spiphy_bytes_counter == 8'h01) ?
				1'b1 : 1'b0;
		end
		wbs_dat_o <= {wbs_dat_o[23:0], spiphy_output_data};
	end else begin
		spiphy_output_data_valid <= 1'b0;
	end
  end
  endcase

/*
  always @(posedge wbs_clk_i)
  if (wbs_rst_i)
	wbs_ack_o <= 1'b0;
  else if (wbs_ack_o & (!burst | (wbs_cti_i == 3'b111)))
	wbs_ack_o <= 1'b0;
  else if (wbs_stb_i & ((!burst & !new_access & new_access_reg) | (burst & burst_reg)))
	wbs_ack_o <= 1'b1;
  else
	wbs_ack_o <= 1'b0;
*/

////////////////////////////////////////
// Physical Interface for QUAD SPI   //
//////////////////////////////////////
quad_spi_phy #(
) spi_phy (
  .spi_wait_states_i	( spiphy_dummy_cycles ),
  .spi_mode_i		( spiphy_mode ),
  .data_clk_i		( wbs_clk_i ),
  .data_rst_i		( wbs_rst_i ),
  .last_transfer_i	( spiphy_last_instruction ),
  .input_data_i		( spiphy_input_data ),
  .input_data_valid_i	( spiphy_input_data_valid ),
  .output_data_o	( spiphy_output_data ),
  .output_data_valid_i	( spiphy_output_data_valid ),

  .data_ready_o  ( spiphy_data_ready ),
  .spi_busy_o		(  ),

  .spi_clk_o	( spi_clk_o ),
  .spi_cs_o	( spi_cs_o ),
  .spi_data0_io	( spi_data0_io ),
  .spi_data1_io	( spi_data1_io ),
  .spi_data2_io	( spi_data2_io ),
  .spi_data3_io	( spi_data3_io ),
  .spi_rst_o	( spi_rst_o )
);

endmodule

