// synopsys translate_off
// `include "timescale.v"
// synopsys translate_on

module quad_spi (
	// Wishbone Slave Bus, used for Instruction ROM access
	input wire [31:0]	wbs0_adr_i,
	input wire [1:0]	wbs0_bte_i,
	input wire [2:0]	wbs0_cti_i,
	input wire		wbs0_cyc_i,
	input wire [31:0]	wbs0_dat_i,
	input wire [3:0]	wbs0_sel_i,
	input wire		wbs0_stb_i,
	input wire		wbs0_we_i,
	input wire		wbs0_cab_i,
	output reg		wbs0_ack_o,
	output wire		wbs0_err_o,
	output wire		wbs0_rty_o,
	output wire [31:0]	wbs0_dat_o,
	// Wishbone Slave Bus, used for Data ROM access
	input wire [31:0]       wbs1_adr_i,
	input wire [1:0]        wbs1_bte_i,
	input wire [2:0]        wbs1_cti_i,
	input wire              wbs1_cyc_i,
	input wire [31:0]       wbs1_dat_i,
	input wire [3:0]        wbs1_sel_i,
	input wire              wbs1_stb_i,
	input wire              wbs1_we_i,
	input wire              wbs1_cab_i,
	output wire             wbs1_ack_o,
	output wire             wbs1_err_o,
	output wire             wbs1_rty_o,
	output wire [31:0]      wbs1_dat_o,

	// Wishbone Slave Bus, used for control ( byte wide )
	input wire [31:0]	wbsc_adr_i,
	input wire 		wbsc_cyc_i,
	input wire [7:0]	wbsc_dat_i,
	input wire 		wbsc_stb_i,
	input wire		wbsc_we_i,
	output wire		wbsc_ack_o,
	output wire [7:0]	wbsc_dat_o,

	input wire		wbs_clk_i,
	input wire		wbs_rst_i,
		    
	output wire	spi_clk_o,
	output wire	spi_cs_o,
	inout wire	spi_data0_io,
	inout wire	spi_data1_io,
	inout wire	spi_data2_io,
	inout wire	spi_data3_io,

	output wire	spi_rst_o
);
  reg [29:0]	adr_reg;
  reg		wb_stb_reg;
  wire		new_access;
  reg		new_access_reg;
  wire		burst;
  reg		burst_reg;
  wire		new_burst;


  assign wbs_err_o = 1'b0;
  assign wbs_rty_o = 1'b0;

  // Assign wb_dat_o


  // Internal Registers:

  reg wbs0_stb_i_reg;
  always @(posedge wbs_clk_i)
	wbs0_stb_i_reg <= wbs0_stb_i;

  assign new_access = (wbs0_stb_i & !wbs0_stb_i_reg);
  always @(posedge wbs_clk_i)
	new_access_reg <= new_access;

  always @(posedge wbs_clk_i)
	burst_reg <= burst;
  assign new_burst = (burst & !burst_reg);
  assign burst = wbs0_cyc_i & (!(wbs0_cti_i == 3'b000)) & (!(wbs0_cti_i == 3'b111));

  always @(posedge wbs_clk_i)
  if (wbs_rst_i)
	adr_reg <= 30'h0000_0000;
  else if (new_access)
	adr_reg <= wbs0_adr_i[31:2];

  else if (burst) begin
	if (wbs0_cti_i == 3'b010)
		case (wbs0_bte_i)
			2'b00: adr_reg <= adr_reg + 1'b1;
			2'b01: adr_reg[1:0] <= adr_reg[1:0] + 1'b1;
			2'b10: adr_reg[2:0] <= adr_reg[2:0] + 1'b1;
			2'b11: adr_reg[3:0] <= adr_reg[3:0] + 1'b1;
		endcase
	else
		adr_reg <= wbs0_adr_i[31:2];
  end

  always @(posedge wbs_clk_i)
  if (wbs_rst_i)
	wbs0_ack_o <= 1'b0;
  else if (wbs0_ack_o & (!burst | (wbs0_cti_i == 3'b111)))
	wbs0_ack_o <= 1'b0;
  else if (wbs0_stb_i & ((!burst & !new_access & new_access_reg) | (burst & burst_reg)))
	wbs0_ack_o <= 1'b1;
  else
	wbs0_ack_o <= 1'b0;
   
endmodule

