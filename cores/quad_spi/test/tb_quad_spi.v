`timescale 1ps / 1ps

module tb_quad_spi;

//////////////////////////////////////////////
// Parameters for cti and bte burst cycles //
////////////////////////////////////////////

// bte:
parameter [1:0] linear = 2'b00,
		beat4  = 2'b01,
		beat8  = 2'b10,
		beat16 = 2'b11;
// cti:
parameter [2:0] classic = 3'b000,
		inc     = 3'b010,
		eob	= 3'b111;


///////////////////////////////////////
//  Input system clock is 100 MHz   //
/////////////////////////////////////
reg sys_clk = 1'b0;

////////////////////////////////////////////
// WB  Clocks                            //
// As they are generated in real design //
/////////////////////////////////////////
wire wb_clk;
wire wb_rst;

clkgen clkgen0(
  .adcs_clk_o( ), // Not using this

  .mems_clk_o ( ),
  .mems_rst_o ( ),

  .wb_clk_o ( wb_clk ),
  .wb_rst_o ( wb_rst ),

  .wbfast_clk_o (  ),
  .wbfast_rst_o (  ),

  .sys_clk_i( sys_clk )
);
///////////////////////////////////////////////////
// Wishbone Masters (I and D buses)             //
/////////////////////////////////////////////////
reg [31:0]      wbm_i_cpu0_adr_o = 32'hXXXX_XXXX;
reg [1:0]       wbm_i_cpu0_bte_o = 2'b00;
reg [2:0]       wbm_i_cpu0_cti_o = 3'b000;
reg             wbm_i_cpu0_cyc_o = 1'b0;
reg [31:0]      wbm_i_cpu0_dat_o = 32'hXXXX_XXXX;
reg [3:0]       wbm_i_cpu0_sel_o = 4'bxxxx;
reg             wbm_i_cpu0_stb_o = 1'b0;
reg             wbm_i_cpu0_we_o  = 1'bX;
wire            wbm_i_cpu0_ack_i;
wire            wbm_i_cpu0_err_i;
wire            wbm_i_cpu0_rty_i;
wire [31:0]     wbm_i_cpu0_dat_i;

reg [31:0]      wbm_d_cpu0_adr_o = 32'hXXXX_XXXX;
reg [1:0]       wbm_d_cpu0_bte_o = 2'b00;
reg [2:0]       wbm_d_cpu0_cti_o = 3'b000;
reg             wbm_d_cpu0_cyc_o = 1'b0;
reg [31:0]      wbm_d_cpu0_dat_o = 32'hXXXX_XXXX;
reg [3:0]       wbm_d_cpu0_sel_o = 4'bxxxx;
reg             wbm_d_cpu0_stb_o = 1'b0;
reg             wbm_d_cpu0_we_o  = 1'bX;
wire            wbm_d_cpu0_ack_i;
wire            wbm_d_cpu0_err_i;
wire            wbm_d_cpu0_rty_i;
wire [31:0]     wbm_d_cpu0_dat_i;

///////////////////////////////////////////////
// Wishbone Slave wires for SPI Controller  //
/////////////////////////////////////////////
// Wishbone Slave Ports
wire            wbs_b_spi1_cyc_i;
wire            wbs_b_spi1_stb_i;
wire [31:0]     wbs_b_spi1_adr_i;
wire            wbs_b_spi1_we_i;
wire [7:0]      wbs_b_spi1_dat_i;
wire [7:0]      wbs_b_spi1_dat_o;
wire            wbs_b_spi1_ack_o;
// Wishbone ROM Data Access
wire            wbs_d_spi1_ack_o;
wire            wbs_d_spi1_err_o;
wire            wbs_d_spi1_rty_o;
wire [31:0]     wbs_d_spi1_dat_o;
wire            wbs_d_spi1_cyc_i;
wire [31:0]     wbs_d_spi1_adr_i;
wire            wbs_d_spi1_stb_i;
wire            wbs_d_spi1_we_i;
wire [3:0]      wbs_d_spi1_sel_i;
wire [31:0]     wbs_d_spi1_dat_i;
wire [2:0]      wbs_d_spi1_cti_i;
wire [1:0]      wbs_d_spi1_bte_i;
// Wishbone ROM Instruction
wire            wbs_i_spi1_ack_o;
wire            wbs_i_spi1_err_o;
wire            wbs_i_spi1_rty_o;
wire [31:0]     wbs_i_spi1_dat_o;
wire            wbs_i_spi1_cyc_i;
wire [31:0]     wbs_i_spi1_adr_i;
wire            wbs_i_spi1_stb_i;
wire            wbs_i_spi1_we_i;
wire [3:0]      wbs_i_spi1_sel_i;
wire [31:0]     wbs_i_spi1_dat_i;
wire [2:0]      wbs_i_spi1_cti_i;
wire [1:0]      wbs_i_spi1_bte_i;
////////////////////////////////////////////////////
//  We need Ibus, Dbus and Bbus                  //
//////////////////////////////////////////////////
///////////////////////////////////////////////
// Wishbone instruction bus arbiter (IBUS)  //
/////////////////////////////////////////////
// This is the simplest Arbiter: 1 master, 3 slaves
// Bus is 32bit wide
arbiter_ibus #(
        .wb_addr_match_width ( 4 ),
	.slave0_adr(4'h0),      // MEM0: 0x0000_0000
	.slave1_adr(4'hf),      // ROM0: 0xF000_0000
	.slave2_adr(4'h1)       // SPI1: 0x1000_0000
)ibus(
  //----------- Masters ----------->
  // Master 0 == CPU0 (OR1200)
  .wbm0_adr_i( wbm_i_cpu0_adr_o ),
  .wbm0_dat_i( wbm_i_cpu0_dat_o ),
  .wbm0_sel_i( wbm_i_cpu0_sel_o ),
  .wbm0_we_i ( wbm_i_cpu0_we_o ),
  .wbm0_cyc_i( wbm_i_cpu0_cyc_o ),
  .wbm0_stb_i( wbm_i_cpu0_stb_o ),
  .wbm0_cti_i( wbm_i_cpu0_cti_o ),
  .wbm0_bte_i( wbm_i_cpu0_bte_o ),
  .wbm0_dat_o( wbm_i_cpu0_dat_i ),
  .wbm0_ack_o( wbm_i_cpu0_ack_i ),
  .wbm0_err_o( wbm_i_cpu0_err_i ),
  .wbm0_rty_o( wbm_i_cpu0_rty_i ),
  //------------ Slaves ----------->
  // Slave 0 == MEM0, Port 0
  .wbs0_adr_o(  ),
  .wbs0_dat_o(  ),
  .wbs0_sel_o(  ),
  .wbs0_we_o (  ),
  .wbs0_cyc_o(  ),
  .wbs0_stb_o(  ),
  .wbs0_cti_o(  ),
  .wbs0_bte_o(  ),
  .wbs0_dat_i( 32'hDEAD_BEEF ),
  .wbs0_ack_i( 1'b0 ),
  .wbs0_err_i( 1'b1 ),
  .wbs0_rty_i( 1'b0 ),
  // Slave 1 == ROM0
  .wbs1_adr_o(  ),
  .wbs1_dat_o(  ),
  .wbs1_sel_o(  ),
  .wbs1_we_o (  ),
  .wbs1_cyc_o(  ),
  .wbs1_stb_o(  ),
  .wbs1_cti_o(  ),
  .wbs1_bte_o(  ),
  .wbs1_dat_i( 32'hDEAD_BEEF ),
  .wbs1_ack_i( 1'b0 ),
  .wbs1_err_i( 1'b1 ),
  .wbs1_rty_i( 1'b0 ),
  // Slave 2 == SPI1
  .wbs2_adr_o( wbs_i_spi1_adr_i ),
  .wbs2_dat_o( wbs_i_spi1_dat_i ),
  .wbs2_sel_o( wbs_i_spi1_sel_i ),
  .wbs2_we_o ( wbs_i_spi1_we_i ),
  .wbs2_cyc_o( wbs_i_spi1_cyc_i ),
  .wbs2_stb_o( wbs_i_spi1_stb_i ),
  .wbs2_cti_o( wbs_i_spi1_cti_i ),
  .wbs2_bte_o( wbs_i_spi1_bte_i ),
  .wbs2_dat_i( wbs_i_spi1_dat_o ),
  .wbs2_ack_i( wbs_i_spi1_ack_o ),
  .wbs2_err_i( wbs_i_spi1_err_o ),
  .wbs2_rty_i( wbs_i_spi1_rty_o ),
  //------------ Clock, reset ----->
  .wb_clk_i ( wb_clk ),
  .wb_rst_i ( wb_rst )
);
// Bridges wires
// From dbus to bbus
wire            wbs_d_bbus_ack_o;
wire            wbs_d_bbus_err_o;
wire            wbs_d_bbus_rty_o;
wire [31:0]     wbs_d_bbus_dat_o;
wire            wbs_d_bbus_cyc_i;
wire [31:0]     wbs_d_bbus_adr_i;
wire            wbs_d_bbus_stb_i;
wire            wbs_d_bbus_we_i;
wire [3:0]      wbs_d_bbus_sel_i;
wire [31:0]     wbs_d_bbus_dat_i;
wire [2:0]      wbs_d_bbus_cti_i;
wire [1:0]      wbs_d_bbus_bte_i;

////////////////////////////////////////
// Wishbone data bus arbiter (DBUS)  //
//////////////////////////////////////
// 3 Masters, 6 Slaves
// Bus is 32bit wide
wb_switch_b3 # (
	.dw ( 32 ),
	.aw ( 32 ),
	.slave0_sel_width( 8 ),     // Width of selection bits 8 == [31:24]
	.slave0_sel_addr ( 8'h00 ), // 0x0000_0000 to 0x00FF_FFFF
	.slave0_sel_mask ( 8'hFF ),
	.slave1_sel_width( 8 ),
	.slave1_sel_addr ( 8'h01 ), // 0x0100_0000 to 0x01FF_FFFF
	.slave1_sel_mask ( 8'hFF ),
	.slave2_sel_width( 8 ),
	.slave2_sel_addr ( 8'b1000_0000), // 0x8000_0000 to 0x81FF_FFFF
					// 0x9000_0000 to 0x91FF_FFFF
					// 0xA000_0000 to 0xA1FF_FFFF
					// 0xB000_0000 to 0xB1FF_FFFF
	.slave2_sel_mask ( 8'b1100_1110 ),
	.slave3_sel_width( 4 ),
	.slave3_sel_addr ( 4'h7 ),      // 0x7000_0000 to 0x7FFF_FFFF
	.slave3_sel_mask ( 4'hF ),
	.slave4_sel_width( 8 ),
	.slave4_sel_addr ( 8'h92 ),     // 0x9200_0000 to 0x92FF_FFFF
	.slave4_sel_mask ( 8'hFF ),
	.slave5_sel_width( 4 ),
	.slave5_sel_addr ( 4'h1 ),      // 0x1000_0000 to 0x1FFF_FFFF (128MBytes)
	.slave5_sel_mask ( 4'hF )
) dbus (
  //----------- Masters ----------->
  // Master 0 == CPU0
  .wbm0_adr_i( wbm_d_cpu0_adr_o ),
  .wbm0_bte_i( wbm_d_cpu0_bte_o ),
  .wbm0_cti_i( wbm_d_cpu0_cti_o ),
  .wbm0_cyc_i( wbm_d_cpu0_cyc_o ),
  .wbm0_dat_i( wbm_d_cpu0_dat_o ),
  .wbm0_sel_i( wbm_d_cpu0_sel_o ),
  .wbm0_stb_i( wbm_d_cpu0_stb_o ),
  .wbm0_we_i ( wbm_d_cpu0_we_o ),
  .wbm0_ack_o( wbm_d_cpu0_ack_i ),
  .wbm0_err_o( wbm_d_cpu0_err_i ),
  .wbm0_rty_o( wbm_d_cpu0_rty_i ),
  .wbm0_dat_o( wbm_d_cpu0_dat_i ),
  // Master 1 == DBG0
  .wbm1_adr_i( 32'h0000_0000 ),
  .wbm1_bte_i( 2'b00 ),
  .wbm1_cti_i( 3'b000 ),
  .wbm1_cyc_i( 1'b0 ),
  .wbm1_dat_i( 32'hDEAD_BEEF ),
  .wbm1_sel_i( 4'b0000 ),
  .wbm1_stb_i( 1'b0 ),
  .wbm1_we_i ( 1'b0 ),
  .wbm1_ack_o(  ),
  .wbm1_err_o(  ),
  .wbm1_rty_o(  ),
  .wbm1_dat_o(  ),
  // Master 2 == ETH0
  .wbm2_adr_i( 32'h0000_0000 ),
  .wbm2_bte_i( 2'b00 ),
  .wbm2_cti_i( 3'b000 ),
  .wbm2_cyc_i( 1'b0 ),
  .wbm2_dat_i( 32'hDEAD_BEEF ),
  .wbm2_sel_i( 4'b0000 ),
  .wbm2_stb_i( 1'b0 ),
  .wbm2_we_i ( 1'b0 ),
  .wbm2_ack_o(  ),
  .wbm2_err_o(  ),
  .wbm2_rty_o(  ),    
  .wbm2_dat_o(  ),
  //------------ Slaves ----------->
  // Slave 0 == MEM0, Port 1
  .wbs0_adr_o(  ),
  .wbs0_bte_o(  ),
  .wbs0_cti_o(  ),
  .wbs0_cyc_o(  ),
  .wbs0_dat_o(  ),
  .wbs0_sel_o(  ),
  .wbs0_stb_o(  ),
  .wbs0_we_o (  ),
  .wbs0_ack_i( 1'b0 ),
  .wbs0_err_i( 1'b1 ),
  .wbs0_rty_i( 1'b0 ),
  .wbs0_dat_i( 32'hDEAD_BEEF ),
  // Slave 1 == MEM1, Port 0
  .wbs1_adr_o(  ),
  .wbs1_bte_o(  ),
  .wbs1_cti_o(  ),
  .wbs1_cyc_o(  ),
  .wbs1_dat_o(  ),
  .wbs1_sel_o(  ),
  .wbs1_stb_o(  ),
  .wbs1_we_o (  ),
  .wbs1_ack_i( 1'b0 ),
  .wbs1_err_i( 1'b1 ),
  .wbs1_rty_i( 1'b0 ),
  .wbs1_dat_i( 32'hDEAD_BEEF ),
  // Slave 2 == Bridge to BBUS0
  .wbs2_adr_o( wbs_d_bbus_adr_i ),
  .wbs2_bte_o( wbs_d_bbus_bte_i ),
  .wbs2_cti_o( wbs_d_bbus_cti_i ),
  .wbs2_cyc_o( wbs_d_bbus_cyc_i ),
  .wbs2_dat_o( wbs_d_bbus_dat_i ),
  .wbs2_sel_o( wbs_d_bbus_sel_i ),
  .wbs2_stb_o( wbs_d_bbus_stb_i ),
  .wbs2_we_o ( wbs_d_bbus_we_i ),
  .wbs2_ack_i( wbs_d_bbus_ack_o ),
  .wbs2_err_i( wbs_d_bbus_err_o ),
  .wbs2_rty_i( wbs_d_bbus_rty_o ),
  .wbs2_dat_i( wbs_d_bbus_dat_o ),
  // Slave 3 == Bridge to CBUS
  .wbs3_adr_o(  ),
  .wbs3_bte_o(  ),
  .wbs3_cti_o(  ),
  .wbs3_cyc_o(  ),
  .wbs3_dat_o(  ),
  .wbs3_sel_o(  ),
  .wbs3_stb_o(  ),
  .wbs3_we_o (  ),
  .wbs3_ack_i( 1'b0 ),
  .wbs3_err_i( 1'b1 ),
  .wbs3_rty_i( 1'b0 ),
  .wbs3_dat_i( 32'hDEAD_BEEF ),
  // Slave 4 == ETH0, Slave
  .wbs4_adr_o(  ),
  .wbs4_bte_o(  ),
  .wbs4_cti_o(  ),
  .wbs4_cyc_o(  ),
  .wbs4_dat_o(  ),
  .wbs4_sel_o(  ),
  .wbs4_stb_o(  ),
  .wbs4_we_o (  ),
  .wbs4_ack_i( 1'b0 ),
  .wbs4_err_i( 1'b1 ),
  .wbs4_rty_i( 1'b0 ),
  .wbs4_dat_i( 32'hDEAD_BEEF ),
  // Slave 5 == SPI1 Flash
  .wbs5_adr_o( wbs_d_spi1_adr_i ),
  .wbs5_bte_o( wbs_d_spi1_bte_i ),
  .wbs5_cti_o( wbs_d_spi1_cti_i ),
  .wbs5_cyc_o( wbs_d_spi1_cyc_i ),
  .wbs5_dat_o( wbs_d_spi1_dat_i ),
  .wbs5_sel_o( wbs_d_spi1_sel_i ),
  .wbs5_stb_o( wbs_d_spi1_stb_i ),
  .wbs5_we_o ( wbs_d_spi1_we_i ),
  .wbs5_ack_i( wbs_d_spi1_ack_o ),
  .wbs5_err_i( wbs_d_spi1_err_o ),
  .wbs5_rty_i( wbs_d_spi1_rty_o ),
  .wbs5_dat_i( wbs_d_spi1_dat_o ),
  //------------ Clock, reset ----->
  .wb_clk ( wb_clk ),
  .wb_rst ( wb_rst )
);
////////////////////////////////////////////////////////
// Wishbone byte bus arbitrer for Perepherials BBUS  //
//////////////////////////////////////////////////////
// 1 Master, 8 Slaves
// Bus is 8bit wide
arbiter_bbus #(
	.wb_addr_match_width (8),
	.slave0_adr(8'h90), //<-- UART0
	.slave1_adr(8'h91),
	.slave2_adr(8'h80),
	.slave3_adr(8'h81),
	.slave4_adr(8'hB0), // <-- SPI1
	.slave5_adr(8'hB1),
	.slave6_adr(8'hA0),
	.slave7_adr(8'hA1)
) bbus (
  //----------- Masters ----------->
  // Master 0 == Bridge from DBUS, Slave 2
  .wbm0_adr_i( wbs_d_bbus_adr_i ),
  .wbm0_dat_i( wbs_d_bbus_dat_i ),
  .wbm0_sel_i( wbs_d_bbus_sel_i ),
  .wbm0_we_i ( wbs_d_bbus_we_i ),
  .wbm0_cyc_i( wbs_d_bbus_cyc_i ),
  .wbm0_stb_i( wbs_d_bbus_stb_i ),
  .wbm0_cti_i( wbs_d_bbus_cti_i ),
  .wbm0_bte_i( wbs_d_bbus_bte_i ),
  .wbm0_dat_o( wbs_d_bbus_dat_o ),
  .wbm0_ack_o( wbs_d_bbus_ack_o ),
  .wbm0_err_o( wbs_d_bbus_err_o ),
  .wbm0_rty_o( wbs_d_bbus_rty_o ),
  //------------ Slaves ----------->
  // Slave 0 == UART0
  .wbs0_adr_o(  ),
  .wbs0_dat_o(  ),
  .wbs0_we_o (  ),
  .wbs0_cyc_o(  ),
  .wbs0_stb_o(  ),
  .wbs0_cti_o(  ),
  .wbs0_bte_o(  ),
  .wbs0_dat_i( 8'h00 ),
  .wbs0_ack_i( 1'b0 ),
  .wbs0_err_i( 1'b1 ),
  .wbs0_rty_i( 1'b0 ),
  // Slave 1 == POW0
  .wbs1_adr_o(  ),
  .wbs1_dat_o(  ),
  .wbs1_we_o (  ),
  .wbs1_cyc_o(  ),
  .wbs1_stb_o(  ),
  .wbs1_cti_o(  ),
  .wbs1_bte_o(  ),
  .wbs1_dat_i( 8'h00 ),
  .wbs1_ack_i( 1'b0 ),
  .wbs1_err_i( 1'b1 ),
  .wbs1_rty_i( 1'b0 ),
  // Slave 2 == TWI0
  .wbs2_adr_o(  ),
  .wbs2_dat_o(  ),
  .wbs2_we_o (  ),
  .wbs2_cyc_o(  ),
  .wbs2_stb_o(  ),
  .wbs2_cti_o(  ),
  .wbs2_bte_o(  ),
  .wbs2_dat_i( 8'h00 ),
  .wbs2_ack_i( 1'b0 ),
  .wbs2_err_i( 1'b0 ),
  .wbs2_rty_i( 1'b0 ),
  // Slave 3 == SPI2
  .wbs3_adr_o(  ),
  .wbs3_dat_o(  ),
  .wbs3_we_o (  ),
  .wbs3_cyc_o(  ),
  .wbs3_stb_o(  ),
  .wbs3_cti_o(  ),
  .wbs3_bte_o(  ),
  .wbs3_dat_i( 8'h00 ),
  .wbs3_ack_i( 1'b0 ),
  .wbs3_err_i( 1'b0 ),
  .wbs3_rty_i( 1'b0 ),
  // Slave 4 == SPI1
  .wbs4_adr_o( wbs_b_spi1_adr_i ),
  .wbs4_dat_o( wbs_b_spi1_dat_i ),
  .wbs4_we_o ( wbs_b_spi1_we_i ),
  .wbs4_cyc_o( wbs_b_spi1_cyc_i ),
  .wbs4_stb_o( wbs_b_spi1_stb_i ),
  .wbs4_cti_o(  ),
  .wbs4_bte_o(  ),
  .wbs4_dat_i( wbs_b_spi1_dat_o ),
  .wbs4_ack_i( wbs_b_spi1_ack_o ),
  .wbs4_err_i( 1'b0 ),
  .wbs4_rty_i( 1'b0 ),
  // Slave 5 == ( Not connected )
  .wbs5_adr_o(  ),
  .wbs5_dat_o(  ),
  .wbs5_we_o (  ),
  .wbs5_cyc_o(  ),
  .wbs5_stb_o(  ),
  .wbs5_cti_o(  ),
  .wbs5_bte_o(  ),
  .wbs5_dat_i( 8'h00 ),
  .wbs5_ack_i( 1'b0 ),
  .wbs5_err_i( 1'b1 ),
  .wbs5_rty_i( 1'b0 ),
  // Slave 6 == ( Not connected )
  .wbs6_adr_o(  ),
  .wbs6_dat_o(  ),
  .wbs6_we_o (  ),
  .wbs6_cyc_o(  ),
  .wbs6_stb_o(  ),
  .wbs6_cti_o(  ),
  .wbs6_bte_o(  ),
  .wbs6_dat_i( 8'h00 ),
  .wbs6_ack_i( 1'b0 ),
  .wbs6_err_i( 1'b0 ),
  .wbs6_rty_i( 1'b0 ),
  // Slave 7 == ( Not connected )
  .wbs7_adr_o(  ),
  .wbs7_dat_o(  ),
  .wbs7_we_o (  ),
  .wbs7_cyc_o(  ),
  .wbs7_stb_o(  ),
  .wbs7_cti_o(  ),
  .wbs7_bte_o(  ),
  .wbs7_dat_i( 8'h00 ),
  .wbs7_ack_i( 1'b0 ),
  .wbs7_err_i( 1'b0 ),
  .wbs7_rty_i( 1'b0 ),
  //------------ Clock, reset ----->
  .wb_clk_i ( wb_clk ),
  .wb_rst_i ( wb_rst )
);
///////////////////////////////////
//  FLASH Controller            //
/////////////////////////////////
// SPI wires
wire 	spi1_cs_o;
wire 	spi1_clk_o;
wire 	spi1_data0_io;
wire 	spi1_data1_io;
wire 	spi1_data2_io;
wire 	spi1_data3_io;
wire 	spi1_reset_o;

quad_spi spi1 (
  .wbs0_adr_i( wbs_i_spi1_adr_i ),
  .wbs0_bte_i( wbs_i_spi1_bte_i ),
  .wbs0_cti_i( wbs_i_spi1_cti_i ),
  .wbs0_cyc_i( wbs_i_spi1_cyc_i ),
  .wbs0_dat_i( wbs_i_spi1_dat_i ),
  .wbs0_sel_i( wbs_i_spi1_sel_i ),
  .wbs0_stb_i( wbs_i_spi1_stb_i ),
  .wbs0_we_i ( wbs_i_spi1_we_i ),
  .wbs0_ack_o( wbs_i_spi1_ack_o ),
  .wbs0_err_o( wbs_i_spi1_err_o ),
  .wbs0_rty_o( wbs_i_spi1_rty_o ),
  .wbs0_dat_o( wbs_i_spi1_dat_o ),

  .wbs1_adr_i( wbs_d_spi1_adr_i ),
  .wbs1_bte_i( wbs_d_spi1_bte_i ),
  .wbs1_cti_i( wbs_d_spi1_cti_i ),
  .wbs1_cyc_i( wbs_d_spi1_cyc_i ),
  .wbs1_dat_i( wbs_d_spi1_dat_i ),
  .wbs1_sel_i( wbs_d_spi1_sel_i ),
  .wbs1_stb_i( wbs_d_spi1_stb_i ),
  .wbs1_we_i ( wbs_d_spi1_we_i ),
  .wbs1_ack_o( wbs_d_spi1_ack_o ),
  .wbs1_err_o( wbs_d_spi1_err_o ),
  .wbs1_rty_o( wbs_d_spi1_rty_o ),
  .wbs1_dat_o( wbs_d_spi1_dat_o ),

  .wbsc_adr_i( wbs_b_spi1_adr_i ),
  .wbsc_cyc_i( wbs_b_spi1_cyc_i ),
  .wbsc_dat_i( wbs_b_spi1_dat_i ),
  .wbsc_stb_i( wbs_b_spi1_stb_i ),
  .wbsc_we_i ( wbs_b_spi1_we_i ),
  .wbsc_ack_o( wbs_b_spi1_ack_o ),
  .wbsc_dat_o( wbs_b_spi1_dat_o ),

  .wbs_clk_i ( wb_clk ),
  .wbs_rst_i ( wb_rst ),

  .spi_clk_o    ( spi1_clk_o ),
  .spi_cs_o     ( spi1_cs_o ),
  .spi_data0_io ( spi1_data0_io ),
  .spi_data1_io ( spi1_data1_io ),
  .spi_data2_io ( spi1_data2_io ),
  .spi_data3_io ( spi1_data3_io ),
  .spi_rst_o    ( spi1_reset_o )
);

/////////////////////////////////////////////
//  Micron SPI Flash                      //
///////////////////////////////////////////
N25Qxxx spi1_chip (
  .S		( spi1_cs_o ),
  .C_		( spi1_clk_o ),
  .HOLD_DQ3	( spi1_data3_io ),
  .DQ0		( spi1_data0_io ),
  .DQ1		( spi1_data1_io ),
  .Vcc		( 32'd3300 ), // 3.3V power
  .Vpp_W_DQ2	( spi1_data2_io )
//.RESET2	( spi1_reset_o )        // We don't have a reset !!!
);
//===================  TASKS ========================================//
///////////////////////////////////////
//  Wishbone Write                  //
/////////////////////////////////////
task wbm_i_write;
	input [31:0] addr;
	input  [3:0] wsel;
	input [31:0] data;
	input	  cyc_end;
begin
  @ (negedge wb_clk);
  wbm_i_cpu0_adr_o <= addr;
  wbm_i_cpu0_sel_o <= wsel;
  wbm_i_cpu0_we_o  <= 1'b1;
  wbm_i_cpu0_stb_o <= 1'b1;
  wbm_i_cpu0_cyc_o <= 1'b1;
  wbm_i_cpu0_dat_o <= data;
  wbm_i_cpu0_cti_o <= classic;
  wbm_i_cpu0_bte_o <= linear;
  @ (posedge ( wbm_i_cpu0_ack_i | wbm_i_cpu0_err_i | wbm_i_cpu0_rty_i));
  $display("WBM IBus Write  \t Addr:0x%H \t Data:0x%H \t Mask:0x%H", 
  	wbm_i_cpu0_adr_o, wbm_i_cpu0_dat_o, wbm_i_cpu0_sel_o);
  @ (posedge wb_clk); #1
  wbm_i_cpu0_adr_o <= 'hX;
  wbm_i_cpu0_sel_o <= 'hx;
  wbm_i_cpu0_dat_o <= 'hX;
  wbm_i_cpu0_stb_o <= cyc_end;
  wbm_i_cpu0_cyc_o <= cyc_end;
  wbm_i_cpu0_we_o  <= 1'bX;
end
endtask
//-------------------------------------------------------------------//
///////////////////////////////////////
//  Wishbone Read                   //
/////////////////////////////////////
task wbm_i_read;
	input  [31:0] addr;
	input  [3:0]  wsel;
	output [31:0] data;
	input  [2:0] cti;
	input  [1:0] bte;
	input	   cyc_end;
begin
  @ (negedge wb_clk);
  wbm_i_cpu0_adr_o <= addr;
  wbm_i_cpu0_sel_o <= wsel;
  wbm_i_cpu0_we_o  <= 1'b0;
  wbm_i_cpu0_stb_o <= 1'b1;
  wbm_i_cpu0_cyc_o <= 1'b1;
  wbm_i_cpu0_cti_o <= cti;
  wbm_i_cpu0_bte_o <= bte;
  @ (posedge wb_clk);
  while (wbm_i_cpu0_ack_i == 1'b0) begin
	@(posedge wb_clk);
  end
  data <= wbm_i_cpu0_dat_i;
  $display("WBM IBus Read  \t Addr:0x%H \t Data:0x%H",
  	wbm_i_cpu0_adr_o, wbm_i_cpu0_dat_i);
  // @ (negedge wbs0_clk);
  wbm_i_cpu0_stb_o <= cyc_end;
  wbm_i_cpu0_cyc_o <= cyc_end;
  wbm_i_cpu0_we_o  <= 1'bX;
end
endtask
//-------------------------------------------------------------------//
task wbm_d_write;
	input [31:0] addr;
	input  [3:0] wsel;
	input [31:0] data;
	input     cyc_end;
begin
  @ (negedge wb_clk);
  wbm_d_cpu0_adr_o <= addr;
  wbm_d_cpu0_sel_o <= wsel;
  wbm_d_cpu0_we_o  <= 1'b1;
  wbm_d_cpu0_stb_o <= 1'b1;
  wbm_d_cpu0_cyc_o <= 1'b1;
  wbm_d_cpu0_dat_o <= data;
  wbm_d_cpu0_cti_o <= classic;
  wbm_d_cpu0_bte_o <= linear;
  @ (posedge ( wbm_d_cpu0_ack_i | wbm_d_cpu0_err_i | wbm_d_cpu0_rty_i));
	$display("WBM DBus Write  \t Addr:0x%H \t Data:0x%H \t Mask:0x%H",
		wbm_d_cpu0_adr_o, wbm_d_cpu0_dat_o, wbm_d_cpu0_sel_o);
  @ (posedge wb_clk); #1
  wbm_d_cpu0_adr_o <= 'hX;
  wbm_d_cpu0_sel_o <= 'hx;
  wbm_d_cpu0_dat_o <= 'hX;
  wbm_d_cpu0_stb_o <= cyc_end;
  wbm_d_cpu0_cyc_o <= cyc_end;
  wbm_d_cpu0_we_o  <= 1'bX;
end
endtask
//-------------------------------------------------------------------//
task wbm_d_read;
	input  [31:0] addr;
	input  [3:0]  wsel;
	output [31:0] data;
	input  [2:0] cti;
	input  [1:0] bte;
	input      cyc_end;
begin
  @ (negedge wb_clk);
  wbm_d_cpu0_adr_o <= addr;
  wbm_d_cpu0_sel_o <= wsel;
  wbm_d_cpu0_we_o  <= 1'b0;
  wbm_d_cpu0_stb_o <= 1'b1;
  wbm_d_cpu0_cyc_o <= 1'b1;
  wbm_d_cpu0_cti_o <= cti;
  wbm_d_cpu0_bte_o <= bte;
  @ (posedge wb_clk);
  while (wbm_d_cpu0_ack_i == 1'b0) begin
	@(posedge wb_clk);
  end
  data <= wbm_d_cpu0_dat_i;
  $display("WBM DBus Read  \t Addr:0x%H \t Data:0x%H",
	wbm_d_cpu0_adr_o, wbm_d_cpu0_dat_i);
  // @ (negedge  wbs0_clk);
  wbm_d_cpu0_stb_o <= cyc_end;
  wbm_d_cpu0_cyc_o <= cyc_end;
  wbm_d_cpu0_we_o <= 1'bX;
end
endtask
//===================  SAVING TO THE FILE ===========================//
initial begin
  $dumpfile( "tb_quad_spi.vcd" );
  $dumpvars;
end
//=================  Clocks ==========================================//
always #5000.0 sys_clk = ~sys_clk; // 100 MHz
//============= WBS0 Tasks =============//
reg [31:0] wbm_readout_data;  
initial begin
	@ (negedge wb_rst);
	$display("... Reset reached");
	$display("... 3 Single classic reads ...");
	@ (posedge wb_clk);
	wbm_i_read(32'h1000_0100,	4'hF,	wbm_readout_data, inc, linear, 1'b1);
	// @ (posedge wb_clk);
	wbm_i_read(32'h1000_0104,	4'hF,	wbm_readout_data, inc, linear, 1'b1);
	// @ (posedge wb_clk);
	wbm_i_read(32'h1000_0108,	4'hF,	wbm_readout_data, inc, linear, 1'b1);
	// @ (posedge wb_clk);
	wbm_i_read(32'h1000_010C,       4'hF,   wbm_readout_data, eob, linear, 1'b0);
	@ (posedge wb_clk);
	wbm_i_read(32'h1000_0110,       4'hF,   wbm_readout_data, classic, linear, 1'b0);
	#1000000
	#100000 $finish;
end
initial begin
	@ (negedge wb_rst);

	@ (posedge wb_clk);
	wbm_d_write(32'hb000_0035,	4'hF,   32'h0000_0000, 1'b0);

	@ (posedge wb_clk);
	wbm_d_read(32'h1000_0100,       4'hF,   wbm_readout_data, classic, linear, 1'b0);
	@ (posedge wb_clk);
	wbm_d_read(32'h1000_0104,       4'hF,   wbm_readout_data, classic, linear, 1'b0);
	@ (posedge wb_clk);
	wbm_d_read(32'h1000_0108,       4'hF,   wbm_readout_data, classic, linear, 1'b0);
end
initial begin
end
endmodule
