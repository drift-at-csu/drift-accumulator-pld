// synopsys translate_off
// `include "timescale.v"
// synopsys translate_on
//
//--------------------------------------------------------------------------//
// If the last transfer was zero, then CS stays low between bytes transfered
// the top level controller should not start data transfer if spi_busy_o is
// high. spi_mode_i can be changed only after last transfer.
//=> spi_mode_i == 01
// data_clk_i:		_^_^_^_^_^_^_^_^_^_^_^_^_^_^_^_^_^_^_^_^_^_^_^_^_^_^_^
// last_transfer_i:	________________________^^__________________^^________
//
// input_data_valid_i:	__^^____________________^^____________________________
// input_data_i:	--[]--------------------[]----------------------------
// 
//
// spi_busy_o		___^^^^^^^^^^^^^^^^______^^^^^^^^^^^^^^^^____^^^^^^^^^
// spi_data0_io:	---D0D1D2D3D4D5D6D7------D0D1D2D3D4D5D6D7-------------
// spi_clk_o:		____^_^_^_^_^_^_^_^_______^_^_^_^_^_^_^_^_____^_^_^_^_
// spi_cs_o:		^^^______________________________________^^^^_________
//
// output_data_valid_i: __^^________________________________________^^________
// output_data_o:	------------------[]----------------------------------
// output_data_ready_o: __________________^^__________________________________

module quad_spi_phy (
	// Custom Control Lines
	input wire [7:0]	spi_wait_states_i,
					// How many Wait states to insert
					// After transfer
	input wire [1:0]	spi_mode_i,
					// The Mode in which SPI exists:
					// 00: Undefined
					// 01: Single SPI mode
					// 10: Dual SPI mode
					// 11: Quad SPI mode
	input wire 		data_clk_i,
	input wire		data_rst_i,

	input wire		last_transfer_i,

	input wire [7:0]	input_data_i,
	input wire		input_data_valid_i,

	output reg [7:0]	output_data_o,
	input wire		output_data_valid_i,

	output reg		data_ready_o,

	output wire		spi_busy_o,

	// SPI Physical Interface
	output wire	spi_clk_o,
	output wire	spi_cs_o,
	inout wire	spi_data0_io,
	inout wire	spi_data1_io,
	inout wire	spi_data2_io,
	inout wire	spi_data3_io,
	output wire	spi_rst_o
);
  // Always assign reset to 1?
  assign spi_rst_o = 1'b1;

  // internal registers
  reg spi_busy_reg = 1'b0;
  reg spi_cs_reg = 1'b1;
  reg [7:0] bit_counter_reg = 8'h00;
  reg [7:0] wait_states_reg = 8'h00;
  reg [1:0] spi_mode_reg = 2'b01; // We expect Single SPI mode
  reg last_transfer_reg = 1'b1;   // Single transfers mode
  reg [7:0] data_transfer_reg = 8'h00;
  reg spi_write_en_reg = 1'b0;


  wire spi_data0_o;
  wire spi_data1_o;
  wire spi_data2_o;
  wire spi_data3_o;
  // Assign Outputs, based on spi_mode_reg and bit_counter_reg from data_transfer_reg
  // MSB first
  assign spi_data0_o = 	(spi_mode_reg == 2'b01) ? data_transfer_reg[bit_counter_reg << 0] : // Single
  			(spi_mode_reg == 2'b10) ? data_transfer_reg[bit_counter_reg << 1] : // Double
						  data_transfer_reg[bit_counter_reg << 2];  // Quad
  assign spi_data1_o = 	(spi_mode_reg == 2'b01) ? 1'b0 : // Single
  			(spi_mode_reg == 2'b10) ? data_transfer_reg[bit_counter_reg << 1+1]: // Double
						  data_transfer_reg[bit_counter_reg << 2+1]; // Quad
  assign spi_data2_o =  (spi_mode_reg == 2'b01) ? 1'b1 : // Single
			(spi_mode_reg == 2'b10) ? 1'b1 : // Double
						  data_transfer_reg[bit_counter_reg << 2+2]; // Quad
  assign spi_data3_o =  (spi_mode_reg == 2'b01) ? 1'b1 : // Single
  			(spi_mode_reg == 2'b10) ? 1'b1 : // Double
						  data_transfer_reg[bit_counter_reg << 2+3]; // Quad


  // Direction defined by the spi_mode_reg and  spi_write_en_reg
  // DATA0: 	MOSI in Single mode (spi_mode_reg == 2'b01) (always write)
  // 		In Dual or Quad mode it's read/write
  // 		
  assign spi_data0_io = ((spi_mode_reg == 2'b01) | (spi_write_en_reg)) ? spi_data0_o : 1'bZ;
  // DATA1:	MISO in Single mode (spi_mode_reg == 2'b01) (always read)
  // 		In Dual or Quad mode it's read/write
  assign spi_data1_io = ((spi_mode_reg == 2'b01)|  (~spi_write_en_reg)) ? 1'bZ : spi_data1_o;
  // DATA2 and DATA3: 
  // 		Should both be 1 in Single and Dual Mode
  // 		In Quad Mode it's read/write
  assign spi_data2_io = ((spi_mode_reg == 2'b11)&(~spi_write_en_reg)) ? 1'bZ : spi_data2_o;
  assign spi_data3_io = ((spi_mode_reg == 2'b11)&(~spi_write_en_reg)) ? 1'bZ : spi_data3_o;


  assign spi_cs_o   = spi_cs_reg;
  assign spi_clk_o  = spi_busy_reg ? (~data_clk_i) : 1'b0;
  assign spi_busy_o = spi_busy_reg;

  always @(posedge data_clk_i) begin
	
  end
  (* FSM_ENCODING="SEQUENTIAL", SAFE_IMPLEMENTATION="NO" *) reg [1:0] spi_fsm_state = 2'b00;
  always@(posedge data_clk_i)
  if (data_rst_i) begin
  	spi_fsm_state <= 2'b00; // Idle state
	spi_busy_reg <= 1'b0;
	spi_cs_reg <= 1'b1;
	bit_counter_reg <= 8'h00;
	data_ready_o <= 1'b0;
  end else
  case (spi_fsm_state)
  2'b00 : begin // Idle
  	// Let's wait until input_data_valid_i or output_data_valid_i
	if ( input_data_valid_i || output_data_valid_i ) begin
		spi_fsm_state <= 2'b01; // Transfer data
		spi_busy_reg <= 1'b1;
		spi_cs_reg <= 1'b0;

		bit_counter_reg <= 	(spi_mode_i == 2'b01) ? 8'h07 : // Single
					(spi_mode_i == 2'b10) ? 8'h03 : // Double
								8'h01;  // Quad
		wait_states_reg <= spi_wait_states_i;
		spi_mode_reg <= spi_mode_i;
		data_transfer_reg <= input_data_i;
		last_transfer_reg <= last_transfer_i;
		// We also need to change a direction here
		spi_write_en_reg <= input_data_valid_i ? 1'b1 : 1'b0;
	end else begin
		spi_write_en_reg <= 1'b0;
	end
	data_ready_o <= 1'b0;
  end
  2'b01 : begin // Transfer data
	if (bit_counter_reg == 8'h00 ) begin	
		if (wait_states_reg == 8'h00) begin
			spi_fsm_state <= 2'b00; // Idle
			spi_cs_reg <= last_transfer_reg ? 1'b1 : 1'b0;
			spi_busy_reg <= 1'b0;
			data_ready_o <= 1'b1;
		end else begin
			spi_fsm_state <= 2'b10; // Wait States
			wait_states_reg <= wait_states_reg - 8'h01;
		end
	end else begin
		bit_counter_reg = bit_counter_reg - 8'h01;
	end
	// 01: Single SPI mode
        // 10: Dual SPI mode
        // 11: Quad SPI mode
	case ( spi_mode_reg )
		2'b00: output_data_o <= 8'hDE;
		2'b01: output_data_o <= { output_data_o[6:0], 
					spi_data1_io };
		2'b10: output_data_o <= { output_data_o[5:0], 
					spi_data1_io, spi_data0_io};
		2'b11: output_data_o <= { output_data_o[3:0],
					spi_data3_io, spi_data2_io, 
					spi_data1_io, spi_data0_io};
	endcase
  end
  2'b10 : begin // Wait states
  	if (wait_states_reg == 8'h00) begin
		spi_fsm_state <= 2'b00; // Idle
		spi_busy_reg <= 1'b0;
		data_ready_o <= 1'b1;
	end else begin
		wait_states_reg <= wait_states_reg - 8'h01;
	end
  end
  2'b11 : begin // Undefined
  	spi_fsm_state <= 2'b00;
  end
  endcase


endmodule

