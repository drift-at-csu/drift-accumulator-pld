/*
 * Top Module for QDRII controller (Wishbone Bus)
 */
module qdrii_wb #(
	parameter clock_setup_time = 13'h1400,
	parameter iodelay_group_name = "iodelay_group_name",
	parameter data0_delay = 0,
	parameter data1_delay = 0,
	parameter data2_delay = 0,
	parameter data3_delay = 0,
	parameter data4_delay = 0,
	parameter data5_delay = 0,
	parameter data6_delay = 0,
	parameter data7_delay = 0,
	parameter data8_delay = 0,
	parameter echo_clk_p_delay = 0,
	parameter echo_clk_n_delay = 0

)(	// Wishbone Slave interfaces
	// Slave 0
	input wire [31:0]	wbs0_adr_i,
	input wire [1:0]	wbs0_bte_i,
	input wire [2:0]	wbs0_cti_i,
	input wire		wbs0_cyc_i,
	input wire [31:0]	wbs0_dat_i,
	input wire [3:0]	wbs0_sel_i,
	input wire		wbs0_stb_i,
	input wire		wbs0_we_i,
	input wire		wbs0_cab_i,
	output wire		wbs0_ack_o,
	output wire		wbs0_err_o,
	output wire		wbs0_rty_o,
	output wire [31:0]	wbs0_dat_o,

	input wire		wbs0_clk_i,
	input wire		wbs0_rst_i,

	// Slave 1
	input wire [31:0]	wbs1_adr_i,
	input wire [1:0]	wbs1_bte_i,
	input wire [2:0]	wbs1_cti_i,
	input wire		wbs1_cyc_i,
	input wire [31:0]	wbs1_dat_i,
	input wire [3:0]	wbs1_sel_i,
	input wire		wbs1_stb_i,
	input wire		wbs1_we_i,
	input wire		wbs1_cab_i,
	output wire		wbs1_ack_o,
	output wire		wbs1_err_o,
	output wire		wbs1_rty_o,
	output wire [31:0]	wbs1_dat_o,

	input wire		wbs1_clk_i,
	input wire		wbs1_rst_i,

	// QDRII Interface Note: Clock Generation is within the
	// module now
	output wire		qdr_rps_o,
	output wire		qdr_wps_o,
	output wire		qdr_bws_o,
	output wire [21:0]	qdr_addr_o, 
	output wire [8:0]	qdr_data_o,
	input wire  [8:0]	qdr_data_i,

	output wire	qdr_read_clk_p_o,
	output wire	qdr_read_clk_n_o,

	output wire	qdr_write_clk_p_o,
	output wire	qdr_write_clk_n_o,

	input wire	qdr_echo_clk_p_i,
	input wire	qdr_echo_clk_n_i,

	// This is the Fast clock input for clock generator, which generates
	// all the other clocks and resets. Should be connected to the MMC
	// output or a global clock signal
	input wire	qdr_clk_i, // This is the Fast clock input
	// This should be connected to the inverse of MMC LOCKED signal
	input wire	qdr_rst_i,

	// Power Management
	output wire 	pow_doff_o

	// Test Signals
);
////////////////////////////////
//  Clocks Generator         //
//////////////////////////////
// These are the clocks, used with oserdeses
wire io_fast_clk;
wire io_fast_clk_n;
wire io_slow_clk;
wire io_slow_clk_n;
wire io_slow_rst;

qdrii_wb_clkgen #(
	.iodelay_group_name(iodelay_group_name)
)qdr_clkgen0 (
  .global_reset_i	( wbs0_rst_i ),
  // Iserdes/Oserdes Chain
  .io_clk_o		( io_fast_clk ),
  .io_clk_n_o		( io_fast_clk_n ),
  .io_clkdiv_o		( io_slow_clk ),
  .io_clkdiv_n_o	( io_slow_clk_n ),
  .io_clkdiv_rst_o	( io_slow_rst ),

  .wr_clk_p_o		( qdr_write_clk_p_o ),       // KP,  250 MHz
  .wr_clk_n_o		( qdr_write_clk_n_o ),       // KN,  250 MHz

  .rd_clk_p_o		( qdr_read_clk_p_o ),       // RP,  250 MHz
  .rd_clk_n_o		( qdr_read_clk_n_o ),

  .clocks_good_o 	( ),
  // Input 250Mhz Freq and pow-on reset
  .clk_i ( qdr_clk_i ),
  .rst_i ( qdr_rst_i )
);
//===================================================================//

// For Power Management On Power-on: all the power pins are at 0
// Then pow_s5 should go up, then pow_s3, then stable clock should be provided
// on K clocks, then pow_doff should go high, after 20usecs the normal
// operations are allowed
// pow_s5 and pow_s3 are treated separately from this module, end they
// expected to be already turned on. If they are not, then indeed the QDR
// chi itself will not work
reg pow_doff_reg = 1'b0;
always @ (posedge io_slow_clk) begin
	if (io_slow_rst)
		pow_doff_reg <= 1'b0;
	else
		pow_doff_reg <= 1'b1;
end
assign pow_doff_o = pow_doff_reg;

reg [12:0] after_qdr_reset_counter = 13'h000;
reg mem_is_good = 1'b0; // <--- This indicates when the memory is fully setup
always @ (posedge io_slow_clk) begin
	if (io_slow_rst) begin
		after_qdr_reset_counter <= 13'h000;
		mem_is_good <= 1'b0;
	end else begin
		if (after_qdr_reset_counter == clock_setup_time) begin // Should be 12'h1400
			mem_is_good <= 1'b1;
		end else begin
			after_qdr_reset_counter <= after_qdr_reset_counter + 1'b1;
			mem_is_good <= 1'b0;
		end
	end
end
//-------------------------------------------------------------------//
////////////////////////////////////////
//   Read FIFO, Port0                //
//////////////////////////////////////
// Read Chain FIFOs, Port0 (controller side)
wire [22:2]	fifo_cntr_rd_port0_addr;
wire  		cntr_fifo_rd_port0_addr_pull;
wire		fifo_cntr_rd_port0_addr_empty;
// Read Chain FIFOs, Port 0 (Wishbone Side)
wire [22:2]	wbs0_fifo_rd_addr;
wire		wbs0_fifo_rd_full;
wire		wbs0_fifo_rd_push;
// Controller Side
wire [31:0]     cntr_fifo_rd_port0_data;
wire            cntr_fifo_rd_port0_data_push;
wire            fifo_cntr_rd_port0_data_full;
// Wishbone side
wire [31:0]     wbs0_fifo_rd_data;
wire            wbs0_fifo_rd_pull;
wire            wbs0_fifo_rd_empty;

qdrii_wb_read_fifo read_fifo_port0_inst (
  //---> Wishbone Side
  .data_o       ( wbs0_fifo_rd_data ),
  .data_empty_o ( wbs0_fifo_rd_empty ),
  .data_pull_i  ( wbs0_fifo_rd_pull ),
  .addr_i       ( wbs0_fifo_rd_addr ),
  .addr_full_o  ( wbs0_fifo_rd_full ),
  .addr_push_i  ( wbs0_fifo_rd_push ),

  .wb_rst_i 	( wbs0_rst_i ),
  .wb_clk_i 	( wbs0_clk_i ),
  //---> QDR Side
  // Reading (iserdeses)
  .data_i       ( cntr_fifo_rd_port0_data ),
  .data_full_o  ( fifo_cntr_rd_port0_data_full ),
  .data_push_i  ( cntr_fifo_rd_port0_data_push ),
  .data_rst_i 	( io_slow_rst ),
  .data_clk_i 	( io_slow_clk_n ), // was n
  // Writing (oserdeses)
  .addr_o       ( fifo_cntr_rd_port0_addr ),
  .addr_empty_o ( fifo_cntr_rd_port0_addr_empty ),
  .addr_pull_i  ( cntr_fifo_rd_port0_addr_pull ), 
  .addr_rst_i 	( io_slow_rst ),
  .addr_clk_i 	( io_slow_clk_n ) // was p
);
//-------------------------------------------------------------------//
////////////////////////////////////////
//   Read FIFO, Port1        //
//////////////////////////////////////
// Read Chain FIFOs, Port1 (controller side)
wire [22:2]	fifo_cntr_rd_port1_addr;
wire		cntr_fifo_rd_port1_addr_pull;
wire		fifo_cntr_rd_port1_addr_empty;
// Read Chain FIFOs, Port 1 (Wishbone Side)
wire [22:2]	wbs1_fifo_rd_addr;
wire		wbs1_fifo_rd_full;
wire		wbs1_fifo_rd_push;
// Controller Side Data
wire [31:0]	cntr_fifo_rd_port1_data;
wire		cntr_fifo_rd_port1_data_push;
wire		fifo_cntr_rd_port1_data_full;
// Wishbone side Data
wire [31:0]	wbs1_fifo_rd_data;
wire		wbs1_fifo_rd_pull;
wire		wbs1_fifo_rd_empty;

qdrii_wb_read_fifo read_fifo_port1_inst (
  .data_o	( wbs1_fifo_rd_data ),
  .data_empty_o	( wbs1_fifo_rd_empty ),
  .data_pull_i	( wbs1_fifo_rd_pull ),
  .addr_i	( wbs1_fifo_rd_addr ),
  .addr_full_o	( wbs1_fifo_rd_full ),
  .addr_push_i	( wbs1_fifo_rd_push ),
  .wb_rst_i 	( wbs1_rst_i ),
  .wb_clk_i 	( wbs1_clk_i ),
  // Read Part of read FIFO (iserdeses)
  .data_i 	( cntr_fifo_rd_port1_data ),
  .data_full_o	( fifo_cntr_rd_port1_data_full ),
  .data_push_i	( cntr_fifo_rd_port1_data_push ),
  .data_rst_i 	( io_slow_rst ),
  .data_clk_i 	( io_slow_clk_n ), // was n
  // Write part of read FIFO ( oserdeses )
  .addr_o 	( fifo_cntr_rd_port1_addr ),
  .addr_empty_o	( fifo_cntr_rd_port1_addr_empty ),
  .addr_pull_i	( cntr_fifo_rd_port1_addr_pull ),
  .addr_rst_i 	( io_slow_rst ),
  .addr_clk_i 	( io_slow_clk_n ) // was p
);
//-------------------------------------------------------------------//
////////////////////////////////
//  Write FIFO, Port 0       //
//////////////////////////////
// Controller side
wire [22:2]	fifo_cntr_wr_port0_addr;
wire [31:0]  	fifo_cntr_wr_port0_data;
wire [3:0]	fifo_cntr_wr_port0_bsel;
wire		cntr_fifo_wr_port0_pull;
wire		fifo_cntr_wr_port0_empty;
// Wishbone Side
wire [22:2]	wbs0_fifo_wr_addr;
wire [31:0]	wbs0_fifo_wr_data;
wire [3:0]	wbs0_fifo_wr_bsel;
wire		wbs0_fifo_wr_push;
wire		wbs0_fifo_wr_full;
qdrii_wb_write_fifo write_fifo_port0_inst (
  .data_i ( wbs0_fifo_wr_data ),
  .bsel_i ( wbs0_fifo_wr_bsel ),
  .addr_i ( wbs0_fifo_wr_addr ),
  .full_o ( wbs0_fifo_wr_full ),
  .push_i ( wbs0_fifo_wr_push ),
  .wb_rst_i( wbs0_rst_i ), // It's not used here
  .wb_clk_i( wbs0_clk_i ),

  .data_o  ( fifo_cntr_wr_port0_data ),
  .bsel_o  ( fifo_cntr_wr_port0_bsel ),
  .addr_o  ( fifo_cntr_wr_port0_addr ),
  .empty_o ( fifo_cntr_wr_port0_empty ),
  .pull_i  ( cntr_fifo_wr_port0_pull ),

  .qdr_rst_i ( io_slow_rst ),
  .qdr_clk_i ( io_slow_clk_n )
);
////////////////////////////////
//  Write FIFO, Port 1       //
//////////////////////////////
// Controller side
wire [22:2]	fifo_cntr_wr_port1_addr;
wire [31:0]	fifo_cntr_wr_port1_data;
wire [3:0]	fifo_cntr_wr_port1_bsel;
wire		cntr_fifo_wr_port1_pull;
wire		fifo_cntr_wr_port1_empty;
// Wishbone Side
wire [22:2]	wbs1_fifo_wr_addr;
wire [31:0]	wbs1_fifo_wr_data;
wire [3:0]	wbs1_fifo_wr_bsel;
wire		wbs1_fifo_wr_push;
wire		wbs1_fifo_wr_full;

qdrii_wb_write_fifo write_fifo_port1_inst (
  .data_i ( wbs1_fifo_wr_data ),
  .bsel_i ( wbs1_fifo_wr_bsel ),
  .addr_i ( wbs1_fifo_wr_addr ),
  .full_o ( wbs1_fifo_wr_full ),
  .push_i ( wbs1_fifo_wr_push ),
  .wb_rst_i( wbs1_rst_i ), // It's not used here
  .wb_clk_i( wbs1_clk_i ),

  .data_o  ( fifo_cntr_wr_port1_data ),
  .bsel_o  ( fifo_cntr_wr_port1_bsel ),
  .addr_o  ( fifo_cntr_wr_port1_addr ),
  .empty_o ( fifo_cntr_wr_port1_empty ),
  .pull_i  ( cntr_fifo_wr_port1_pull ),
  .qdr_rst_i ( io_slow_rst ),
  .qdr_clk_i ( io_slow_clk_n )
);
//-------------------------------------------------------------------//
///////////////////////////////////////////////
//  QDRII Memory controller                 //
//  it is fully in slow qdr clock domain   //
////////////////////////////////////////////
// physical interface wires
wire [3:0] 
	cntr_phys_addr00, cntr_phys_addr01, cntr_phys_addr02,
	cntr_phys_addr03, cntr_phys_addr04, cntr_phys_addr05,
	cntr_phys_addr06, cntr_phys_addr07, cntr_phys_addr08,
	cntr_phys_addr09, cntr_phys_addr10, cntr_phys_addr11,
	cntr_phys_addr12, cntr_phys_addr13, cntr_phys_addr14,
	cntr_phys_addr15, cntr_phys_addr16, cntr_phys_addr17,
	cntr_phys_addr18, cntr_phys_addr19, cntr_phys_addr20,
	cntr_phys_addr21;
wire [3:0] 
	cntr_phys_dout00, cntr_phys_dout01, cntr_phys_dout02,
	cntr_phys_dout03, cntr_phys_dout04, cntr_phys_dout05,
	cntr_phys_dout06, cntr_phys_dout07, cntr_phys_dout08;
wire [3:0]
	cntr_phys_rps, cntr_phys_wps, cntr_phys_bws;

wire [3:0]
	phys_cntr_dinp00, phys_cntr_dinp01, phys_cntr_dinp02,
	phys_cntr_dinp03, phys_cntr_dinp04, phys_cntr_dinp05,
	phys_cntr_dinp06, phys_cntr_dinp07, phys_cntr_dinp08;

wire [3:0]
	phys_cntr_echo_clk_p, phys_cntr_echo_clk_n;

wire [3:0]
	cntr_phys_address_mask, cntr_phys_data_o_mask, cntr_phys_bws_mask;

wire cntr_phys_iserdes_en;
wire cntr_phys_iserdes_rst;
wire cntr_phys_iserdes_bitslip;
wire cntr_phys_oserdes_en;
wire cntr_phys_oserdes_rst;

qdrii_wb_controller #( 
  .CLOCKS_RATIO ( 4 ) 
) qdr_controller_inst (
  //---> FIFOs Interface
  // READ FIFOs, Port 0
  .rd0_data_o 	   ( cntr_fifo_rd_port0_data ),
  .rd0_data_push_o ( cntr_fifo_rd_port0_data_push ),
  .rd0_data_full_i ( fifo_cntr_rd_port0_data_full ),
  .rd0_addr_i 	   ( fifo_cntr_rd_port0_addr ),
  .rd0_addr_pull_o ( cntr_fifo_rd_port0_addr_pull ),
  .rd0_addr_empty_i( fifo_cntr_rd_port0_addr_empty ),
  // WRITE FIFO, Port 0
  .wr0_addr_i ( fifo_cntr_wr_port0_addr ),
  .wr0_bsel_i ( fifo_cntr_wr_port0_bsel ),
  .wr0_data_i ( fifo_cntr_wr_port0_data ),
  .wr0_pull_o ( cntr_fifo_wr_port0_pull ),
  .wr0_empty_i( fifo_cntr_wr_port0_empty ),
  // READ FIFOs, Port 1
  .rd1_data_o	   ( cntr_fifo_rd_port1_data ),
  .rd1_data_push_o ( cntr_fifo_rd_port1_data_push ),
  .rd1_data_full_i ( fifo_cntr_rd_port1_data_full ),

  .rd1_addr_i	   ( fifo_cntr_rd_port1_addr ),
  .rd1_addr_pull_o ( cntr_fifo_rd_port1_addr_pull ),
  .rd1_addr_empty_i( fifo_cntr_rd_port1_addr_empty ),
  // WRITE FIFO, Port 1
  .wr1_addr_i ( fifo_cntr_wr_port1_addr ),
  .wr1_bsel_i ( fifo_cntr_wr_port1_bsel ),
  .wr1_data_i ( fifo_cntr_wr_port1_data ),
  .wr1_pull_o ( cntr_fifo_wr_port1_pull ),
  .wr1_empty_i( fifo_cntr_wr_port1_empty ),
  //---> Physical Interface
  .addr00_o ( cntr_phys_addr00 ),
  .addr01_o ( cntr_phys_addr01 ),
  .addr02_o ( cntr_phys_addr02 ),
  .addr03_o ( cntr_phys_addr03 ),
  .addr04_o ( cntr_phys_addr04 ),
  .addr05_o ( cntr_phys_addr05 ),
  .addr06_o ( cntr_phys_addr06 ),
  .addr07_o ( cntr_phys_addr07 ),
  .addr08_o ( cntr_phys_addr08 ),
  .addr09_o ( cntr_phys_addr09 ),
  .addr10_o ( cntr_phys_addr10 ),
  .addr11_o ( cntr_phys_addr11 ),
  .addr12_o ( cntr_phys_addr12 ),
  .addr13_o ( cntr_phys_addr13 ),
  .addr14_o ( cntr_phys_addr14 ),
  .addr15_o ( cntr_phys_addr15 ),
  .addr16_o ( cntr_phys_addr16 ),
  .addr17_o ( cntr_phys_addr17 ),
  .addr18_o ( cntr_phys_addr18 ),
  .addr19_o ( cntr_phys_addr19 ),
  .addr20_o ( cntr_phys_addr20 ),
  .addr21_o ( cntr_phys_addr21 ),

  .dout00_o ( cntr_phys_dout00 ),
  .dout01_o ( cntr_phys_dout01 ),
  .dout02_o ( cntr_phys_dout02 ),
  .dout03_o ( cntr_phys_dout03 ),
  .dout04_o ( cntr_phys_dout04 ),
  .dout05_o ( cntr_phys_dout05 ),
  .dout06_o ( cntr_phys_dout06 ),
  .dout07_o ( cntr_phys_dout07 ),
  .dout08_o ( cntr_phys_dout08 ),

  .rps_o ( cntr_phys_rps ),
  .wps_o ( cntr_phys_wps ),
  .bws_o ( cntr_phys_bws ),

  .dinp00_i ( phys_cntr_dinp00 ),
  .dinp01_i ( phys_cntr_dinp01 ),
  .dinp02_i ( phys_cntr_dinp02 ),
  .dinp03_i ( phys_cntr_dinp03 ),
  .dinp04_i ( phys_cntr_dinp04 ),
  .dinp05_i ( phys_cntr_dinp05 ),
  .dinp06_i ( phys_cntr_dinp06 ),
  .dinp07_i ( phys_cntr_dinp07 ),
  .dinp08_i ( phys_cntr_dinp08 ),

  .echo_clk_p_i( phys_cntr_echo_clk_p ),
  .echo_clk_n_i( phys_cntr_echo_clk_n ),

  .iserdes_enable_o ( cntr_phys_iserdes_en ),
  .iserdes_reset_o  ( cntr_phys_iserdes_rst ),
  .iserdes_bitslip_o( cntr_phys_iserdes_bitslip),

  .oserdes_enable_o( cntr_phys_oserdes_en ),
  .oserdes_reset_o( cntr_phys_oserdes_rst),

  .address_mask_o( cntr_phys_address_mask ),
  .data_o_mask_o ( cntr_phys_data_o_mask  ),
  .bws_mask_o    ( cntr_phys_bws_mask ),

  //--------- Clocks and reset -------//
  .rst_i  ( io_slow_rst ),
  .clk_i  ( io_slow_clk ),
  .clk_n_i( io_slow_clk_n )
);
//-------------------------------------------------------------------//
////////////////////////////////////////////////////
//  QDR Physical Interface (oserdes and iserdes) //
//////////////////////////////////////////////////
qdrii_wb_phys #(
	.CLOCKS_RATIO ( 4 ),
	.iodelay_group_name ( iodelay_group_name  ),
	.data0_delay ( data0_delay ),
	.data1_delay ( data1_delay ),
	.data2_delay ( data2_delay ),
	.data3_delay ( data3_delay ),
	.data4_delay ( data4_delay ),
	.data5_delay ( data5_delay ),
	.data6_delay ( data6_delay ),
	.data7_delay ( data7_delay ),
	.data8_delay ( data8_delay ),
	.echo_clk_p_delay ( echo_clk_p_delay ),
	.echo_clk_n_delay ( echo_clk_n_delay )
) qdr_phys_inst (
  //---> Controller Interface
  .addr00_i( cntr_phys_addr00 ),
  .addr01_i( cntr_phys_addr01 ),
  .addr02_i( cntr_phys_addr02 ),
  .addr03_i( cntr_phys_addr03 ),
  .addr04_i( cntr_phys_addr04 ),
  .addr05_i( cntr_phys_addr05 ),
  .addr06_i( cntr_phys_addr06 ),
  .addr07_i( cntr_phys_addr07 ),
  .addr08_i( cntr_phys_addr08 ),
  .addr09_i( cntr_phys_addr09 ),
  .addr10_i( cntr_phys_addr10 ),
  .addr11_i( cntr_phys_addr11 ),
  .addr12_i( cntr_phys_addr12 ),
  .addr13_i( cntr_phys_addr13 ),
  .addr14_i( cntr_phys_addr14 ),
  .addr15_i( cntr_phys_addr15 ),
  .addr16_i( cntr_phys_addr16 ),
  .addr17_i( cntr_phys_addr17 ),
  .addr18_i( cntr_phys_addr18 ),
  .addr19_i( cntr_phys_addr19 ),
  .addr20_i( cntr_phys_addr20 ),
  .addr21_i( cntr_phys_addr21 ),

  .dout00_i( cntr_phys_dout00 ),
  .dout01_i( cntr_phys_dout01 ),
  .dout02_i( cntr_phys_dout02 ),
  .dout03_i( cntr_phys_dout03 ),
  .dout04_i( cntr_phys_dout04 ),
  .dout05_i( cntr_phys_dout05 ),
  .dout06_i( cntr_phys_dout06 ),
  .dout07_i( cntr_phys_dout07 ),
  .dout08_i( cntr_phys_dout08 ),

  .rps_i( cntr_phys_rps ),
  .wps_i( cntr_phys_wps ),
  .bws_i( cntr_phys_bws ),

  .dinp00_o ( phys_cntr_dinp00 ),
  .dinp01_o ( phys_cntr_dinp01 ),
  .dinp02_o ( phys_cntr_dinp02 ),
  .dinp03_o ( phys_cntr_dinp03 ),
  .dinp04_o ( phys_cntr_dinp04 ),
  .dinp05_o ( phys_cntr_dinp05 ),
  .dinp06_o ( phys_cntr_dinp06 ),
  .dinp07_o ( phys_cntr_dinp07 ),
  .dinp08_o ( phys_cntr_dinp08 ),

  .echo_clk_p_o (phys_cntr_echo_clk_p),
  .echo_clk_n_o (phys_cntr_echo_clk_n),

  //---> QDR Interface (connect directly to the pad)
  .qdr2_rps_o ( qdr_rps_o ),
  .qdr2_wps_o ( qdr_wps_o ),
  .qdr2_bws_o ( qdr_bws_o ),
  .qdr2_addr_o( qdr_addr_o ),
  .qdr2_data_o( qdr_data_o ),
  .qdr2_data_i( qdr_data_i ),

  .qdr2_echo_clk_p_i( qdr_echo_clk_p_i ),
  .qdr2_echo_clk_n_i( qdr_echo_clk_n_i ),

  //-------- iserdes controls ------//
  .iserdes_enable_i( cntr_phys_iserdes_en ),
  .iserdes_reset_i ( cntr_phys_iserdes_rst ),
  .iserdes_bitslip_i (cntr_phys_iserdes_bitslip),

  //-------- oserdes controls ------//
  .oserdes_enable_i( cntr_phys_oserdes_en ),
  .oserdes_reset_i( cntr_phys_oserdes_rst ),

  .address_mask_i( cntr_phys_address_mask ),
  .data_o_mask_i ( cntr_phys_data_o_mask  ),
  .bws_mask_i    ( cntr_phys_bws_mask ),

  //---> Clocks and Reset
  .io_divclk_i	( io_slow_clk ),
  .io_clk_i	( io_fast_clk ),
  .io_clk_n_i	( io_fast_clk_n ),
  .io_rst_i	( io_slow_rst )
);
//-------------------------------------------------------------------//
/////////////////////////////////////////////
//   Wishbone Slave Interface, Port 0     //
//   it's fully in wb_clk domain         //
//////////////////////////////////////////
qdrii_wb_wishbone_b3  #(
  .DATA_WIDTH	( 32 ),
  .ADDRESS_WIDTH( 23 )
) wbslave0_inst (
  //---> Wisbone Bus Slave interface
  .wb_adr_i ( wbs0_adr_i[22:0] ),
  .wb_bte_i ( wbs0_bte_i ),
  .wb_cti_i ( wbs0_cti_i ),
  .wb_cyc_i ( wbs0_cyc_i ),
  .wb_dat_i ( wbs0_dat_i ),
  .wb_sel_i ( wbs0_sel_i ),
  .wb_stb_i ( wbs0_stb_i ),
  .wb_we_i  ( wbs0_we_i	),
  .wb_ack_o ( wbs0_ack_o ),
  .wb_err_o ( wbs0_err_o ),
  .wb_rty_o ( wbs0_rty_o ),
  .wb_dat_o ( wbs0_dat_o ),

  .wb_clk_i ( wbs0_clk_i ),
  .wb_rst_i ( wbs0_rst_i ),
  //---> FIFOs Interface 
  .qdr_write_data_o ( wbs0_fifo_wr_data ),
  .qdr_write_bsel_o ( wbs0_fifo_wr_bsel ),
  .qdr_write_addr_o ( wbs0_fifo_wr_addr ),
  .qdr_write_push_o ( wbs0_fifo_wr_push ),
  .qdr_write_full_i ( wbs0_fifo_wr_full ),

  .qdr_read_data_i  ( wbs0_fifo_rd_data  ),
  .qdr_read_pull_o  ( wbs0_fifo_rd_pull  ),
  .qdr_read_empty_i ( wbs0_fifo_rd_empty ),
  .qdr_read_clear_o (  ),

  .qdr_read_addr_o  ( wbs0_fifo_rd_addr ),
  .qdr_read_push_o  ( wbs0_fifo_rd_push ),
  .qdr_read_full_i  ( wbs0_fifo_rd_full ),
  //---> Control Signal, indicating that FIFO is ready
  .qdr_ready_i  (mem_is_good)
);
/////////////////////////////////////////////
//   Wishbone Slave Interface, Port 1     //
//   it's fully in wb_clk domain         //
//////////////////////////////////////////
qdrii_wb_wishbone_b3  #(
  .DATA_WIDTH   ( 32 ),
  .ADDRESS_WIDTH( 23 )
) wbslave1_inst (
  //---> Wisbone Bus Slave interface
  .wb_adr_i ( wbs1_adr_i[22:0] ),
  .wb_bte_i ( wbs1_bte_i ),
  .wb_cti_i ( wbs1_cti_i ),
  .wb_cyc_i ( wbs1_cyc_i ),
  .wb_dat_i ( wbs1_dat_i ),
  .wb_sel_i ( wbs1_sel_i ),
  .wb_stb_i ( wbs1_stb_i ),
  .wb_we_i  ( wbs1_we_i  ),
  .wb_ack_o ( wbs1_ack_o ),
  .wb_err_o ( wbs1_err_o ),
  .wb_rty_o ( wbs1_rty_o ),
  .wb_dat_o ( wbs1_dat_o ),

  .wb_clk_i ( wbs1_clk_i ),
  .wb_rst_i ( wbs1_rst_i ),
  //---> FIFOs Interface
  .qdr_write_data_o ( wbs1_fifo_wr_data ),
  .qdr_write_bsel_o ( wbs1_fifo_wr_bsel ),
  .qdr_write_addr_o ( wbs1_fifo_wr_addr ),
  .qdr_write_push_o ( wbs1_fifo_wr_push ),
  .qdr_write_full_i ( wbs1_fifo_wr_full ),

  .qdr_read_data_i  ( wbs1_fifo_rd_data  ),
  .qdr_read_pull_o  ( wbs1_fifo_rd_pull  ),
  .qdr_read_empty_i ( wbs1_fifo_rd_empty ),
  .qdr_read_clear_o (  ),

  .qdr_read_addr_o  ( wbs1_fifo_rd_addr ),
  .qdr_read_push_o  ( wbs1_fifo_rd_push ),
  .qdr_read_full_i  ( wbs1_fifo_rd_full ),

  //---> Control Signal, indicating that FIFO is ready
  .qdr_ready_i 	(mem_is_good)

);
endmodule
