`timescale 1ps/1ps

module qdrii_wb_clkgen #(
	parameter iodelay_group_name = "memX_delay_group"

)(
  // Global reset is tied to wb_clk_0
  input wire global_reset_i,

  // Clocks for oserdeses and iserdese:
  output wire io_clk_o,		// <--- fast clock 250 MHz
  output wire io_clk_n_o,	//
  output wire io_clkdiv_o,	// <--- slow clock 125 MHz
  output wire io_clkdiv_n_o,	// 
  output wire io_clkdiv_rst_o,	// Synchronous reset for out_clkdiv_o

  //---> Following Output clocks should be connected directly to the pad
  output wire wr_clk_p_o,	// KP,	250 MHz
  output wire wr_clk_n_o,	// KN,	250 MHz

  output wire rd_clk_p_o,	// RP,	250 MHz
  output wire rd_clk_n_o,	// RN,	250 MHz

  //---> Status flag, indicating that output clocks are locked
  output wire clocks_good_o, 	// Return 1 when the output clocks are locked

  input wire  clk_i,	// Main Input clock, 250 MHz
  input wire  rst_i	// Reset from the top level (whenever the clk_i stabilizes)

  // The ports readout clocks are handled within the top module
);
//-------------------------------------------------------------------//
// Let's First Buffer Input and Output Clocks
wire out_clk_p_o;
wire out_clk_n_o;
OBUF #(
	.IOSTANDARD("DEFAULT"), // Specify the output I/O standard
	.SLEW("FAST")           // Specify the output slew rate
) wr_clk_p_buffer ( 
	.O	(wr_clk_p_o), 
	.I	(out_clk_p_o)  
);
OBUF #(
	.IOSTANDARD("DEFAULT"), // Specify the output I/O standard
	.SLEW("FAST")           // Specify the output slew rate
) wr_clk_n_buffer ( 
	.O	(wr_clk_n_o), 
	.I	(out_clk_n_o)
);
OBUF #(
	.IOSTANDARD("DEFAULT"), // Specify the output I/O standard
	.SLEW("FAST")           // Specify the output slew rate
) rd_clk_p_buffer (   
	.O	(rd_clk_p_o), 
	.I	(out_clk_p_o)
);
OBUF #(
	.IOSTANDARD("DEFAULT"), // Specify the output I/O standard
	.SLEW("FAST")           // Specify the output slew rate
) rd_clk_n_buffer (   
	.O	(rd_clk_n_o), 
	.I	(out_clk_n_o)
);

//===================================================================//
/////////////////////////////////////////////
//  Oserdeses/Iserdeses reset generation  //
///////////////////////////////////////////
reg [15:0]  io_clkdiv_rst_sreg = 16'hFFFF;
always @( posedge io_clkdiv_o ) begin
	if (global_reset_i)
		io_clkdiv_rst_sreg <= 16'hFFFF;
	else
		io_clkdiv_rst_sreg <= {io_clkdiv_rst_sreg[14:0], global_reset_i };
end
assign io_clkdiv_rst_o = io_clkdiv_rst_sreg[15];

///////////////////////////////////////////////////////////////
//  Memory needs a separate MMC in it's bank                //
//  Since the phaser docs and verilog models are no avail, // 
//  I have decided to use a dedicated MMC in each memory  //
//  bank. It's an overkill, but at least I can simulate  //
//  clocks behavior this way. Since the MMC can drive   //
//  oserdeses/iserdeses directly, we don't use clock   //
//  buffers here.                                     //
///////////////////////////////////////////////////////
// At the end that is how the clocks
// should be alligned to eachother: Rising edge of clkdiv
// should coincide with rising edge of corresponding fast clock clk
//
// __|^^|__|^^|__|^^|__|^^|__	clk
//
// __|^^^^^|_____|^^^^^|_____	clkdiv
//
//
wire delay_calib_clk;	// This is 200MHz clock for calibrating idelays
wire mmcm_feedback;	// Feedback for the MMCM

wire io_clk_o_unbuff;
wire io_clk_n_o_unbuff;
wire io_clkdiv_o_unbuff;
wire io_clkdiv_n_o_unbuff;

wire out_clk_p_o_unbuff;
wire out_clk_n_o_unbuff;

wire delay_calib_clk_unbuff;
MMCME2_BASE #(
	.BANDWIDTH ("OPTIMIZED"),
	.CLKFBOUT_MULT_F (4.000),	// This means 250MHz input clock x 4 = VCO runs at 1000MHz
	.CLKFBOUT_PHASE  (0.0),
	.CLKIN1_PERIOD   (4.000),	// Input clock (250MHz) period is 4.000 ns

	.CLKOUT1_DIVIDE(8),    	// io_clkdiv_o, 1000MHz/8 = 125MHz
	.CLKOUT2_DIVIDE(4),	// wr_clk_o,	250 MHz
	.CLKOUT3_DIVIDE(1),	// <--- Not Using
	.CLKOUT4_DIVIDE(1), 	// <--- Not Using
	.CLKOUT5_DIVIDE(1),	// <--- Not Using
	.CLKOUT6_DIVIDE(5),	// delay_calib_clk, 200 MHz, 1tap=78psec
	.CLKOUT0_DIVIDE_F(4.0), // io_clk_o,  	250 MHz

	.CLKOUT0_DUTY_CYCLE(0.5),
	.CLKOUT1_DUTY_CYCLE(0.5),
	.CLKOUT2_DUTY_CYCLE(0.5),
	.CLKOUT3_DUTY_CYCLE(0.5),
	.CLKOUT4_DUTY_CYCLE(0.5),
	.CLKOUT5_DUTY_CYCLE(0.5),
	.CLKOUT6_DUTY_CYCLE(0.5),

	.CLKOUT0_PHASE(0.0),  	// io_clk_o, 	250 MHz
	.CLKOUT1_PHASE(0.0),  	// io_clkdiv_o	125 MHz
	.CLKOUT2_PHASE(90.00),	// out_clk_p/n_o, 250 MHz (multiple of 11.25)
	.CLKOUT3_PHASE(0.0),	// <--- Not Using
	.CLKOUT4_PHASE(0.0),	// <--- Not Using
	.CLKOUT5_PHASE(0.0),	// <--- Not Using
	.CLKOUT6_PHASE(0.0), 	// delay_calib_clk, 200 MHz

	.CLKOUT4_CASCADE("FALSE"), // Cascase CLKOUT4 counter with CLKOUT6 (FALSE, TRUE)
	.DIVCLK_DIVIDE(1),         // Master division value (1-106)
	.REF_JITTER1(0.0),         // Reference input jitter in UI (0.000-0.999).
	.STARTUP_WAIT("TRUE")     // Delays DONE until MMCM is locked (FALSE, TRUE)
) mmcm0 (
	.CLKOUT0	( io_clk_o_unbuff  ),
	.CLKOUT0B	( io_clk_n_o_unbuff ),
	.CLKOUT1	( io_clkdiv_o_unbuff ),
	.CLKOUT1B	( io_clkdiv_n_o_unbuff ),
	.CLKOUT2	( out_clk_p_o_unbuff ),
	.CLKOUT2B	( out_clk_n_o_unbuff ),
	.CLKOUT3	( ),
	.CLKOUT3B	( ),
	//-----------> Clocks without inversed component
	.CLKOUT4	( ), // Not using
	.CLKOUT5	( ), // Not using
	.CLKOUT6	( delay_calib_clk_unbuff ),
	.CLKFBOUT	( /*mmcm_feedback*/ ),
	.CLKFBOUTB	( ),
	.LOCKED		( clocks_good_o ),
	.CLKIN1		( clk_i ),
	.PWRDWN		( 1'b0 ),
	.RST		( rst_i ),
	.CLKFBIN	( mmcm_feedback )
);
//---> ISERDESES/OSERDESES clocks
BUFH io_clk_buffer (
	.O(io_clk_o	   ),	// 1-bit output: Clock output
        .I(io_clk_o_unbuff)	// 1-bit input: Clock input
);
assign mmcm_feedback = io_clk_o;

BUFH io_clk_n_buffer (
	.O(io_clk_n_o ),	// 1-bit output: Clock output
	.I(io_clk_n_o_unbuff)	// 1-bit input: Clock input
);
BUFH io_clkdiv_buffer (
	.O(io_clkdiv_o ),	// 1-bit output: Clock output port
	.I(io_clkdiv_o_unbuff)	// 1-bit input: Clock buffer input
);
BUFH io_clkdiv_n_buffer (
	.O(io_clkdiv_n_o ),      // 1-bit output: Clock output port
	.I(io_clkdiv_n_o_unbuff) // 1-bit input: Clock buffer input
);
//---> Write and read clock buffers:
BUFH out_clk_p_buffer (
	.O(out_clk_p_o),	// 1-bit output: Clock output
	.I(out_clk_p_o_unbuff)	// 1-bit input: Clock input
);
BUFH out_clk_n_buffer (
	.O(out_clk_n_o),	// 1-bit output: Clock output
	.I(out_clk_n_o_unbuff)	// 1-bit input: Clock input
);
// IO delay calibration clock buffer
BUFH delay_calib_clk_buffer (
	.O(delay_calib_clk),       // 1-bit output: Clock output (connect to I/O clock loads).
	.I(delay_calib_clk_unbuff) // 1-bit input: Clock input (connect to an IBUFG or BUFMR).
);
//////////////////////////////////////////////////////////
// We are using IDELAY, therefore we need IDELAYCNTRL  //
////////////////////////////////////////////////////////
(* IODELAY_GROUP = iodelay_group_name *)
IDELAYCTRL delay_calibrator (
	.RDY	( ),			// 1-bit output: Ready output
	.REFCLK	( delay_calib_clk  ),	// 1-bit input: Reference clock input
	.RST	( io_clkdiv_rst_o )	// 1-bit input: Active high reset input
);
endmodule /* qdrii_wb_clkgen */
