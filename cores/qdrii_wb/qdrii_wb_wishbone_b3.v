/*
 * A wishbone b3 bus interface
 */
module qdrii_wb_wishbone_b3
#(parameter     DATA_WIDTH    =  32, // Data Width of wishbone bus
		ADDRESS_WIDTH  = 32, // Address Width of wishbone bus
		BURST_LENGTH = 9)
(
	// Wishbone Slave interface

	input wire [ADDRESS_WIDTH-1:0]	wb_adr_i,
	input wire [1:0]		wb_bte_i,
	input wire [2:0]		wb_cti_i,
	input wire			wb_cyc_i,
	input wire [DATA_WIDTH-1:0]	wb_dat_i,
	input wire [3:0]		wb_sel_i,
	input wire			wb_stb_i,
	input wire			wb_we_i,

	output wire			wb_ack_o,
	output wire			wb_err_o,
	output wire			wb_rty_o,
	output wire [DATA_WIDTH-1:0]	wb_dat_o,

	input wire	wb_clk_i,
	input wire	wb_rst_i,

	// Data I/O lines
	
	output wire [DATA_WIDTH-1:0]	qdr_write_data_o,
	output wire [(DATA_WIDTH/8)-1:0]qdr_write_bsel_o,
	output wire [20:0]		qdr_write_addr_o,
	output wire 			qdr_write_push_o,
	input wire			qdr_write_full_i,

	input wire  [DATA_WIDTH-1:0]	qdr_read_data_i,
	output wire			qdr_read_pull_o,  // pull data from fifo
	input wire			qdr_read_empty_i, // 0 if readout fifo is empty
	output wire			qdr_read_clear_o, // for clearing burst leftovers

	output wire [20:0]		qdr_read_addr_o,  // push address for readout
	output reg			qdr_read_push_o,  // push address into the FIFO
	input wire			qdr_read_full_i,  // 1 if FIFO is full

	input wire qdr_ready_i
);
  // since the qdr_ready_i is in qdr domain, let's sync it to wb domain
  reg wb_qdr_ready_reg = 1'b0;
  always @(posedge wb_clk_i) begin
  	if (wb_rst_i)
		wb_qdr_ready_reg <= 1'b0;
	else
		wb_qdr_ready_reg <= qdr_ready_i;
  end
  assign wb_err_o = 1'b0;
  assign wb_rty_o = 1'b0;

  assign qdr_write_data_o = wb_dat_i;
  assign qdr_write_bsel_o = wb_sel_i;
  assign qdr_write_addr_o = wb_adr_i[22:2];

  reg wb_read_ack_reg;
  assign wb_ack_o = wb_read_ack_reg | wb_write_ack;

  wire write_cycle;
  assign write_cycle = wb_qdr_ready_reg & wb_cyc_i & wb_stb_i & wb_we_i;
  assign wb_write_ack = write_cycle & (!qdr_write_full_i);
  assign qdr_write_push_o = wb_write_ack;


  /////////////////////////
  //  Read Section      //
  ///////////////////////
  wire read_cycle;
  assign read_cycle = wb_qdr_ready_reg & wb_cyc_i & wb_stb_i & (!wb_we_i);

  assign wb_dat_o = qdr_read_data_i;

  reg allow_read_pull = 1'b0;
  assign qdr_read_pull_o = allow_read_pull & read_cycle & (!qdr_read_empty_i);

  reg [20:0] burst_read_addr;
  assign qdr_read_addr_o = burst_read_addr;
  reg [15:0] burst_leftover_counter;
  // State Machine for Read
  (* FSM_ENCODING="SEQUENTIAL", SAFE_IMPLEMENTATION="NO" *) reg [1:0] read_fsm_state = 2'b00;
  always@(posedge wb_clk_i)
  if (wb_rst_i) begin
	read_fsm_state <= 2'b00; // Idle state
	burst_read_addr <= 32'h0000_0000;
	burst_leftover_counter <= 16'h0000;
	qdr_read_push_o <= 1'b0;
	wb_read_ack_reg <= 1'b0;
	allow_read_pull <= 1'b0;
  end else 
  case (read_fsm_state)
  2'b00 : begin // Idle state, waiting for read access
	if ( read_cycle & ((wb_cti_i == 3'b000)|(wb_cti_i == 3'b111) )) begin
		// We have a single read cycle
		read_fsm_state <= 2'b01;
		qdr_read_push_o <= 1'b1;
	end else if (read_cycle & (wb_cti_i == 3'b010)) begin
		// Let's begin a burst access
		read_fsm_state <= 2'b10;
		qdr_read_push_o <= 1'b1;
	end else begin
		// Keep in Idle State
		read_fsm_state <= 2'b00;
		qdr_read_push_o <= 1'b0;
	end
	// The outputs in idle state
	// <outputs> <= <values>;
	burst_read_addr <= wb_adr_i[22:2];
	burst_leftover_counter <= 16'h0000;
	wb_read_ack_reg <= 1'b0;
	allow_read_pull <= 1'b1;
  end
  2'b01 : begin // Single memory read access
	if ( qdr_read_pull_o ) begin
		// We do have the read data
		read_fsm_state <= 2'b11;
		wb_read_ack_reg <= 1'b1;
	end else begin
		read_fsm_state <= 2'b01;
	end
	// This is what to do in single access
	qdr_read_push_o <= 1'b0;
	burst_read_addr <= wb_adr_i[22:2];
  end
  2'b10 : begin // Burst in progress
	if ( read_cycle & (wb_cti_i == 3'b111) ) begin
		read_fsm_state <= 2'b11;
		// This is the last word in burst, we need to clean after it
		qdr_read_push_o <= 1'b0;
		wb_read_ack_reg <= 1'b0;
	end else begin
		read_fsm_state <= 2'b10;
		// Just a normal burst, let's acknowledge the data
		// when it's available
		qdr_read_push_o <= 1'b1;

		if ((!qdr_read_empty_i) & (read_cycle)) begin
			// This means we don't have a wait states
			// and we have something in Read FIFO.
		end else begin
			// It can happen when master inserts a wait state,
			// or the data is not available yet
			burst_leftover_counter <= burst_leftover_counter + 1'b1;
		end
		// Acknowledge should be 1 cycle later than pull???
		if (qdr_read_pull_o) begin
			wb_read_ack_reg <= 1'b1;
		end
	end
	//---> Processing Burst
	if ( wb_bte_i == 2'b00) begin
		burst_read_addr      <= burst_read_addr      + 1'b1;
	end else if (wb_bte_i == 2'b01) begin
		burst_read_addr[1:0] <= burst_read_addr[1:0] + 1'b1;
	end else if (wb_bte_i == 2'b10) begin
		burst_read_addr[2:0] <= burst_read_addr[2:0] + 1'b1;
	end else begin
		burst_read_addr[3:0] <= burst_read_addr[3:0] + 1'b1;
	end
  end
  2'b11: begin // Cleaning after the burst
	if (burst_leftover_counter == 0) begin
		// We did cleaned the leftovers
		read_fsm_state <= 2'b00; // go to idle
	end else begin
		read_fsm_state <= 2'b11;

		if (!qdr_read_empty_i) begin
			burst_leftover_counter <= burst_leftover_counter - 1'b1;
		end else begin
		end
	end
	// What we can do while cleaning the burst
	wb_read_ack_reg <= 1'b0;
  end
  endcase

endmodule
