/*
 * Asynchronous FIFO, Write Chain.
 *   (mem)			(bus)
 *   qdr_clk			wb_clk
 *	----------------------------
 *   <== ADDR |  ADDR FIFO  |	ADDR <===
 *      ----------------------------
 * // Only DATA reverted, comparing to read chain
 *      ----------------------------
 *   <== DATA |  DATA FIFO  |   DATA <===
 *      ----------------------------
 *
 *      	 BSEL
 */
`timescale 1ns/1ps

module qdrii_wb_write_fifo #(
	parameter	DATA_WIDTH    = 32,
	parameter	BSEL_WIDTH    = 4,
	parameter	ADDRES_WIDTH  = 21
) (	//---> Wishbone side of the data fifo:
	input wire [31:0]	data_i,
	input wire [3:0]	bsel_i,
	input wire [20:0]	addr_i,

	output wire		full_o,
	input wire		push_i,

	input wire wb_rst_i, // It's not used here
	input wire wb_clk_i,

	//---> QDR side of the fifos
	output wire [31:0]	data_o,
	output wire [3:0]	bsel_o,
	output wire [20:0]	addr_o,

	output wire		empty_o,
	input wire		pull_i,

	input wire qdr_rst_i,
	input wire qdr_clk_i
);

wire data_empty_o, addr_empty_o; assign empty_o = data_empty_o | addr_empty_o;
wire data_full_o, addr_full_o;   assign full_o  = data_full_o  | addr_full_o;

// We want to instantiate 2 FIFOs here:
FIFO18E1 #(
	.ALMOST_EMPTY_OFFSET(13'h0080),	// Sets the almost empty threshold
	.ALMOST_FULL_OFFSET(13'h0080),	// Sets almost full threshold
	.DATA_WIDTH(36),		// Sets data width to 4-36
	.DO_REG(1),			// Enable output register (1-0) Must be 1 if EN_SYN = FALSE
	.EN_SYN("FALSE"),		// Specifies FIFO as dual-clock (FALSE) or Synchronous (TRUE)
	.FIFO_MODE("FIFO18_36"),	// Sets mode to FIFO18 or FIFO18_36
	.FIRST_WORD_FALL_THROUGH("FALSE"),	// Sets the FIFO FWFT to FALSE, TRUE
	.INIT(36'h000000000),		// Initial values on output port
	.SIM_DEVICE("7SERIES"),		// Must be set to "7SERIES" for simulation behavior
	.SRVAL(36'h000000000)		// Set/Reset value for output port
) data_fifo (
	// Wishbone side is read
	// Read Data: 32-bit (each) output: Read output data
	.DO( data_o[31:0] ),	// 32-bit output: Data output
	.DOP( bsel_o[3:0] ),	// 4-bit output: Parity data output
	// Status: 1-bit (each) output: Flags and other FIFO status outputs
	.ALMOSTEMPTY(  ),	// 1-bit output: Almost empty flag
	.ALMOSTFULL ( data_full_o  ),	// 1-bit output: Almost full flag
	.EMPTY      ( data_empty_o ),	// 1-bit output: Empty flag
	.FULL       (  ),	// 1-bit output: Full flag
	.RDCOUNT    (  ),	// 12-bit output: Read count
	.RDERR      (  ),	// 1-bit output: Read error
	.WRCOUNT    (  ),	// 12-bit output: Write count
	.WRERR      (  ),	// 1-bit output: Write error
	// Read Control Signals: 1-bit (each) input: Read  clock, enable and reset input signals
	.RDCLK  ( qdr_clk_i ),	// 1-bit input: Read clock
	.RDEN   ( pull_i ),	// 1-bit input: Read enable
	.REGCE  ( 1'b0 ),	// 1-bit input: Clock enable
	.RST    ( qdr_rst_i ),	// 1-bit input: Asynchronous Reset
	.RSTREG ( 1'b0 ),	// 1-bit input: Output register set/reset
	// Write Control Signals: 1-bit (each) input: Write clock and enable input signals
	.WRCLK	( wb_clk_i ),	// 1-bit input: Write clock
	.WREN	( push_i ),	// 1-bit input: Write enable
	// Write Data: 32-bit (each) input: Write input data
	.DI	( data_i[31:0]),// 32-bit input: Data input
	.DIP	( bsel_i[3:0] )	// 4-bit input: Parity input
);
FIFO18E1 #(
	.ALMOST_EMPTY_OFFSET(13'h0080), // Sets the almost empty threshold
	.ALMOST_FULL_OFFSET(13'h0080),  // Sets almost full threshold
	.DATA_WIDTH(36),                 // Sets data width to 4-36
	.DO_REG(1),                     // Enable output register (1-0) Must be 1 if EN_SYN = FALSE
	.EN_SYN("FALSE"),               // Specifies FIFO as dual-clock (FALSE) or Synchronous (TRUE)
	.FIFO_MODE("FIFO18_36"),        // Sets mode to FIFO18 or FIFO18_36
	.FIRST_WORD_FALL_THROUGH("FALSE"),      // Sets the FIFO FWFT to FALSE, TRUE
	.INIT(36'h000000000),           // Initial values on output port
	.SIM_DEVICE("7SERIES"),         // Must be set to "7SERIES" for simulation behavior
	.SRVAL(36'h000000000)           // Set/Reset value for output port
) addr_fifo (
	// Read Data: 32-bit (each) output: Read output data
	.DO( addr_o[20:0] ),	// 32-bit output: Data output
	.DOP(  ),		// 4-bit output: Parity data output
	// Status: 1-bit (each) output: Flags and other FIFO status outputs
	.ALMOSTEMPTY(  ),	// 1-bit output: Almost empty flag
	.ALMOSTFULL ( addr_full_o  ),	// 1-bit output: Almost full flag
	.EMPTY      ( addr_empty_o ),	// 1-bit output: Empty flag
	.FULL       (  ),	// 1-bit output: Full flag
	.RDCOUNT    (  ),	// 12-bit output: Read count
	.RDERR      (  ),	// 1-bit output: Read error
	.WRCOUNT    (  ),	// 12-bit output: Write count
	.WRERR      (  ),	// 1-bit output: Write error
	// Read Control Signals: 1-bit (each) input: Read  clock, enable and reset input signals
	.RDCLK  ( qdr_clk_i ),	// 1-bit input: Read clock
	.RDEN   ( pull_i ),// 1-bit input: Read enable
	.REGCE  ( 1'b0 ),	// 1-bit input: Clock enable
	.RST    ( qdr_rst_i ),	// 1-bit input: Asynchronous Reset
	.RSTREG ( 1'b0 ),	// 1-bit input: Output register set/reset
	// Write Control Signals: 1-bit (each) input: Write clock and enable input signals
	.WRCLK  ( wb_clk_i),	// 1-bit input: Write clock
	.WREN   ( push_i ),	// 1-bit input: Write enable
	// Write Data: 32-bit (each) input: Write input data
	.DI     ( {11'h0, addr_i[20:0]}),// 32-bit input: Data input
	.DIP    ( 4'b0000 )	// 4-bit input: Parity input
);
endmodule
