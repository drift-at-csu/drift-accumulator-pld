/* This is a wrapper for Xilinx oserdese2 primitive */
`timescale 1ns/1ps

module qdrii_wb_phys_iserdes #(
	parameter	CLOCKS_RATIO = 4, //Ratio of fast to slow clocks
	parameter	iodelay_group_name = "memX_delay_group",
	parameter	chan_delay = 0
)(
  //---> Controller Parallelized outputs
  input wire				dinp_i,
  output wire [CLOCKS_RATIO-1:0]	dout_o,
  // reset is in QDR slow clock domain
  input wire slow_rst_i,
  input wire slow_clk_i,
  input wire fast_clk_p_i,
  input wire fast_clk_n_i,
  input wire enable_i,
  input wire bitslip_i
);

wire delayed_dinp_i;
wire dinp_i_buffered;
(* IODELAY_GROUP = iodelay_group_name *)
IDELAYE2 #(
	.CINVCTRL_SEL("FALSE"),         // Enable dynamic clock inversion (FALSE, TRUE)
	.DELAY_SRC("IDATAIN"),          // Delay input (IDATAIN, DATAIN)
	.HIGH_PERFORMANCE_MODE("FALSE"),// Reduced jitter ("TRUE"), Reduced power ("FALSE")
	.IDELAY_TYPE("VARIABLE"),	// FIXED, VARIABLE, VAR_LOAD, VAR_LOAD_PIPE
	// 5 taps means 0.39nsec
	.IDELAY_VALUE(chan_delay),	// Input delay tap setting (0-31)
	.PIPE_SEL("FALSE"),		// Select pipelined mode, FALSE, TRUE
	.REFCLK_FREQUENCY(200.0),	// IDELAYCTRL clock input frequency in MHz (190.0-210.0).
	.SIGNAL_PATTERN("DATA")		// DATA, CLOCK input signal
) idelay (
	.CNTVALUEOUT(  ),       // 5-bit output: Counter value output
	.DATAOUT( delayed_dinp_i ),   // 1-bit output: Delayed data output
	.C      ( slow_clk_i ),       // 1-bit input: Clock input (VARIABLE, VAR_LOAD, VAR_LOAD_PIPE)
	.CE     ( 1'b0 ),       // 1-bit input: Active high enable increment/decrement input
	.CINVCTRL( 1'b0 ),      // 1-bit input: Dynamic clock inversion input
	.CNTVALUEIN( 5'b00000 ),// 5-bit input: Counter value input
	.DATAIN ( 1'b0 ),       // 1-bit input: Internal delay data input
	.IDATAIN( dinp_i_buffered ), 	// 1-bit input: Data input from the I/O
	.INC    ( 1'b0 ),       // 1-bit input: Increment / Decrement tap delay input
	.LD     ( 1'b0 ),       // 1-bit input: Load IDELAY_VALUE input
	.LDPIPEEN( 1'b0 ),      // 1-bit input: Enable PIPELINE register to load data input
	.REGRST ( slow_rst_i )        // 1-bit input: Active-high reset tap-delay input (VAR_LOAD_PIPE)
);

wire [1:0] first_half_dout;
ISERDESE2 #(
	.DATA_RATE("DDR"),		// DDR, SDR
        .DATA_WIDTH( 4 /*CLOCKS_RATIO*/ ),	// Parallel data width (2-8,10,14)
	.DYN_CLKDIV_INV_EN("FALSE"),	// Enable DYNCLKDIVINVSEL inversion (FALSE, TRUE)
	.DYN_CLK_INV_EN("FALSE"),	// Enable DYNCLKINVSEL inversion (FALSE, TRUE)
	// INIT_Q1 - INIT_Q4: Initial value on the  Q outputs (0/1)
	.INIT_Q1(1'b0),
	.INIT_Q2(1'b0),
	.INIT_Q3(1'b0),
	.INIT_Q4(1'b0),
	// .INTERFACE_TYPE("NETWORKING"),
	.INTERFACE_TYPE("MEMORY"), // MEMORY, MEMORY_DDR3, MEMORY_QDR, NETWORKING, OVERSAMPLE
	.IOBDELAY("IFD"),	// NONE, BOTH, IBUF, IFD
	.NUM_CE(1),		// Number of clock enables (1,2)
	.OFB_USED("FALSE"),	// Select OFB path (FALSE, TRUE)
	.SERDES_MODE("MASTER"),	// MASTER, SLAVE
	// SRVAL_Q1 - SRVAL_Q4: Q output  values when  SR is used  (0/1)
	.SRVAL_Q1(1'b0),
	.SRVAL_Q2(1'b0),
	.SRVAL_Q3(1'b0),
	.SRVAL_Q4(1'b0) 
	
) iserdes (
	.O( ), 	// 1-bit output: Combinatorial output
	// Q1 - Q8: 1-bit (each)  output: Registered data outputs
	.Q1( first_half_dout[0]/*dout_o[0]*/ ),
	.Q2( first_half_dout[1]/*dout_o[1]*/ ),
	.Q3( dout_o[0] ),
	.Q4( dout_o[1] ),
	.Q5(  ),
	.Q6(  ),
	.Q7(  ),
	.Q8(  ),
	// SHIFTOUT1-SHIFTOUT2: 1-bit (each)  output: Data width  expansion output  ports
	.SHIFTOUT1(  ),
	.SHIFTOUT2(  ),
	// 1-bit input: The BITSLIP pin performs a Bitslip operation synchronous to
	// CLKDIV when asserted (active High). Subsequently, the data seen on the Q1
	// to Q8 output ports will shift, as in a barrel-shifter operation, one
	// position every time Bitslip is invoked (DDR operation is different from
	// SDR).
	.BITSLIP( /* bitslip_i */ 1'b0 ),
	// CE1, CE2: 1-bit (each) input: Data register clock enable inputs
	.CE1( 1'b1 /* enable_i */ ),
	.CE2( /* 1'b1 */ /* enable_i */ ),
	.CLKDIVP( /* slow_clk_i */ /* 1'b0*/ ),	// 1-bit input: TBD
	.CLKDIV(  slow_clk_i  ), // 1-bit input: Divided clock
	.OCLK  (  fast_clk_p_i /* 1'b0 */ ), // 1-bit input: High speed output clock
	.OCLKB ( !fast_clk_p_i ),              // 1-bit input: High speed negative edge output clock
	// Clocks: 1-bit (each) input: ISERDESE2 clock input ports
	.CLK   (  fast_clk_p_i ),	// 1-bit input: High-speed clock
	.CLKB  ( !fast_clk_p_i ),	// 1-bit input: High-speed secondary clock
	// Dynamic Clock Inversions: 1-bit (each) input: Dynamic clock inversion pins to
	// switch clock polarity
	.DYNCLKDIVSEL( 1'b0 ),	// 1-bit input: Dynamic CLKDIV inversion
	.DYNCLKSEL   ( 1'b0 ),	// 1-bit input: Dynamic CLK/CLKB inversion
	// Input Data: 1-bit (each) input: ISERDESE2 data  input ports
	.D    ( dinp_i_buffered ),	// 1-bit input: Data input
	.DDLY ( delayed_dinp_i ),	// 1-bit input: Serial data from IDELAYE2
	.OFB  ( /* 1'b0 */ ),		// 1-bit input: Data feedback from OSERDESE2
	.RST  ( /* 1'b0 */ slow_rst_i ),	// 1-bit input: Active high asynchronous reset
	// SHIFTIN1-SHIFTIN2: 1-bit (each) input: Data width expansion  input ports
	.SHIFTIN1( 1'b0 ),
	.SHIFTIN2( 1'b0 ) 
);

reg [1:0] first_half_dout_reg;
// This buffers first half of the word
always @ (posedge slow_clk_i) begin
  first_half_dout_reg <= first_half_dout;
end
assign dout_o[2] = first_half_dout_reg[0];
assign dout_o[3] = first_half_dout_reg[1];

IBUF #(
	.IBUF_LOW_PWR("FALSE"),  // Low power (TRUE) vs. performance (FALSE) setting 
	.IOSTANDARD("DEFAULT")  // Specify the input I/O standard
) buffer (
	.O(dinp_i_buffered),     // Buffer output
	.I(dinp_i)      // Buffer input (connect directly to top-level port)
);
endmodule
