/*
 *  QDR 2 Physical, collection of iserdese2 and oserdese2 for propagation
 *  of serial data, structured by controller to physical pads
 *
 *                qdr_slow_clk
 *   -------------------                 ---------------
 *   |                 |                 |             |
 *   |                 |  ===> addr00    |             |
 *   |                 |  ===> ....      |             |
 *   |                 |  ===> addr21    |             |
 *   |                 |                 |             |
 *   |                 |  ===> dout0     |             |
 *   |                 |  ===> ....      |             |
 *   |                 |  ===> dout9     |             |
 *   |                 |                 |             |
 *   |                 |  ===> bws       |             |
 *   | QDR2 Controller |  ===> wps       | QDR2 Phys.  |
 *   |                 |                 |             |
 *   |                 |  ===> rps       |             |
 *   |                 |                 |             |
 *   |                 |                 |             |
 *   |                 |                 |             |
 *   |                 |                 ---------------
 *   -------------------
 *
 */
`timescale 1ns/1ps

module qdrii_wb_phys #(	
	parameter CLOCKS_RATIO = 4, //Ratio of fast to slow clocks
	parameter	iodelay_group_name = "memX_delay_group",
	parameter	data0_delay = 0,
	parameter	data1_delay = 0,
	parameter	data2_delay = 0,
	parameter	data3_delay = 0,
	parameter	data4_delay = 0,
	parameter	data5_delay = 0,
	parameter	data6_delay = 0,
	parameter	data7_delay = 0,
	parameter	data8_delay = 0,
	parameter	echo_clk_p_delay = 0,
	parameter	echo_clk_n_delay = 0
)(
	//---> Controller outputs
	input wire [CLOCKS_RATIO-1:0] addr00_i,
	input wire [CLOCKS_RATIO-1:0] addr01_i,
	input wire [CLOCKS_RATIO-1:0] addr02_i,
	input wire [CLOCKS_RATIO-1:0] addr03_i,
	input wire [CLOCKS_RATIO-1:0] addr04_i,
	input wire [CLOCKS_RATIO-1:0] addr05_i,
	input wire [CLOCKS_RATIO-1:0] addr06_i,
	input wire [CLOCKS_RATIO-1:0] addr07_i,
	input wire [CLOCKS_RATIO-1:0] addr08_i,
	input wire [CLOCKS_RATIO-1:0] addr09_i,
	input wire [CLOCKS_RATIO-1:0] addr10_i,
	input wire [CLOCKS_RATIO-1:0] addr11_i,
	input wire [CLOCKS_RATIO-1:0] addr12_i,
	input wire [CLOCKS_RATIO-1:0] addr13_i,
	input wire [CLOCKS_RATIO-1:0] addr14_i,
	input wire [CLOCKS_RATIO-1:0] addr15_i,
	input wire [CLOCKS_RATIO-1:0] addr16_i,
	input wire [CLOCKS_RATIO-1:0] addr17_i,
	input wire [CLOCKS_RATIO-1:0] addr18_i,
	input wire [CLOCKS_RATIO-1:0] addr19_i,
	input wire [CLOCKS_RATIO-1:0] addr20_i,
	input wire [CLOCKS_RATIO-1:0] addr21_i,

	input wire [CLOCKS_RATIO-1:0] dout00_i,
	input wire [CLOCKS_RATIO-1:0] dout01_i,
	input wire [CLOCKS_RATIO-1:0] dout02_i,
	input wire [CLOCKS_RATIO-1:0] dout03_i,
	input wire [CLOCKS_RATIO-1:0] dout04_i,
	input wire [CLOCKS_RATIO-1:0] dout05_i,
	input wire [CLOCKS_RATIO-1:0] dout06_i,
	input wire [CLOCKS_RATIO-1:0] dout07_i,
	input wire [CLOCKS_RATIO-1:0] dout08_i,

	input wire [CLOCKS_RATIO-1:0] rps_i,
	input wire [CLOCKS_RATIO-1:0] wps_i,
	input wire [CLOCKS_RATIO-1:0] bws_i,

	output wire [CLOCKS_RATIO-1:0] dinp00_o,
	output wire [CLOCKS_RATIO-1:0] dinp01_o,
	output wire [CLOCKS_RATIO-1:0] dinp02_o,
	output wire [CLOCKS_RATIO-1:0] dinp03_o,
	output wire [CLOCKS_RATIO-1:0] dinp04_o,
	output wire [CLOCKS_RATIO-1:0] dinp05_o,
	output wire [CLOCKS_RATIO-1:0] dinp06_o,
	output wire [CLOCKS_RATIO-1:0] dinp07_o,
	output wire [CLOCKS_RATIO-1:0] dinp08_o,

	output wire [CLOCKS_RATIO-1:0] echo_clk_p_o,
	output wire [CLOCKS_RATIO-1:0] echo_clk_n_o,

	//--->QDRII interface
	output wire 		qdr2_rps_o, // read request
	output wire 		qdr2_wps_o, // write request
	output wire 		qdr2_bws_o, // byte select
	output wire [21:0] 	qdr2_addr_o,
	output wire [8:0]	qdr2_data_o,

	input wire  [8:0] 	qdr2_data_i, // Not implemented yet

	input wire		qdr2_echo_clk_p_i,
	input wire		qdr2_echo_clk_n_i,

	//---> Iserdeses Control lines
	input wire iserdes_enable_i,
	input wire iserdes_reset_i,
	input wire iserdes_bitslip_i,

	//---> Oserdeses Control lines
	input wire oserdes_enable_i,
	input wire oserdes_reset_i,

	input wire [CLOCKS_RATIO-1:0] address_mask_i,
	input wire [CLOCKS_RATIO-1:0] data_o_mask_i,
	input wire [CLOCKS_RATIO-1:0] bws_mask_i,

	// Iserdeses / Oserdeses Clocks
	input wire io_divclk_i,
	input wire io_clk_i,
	input wire io_clk_n_i,
	input wire io_rst_i
);
////////////////////////////////////////
// Addres Lines oserdeses            //
//////////////////////////////////////
qdrii_wb_phys_oserdes #( .CLOCKS_RATIO (CLOCKS_RATIO) ) addr00_inst (
  .dinp_i 	( addr00_i ),		.dout_o 	( qdr2_addr_o[0] ),
  .mask_i	( address_mask_i ),
  .slow_rst_i	( oserdes_reset_i ),	.slow_clk_i	( io_divclk_i ),
  .slow_enable_i( oserdes_enable_i ), 	.fast_clk_i 	( io_clk_i ) );
qdrii_wb_phys_oserdes #( .CLOCKS_RATIO (CLOCKS_RATIO) ) addr01_inst (
  .dinp_i 	( addr01_i ), 		.dout_o 	( qdr2_addr_o[1] ),
  .mask_i	( address_mask_i ),
  .slow_rst_i	( oserdes_reset_i ), 	.slow_clk_i	( io_divclk_i ), 
  .slow_enable_i( oserdes_enable_i ), 	.fast_clk_i 	( io_clk_i ) );
qdrii_wb_phys_oserdes #( .CLOCKS_RATIO (CLOCKS_RATIO) ) addr02_inst (
  .dinp_i 	( addr02_i ),		.dout_o 	( qdr2_addr_o[2] ),
  .mask_i	( address_mask_i ),
  .slow_rst_i	( oserdes_reset_i ),	.slow_clk_i	( io_divclk_i ), 
  .slow_enable_i( oserdes_enable_i ), 	.fast_clk_i 	( io_clk_i ) );
qdrii_wb_phys_oserdes #( .CLOCKS_RATIO (CLOCKS_RATIO) ) addr03_inst (
  .dinp_i 	( addr03_i ),		.dout_o 	( qdr2_addr_o[3] ),
  .mask_i	( address_mask_i ),
  .slow_rst_i	( oserdes_reset_i ),	.slow_clk_i	( io_divclk_i ), 
  .slow_enable_i( oserdes_enable_i ), 	.fast_clk_i 	( io_clk_i ) );
qdrii_wb_phys_oserdes #( .CLOCKS_RATIO (CLOCKS_RATIO) ) addr04_inst (
  .dinp_i 	( addr04_i ), 		.dout_o 	( qdr2_addr_o[4] ),
  .mask_i	( address_mask_i ),
  .slow_rst_i	( oserdes_reset_i ), 	.slow_clk_i	( io_divclk_i ), 
  .slow_enable_i( oserdes_enable_i ), 	.fast_clk_i 	( io_clk_i ) );
qdrii_wb_phys_oserdes #( .CLOCKS_RATIO (CLOCKS_RATIO) ) addr05_inst (
  .dinp_i 	( addr05_i ), 		.dout_o 	( qdr2_addr_o[5] ),
  .mask_i	( address_mask_i ),
  .slow_rst_i	( oserdes_reset_i ), 	.slow_clk_i	( io_divclk_i ), 
  .slow_enable_i( oserdes_enable_i ),	.fast_clk_i 	( io_clk_i ) );
qdrii_wb_phys_oserdes #( .CLOCKS_RATIO (CLOCKS_RATIO) ) addr06_inst (
  .dinp_i 	( addr06_i ),		.dout_o 	( qdr2_addr_o[6] ),
  .mask_i	( address_mask_i ),
  .slow_rst_i	( oserdes_reset_i ), 	.slow_clk_i	( io_divclk_i ), 
  .slow_enable_i( oserdes_enable_i ),	.fast_clk_i 	( io_clk_i ) );
qdrii_wb_phys_oserdes #( .CLOCKS_RATIO (CLOCKS_RATIO) ) addr07_inst (
  .dinp_i 	( addr07_i ),		.dout_o 	( qdr2_addr_o[7] ),
  .mask_i	( address_mask_i ),
  .slow_rst_i	( oserdes_reset_i ),	.slow_clk_i	( io_divclk_i ), 
  .slow_enable_i( oserdes_enable_i ),	.fast_clk_i 	( io_clk_i ) );
qdrii_wb_phys_oserdes #( .CLOCKS_RATIO (CLOCKS_RATIO) ) addr08_inst (
  .dinp_i 	( addr08_i ),		.dout_o		( qdr2_addr_o[8] ),
  .mask_i	( address_mask_i ),
  .slow_rst_i	( oserdes_reset_i ),	.slow_clk_i	( io_divclk_i ), 
  .slow_enable_i( oserdes_enable_i ),	.fast_clk_i 	( io_clk_i ) );
qdrii_wb_phys_oserdes #( .CLOCKS_RATIO (CLOCKS_RATIO) ) addr09_inst (
  .dinp_i 	( addr09_i ),		.dout_o 	( qdr2_addr_o[9] ),
  .mask_i	( address_mask_i ),
  .slow_rst_i	( oserdes_reset_i ),	.slow_clk_i	( io_divclk_i ), 
  .slow_enable_i( oserdes_enable_i ),	.fast_clk_i 	( io_clk_i ) );
qdrii_wb_phys_oserdes #( .CLOCKS_RATIO (CLOCKS_RATIO) ) addr10_inst (
  .dinp_i 	( addr10_i ), 		.dout_o 	( qdr2_addr_o[10] ),
  .mask_i	( address_mask_i ),
  .slow_rst_i	( oserdes_reset_i ),	.slow_clk_i	( io_divclk_i ), 
  .slow_enable_i( oserdes_enable_i ), 	.fast_clk_i 	( io_clk_i ) );
qdrii_wb_phys_oserdes #( .CLOCKS_RATIO (CLOCKS_RATIO) ) addr11_inst (
  .dinp_i 	( addr11_i ), 		.dout_o 	( qdr2_addr_o[11] ),
  .mask_i	( address_mask_i ),
  .slow_rst_i	( oserdes_reset_i ), 	.slow_clk_i	( io_divclk_i ), 
  .slow_enable_i( oserdes_enable_i ), 	.fast_clk_i 	( io_clk_i ) );
qdrii_wb_phys_oserdes #( .CLOCKS_RATIO (CLOCKS_RATIO) ) addr12_inst (
  .dinp_i 	( addr12_i ), 		.dout_o 	( qdr2_addr_o[12] ),
  .mask_i	( address_mask_i ),
  .slow_rst_i	( oserdes_reset_i ),	.slow_clk_i	( io_divclk_i ), 
  .slow_enable_i( oserdes_enable_i ), 	.fast_clk_i	( io_clk_i ) );
qdrii_wb_phys_oserdes #( .CLOCKS_RATIO (CLOCKS_RATIO) ) addr13_inst (
  .dinp_i 	( addr13_i ),		.dout_o 	( qdr2_addr_o[13] ),
  .mask_i	( address_mask_i ),
  .slow_rst_i	( oserdes_reset_i ), 	.slow_clk_i	( io_divclk_i ), 
  .slow_enable_i( oserdes_enable_i ),	.fast_clk_i 	( io_clk_i ) );
qdrii_wb_phys_oserdes #( .CLOCKS_RATIO (CLOCKS_RATIO) ) addr14_inst (
  .dinp_i 	( addr14_i ),		.dout_o 	( qdr2_addr_o[14] ),
  .mask_i	( address_mask_i ),
  .slow_rst_i	( oserdes_reset_i ),	.slow_clk_i	( io_divclk_i ), 
  .slow_enable_i( oserdes_enable_i ),	.fast_clk_i 	( io_clk_i ) );
qdrii_wb_phys_oserdes #( .CLOCKS_RATIO (CLOCKS_RATIO) ) addr15_inst (
  .dinp_i 	( addr15_i ), 		.dout_o 	( qdr2_addr_o[15] ),
  .mask_i	( address_mask_i ),
  .slow_rst_i	( oserdes_reset_i ),	.slow_clk_i	( io_divclk_i ), 
  .slow_enable_i( oserdes_enable_i ),	.fast_clk_i	( io_clk_i ) );
qdrii_wb_phys_oserdes #( .CLOCKS_RATIO (CLOCKS_RATIO) ) addr16_inst (
  .dinp_i 	( addr16_i ), 		.dout_o 	( qdr2_addr_o[16] ),
  .mask_i	( address_mask_i ),
  .slow_rst_i	( oserdes_reset_i ), 	.slow_clk_i	( io_divclk_i ), 
  .slow_enable_i( oserdes_enable_i ), 	.fast_clk_i 	( io_clk_i ) );
qdrii_wb_phys_oserdes #( .CLOCKS_RATIO (CLOCKS_RATIO) ) addr17_inst (
  .dinp_i 	( addr17_i ),		.dout_o 	( qdr2_addr_o[17] ),
  .mask_i	( address_mask_i ),
  .slow_rst_i	( oserdes_reset_i ),	.slow_clk_i	( io_divclk_i ), 
  .slow_enable_i( oserdes_enable_i ),	.fast_clk_i 	( io_clk_i ) );
qdrii_wb_phys_oserdes #( .CLOCKS_RATIO (CLOCKS_RATIO) ) addr18_inst (
  .dinp_i 	( addr18_i ),		.dout_o 	( qdr2_addr_o[18] ),
  .mask_i	( address_mask_i ),
  .slow_rst_i	( oserdes_reset_i ),	.slow_clk_i	( io_divclk_i ), 
  .slow_enable_i( oserdes_enable_i ),	.fast_clk_i 	( io_clk_i ) );
qdrii_wb_phys_oserdes #( .CLOCKS_RATIO (CLOCKS_RATIO) ) addr19_inst (
  .dinp_i 	( addr19_i ), 		.dout_o 	( qdr2_addr_o[19] ),
  .mask_i	( address_mask_i ),
  .slow_rst_i	( oserdes_reset_i ),	.slow_clk_i	( io_divclk_i ), 
  .slow_enable_i( oserdes_enable_i ),	.fast_clk_i 	( io_clk_i ) );
qdrii_wb_phys_oserdes #( .CLOCKS_RATIO (CLOCKS_RATIO) ) addr20_inst (
  .dinp_i 	( addr20_i ),		.dout_o 	( qdr2_addr_o[20] ),
  .mask_i	( address_mask_i ),
  .slow_rst_i	( oserdes_reset_i ),	.slow_clk_i	( io_divclk_i ), 
  .slow_enable_i( oserdes_enable_i ),	.fast_clk_i 	( io_clk_i ) );
qdrii_wb_phys_oserdes #( .CLOCKS_RATIO (CLOCKS_RATIO) ) addr21_inst (
  .dinp_i 	( addr21_i ),		.dout_o 	( qdr2_addr_o[21] ),
  .mask_i	( address_mask_i ),
  .slow_rst_i	( oserdes_reset_i ),	.slow_clk_i	( io_divclk_i ), 
  .slow_enable_i( oserdes_enable_i ),	.fast_clk_i	( io_clk_i ) );

/////////////////////////////
// Output Data oserdeses  //
///////////////////////////
qdrii_wb_phys_oserdes #( .CLOCKS_RATIO (CLOCKS_RATIO) ) dout00_inst (
  .dinp_i 	( dout00_i ), 		.dout_o 	( qdr2_data_o[0] ),
  .mask_i	( data_o_mask_i ),
  .slow_rst_i	( oserdes_reset_i ),	.slow_clk_i	( io_divclk_i ), 
  .slow_enable_i( oserdes_enable_i ),	.fast_clk_i	( io_clk_i ) );
qdrii_wb_phys_oserdes #( .CLOCKS_RATIO (CLOCKS_RATIO) ) dout01_inst (
  .dinp_i 	( dout01_i ), 		.dout_o 	( qdr2_data_o[1] ),
  .mask_i	( data_o_mask_i ),
  .slow_rst_i	( oserdes_reset_i ), 	.slow_clk_i	( io_divclk_i ), 
  .slow_enable_i( oserdes_enable_i ),	.fast_clk_i 	( io_clk_i ) );
qdrii_wb_phys_oserdes #( .CLOCKS_RATIO (CLOCKS_RATIO) ) dout02_inst (
  .dinp_i 	( dout02_i ), 		.dout_o 	( qdr2_data_o[2] ),
  .mask_i	( data_o_mask_i ),
  .slow_rst_i	( oserdes_reset_i ),	.slow_clk_i	( io_divclk_i ), 
  .slow_enable_i( oserdes_enable_i ),	.fast_clk_i 	( io_clk_i ) );
qdrii_wb_phys_oserdes #( .CLOCKS_RATIO (CLOCKS_RATIO) ) dout03_inst (
  .dinp_i 	( dout03_i ), 		.dout_o 	( qdr2_data_o[3] ),
  .mask_i	( data_o_mask_i ),
  .slow_rst_i	( oserdes_reset_i ),	.slow_clk_i	( io_divclk_i ), 
  .slow_enable_i( oserdes_enable_i ),	.fast_clk_i 	( io_clk_i ) );
qdrii_wb_phys_oserdes #( .CLOCKS_RATIO (CLOCKS_RATIO) ) dout04_inst (
  .dinp_i 	( dout04_i ), 		.dout_o 	( qdr2_data_o[4] ),
  .mask_i	( data_o_mask_i ),
  .slow_rst_i	( oserdes_reset_i ),	.slow_clk_i	( io_divclk_i ), 
  .slow_enable_i( oserdes_enable_i ),	.fast_clk_i 	( io_clk_i ) );
qdrii_wb_phys_oserdes #( .CLOCKS_RATIO (CLOCKS_RATIO) ) dout05_inst (
  .dinp_i 	( dout05_i ), 		.dout_o 	( qdr2_data_o[5] ),
  .mask_i	( data_o_mask_i ),
  .slow_rst_i	( oserdes_reset_i ),	.slow_clk_i	( io_divclk_i ), 
  .slow_enable_i( oserdes_enable_i ),	.fast_clk_i	( io_clk_i ) );
qdrii_wb_phys_oserdes #( .CLOCKS_RATIO (CLOCKS_RATIO) ) dout06_inst (
  .dinp_i	( dout06_i ),		.dout_o 	( qdr2_data_o[6] ),
  .mask_i       ( data_o_mask_i ),
  .slow_rst_i	( oserdes_reset_i ), 	.slow_clk_i	( io_divclk_i ), 
  .slow_enable_i( oserdes_enable_i ),	.fast_clk_i 	( io_clk_i ) );
qdrii_wb_phys_oserdes #( .CLOCKS_RATIO (CLOCKS_RATIO) ) dout07_inst (
  .dinp_i 	( dout07_i ),		.dout_o 	( qdr2_data_o[7] ),
  .mask_i	( data_o_mask_i ),
  .slow_rst_i	( oserdes_reset_i ),	.slow_clk_i	( io_divclk_i ), 
  .slow_enable_i( oserdes_enable_i ),	.fast_clk_i 	( io_clk_i) );
qdrii_wb_phys_oserdes #( .CLOCKS_RATIO (CLOCKS_RATIO) ) dout08_inst (
  .dinp_i 	( dout08_i ),		.dout_o 	( qdr2_data_o[8] ),
  .mask_i	( data_o_mask_i ),
  .slow_rst_i	( oserdes_reset_i ),	.slow_clk_i	( io_divclk_i ),
  .slow_enable_i( oserdes_enable_i ),	.fast_clk_i 	( io_clk_i ) );

///////////////////////////////////////////////
// Control Signals oserdeses: rps, wps, bws //
/////////////////////////////////////////////
qdrii_wb_phys_oserdes #( .CLOCKS_RATIO (CLOCKS_RATIO) ) rps_inst (
  .dinp_i 	( rps_i ), 		.dout_o 	( qdr2_rps_o ),
  .mask_i       ( 4'b1010 ), // RPS is valid on rising edge of the K clock
  .slow_rst_i	( oserdes_reset_i ),	.slow_clk_i	( io_divclk_i ), 
  .slow_enable_i( oserdes_enable_i ),	.fast_clk_i	( io_clk_i) );
qdrii_wb_phys_oserdes #( .CLOCKS_RATIO (CLOCKS_RATIO) ) wps_inst (
  .dinp_i 	( wps_i ), 		.dout_o 	( qdr2_wps_o ),
  .mask_i	( 4'b1010 ), // WPS is valid on rising edge of the K clock
  .slow_rst_i	( oserdes_reset_i ),	.slow_clk_i	( io_divclk_i ), 
  .slow_enable_i( oserdes_enable_i ),	.fast_clk_i 	( io_clk_i ) );
// This is a source of the bug
// the mask on bws should be always on when write operation is in progress
qdrii_wb_phys_oserdes #( .CLOCKS_RATIO (CLOCKS_RATIO) ) bws_inst (
  .dinp_i 	( bws_i ),		.dout_o 	( qdr2_bws_o ),
  .mask_i       ( bws_mask_i ),
  .slow_rst_i	( oserdes_reset_i ),	.slow_clk_i	( io_divclk_i ), 
  .slow_enable_i( oserdes_enable_i), 	.fast_clk_i 	( io_clk_i ) );

//===================================================================//
// ISERDESES below this line
/////////////////////////////////////////////
// Input Data iserdeses                   //
///////////////////////////////////////////
qdrii_wb_phys_iserdes #( 	.CLOCKS_RATIO ( CLOCKS_RATIO ), 
	.iodelay_group_name ( iodelay_group_name ),	.chan_delay ( data0_delay )
) dinp00_inst (
  .dinp_i	( qdr2_data_i[0] ), 	.dout_o 	( dinp00_o ),
  .slow_rst_i	( iserdes_reset_i ), 	.slow_clk_i	( io_divclk_i ),
  .fast_clk_p_i ( io_clk_i ), 		.fast_clk_n_i 	( io_clk_n_i ),
  .enable_i	( iserdes_enable_i ), 	.bitslip_i 	( iserdes_bitslip_i ) );
qdrii_wb_phys_iserdes #( 	.CLOCKS_RATIO (CLOCKS_RATIO),
	.iodelay_group_name ( iodelay_group_name ),	.chan_delay ( data1_delay )
) dinp01_inst (
  .dinp_i	( qdr2_data_i[1] ), 	.dout_o 	( dinp01_o ),
  .slow_rst_i	( iserdes_reset_i ), 	.slow_clk_i	( io_divclk_i ),
  .fast_clk_p_i ( io_clk_i ), 		.fast_clk_n_i 	( io_clk_n_i ),
  .enable_i	( iserdes_enable_i), 	.bitslip_i 	( iserdes_bitslip_i ) );
qdrii_wb_phys_iserdes #( 	.CLOCKS_RATIO (CLOCKS_RATIO),
	.iodelay_group_name ( iodelay_group_name ),	.chan_delay ( data2_delay )
) dinp02_inst (
  .dinp_i	( qdr2_data_i[2] ), 	.dout_o 	( dinp02_o ),
  .slow_rst_i	( iserdes_reset_i ), 	.slow_clk_i	( io_divclk_i ),
  .fast_clk_p_i ( io_clk_i ), 		.fast_clk_n_i 	( io_clk_n_i ),
  .enable_i	( iserdes_enable_i ),	.bitslip_i 	( iserdes_bitslip_i ) );
qdrii_wb_phys_iserdes #( 	.CLOCKS_RATIO (CLOCKS_RATIO),
	.iodelay_group_name ( iodelay_group_name ),	.chan_delay ( data3_delay )
) dinp03_inst (
  .dinp_i	( qdr2_data_i[3] ), 	.dout_o 	( dinp03_o ),
  .slow_rst_i	( iserdes_reset_i ), 	.slow_clk_i	( io_divclk_i ),
  .fast_clk_p_i ( io_clk_i ), 		.fast_clk_n_i 	( io_clk_n_i ),
  .enable_i	( iserdes_enable_i ), 	.bitslip_i 	( iserdes_bitslip_i ) );
qdrii_wb_phys_iserdes #( 	.CLOCKS_RATIO (CLOCKS_RATIO),
	.iodelay_group_name ( iodelay_group_name ),	.chan_delay ( data4_delay )
) dinp04_inst (
  .dinp_i	( qdr2_data_i[4] ), 	.dout_o 	( dinp04_o ),
  .slow_rst_i	( iserdes_reset_i ), 	.slow_clk_i	( io_divclk_i ),
  .fast_clk_p_i ( io_clk_i ),		.fast_clk_n_i 	( io_clk_n_i ),
  .enable_i	( iserdes_enable_i ),	.bitslip_i 	( iserdes_bitslip_i ) );
qdrii_wb_phys_iserdes #( 	.CLOCKS_RATIO (CLOCKS_RATIO),
	.iodelay_group_name ( iodelay_group_name ),	.chan_delay ( data5_delay )
) dinp05_inst (
  .dinp_i	( qdr2_data_i[5] ), 	.dout_o 	( dinp05_o ),
  .slow_rst_i	( iserdes_reset_i ), 	.slow_clk_i	( io_divclk_i ),
  .fast_clk_p_i ( io_clk_i ), 		.fast_clk_n_i 	( io_clk_n_i ),
  .enable_i	( iserdes_enable_i ),	.bitslip_i 	( iserdes_bitslip_i ) );
qdrii_wb_phys_iserdes #(	.CLOCKS_RATIO (CLOCKS_RATIO),
	.iodelay_group_name ( iodelay_group_name ),	.chan_delay ( data6_delay )
) dinp06_inst (
  .dinp_i	( qdr2_data_i[6] ), 	.dout_o 	( dinp06_o ),
  .slow_rst_i	( iserdes_reset_i ), 	.slow_clk_i	( io_divclk_i ),
  .fast_clk_p_i ( io_clk_i ),		.fast_clk_n_i 	( io_clk_n_i ),
  .enable_i	( iserdes_enable_i ), 	.bitslip_i 	( iserdes_bitslip_i ) );
qdrii_wb_phys_iserdes #(	.CLOCKS_RATIO (CLOCKS_RATIO),
	.iodelay_group_name ( iodelay_group_name ),	.chan_delay ( data7_delay )
) dinp07_inst (
  .dinp_i	( qdr2_data_i[7] ), 	.dout_o 	( dinp07_o ),
  .slow_rst_i	( iserdes_reset_i ), 	.slow_clk_i	( io_divclk_i ),
  .fast_clk_p_i	( io_clk_i), 		.fast_clk_n_i 	( io_clk_n_i ),
  .enable_i	( iserdes_enable_i ), 	.bitslip_i 	( iserdes_bitslip_i ) );
qdrii_wb_phys_iserdes #(	.CLOCKS_RATIO (CLOCKS_RATIO),
	.iodelay_group_name ( iodelay_group_name ),	.chan_delay ( data8_delay )
) dinp08_inst (
  .dinp_i	( qdr2_data_i[8]), 	.dout_o 	( dinp08_o ),
  .slow_rst_i	( iserdes_reset_i ), 	.slow_clk_i	( io_divclk_i ),
  .fast_clk_p_i ( io_clk_i ), 		.fast_clk_n_i 	( io_clk_n_i ),
  .enable_i	( iserdes_enable_i ),	.bitslip_i 	( iserdes_bitslip_i ) );
////////////////////////////
// Echo Clock iserdeses  //
//////////////////////////
qdrii_wb_phys_iserdes #(	.CLOCKS_RATIO (CLOCKS_RATIO), 
	.iodelay_group_name ( iodelay_group_name ),	.chan_delay ( echo_clk_p_delay )
) echo_clk_p_inst (
  .dinp_i	( qdr2_echo_clk_p_i ),	.dout_o		( echo_clk_p_o ),
  .slow_rst_i	( iserdes_reset_i ),	.slow_clk_i     ( io_divclk_i ),
  .fast_clk_p_i ( io_clk_i ),		.fast_clk_n_i   ( io_clk_n_i ),
  .enable_i	( iserdes_enable_i ),	.bitslip_i	( iserdes_bitslip_i ) );
qdrii_wb_phys_iserdes #(	.CLOCKS_RATIO (CLOCKS_RATIO),
	.iodelay_group_name ( iodelay_group_name ),	.chan_delay ( echo_clk_n_delay )
) echo_clk_n_inst (
  .dinp_i	( qdr2_echo_clk_n_i ),	.dout_o		( echo_clk_n_o ),
  .slow_rst_i	( iserdes_reset_i ),	.slow_clk_i	( io_divclk_i ),
  .fast_clk_p_i	( io_clk_i ),		.fast_clk_n_i	( io_clk_n_i ),
  .enable_i     ( iserdes_enable_i ),	.bitslip_i	( iserdes_bitslip_i ) );
endmodule
