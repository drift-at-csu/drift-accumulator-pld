/* This is a wrapper for Xilinx oserdese2 primitive */
`timescale 1ns/1ps

module qdrii_wb_phys_oserdes #(
	parameter	CLOCKS_RATIO = 4 // Ratio of fast to slow clocks
)(
	//---> Controller Parallelized outputs
	input wire [CLOCKS_RATIO-1:0] 	dinp_i,
	output wire			dout_o,
	input wire [CLOCKS_RATIO-1:0]	mask_i,
	// reset is in QDR slow clock domain
	input wire slow_rst_i,
	input wire slow_clk_i,
	input wire slow_enable_i,
	input wire fast_clk_i
);
wire obuf_tristate_control;
wire obuf_data_out;

OSERDESE2 #(
	.DATA_RATE_OQ("DDR"),		// DDR, SDR
        .DATA_RATE_TQ("DDR"),		// DDR, BUF, SDR
	.DATA_WIDTH(CLOCKS_RATIO),	// Parallel data width (2-8,10,14)
	.INIT_OQ(1'b1),			// Initial value of OQ output (1'b0,1'b1)
	.INIT_TQ(1'b1),			// Initial value of TQ output (1'b0,1'b1)
	.SERDES_MODE("MASTER"),		// MASTER, SLAVE
	.SRVAL_OQ(1'b1),		// OQ output value when SR is used (1'b0,1'b1)
	.SRVAL_TQ(1'b1),		// TQ output value when SR is used (1'b0,1'b1)
	.TBYTE_CTL("FALSE"),		// Enable tristate byte operation (FALSE, TRUE)
	.TBYTE_SRC("FALSE"),		// Tristate byte source (FALSE, TRUE)
	.TRISTATE_WIDTH(CLOCKS_RATIO)		// 3-state converter width (1,4)
)
oserdes_inst (
	.OFB(  ),		// 1-bit output: Feedback path for data
	.OQ (obuf_data_out),		// 1-bit output: Data path output
	// SHIFTOUT1 / SHIFTOUT2: 1-bit (each) output: Data output expansion (1-bit each)
	.SHIFTOUT1(  ),
	.SHIFTOUT2(  ),
	.TBYTEOUT(  ),	// 1-bit output: Byte group tristate
	.TFB(  ),		// 1-bit output: 3-state control for fabric
	.TQ( obuf_tristate_control ),		// 1-bit output: 3-state control iob
	.CLK( fast_clk_i ),		// 1-bit input: High speed clock
	.CLKDIV( slow_clk_i ),	// 1-bit input: Divided clock
	// D1 - D8: 1-bit (each) input: Parallel data inputs (1-bit each)
	// D1 will appear first
	.D1( dinp_i[0] ),
	.D2( dinp_i[1] ),
	.D3( dinp_i[2] ),
	.D4( dinp_i[3] ),
	.D5( 1'b0 ),
	.D6( 1'b0 ),
	.D7( 1'b0 ),
	.D8( 1'b0 ),
	.OCE( slow_enable_i ),		// 1-bit input: Output data clock enable
	.RST( slow_rst_i ),		// 1-bit input: Reset
	// SHIFTIN1 / SHIFTIN2: 1-bit (each) input: Data input expansion (1-bit each)
	.SHIFTIN1(1'b0),
	.SHIFTIN2(1'b0),
	// T1 - T4: 1-bit (each) input: Parallel 3-state inputs
	.T1( mask_i[0] ),
	.T2( mask_i[1] ),
	.T3( mask_i[2] ),
	.T4( mask_i[3] ),
	.TBYTEIN( 1'b0 ),		// 1-bit input: Byte group tristate
	.TCE( slow_enable_i )		// 1-bit input: 3-state clock enable
);

OBUFT #(
	.DRIVE(4),   // Specify the output drive strength
        .IOSTANDARD("DEFAULT"), // Specify the output I/O standard
	.SLEW("SLOW") // Specify the output slew rate
) obuffer (
	.O(dout_o),	// Buffer output (connect directly to top-level port)
	.I(obuf_data_out),	// Buffer input
	.T(obuf_tristate_control)	// 3-state enable input 
);

endmodule
