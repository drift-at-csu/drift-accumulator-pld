/*
 *  QDR 2 Controller, responsible for reading data from FIFOs and
 *  puting it into the QDR WRITE lines, also responsible for taking
 *  the readout data from QDR lines and putting them into the READ FIFO
 *
 *             Async FIFOs                qdr_clk
 *                                        domain
 *  --------------    ----------------------------------
 *  wb0_clk
 *  domain
 *             -----------            -------------------
 *   <==       |Read_data|     <==    |                 |
 *             -----------            |                 |
 *             -----------            |                 |
 *   ==>       |Read_addr|     ==>    |                 |
 *             -----------            |                 | 
 *                                    |                 |
 *            ------------            |                 |
 *            |Write_data|            |                 |
 *   ==>      ------------     ==>    |                 |
 *            |Write_addr|            |                 |
 *            ------------            | QDR2 Controller |
 *  --------------                    |                 |
 *  wb1_clk                           |                 |
 *  domain                            |                 |
 *             -----------            |                 |
 *   <==       |Read_data|     <==    |                 |
 *             -----------            |                 |
 *             -----------            |                 |
 *   ==>       |Read_addr|     ==>    |                 |
 *             -----------            |                 | 
 *                                    |                 |
 *            ------------            |                 |
 *            |Write_data|            |                 |
 *   ==>      ------------     ==>    |                 |
 *            |Write_addr|            |                 |
 *            ------------            -------------------
 *
 */
`timescale 1ns/1ps

module qdrii_wb_controller
#(parameter
	CLOCKS_RATIO = 4 // Ratio of fast to slow clock freq
)(
	//--------> Port 0
	// READ FIFO, Port 0  Interface
	output wire [31:0] 	rd0_data_o,
	output wire		rd0_data_push_o, // Push data to fifo
	input  wire		rd0_data_full_i,

	input  wire [20:0]	rd0_addr_i,
	output wire		rd0_addr_pull_o, // Pull address from fifo
	input  wire		rd0_addr_empty_i,
	// Write FIFO, Port 0 Interface
	input  wire [20:0]	wr0_addr_i,
	input  wire [3:0]	wr0_bsel_i,
	input  wire [31:0]	wr0_data_i,
	output wire		wr0_pull_o, // Pull data, addr and bsel from fifo
	input  wire		wr0_empty_i, // if 1 indicates that write buffer is empty
	//----------> Port 1
	// READ FIFO, Port 1  Interface
	output wire [31:0]	rd1_data_o,
	output wire		rd1_data_push_o, // Push data to fifo
	input  wire		rd1_data_full_i,

	input  wire [20:0]	rd1_addr_i,
	output wire		rd1_addr_pull_o, // Pull address from fifo
	input  wire		rd1_addr_empty_i,
	// Write FIFO, Port 1 Interface
	input  wire [20:0]	wr1_addr_i,
	input  wire [3:0]	wr1_bsel_i,
	input  wire [31:0]	wr1_data_i,
	output wire		wr1_pull_o, // Pull data, addr and bsel from fifo
	input  wire		wr1_empty_i, // if 1, indicates that the write buffer is empty

	//--->Physical interface
	output wire [CLOCKS_RATIO-1:0] addr00_o,
	output wire [CLOCKS_RATIO-1:0] addr01_o,
	output wire [CLOCKS_RATIO-1:0] addr02_o,
	output wire [CLOCKS_RATIO-1:0] addr03_o,
	output wire [CLOCKS_RATIO-1:0] addr04_o,
	output wire [CLOCKS_RATIO-1:0] addr05_o,
	output wire [CLOCKS_RATIO-1:0] addr06_o,
	output wire [CLOCKS_RATIO-1:0] addr07_o,
	output wire [CLOCKS_RATIO-1:0] addr08_o,
	output wire [CLOCKS_RATIO-1:0] addr09_o,
	output wire [CLOCKS_RATIO-1:0] addr10_o,
	output wire [CLOCKS_RATIO-1:0] addr11_o,
	output wire [CLOCKS_RATIO-1:0] addr12_o,
	output wire [CLOCKS_RATIO-1:0] addr13_o,
	output wire [CLOCKS_RATIO-1:0] addr14_o,
	output wire [CLOCKS_RATIO-1:0] addr15_o,
	output wire [CLOCKS_RATIO-1:0] addr16_o,
	output wire [CLOCKS_RATIO-1:0] addr17_o,
	output wire [CLOCKS_RATIO-1:0] addr18_o,
	output wire [CLOCKS_RATIO-1:0] addr19_o,
	output wire [CLOCKS_RATIO-1:0] addr20_o,
	output wire [CLOCKS_RATIO-1:0] addr21_o,

	output wire [CLOCKS_RATIO-1:0] dout00_o,
	output wire [CLOCKS_RATIO-1:0] dout01_o,
	output wire [CLOCKS_RATIO-1:0] dout02_o,
	output wire [CLOCKS_RATIO-1:0] dout03_o,
	output wire [CLOCKS_RATIO-1:0] dout04_o,
	output wire [CLOCKS_RATIO-1:0] dout05_o,
	output wire [CLOCKS_RATIO-1:0] dout06_o,
	output wire [CLOCKS_RATIO-1:0] dout07_o,
	output wire [CLOCKS_RATIO-1:0] dout08_o,

	output wire [CLOCKS_RATIO-1:0] rps_o,
	output wire [CLOCKS_RATIO-1:0] wps_o,
	output wire [CLOCKS_RATIO-1:0] bws_o,

	input wire [CLOCKS_RATIO-1:0] dinp00_i,
	input wire [CLOCKS_RATIO-1:0] dinp01_i,
	input wire [CLOCKS_RATIO-1:0] dinp02_i,
	input wire [CLOCKS_RATIO-1:0] dinp03_i,
	input wire [CLOCKS_RATIO-1:0] dinp04_i,
	input wire [CLOCKS_RATIO-1:0] dinp05_i,
	input wire [CLOCKS_RATIO-1:0] dinp06_i,
	input wire [CLOCKS_RATIO-1:0] dinp07_i,
	input wire [CLOCKS_RATIO-1:0] dinp08_i,

	input wire [CLOCKS_RATIO-1:0] echo_clk_p_i,
	input wire [CLOCKS_RATIO-1:0] echo_clk_n_i,
	//---> Iserdeses Control Signal
	output wire iserdes_enable_o,
	output wire iserdes_reset_o,
	output wire iserdes_bitslip_o,
	//---> Oserdeses Control Signals
	output wire oserdes_enable_o,
	output wire oserdes_reset_o,
	// If the output_mask is 1, then drive the output lines
	// (ADDR, DATAO ), if 0, then put lines to Zs
	output wire [CLOCKS_RATIO-1:0] address_mask_o,
	output wire [CLOCKS_RATIO-1:0] data_o_mask_o,
	output wire [CLOCKS_RATIO-1:0] bws_mask_o,
	//--->Clock and reset are in QDR slow clock domain (125 MhZ)
	input wire rst_i,
	input wire clk_i,
	input wire clk_n_i
);

  // When 1, indicates that the valid read operation is in progress
  wire read_valid_wire;
  assign read_valid_wire = ~(rd1_addr_empty_i & rd0_addr_empty_i);
  reg  [5:0] read_valid_sreg = 6'b000000;
  always @ (posedge clk_n_i) begin
  	if (rst_i) begin
		read_valid_sreg <= 6'b000000;
	end else begin
		read_valid_sreg <= {read_valid_sreg[4:0], read_valid_wire};
	end
  end
  // When 1, indicates that pending read is from Port 1,
  // When 0, it's from Port 0
  wire port_read_wire;
  assign port_read_wire = ~rd1_addr_empty_i;
  reg  [5:0] port_read_sreg = 6'b000000;
  always @ (posedge clk_n_i) begin
  	if (rst_i) begin
		port_read_sreg <= 6'b000000;
	end else begin
		port_read_sreg <= {port_read_sreg[4:0], port_read_wire};
	end
  end

  // Pulling the write fifos:
  assign wr0_pull_o = ((~wr0_empty_i) & wr1_empty_i)? 1'b1:1'b0;
  assign wr1_pull_o = wr1_empty_i ? 1'b0:1'b1;
  wire write_valid_wire; 
  assign write_valid_wire = wr0_pull_o | wr1_pull_o;
  // Buffering them is useful
  reg  [2:0] write_valid_sreg = 3'b000;
  reg  [2:0] wr0_valid_sreg = 3'b000;
  reg  [2:0] wr1_valid_sreg = 3'b000;
  always @ (posedge clk_n_i) begin
  	if (rst_i) begin
		write_valid_sreg <= 3'b000;
		wr0_valid_sreg <= 3'b000;
		wr1_valid_sreg <= 3'b000;
	end else begin
		write_valid_sreg <= {write_valid_sreg[1:0], write_valid_wire};
		wr0_valid_sreg <= {wr0_valid_sreg[1:0], wr0_pull_o};
		wr1_valid_sreg <= {wr1_valid_sreg[1:0], wr1_pull_o};
	end
  end
  	
  // WPS and RPS signals are valid only on rising edge of K clock
  // therefore if we want to write 4 bytes, we'll have to put all ones here
  // If not, then all zeros
  assign wps_o = (write_valid_sreg[0])?
			(4'b0000): // writing operation, writing
			(4'b1111); // no writing operation, not writing
  // byte mask is inverted, comparing to wishbone
  assign bws_o = (wr0_valid_sreg[0])? 
  			(~wr0_bsel_i) : 
		((wr1_valid_sreg[0])?
			(~wr1_bsel_i) :
			(4'b1111 )); // no writing operation

  // Assigning RPSs
  assign rps_o = (read_valid_sreg[0])?
  			(4'b0000): // reading operation request, reading
			(4'b1111); // no reading request,not reading


  // Now let's assign read Addresses
  wire [21:1] rd_addr_base; 
  assign rd_addr_base = (read_valid_sreg[0] & port_read_sreg[0])?
	(rd1_addr_i[20:0]):(rd0_addr_i[20:0]);

  // and write addresses
  wire [21:1] wr_addr_base;
  assign wr_addr_base = (wr1_valid_sreg[0])?(wr1_addr_i[20:0]):(wr0_addr_i[20:0]);

  // Propagate assigned addresses to the output
  // The addresing of the Memory goes as half-word address
  assign addr00_o[0] = 1'b0;  assign addr00_o[1] = 1'b0; // read(byte0); write(byte0)
  assign addr00_o[2] = 1'b1;  assign addr00_o[3] = 1'b1; // read(byte1); write(byte1)

  assign addr01_o[0] = rd_addr_base[1];  assign addr01_o[1] = wr_addr_base[1];
  assign addr01_o[2] = rd_addr_base[1];  assign addr01_o[3] = wr_addr_base[1];

  assign addr02_o[0] = rd_addr_base[2];  assign addr02_o[1] = wr_addr_base[2];
  assign addr02_o[2] = rd_addr_base[2];  assign addr02_o[3] = wr_addr_base[2];

  assign addr03_o[0] = rd_addr_base[3];  assign addr03_o[1] = wr_addr_base[3];
  assign addr03_o[2] = rd_addr_base[3];  assign addr03_o[3] = wr_addr_base[3];

  assign addr04_o[0] = rd_addr_base[4];  assign addr04_o[1] = wr_addr_base[4];
  assign addr04_o[2] = rd_addr_base[4];  assign addr04_o[3] = wr_addr_base[4];

  assign addr05_o[0] = rd_addr_base[5];  assign addr05_o[1] = wr_addr_base[5];
  assign addr05_o[2] = rd_addr_base[5];  assign addr05_o[3] = wr_addr_base[5];

  assign addr06_o[0] = rd_addr_base[6];  assign addr06_o[1] = wr_addr_base[6];
  assign addr06_o[2] = rd_addr_base[6];  assign addr06_o[3] = wr_addr_base[6];

  assign addr07_o[0] = rd_addr_base[7];  assign addr07_o[1] = wr_addr_base[7];
  assign addr07_o[2] = rd_addr_base[7];  assign addr07_o[3] = wr_addr_base[7];

  assign addr08_o[0] = rd_addr_base[8];  assign addr08_o[1] = wr_addr_base[8];
  assign addr08_o[2] = rd_addr_base[8];  assign addr08_o[3] = wr_addr_base[8];

  assign addr09_o[0] = rd_addr_base[9];  assign addr09_o[1] = wr_addr_base[9];
  assign addr09_o[2] = rd_addr_base[9];  assign addr09_o[3] = wr_addr_base[9];

  assign addr10_o[0] = rd_addr_base[10]; assign addr10_o[1] = wr_addr_base[10];
  assign addr10_o[2] = rd_addr_base[10]; assign addr10_o[3] = wr_addr_base[10];

  assign addr11_o[0] = rd_addr_base[11]; assign addr11_o[1] = wr_addr_base[11];
  assign addr11_o[2] = rd_addr_base[11]; assign addr11_o[3] = wr_addr_base[11];

  assign addr12_o[0] = rd_addr_base[12]; assign addr12_o[1] = wr_addr_base[12];
  assign addr12_o[2] = rd_addr_base[12]; assign addr12_o[3] = wr_addr_base[12];

  assign addr13_o[0] = rd_addr_base[13]; assign addr13_o[1] = wr_addr_base[13];
  assign addr13_o[2] = rd_addr_base[13]; assign addr13_o[3] = wr_addr_base[13];

  assign addr14_o[0] = rd_addr_base[14]; assign addr14_o[1] = wr_addr_base[14];
  assign addr14_o[2] = rd_addr_base[14]; assign addr14_o[3] = wr_addr_base[14];

  assign addr15_o[0] = rd_addr_base[15]; assign addr15_o[1] = wr_addr_base[15];
  assign addr15_o[2] = rd_addr_base[15]; assign addr15_o[3] = wr_addr_base[15];

  assign addr16_o[0] = rd_addr_base[16]; assign addr16_o[1] = wr_addr_base[16];
  assign addr16_o[2] = rd_addr_base[16]; assign addr16_o[3] = wr_addr_base[16];

  assign addr17_o[0] = rd_addr_base[17]; assign addr17_o[1] = wr_addr_base[17];
  assign addr17_o[2] = rd_addr_base[17]; assign addr17_o[3] = wr_addr_base[17];

  assign addr18_o[0] = rd_addr_base[18]; assign addr18_o[1] = wr_addr_base[18];
  assign addr18_o[2] = rd_addr_base[18]; assign addr18_o[3] = wr_addr_base[18];

  assign addr19_o[0] = rd_addr_base[19]; assign addr19_o[1] = wr_addr_base[19];
  assign addr19_o[2] = rd_addr_base[19]; assign addr19_o[3] = wr_addr_base[19];

  assign addr20_o[0] = rd_addr_base[20]; assign addr20_o[1] = wr_addr_base[20];
  assign addr20_o[2] = rd_addr_base[20]; assign addr20_o[3] = wr_addr_base[20];

  assign addr21_o[0] = rd_addr_base[21]; assign addr21_o[1] = wr_addr_base[21];
  assign addr21_o[2] = rd_addr_base[21]; assign addr21_o[3] = wr_addr_base[21];

  // Let's choose which data to read:
  wire [31:0] wr_data_word;
  assign wr_data_word = (wr1_valid_sreg[0])?(wr1_data_i[31:0]):(wr0_data_i[31:0]);

  // Assigning data bytes to write:
  assign dout00_o[0] = wr_data_word[0];  assign dout00_o[1] = wr_data_word[8];
  assign dout00_o[2] = wr_data_word[16]; assign dout00_o[3] = wr_data_word[24];

  assign dout01_o[0] = wr_data_word[1];  assign dout01_o[1] = wr_data_word[9];
  assign dout01_o[2] = wr_data_word[17]; assign dout01_o[3] = wr_data_word[25];

  assign dout02_o[0] = wr_data_word[2];  assign dout02_o[1] = wr_data_word[10];
  assign dout02_o[2] = wr_data_word[18]; assign dout02_o[3] = wr_data_word[26];

  assign dout03_o[0] = wr_data_word[3];  assign dout03_o[1] = wr_data_word[11];
  assign dout03_o[2] = wr_data_word[19]; assign dout03_o[3] = wr_data_word[27];

  assign dout04_o[0] = wr_data_word[4];  assign dout04_o[1] = wr_data_word[12];
  assign dout04_o[2] = wr_data_word[20]; assign dout04_o[3] = wr_data_word[28];

  assign dout05_o[0] = wr_data_word[5];  assign dout05_o[1] = wr_data_word[13];
  assign dout05_o[2] = wr_data_word[21]; assign dout05_o[3] = wr_data_word[29];

  assign dout06_o[0] = wr_data_word[6];  assign dout06_o[1] = wr_data_word[14];
  assign dout06_o[2] = wr_data_word[22]; assign dout06_o[3] = wr_data_word[30];

  assign dout07_o[0] = wr_data_word[7];  assign dout07_o[1] = wr_data_word[15];
  assign dout07_o[2] = wr_data_word[23]; assign dout07_o[3] = wr_data_word[31];
  // TODO: For now bit number eight is not used... so let's use it for
  // reverse data clock monitoring with the oscilloscope should be
  // 1001 or 0110 pattern.
  /*
  assign dout08_o[0] = echo_clk_p_i[0];	 assign dout08_o[1] = echo_clk_p_i[1];
  assign dout08_o[2] = echo_clk_n_i[2];  assign dout08_o[3] = echo_clk_n_i[3];
  */

  // We want to pull FIFOs at the right time, so
  assign rd0_addr_pull_o = ( read_valid_wire & (~port_read_wire)) ? 1'b1:1'b0;
  assign rd1_addr_pull_o = ( read_valid_wire & port_read_wire ) ? 1'b1:1'b0;

  //--------------------> Let's Push the input data if we have it
  assign rd0_data_push_o = read_valid_sreg[4] & (~port_read_sreg[4]);
  assign rd1_data_push_o = read_valid_sreg[4] & ( port_read_sreg[4]);
  //--------------------> We can do cont assignment for both ports
  wire [31:0] dinp_wire; 
  assign dinp_wire[31:24]={ dinp07_i[0], dinp06_i[0], dinp05_i[0], dinp04_i[0],
  			    dinp03_i[0], dinp02_i[0], dinp01_i[0], dinp00_i[0] };
  assign dinp_wire[23:16]={ dinp07_i[1], dinp06_i[1], dinp05_i[1], dinp04_i[1],
			    dinp03_i[1], dinp02_i[1], dinp01_i[1], dinp00_i[1] };
  assign dinp_wire[15:8]= { dinp07_i[2], dinp06_i[2], dinp05_i[2], dinp04_i[2],
			    dinp03_i[2], dinp02_i[2], dinp01_i[2], dinp00_i[2] }; 
  assign dinp_wire[7:0]=  { dinp07_i[3], dinp06_i[3], dinp05_i[3], dinp04_i[3],
			    dinp03_i[3], dinp02_i[3], dinp01_i[3], dinp00_i[3] };
  
  //---> AVD: For testing purposes, let's return clock, instead of the data //
  /*
  assign dinp_wire[7:0]=  { echo_clk_p_i[0], echo_clk_p_i[1], echo_clk_p_i[2], echo_clk_p_i[3],
  			    echo_clk_n_i[0], echo_clk_n_i[1], echo_clk_n_i[2], echo_clk_n_i[3] };
  */

  assign rd0_data_o = dinp_wire;
  assign rd1_data_o = dinp_wire;


  //---------> We want to enable iserdeses:
  reg [3:0] oserdes_reset_sreg = 4'b1111;
  always @ (posedge clk_i) begin
  	if (rst_i) begin
		oserdes_reset_sreg <= 4'b1111;
	end else begin
		oserdes_reset_sreg <= {oserdes_reset_sreg[2:0], 1'b0};
	end
  end
  assign oserdes_reset_o = oserdes_reset_sreg[3];

  reg [15:0] oserdes_enable_sreg = 16'h0000;
  always @ (posedge clk_i) begin
	if (oserdes_reset_o) begin
		oserdes_enable_sreg <= 16'h0000;
	end else begin
		oserdes_enable_sreg <= {oserdes_enable_sreg[14:0], 1'b1}; 
	end
  end
  assign oserdes_enable_o = oserdes_enable_sreg[15];

  reg [3:0] iserdes_reset_sreg = 4'b1111;
  always @ (posedge clk_i) begin
  	if ( !iserdes_enable_o ) begin
		iserdes_reset_sreg <= 4'b1111;
	end else begin
		iserdes_reset_sreg <= {iserdes_reset_sreg[2:0], 1'b0};
	end
  end
  assign iserdes_reset_o = iserdes_reset_sreg[3];

  reg [3:0] iserdes_enable_sreg = 4'b0000;
  always @ (posedge clk_i) begin
  	if (rst_i) begin
		iserdes_enable_sreg <= 4'b0000;
	end else begin
		iserdes_enable_sreg <= {iserdes_enable_sreg[2:0], 1'b1};
	end
  end
  assign iserdes_enable_o = iserdes_enable_sreg[3];

  //--------> The data from the QDR memory is expected to have 1.5 Clock
  //cycle latency. Therefore without bitslip we can't delay input data
  // Therefore we want to pulse bitslip once after iserdeses comes from reset
  // AND iserdeses are enabled.
  reg [15:0] iserdes_bitslip_sreg = 16'h0000;
  always @ (posedge clk_i) begin
  	if ( iserdes_reset_o ) begin
		iserdes_bitslip_sreg <= 16'h0000;
	end else begin
		iserdes_bitslip_sreg <= {iserdes_bitslip_sreg[14:0], 1'b1 };
	end
  end
  assign iserdes_bitslip_o = iserdes_bitslip_sreg[15:14] == 2'b01;
  // assign iserdes_bitslip_o = 1'b0;

  //--------> Masks controls are important for reducing the power consumption
  // read_valid_sreg[0] // <-- if we have valid read request
  // write_valid_sreg[0] // <-- if we have valid write request
  // 0 bit is first read 1 is first write 2 is second read 3 is second write
  assign address_mask_o [0] = ( read_valid_sreg[0])? 1'b0:1'b1;
  assign address_mask_o [1] = (write_valid_sreg[0])? 1'b0:1'b1;
  assign address_mask_o [2] = ( read_valid_sreg[0])? 1'b0:1'b1;
  assign address_mask_o [3] = (write_valid_sreg[0])? 1'b0:1'b1;
  // For data output if we have a valid write, then assign it according to bws
  // rules
  assign data_o_mask_o  = (write_valid_sreg[0])? (bws_o) : 4'b1111;
  assign bws_mask_o = (write_valid_sreg[0])? 4'b0000 : 4'b1111;

endmodule
