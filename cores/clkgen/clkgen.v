`timescale 1ps/1ps
module clkgen (
  // Output for ADC sample clock (40 MHz)
  output wire adcs_clk_o,	// Sample clock, 40 MHz

  // Output for QDR memory DDR clock (250 MHz
  output wire mems_clk_o,	// Clock, 250 MHz, DDR
  output wire mems_rst_o,	// Sync reset

  // Wishbone slow clock (100 MHz)
  output wire wb_clk_o,
  // Synchronous (wb_clk_o) reset
  output wire wb_rst_o,

  // Wishbone fast clock, used for ABUS (200 MHz)
  output wire wbfast_clk_o,
  // Synchronous (wbfast_clk_o) reset
  output wire wbfast_rst_o,
  
  // Main Input clock, 100 MHz
  input wire sys_clk_i, // Main Input clock, 100 MHz

  // This is user configuration clock input from STARTUPE2
  input wire  cclock_i
);
//===================================================================//
//////////////////////////////////////////////////
//// Start Up Instance                         //
////////////////////////////////////////////////
wire mmcm0_locked;
wire global_reset;

assign global_reset = !mmcm0_locked;
STARTUPE2 #(
	.PROG_USR("FALSE"),	// Activate program event security feature. Requires encrypted bitstreams.
	.SIM_CCLK_FREQ(0.0)	// Set the Configuration Clock Frequency(ns) for simulation.
) startup0 (
	.CFGCLK( ),	// 1-bit output: Configuration main clock output
	.CFGMCLK( ),	// 1-bit output: Configuration internal oscillator clock output
	.EOS( ),	// 1-bit output: Active high output signal indicating the End Of Startup.
	.PREQ(  ),	// 1-bit output: PROGRAM request to fabric output
	.CLK(1'b0),	// 1-bit input: User start-up clock input
	.GSR( global_reset ), // 1-bit input: Global Set/Reset input
	.GTS(1'b0 ),    // 1-bit input: Global 3-state input
	.KEYCLEARB(1'b0), // 1-bit input: Clear AES Decrypter Key input from Battery-Backed RAM (BBRAM)
	.PACK(1'b1),    // 1-bit input: PROGRAM acknowledge input
	.USRCCLKO(cclock_i),       // 1-bit input: User CCLK input
	.USRCCLKTS(1'b0),       // 1-bit input: User CCLK 3-state enable input
	.USRDONEO(1'b1),        // 1-bit input: User DONE pin output control
	.USRDONETS(1'b1)  // 1-bit input: User DONE 3-state enable output
);
//---> For Wishbone and ADC clocks

wire mmcm0_feedback;

wire adcs_clk_unbuf;
wire adcs_delay_calib_clk_unbuf;
wire adcs_delay_calib_clk;

wire mems_clk_unbuf;
wire wb_clk_unbuf;
wire wbfast_clk_unbuf;
MMCME2_BASE #(
	.BANDWIDTH ("OPTIMIZED"), 
        .CLKFBOUT_MULT_F (10.000),	//  This means that at 10.000 VCO runs at 1000MHz
	.CLKFBOUT_PHASE	 (0.0),
	.CLKIN1_PERIOD	 (10.000),	// Input clock period is 100MHz => 10ns period

	.CLKOUT1_DIVIDE(25),	// ADC clock needs a 40 MhZ = 1000/25
	.CLKOUT2_DIVIDE(20),	// Wishbone clock at 50 MHz = 1000/20
	.CLKOUT3_DIVIDE(17),	// Wishbone fast clock at 100 MHz (Drop it to 58.82 MHz)
	.CLKOUT4_DIVIDE(5),	// ADCs delay calibration clock at 200MHz = 1000/5
	.CLKOUT5_DIVIDE(1),	
	.CLKOUT6_DIVIDE(1),
	.CLKOUT0_DIVIDE_F(4.0), 	// Mem clock need a 250 MhZ = 1000/4.0

	.CLKOUT0_DUTY_CYCLE(0.5),
	.CLKOUT1_DUTY_CYCLE(0.5),
	.CLKOUT2_DUTY_CYCLE(0.5),
	.CLKOUT3_DUTY_CYCLE(0.5),
	.CLKOUT4_DUTY_CYCLE(0.5),
	.CLKOUT5_DUTY_CYCLE(0.5),
	.CLKOUT6_DUTY_CYCLE(0.5),

	.CLKOUT0_PHASE(0.0),
	.CLKOUT1_PHASE(0.0),
	.CLKOUT2_PHASE(0.0),
	.CLKOUT3_PHASE(0.0),
	.CLKOUT4_PHASE(0.0),
	.CLKOUT5_PHASE(0.0),
	.CLKOUT6_PHASE(0.0),

	.CLKOUT4_CASCADE("FALSE"), // Cascase CLKOUT4 counter with CLKOUT6 (FALSE, TRUE)
	.DIVCLK_DIVIDE(1),         // Master division value (1-106)
	.REF_JITTER1(0.0),         // Reference input jitter in UI (0.000-0.999).
	.STARTUP_WAIT("FALSE")     // Delays DONE until MMCM is locked (FALSE, TRUE)
  ) clk_gen1_inst (
	.CLKOUT0	( mems_clk_unbuf ),  	// QDRs clocks 250MHz, unbuffered
	.CLKOUT0B	( ),
	.CLKOUT1	( adcs_clk_unbuf ), 	// ADCs clocks, 40MHz, unbuffered
	.CLKOUT1B	( ),
	.CLKOUT2	( wb_clk_unbuf ), 	// Wishbone clock, 100MHz, unbuffered
	.CLKOUT2B	( ),
	.CLKOUT3	( wbfast_clk_unbuf ),	// Fast Wishbone clock, 200MHz, unbuffered
	.CLKOUT3B 	( ),
	.CLKOUT4	( adcs_delay_calib_clk_unbuf ), // Delay calibration clock for ADCs
	.CLKOUT5	( ),
	.CLKOUT6	( ),
	.CLKFBOUT	( mmcm0_feedback ),
	.CLKFBOUTB	( ),
	.LOCKED		( mmcm0_locked ),
	.CLKIN1		( sys_clk_i ),
	.PWRDWN		( 1'b0 ),
	.RST		( 1'b0 ),
	.CLKFBIN	( mmcm0_feedback )
  );

// Let's buffer adc_clk_o (global clock)
BUFG adcs_clk_buffer (
	.O(adcs_clk_o), 	// 1-bit output: Clock output
	.I(adcs_clk_unbuf)	// 1-bit input: Clock input
);
// Let's also put a global clock buffer on memory clock
BUFG mems_clk_buffer (
	.O(mems_clk_o),	// 1-bit output: Clock output
	.I(mems_clk_unbuf)	// 1-bit input: Clock input
);
// Buffering wishbone clock
BUFG wb_clk_buffer (
	.O(wb_clk_o),		// 1-bit output: Clock output
	.I(wb_clk_unbuf )	// 1-bit input: Clock input
);
// Buffering fast wishbone clock
BUFG wbfast_clk_buffer (
	.O(wbfast_clk_o),           // 1-bit output: Clock output
	.I(wbfast_clk_unbuf )       // 1-bit input: Clock input
);
// Buffering ADC calibration clock adcs_delay_calib_clk_unbuf
BUFG adcs_delay_calib_clk_buffer (
	.O(adcs_delay_calib_clk),           // 1-bit output: Clock output
	.I(adcs_delay_calib_clk_unbuf )       // 1-bit input: Clock input
);
/////////////////////////////////
//  Wishbone reset generation //
///////////////////////////////
reg [15:0] wbfast_rst_reg = 16'hFFFF;
always @(posedge wbfast_clk_o)
	wbfast_rst_reg <= {wbfast_rst_reg[14:0], global_reset};
assign wbfast_rst_o = wbfast_rst_reg[15];

assign mems_rst_o = global_reset; // wb_rst_o;
assign wb_rst_o = global_reset;


///////////////////////////////////////////////////
// ADCs are using delays, therefore controller  //
// for them goes here                          //
////////////////////////////////////////////////
(* IODELAY_GROUP = "adcX_delay_group" *)
IDELAYCTRL adcs_delay_calibrator (
	.RDY	( ),			// 1-bit output: Ready output
	.REFCLK	( adcs_delay_calib_clk  ),	// 1-bit input: Reference clock input
	.RST	( 1'b0 )		// 1-bit input: Active high reset input
);
endmodule // clkgen
