`timescale 1ps / 1ps

module tb_qdrii_controller;

//////////////////////////////////////////////
// Parameters for cti and bte burst cycles //
////////////////////////////////////////////

// bte:
parameter [1:0] linear = 2'b00,
		beat4  = 2'b01,
		beat8  = 2'b10,
		beat16 = 2'b11;
// cti:
parameter [2:0] classic = 3'b000,
		inc     = 3'b010,
		eob	= 3'b111;


///////////////////////////////////////
//  Input system clock is 100 MHz   //
/////////////////////////////////////
reg sys_clk = 1'b0;

////////////////////////////////////////////
// QDR and WB Slave  Clocks              //
// As they are generated in real design //
/////////////////////////////////////////
wire wb_clk;
wire wb_rst;

wire wbs0_clk; assign wbs0_clk = wb_clk;
wire wbs0_rst; assign wbs0_rst = wb_rst;

wire wbs1_clk; assign wbs1_clk = wb_clk;
wire wbs1_rst; assign wbs1_rst = wb_rst;

wire qdr_clk;
wire qdr_rst;

clkgen clkgen0(
  .adcs_clk_o( ), // Not using this

  .mems_clk_o ( qdr_clk ),
  .mems_rst_o ( qdr_rst ),

  .wb_clk_o ( wb_clk ),
  .wb_rst_o ( wb_rst ),

  .wbfast_clk_o (  ),
  .wbfast_rst_o (  ),

  .sys_clk_i( sys_clk )
);

////////////////////////////////////////////////////
//  Wishbone things                              //
//////////////////////////////////////////////////
reg [31:0]	wbm0_adr_reg = 32'h0000_XXXX;
reg [1:0]	wbm0_bte_reg = 2'bXX;
reg [2:0]	wbm0_cti_reg = 3'bXXX;
reg		wbm0_cyc_reg = 1'b0;
reg [31:0]	wbm0_dat_reg = 32'hXXXX_XXXX;
reg [3:0]	wbm0_sel_reg = 4'bxxxx;
reg 		wbm0_stb_reg = 1'b0;
reg		wbm0_we_reg  = 1'bX;
wire		wbm0_ack_wire;
wire		wbm0_err_wire;
wire		wbm0_rty_wire;
wire [31:0]	wbm0_dat_wire;

reg [31:0]	wbm1_adr_reg = 32'hXXXX_XXXX;
reg [1:0]	wbm1_bte_reg = 2'bXX;
reg [2:0]	wbm1_cti_reg = 3'bXXX;
reg		wbm1_cyc_reg = 1'b0;
reg [31:0]	wbm1_dat_reg = 32'hXXXX_XXXX;
reg [3:0]	wbm1_sel_reg = 4'bxxxx;
reg		wbm1_stb_reg = 1'b0;
reg		wbm1_we_reg  = 1'bX;
wire		wbm1_ack_wire;
wire		wbm1_err_wire;
wire		wbm1_rty_wire;
wire [31:0]	wbm1_dat_wire;


wire            wbs_d_mem0_ack_o;
wire            wbs_d_mem0_err_o;
wire            wbs_d_mem0_rty_o;
wire [31:0]     wbs_d_mem0_dat_o;
wire            wbs_d_mem0_cyc_i;
wire [31:0]     wbs_d_mem0_adr_i;
wire            wbs_d_mem0_stb_i;
wire            wbs_d_mem0_we_i;
wire [3:0]      wbs_d_mem0_sel_i;
wire [31:0]     wbs_d_mem0_dat_i;
wire [2:0]      wbs_d_mem0_cti_i;
wire [1:0]      wbs_d_mem0_bte_i;

wire            wbs_d_mem1_ack_o;
wire            wbs_d_mem1_err_o;
wire            wbs_d_mem1_rty_o;
wire [31:0]     wbs_d_mem1_dat_o;
wire            wbs_d_mem1_cyc_i;
wire [31:0]     wbs_d_mem1_adr_i;
wire            wbs_d_mem1_stb_i;
wire            wbs_d_mem1_we_i;
wire [3:0]      wbs_d_mem1_sel_i;
wire [31:0]     wbs_d_mem1_dat_i;
wire [2:0]      wbs_d_mem1_cti_i;
wire [1:0]      wbs_d_mem1_bte_i;

wb_switch_b3 # (
	.dw ( 32 ),
	.aw ( 32 ),
	.slave0_sel_width( 8 ),     // Width of selection bits 8 == [31:24]
	.slave0_sel_addr ( 8'h00 ), // Matching value of the sel_width bits
	.slave1_sel_width( 8 ),
	.slave1_sel_addr ( 8'h01 ),
	.slave2_sel_width( 8 ),
	.slave2_sel_addr (8'b10XX_0000),
	.slave3_sel_width( 4 ),
	.slave3_sel_addr ( 4'h7 ),
	.slave4_sel_width( 8 ),
	.slave4_sel_addr ( 8'h92 )
) dbus (
  //--->Master 0
  .wbm0_adr_i( wbm0_adr_reg ),
  .wbm0_bte_i( wbm0_bte_reg ),
  .wbm0_cti_i( wbm0_cti_reg ),
  .wbm0_cyc_i( wbm0_cyc_reg ),
  .wbm0_dat_i( wbm0_dat_reg ),
  .wbm0_sel_i( wbm0_sel_reg ),
  .wbm0_stb_i( wbm0_stb_reg ),
  .wbm0_we_i ( wbm0_we_reg ),
  .wbm0_ack_o( wbm0_ack_wire ),
  .wbm0_err_o( wbm0_err_wire ),
  .wbm0_rty_o( wbm0_rty_wire ),
  .wbm0_dat_o( wbm0_dat_wire ),
  //--->Master 1
  .wbm1_adr_i( wbm1_adr_reg ),
  .wbm1_bte_i( wbm1_bte_reg ),
  .wbm1_cti_i( wbm1_cti_reg ),
  .wbm1_cyc_i( wbm1_cyc_reg ),
  .wbm1_dat_i( wbm1_dat_reg ),
  .wbm1_sel_i( wbm1_sel_reg ),
  .wbm1_stb_i( wbm1_stb_reg ),
  .wbm1_we_i ( wbm1_we_reg ),
  .wbm1_ack_o( wbm1_ack_wire ),
  .wbm1_err_o( wbm1_err_wire ),
  .wbm1_rty_o( wbm1_rty_wire ),
  .wbm1_dat_o( wbm1_dat_wire ),
  //---> Master 2
  .wbm2_adr_i( 32'h0000_0000 ),
  .wbm2_bte_i( 2'b00 ),
  .wbm2_cti_i( 3'b000 ),
  .wbm2_cyc_i( 1'b0 ),
  .wbm2_dat_i( 32'h0000_0000 ),
  .wbm2_sel_i( 4'b0000 ),
  .wbm2_stb_i( 1'b0 ),
  .wbm2_we_i ( 1'b0 ),
  .wbm2_ack_o(  ),
  .wbm2_err_o(  ),
  .wbm2_rty_o(  ),
  .wbm2_dat_o(  ),
  //---> Slave 0
  .wbs0_adr_o( wbs_d_mem0_adr_i ),
  .wbs0_bte_o( wbs_d_mem0_bte_i ),
  .wbs0_cti_o( wbs_d_mem0_cti_i ),
  .wbs0_cyc_o( wbs_d_mem0_cyc_i ),
  .wbs0_dat_o( wbs_d_mem0_dat_i ),
  .wbs0_sel_o( wbs_d_mem0_sel_i ),
  .wbs0_stb_o( wbs_d_mem0_stb_i ),
  .wbs0_we_o ( wbs_d_mem0_we_i ),
  .wbs0_ack_i( wbs_d_mem0_ack_o ),
  .wbs0_err_i( wbs_d_mem0_err_o ),
  .wbs0_rty_i( wbs_d_mem0_rty_o ),
  .wbs0_dat_i( wbs_d_mem0_dat_o ),
  //---> Slave 1
  .wbs1_adr_o( wbs_d_mem1_adr_i ),
  .wbs1_bte_o( wbs_d_mem1_bte_i ),
  .wbs1_cti_o( wbs_d_mem1_cti_i ),
  .wbs1_cyc_o( wbs_d_mem1_cyc_i ),
  .wbs1_dat_o( wbs_d_mem1_dat_i ),
  .wbs1_sel_o( wbs_d_mem1_sel_i ),
  .wbs1_stb_o( wbs_d_mem1_stb_i ),
  .wbs1_we_o ( wbs_d_mem1_we_i ),
  .wbs1_ack_i( wbs_d_mem1_ack_o ),
  .wbs1_err_i( wbs_d_mem1_err_o ),
  .wbs1_rty_i( wbs_d_mem1_rty_o ),
  .wbs1_dat_i( wbs_d_mem1_dat_o ),
  //---> Slave 2
  .wbs2_adr_o(  ),
  .wbs2_bte_o(  ),
  .wbs2_cti_o(  ),
  .wbs2_cyc_o(  ),
  .wbs2_dat_o(  ),
  .wbs2_sel_o(  ),
  .wbs2_stb_o(  ),
  .wbs2_we_o (  ),
  .wbs2_ack_i( 1'b0 ),
  .wbs2_err_i( 1'b0 ),
  .wbs2_rty_i( 1'b0 ),
  .wbs2_dat_i( 32'h0000_0000 ),
  //---> Slave 3
  .wbs3_adr_o(  ),
  .wbs3_bte_o(  ),
  .wbs3_cti_o(  ),
  .wbs3_cyc_o(  ),
  .wbs3_dat_o(  ),
  .wbs3_sel_o(  ),
  .wbs3_stb_o(  ),
  .wbs3_we_o (  ),
  .wbs3_ack_i( 1'b0 ),
  .wbs3_err_i( 1'b0 ),
  .wbs3_rty_i( 1'b0 ),
  .wbs3_dat_i( 32'h0000_0000 ),
  //---> Slave 4
  .wbs4_adr_o(  ),
  .wbs4_bte_o(  ),
  .wbs4_cti_o(  ),
  .wbs4_cyc_o(  ),
  .wbs4_dat_o(  ),
  .wbs4_sel_o(  ),
  .wbs4_stb_o(  ),
  .wbs4_we_o (  ),
  .wbs4_ack_i( 1'b0 ),
  .wbs4_err_i( 1'b0 ),
  .wbs4_rty_i( 1'b0 ),
  .wbs4_dat_i( 32'h0000_0000 ),
  //---> Clock and rest
  .wb_clk ( wb_clk ),
  .wb_rst ( wb_rst )
);
//////////////////////////////////////
// QDRII Connection Wires          //
////////////////////////////////////
wire		qdr_rps;
wire		qdr_wps;
wire		qdr_bws;

wire [21:0] 	qdr_addr;
wire [8:0]	qdr_d;
wire [8:0]	qdr_q;
wire [8:0]	qdr_q_buf;

assign qdr_q_buf = qdr_q;

wire qdr_K_P_clk; wire qdr_K_N_clk;
wire qdr_C_P_clk; wire qdr_C_N_clk;
wire qdr_CQ_P_clk; wire qdr_CQ_N_clk;

// We want to generate readout clocks

//---------------------- Instances ----------------------------------//
///////////////////////////////////
//  Memory Bank 0               //
/////////////////////////////////
wire qdr_doff;
cyqdr2_b2 mem_inst (
  // JTAG is not connected
  .TCK ( 1'b1 ),
  .TMS ( 1'b1 ),
  .TDI ( 1'b1 ),
  .TDO (  ),

  .D	 ( qdr_d ), 		// input data
  .K	 ( qdr_K_P_clk ), 	// Main clock
  .K_n	 ( qdr_K_N_clk ), 	// 	input
  .C	 ( qdr_C_P_clk ), 	// Data Output
  .C_n	 ( qdr_C_N_clk ), 	// 	clock input
  .RPS_n ( qdr_rps ), 		// Read Port Select input
  .WPS_n ( qdr_wps ), 		// Write port select input
  .BW0_n ( qdr_bws), 		// Byte Write Select
  .ZQ	 ( 1'b0 ), 		// Programmable Impedance Pin input
  .DOFF_b( qdr_doff ), 		// Internal DLL off pin
  .A	 ( qdr_addr ), 		// Input Address bus
  .CQ	 ( qdr_CQ_P_clk ), 	// Echo clock
  .CQ_n	 ( qdr_CQ_N_clk ),	//	output
  .Q	 ( qdr_q )  		// Output data
);
////////////////////////////////////////
//  Memory Controller                //
//////////////////////////////////////
qdrii_wb #( 
	.clock_setup_time( 13'h100 ),
	.data0_delay (15),
	.data1_delay (15),
	.data2_delay (15),
	.data3_delay (15),
	.data4_delay (15),
	.data5_delay (15),
	.data6_delay (15),
	.data7_delay (15),
	.data8_delay (15),
	.echo_clk_p_delay ( 15 ),
	.echo_clk_n_delay ( 15 )
) controller_inst (
  .wbs0_adr_i ( wbs_d_mem0_adr_i ),
  .wbs0_bte_i ( wbs_d_mem0_bte_i ),
  .wbs0_cti_i ( wbs_d_mem0_cti_i ),
  .wbs0_cyc_i ( wbs_d_mem0_cyc_i ),
  .wbs0_dat_i ( wbs_d_mem0_dat_i ),
  .wbs0_sel_i ( wbs_d_mem0_sel_i ),
  .wbs0_stb_i ( wbs_d_mem0_stb_i ),
  .wbs0_we_i  ( wbs_d_mem0_we_i  ),
  .wbs0_ack_o ( wbs_d_mem0_ack_o ),
  .wbs0_err_o ( wbs_d_mem0_err_o ),
  .wbs0_rty_o ( wbs_d_mem0_rty_o ),
  .wbs0_dat_o ( wbs_d_mem0_dat_o ),

  .wbs0_clk_i ( wb_clk ),
  .wbs0_rst_i ( wb_rst ),

  .wbs1_adr_i ( wbs_d_mem1_adr_i ),
  .wbs1_bte_i ( wbs_d_mem1_bte_i ), 
  .wbs1_cti_i ( wbs_d_mem1_cti_i ), 
  .wbs1_cyc_i ( wbs_d_mem1_cyc_i ),
  .wbs1_dat_i ( wbs_d_mem1_dat_i ),
  .wbs1_sel_i ( wbs_d_mem1_sel_i ),
  .wbs1_stb_i ( wbs_d_mem1_stb_i ),
  .wbs1_we_i  ( wbs_d_mem1_we_i  ),
  .wbs1_ack_o ( wbs_d_mem1_ack_o ),
  .wbs1_err_o ( wbs_d_mem1_err_o ),
  .wbs1_rty_o ( wbs_d_mem1_rty_o ),
  .wbs1_dat_o ( wbs_d_mem1_dat_o ),

  .wbs1_clk_i ( wb_clk ),     
  .wbs1_rst_i ( wb_rst ),

  .qdr_rps_o ( qdr_rps ),
  .qdr_wps_o ( qdr_wps ),
  .qdr_bws_o ( qdr_bws ),
  .qdr_addr_o( qdr_addr ),
  .qdr_data_o( qdr_d ),
  .qdr_data_i( qdr_q_buf ),

  .qdr_read_clk_p_o ( qdr_C_P_clk ),
  .qdr_read_clk_n_o ( qdr_C_N_clk ),

  .qdr_write_clk_p_o( qdr_K_P_clk ),
  .qdr_write_clk_n_o( qdr_K_N_clk ),

  .qdr_echo_clk_p_i ( qdr_CQ_P_clk ),
  .qdr_echo_clk_n_i ( qdr_CQ_N_clk ),

  .qdr_clk_i( qdr_clk ),
  .qdr_rst_i( qdr_rst ),

  .pow_doff_o( qdr_doff )
);

//===================  TASKS ========================================//
///////////////////////////////////////
//  Wishbone Write                  //
/////////////////////////////////////
task wbm0_write;
	input [31:0] addr;
	input  [3:0] wsel;
	input [31:0] data;
	input	  cti;
	input	  bte;
	input	  cyc_end;
begin
  @ (negedge wb_clk);
  wbm0_adr_reg <= addr;
  wbm0_sel_reg <= wsel;
  wbm0_we_reg  <= 1'b1;
  wbm0_stb_reg <= 1'b1;
  wbm0_cyc_reg <= 1'b1;
  wbm0_dat_reg <= data;
  wbm0_cti_reg <= cti;
  wbm0_bte_reg <= bte;
  @ (posedge wb_clk);
  while (wbm0_ack_wire == 1'b0) begin
	@(posedge wb_clk);
  end
  $display("   WBM0 Write  \t Addr:0x%H \t Data:0x%H \t Mask:0x%H", 
	wbm0_adr_reg, wbm0_dat_reg, wbm0_sel_reg);
  wbm0_adr_reg <= 'hX;
  wbm0_sel_reg <= 'hx;
  wbm0_dat_reg <= 'hX;
  wbm0_stb_reg <= cyc_end;
  wbm0_cyc_reg <= cyc_end;
  wbm0_we_reg  <= 1'bX;
end
endtask
//-------------------------------------------------------------------//
task wbm1_write;
  input [31:0] addr;
  input  [3:0] wsel;
  input [31:0] data;
  input	    cyc_end;
  input	[2:0] cti_value;
  input	[1:0] bte_value;
begin
  @ (negedge wb_clk);
  wbm1_adr_reg <= addr;
  wbm1_sel_reg <= wsel;
  wbm1_we_reg  <= 1'b1;
  wbm1_stb_reg <= 1'b1;
  wbm1_cyc_reg <= 1'b1;
  wbm1_dat_reg <= data;
  wbm1_cti_reg <= cti_value;
  wbm1_bte_reg <= bte_value;
  @ (posedge ( wbm1_ack_wire | wbm1_err_wire | wbm1_rty_wire));
  $display("WBS1 Write  \t Addr:0x%H \t Data:0x%H \t Mask:0x%H",
	wbm1_adr_reg, wbm1_dat_reg, wbm1_sel_reg);
  @ (posedge wb_clk);
  // @ (negedge wbs1_clk);
  wbm1_adr_reg <= 'hX;
  wbm1_sel_reg <= 'hx;
  wbm1_dat_reg <= 'hX;
  wbm1_stb_reg <= cyc_end;
  wbm1_cyc_reg <= cyc_end;
  wbm1_we_reg  <= 1'bX;
end
endtask

///////////////////////////////////////
//  Wishbone Read                   //
/////////////////////////////////////
task wbm0_read;
	input  [31:0] addr;
	input  [3:0]  wsel;
	output [31:0] data;
	input  [2:0] cti;
	input  [1:0] bte;
	input	     cyc;
	input	     stb;
begin
  @ (negedge wb_clk);
  wbm0_adr_reg <= addr;
  wbm0_sel_reg <= wsel;
  wbm0_we_reg  <= 1'b0;
  wbm0_stb_reg <= stb;
  wbm0_cyc_reg <= cyc;
  wbm0_cti_reg <= cti;
  wbm0_bte_reg <= bte;
  @ (posedge wb_clk);
  while ((wbm0_ack_wire == 1'b0)&&(cyc == 1'b1)&&(stb == 1'b1)) begin
	@(posedge wb_clk);
  end
  data <= wbm0_dat_wire;
  if ((cyc == 1'b1)&&(stb == 1'b1))
  	$display("WBS0 Read  \t Addr:0x%H \t Data:0x%H",
  		wbm0_adr_reg, wbm0_dat_wire);
  else
  	$display("WBS0 Wait State Inserted");
  wbm0_we_reg  <= 1'bX;
end
endtask
//-------------------------------------------------------------------//
task wbm1_read;
  input  [31:0] addr;
  input  [3:0]  wsel;
  output [31:0] data;
  input	     cyc_end;
  input [2:0] cti_value;
  input [1:0] bte_value;
begin
  @ (negedge wb_clk);
  wbm1_adr_reg <= addr;
  wbm1_sel_reg <= wsel;
  wbm1_we_reg  <= 1'b0;
  wbm1_stb_reg <= 1'b1;
  wbm1_cyc_reg <= 1'b1;
  wbm1_cti_reg <= cti_value;
  wbm1_bte_reg <= bte_value;
  @ (posedge wb_clk);
  while (wbm1_ack_wire == 1'b0) begin
	@(posedge wb_clk);
  end
  data <= wbm1_dat_wire;
  $display("WBM1 Read  \t Addr:0x%H \t Data:0x%H \t Mask:0x%H",
  	wbm1_adr_reg, wbm1_dat_wire, wbm1_sel_reg);
  wbm1_stb_reg <= cyc_end;
  wbm1_cyc_reg <= cyc_end;
  // wbm1_we_reg  <= 1'bX;
end
endtask
//-------------------------------------------------------------------//
/////////////////////////////////
//  Wishbone Burst Read       //
///////////////////////////////
task wbm0_burst_read;
	input  [31:0] addr;
	input  [7 :0] bl;
	input         cyc_end;
	reg 	[8:0] ii;
begin
  @ (negedge wb_clk);
  for (ii = 0; ii < bl; ii = ii + 1) begin
  	if (ii == 5) begin
		wbm0_stb_reg <= 1'b0;
		@ (posedge wb_clk);
		@ (posedge wb_clk);
	end else begin
		wbm0_stb_reg <= 1'b1; 
		wbm0_cyc_reg <= 1'b1;
		wbm0_we_reg  <= 1'b0; 
		wbm0_adr_reg <= addr+4*ii;
		if ((ii+1) == bl) 
			wbm0_cti_reg <= 3'b111; 
		else 
			wbm0_cti_reg <= 3'b010;
			wbm0_bte_reg <= 2'b00;

		@ (posedge wb_clk);
		while (wbm0_ack_wire == 1'b0) begin 
			@(posedge wb_clk); 
		end
		$display("---WBM0 Burst Read #%d \t Addr:0x%H \t Data:0x%H",
			ii+1, wbm0_adr_reg, wbm0_dat_wire);
	end
  end 
  wbm0_stb_reg <= cyc_end; 
  wbm0_cyc_reg <= cyc_end;
  wbm0_we_reg  <= 1'b0; 
  wbm0_adr_reg <= 'hx;
  // wbm0_cti_reg <= 'hx; 
  // wbm0_bte_reg <= 'hx;
end
endtask
//-------------------------------------------------------------------//
task wbm1_burst_read;
	input  [31:0] addr;
	input  [7 :0] bl;
	input	      cyc_end;
	reg     [8:0] ii;
begin
  @ (negedge wb_clk);
  for (ii = 0; ii < bl; ii = ii + 1) begin
	wbm1_stb_reg <= 1'b1;
	wbm1_cyc_reg <= 1'b1;
	wbm1_we_reg  <= 1'b0;
	wbm1_adr_reg <= addr +4*ii;
	if ((ii+1) == bl)
		wbm1_cti_reg <= 3'b111;
	else
		wbm1_cti_reg <= 3'b010;
	wbm1_bte_reg <= 2'b00;
	@ (posedge wb_clk);
	while (wbm1_ack_wire == 1'b0) begin 
		@(posedge wb_clk); 
	end
	$display("WBS1 Burst Read #%d \t Addr:0x%H \t Data:0x%H",
		ii+1, wbm1_adr_reg, wbm1_dat_wire);
  end
  wbm1_stb_reg <= cyc_end;
  wbm1_cyc_reg <= cyc_end;
  wbm1_we_reg  <= 1'b0;
  wbm1_adr_reg <= 'hx;
  // wbm1_cti_reg <= 'hx;
  // wbm1_bte_reg <= 'hx;
end
endtask

//===================  SAVING TO THE FILE ===========================//
initial begin
  $dumpfile( "tb_qdrii_controller.vcd" );
  $dumpvars;
end
//=================  Clocks ==========================================//
always #5000.0 sys_clk = ~sys_clk; // 100 MHz
//============= WBS0 Tasks =============//
reg [31:0] wbm0_readout_data;  
initial begin
	@ (posedge wbs0_rst);
	@ (negedge wbs0_rst);
	$display("... Port 0 Reset reached");
	$display("...Classic Read # 0 (should be nothing)");
	wbm0_read (32'h100+4*2,	4'hF,	wbm0_readout_data, classic, linear, 1'b1, 1'b1); // Classic
	$display("... Port 0 Writing 18 words pattern");
	wbm0_write(32'h100,   4'b1111,	32'h1112_1314, classic, linear, 1'b1);
	wbm0_write(32'h100+4, 4'b0001,	32'h1102_0202, classic, linear, 1'b1);
	wbm0_write(32'h100+8, 4'b0010,	32'h0303_0303, classic, linear, 1'b1);
	wbm0_write(32'h100+12, 4'b0100,	32'h0404_0404, classic, linear, 1'b1);
	wbm0_write(32'h100+16, 4'b1000,	32'h0505_0505, classic, linear, 1'b1);
	wbm0_write(32'h100+20, 4'hF, 	32'h0606_0606, eob,	linear, 1'b1);
	wbm0_write(32'h100+24, 4'hF, 	32'h0707_0707, classic, linear, 1'b1);
	wbm0_write(32'h100+28, 4'hF, 	32'h0808_0808, eob,	linear, 1'b1);
	wbm0_write(32'h100+32, 4'hF, 	32'h0909_0909, eob,	linear,	1'b1);
	wbm0_write(32'h100+36, 4'b1111,	32'h1010_1010, eob,	linear, 1'b1);
	wbm0_write(32'h100+40,4'b0001,  32'h1111_1111, eob,	linear, 1'b1);
	wbm0_write(32'h100+44,4'b0010,  32'h1212_1212, classic,	linear,	1'b1);
	wbm0_write(32'h100+48,4'b0100,  32'h1313_1313, classic, linear, 1'b1);
	wbm0_write(32'h100+52,4'b1000,  32'h1414_1414, eob,	linear, 1'b0);
	wbm0_write(32'h100+56, 4'hF,	32'h1515_1515, inc,	linear, 1'b1);
	wbm0_write(32'h100+60, 4'hF,	32'h1616_1616, inc,	linear, 1'b1);
	wbm0_write(32'h100+64, 4'hF,	32'h1717_1717, inc,	linear,	1'b1);
	wbm0_write(32'h100+68, 4'hF,	32'h1818_1818, eob,	linear, 1'b1);
	wbm0_read(32'h100+4*0	,4'hF,	wbm0_readout_data, inc, linear, 1'b1, 1'b0); // WS
	wbm0_read(32'h100+4*1	,4'hF,  wbm0_readout_data, inc, linear, 1'b1, 1'b0); // WS
	$display("...Burst # 1 (3 words)");
	wbm0_read(32'h100+4*2   ,4'hF,  wbm0_readout_data, inc, linear, 1'b1, 1'b1); // Burst 0
	wbm0_read(32'h100+4*2   ,4'hF,  wbm0_readout_data, inc, linear, 1'b1, 1'b0); // WS
	wbm0_read(32'h100+4*2   ,4'hF,  wbm0_readout_data, inc, linear, 1'b1, 1'b0); // WS
	wbm0_read(32'h100+4*3   ,4'hF,  wbm0_readout_data, inc, linear, 1'b1, 1'b1); // Burst 1
	wbm0_read(32'h100+4*3   ,4'hF,  wbm0_readout_data, inc, linear, 1'b1, 1'b0); // WS
	wbm0_read(32'h100+4*4   ,4'hF,  wbm0_readout_data, eob, linear, 1'b1, 1'b1); // Burst 2
	wbm0_read(32'h100+4*4   ,4'hF,  wbm0_readout_data, eob, linear, 1'b1, 1'b0); // WS
	$display("...Burst # 2 (1 word)");
	wbm0_read(32'h100+4*5   ,4'hF,  wbm0_readout_data, eob, linear, 1'b1, 1'b1); // Burst 0
	$display("...Classic Read # 1");
	wbm0_read(32'h100+4*2,	4'hF,	wbm0_readout_data, classic, linear, 1'b1, 1'b1); // Classic
	$display("...Classic Read # 2");
	wbm0_read(32'h100+4*0,	4'hF,   wbm0_readout_data, classic, linear, 1'b1, 1'b1); // Classic
	$display("...Burst # 3 (10 words)");
	wbm0_read(32'h100+4*0,	4'hF,	wbm0_readout_data, inc, linear, 1'b1, 1'b1); // Burst 0
	wbm0_read(32'h100+4*1,  4'hF,   wbm0_readout_data, inc, linear, 1'b1, 1'b1); // Burst 1
	wbm0_read(32'h100+4*2,  4'hF,   wbm0_readout_data, inc, linear, 1'b1, 1'b1); // Burst 2
	wbm0_read(32'h100+4*3,  4'hF,   wbm0_readout_data, inc, linear, 1'b1, 1'b1); // Burst 3
	wbm0_read(32'h100+4*4,  4'hF,   wbm0_readout_data, inc, linear, 1'b1, 1'b1); // Burst 4
	wbm0_read(32'h100+4*5,  4'hF,   wbm0_readout_data, inc, linear, 1'b1, 1'b1); // Burst 5
	wbm0_read(32'h100+4*6,  4'hF,   wbm0_readout_data, inc, linear, 1'b1, 1'b1); // Burst 6
	wbm0_read(32'h100+4*7,  4'hF,   wbm0_readout_data, inc, linear, 1'b1, 1'b1); // Burst 7
	wbm0_read(32'h100+4*8,  4'hF,   wbm0_readout_data, inc, linear, 1'b1, 1'b1); // Burst 8
	wbm0_read(32'h100+4*9,  4'hF,   wbm0_readout_data, eob, linear, 1'b1, 1'b1); // Burst 9
	wbm0_read(32'h100+4*0,  4'hF,   wbm0_readout_data, classic, linear, 1'b0, 1'b0); // WS
	#1000000 $finish;
end
reg [31:0] wbm1_readout_data;
initial begin
	// @ (posedge wbs1_rst);
	// @ (negedge wbs1_rst);
	#1000
	$display("... Port 1 Reset reached");
	wbm1_write(32'h0100_0200+68, 4'hF,	32'h1818_1818, 1'b0,	classic,	linear);
	wbm1_write(32'h0100_0200+64, 4'hF,	32'h1717_1717, 1'b0, 	classic,	linear);
	wbm1_write(32'h0100_0200+60, 4'hF,    32'h1616_1616, 1'b0,	classic,        linear);
	wbm1_write(32'h0100_0200+56, 4'hF,    32'h1515_1515, 1'b0,	classic,        linear);
	wbm1_write(32'h0100_0200+52, 4'b1000,	32'h1414_1414, 1'b0,	classic,        linear);
	wbm1_write(32'h0100_0200+48, 4'b0100,	32'h1313_1313, 1'b0,	classic,        linear);
	wbm1_write(32'h0100_0200+44, 4'b0010,	32'h1212_1212, 1'b0,	classic,        linear);
	wbm1_write(32'h0100_0200+40, 4'b0001,	32'h1111_1111, 1'b0,	classic,        linear);
	wbm1_write(32'h0100_0200+36, 4'b1111,	32'h1010_1010, 1'b0,	classic,        linear);
	wbm1_write(32'h0100_0200+32, 4'hF,	32'h0909_0909, 1'b0,	classic,        linear);
	wbm1_write(32'h0100_0200+28, 4'hF,	32'h0808_0808, 1'b0,	classic,        linear);
	wbm1_write(32'h0100_0200+24, 4'hF,	32'h0707_0707, 1'b0,	classic,        linear);
	wbm1_write(32'h0100_0200+20, 4'hF,	32'h0606_0606, 1'b0,	classic,        linear);
	wbm1_write(32'h0100_0200+16, 4'b1000,	32'h0505_0505, 1'b0,	classic,        linear);
	wbm1_write(32'h0100_0200+12, 4'b0100,	32'h0404_0404, 1'b0,	classic,        linear);
	wbm1_write(32'h0100_0200+8,  4'b0010,	32'h0303_0303, 1'b0,	classic,        linear);
	wbm1_write(32'h0100_0200+4,  4'b0001,	32'h0202_0202, 1'b0,	classic,        linear);
	wbm1_write(32'h0100_0200,    4'b1111,	32'h0101_0101, 1'b0,	classic,        linear);
	/*
	$display("... Port1: Reading 18 words 2 times...");
	#20000 wbm1_burst_read(32'h0100_0200, 18, 1'b0);
	$display("... Port1: Reading 1st 18 words done ...");
	#20000 wbm1_burst_read(32'h0100_0200, 18, 1'b0);
	$display("... Port1: Reading 2nd 18 words done ...");
	#200000
	$display("... Port1: Running second test ...");
	wbm1_write(32'h0100_0300, 4'b1111, 32'h1234_5678, 	1'b1, classic, linear);
	wbm1_read (32'h0100_0300, 4'b1111, wbm1_readout_data, 1'b0, classic, linear);
	$display("... Port1: Burst write, beat 4 ...");
	wbm1_write(32'h0100_0400, 4'b1111, 32'h0001_0002, 	1'b1,  inc, beat4);
	wbm1_write(32'h0100_0404, 4'b1111, 32'h0003_0004, 	1'b1,  inc, beat4);
	wbm1_write(32'h0100_0408, 4'b1111, 32'h0005_0006, 	1'b1,  inc, beat4);
	wbm1_write(32'h0100_040C, 4'b1111, 32'h0007_0008, 	1'b1,  eob, beat4);
	wbm1_write(32'h0100_0408, 4'b1000, 32'hA1FF_FFFF, 	1'b1, classic, linear);
	$display("... Port1: Burst read,  beat 4 ...");
	wbm1_read (32'h0100_0400, 4'b1111, wbm1_readout_data, 1'b1,  inc, beat4);
	wbm1_read (32'h0100_0404, 4'b1111, wbm1_readout_data, 1'b1,  inc, beat4);
	wbm1_read (32'h0100_0408, 4'b1111, wbm1_readout_data, 1'b1,  inc, beat4);
	wbm1_read (32'h0100_040C, 4'b1111, wbm1_readout_data, 1'b1,  inc, beat4);
	wbm1_read (32'h0100_0400, 4'b1111, wbm1_readout_data, 1'b1,  inc, beat4);
	wbm1_read (32'h0100_0404, 4'b1111, wbm1_readout_data, 1'b1,  inc, beat4);
	wbm1_read (32'h0100_0408, 4'b1111, wbm1_readout_data, 1'b1,  inc, beat4);
	wbm1_read (32'h0100_040C, 4'b1111, wbm1_readout_data, 1'b1,  eob, beat4);
	$display("... Port1: Burst read, linear ...");
	wbm1_write(32'h0100_0500, 4'b1111, 32'hDEAD_DEAD,     1'b1,  inc, linear);
	wbm1_write(32'h0100_0504, 4'b1111, 32'h5555_5555,     1'b1,  eob, linear);

	wbm1_read (32'h0100_0500, 4'b1111, wbm1_readout_data, 1'b1,  inc, beat4);
	wbm1_read (32'h0100_0504, 4'b1111, wbm1_readout_data, 1'b1,  inc, linear);
	wbm1_read (32'h0100_0508, 4'b1111, wbm1_readout_data, 1'b0,  eob, beat4);
	*/
	// #100 $finish;
end

endmodule
