/*
 * This Module is used to acces configuration SPI Flash through
 * a JTAG (1), because spi clock is connected to a dedicated configuration
 * clock of Kintex7 CCLK_O
 */

module bscan_spi(
  output wire data0_o,  // MOSI
  input wire  data1_i,	// MISO
  inout wire  data2_io,	// WP <wasn't there>
  inout wire  data3_io,	// HOLD
  output wire cs_o,	// CSB

  output wire clk_o	// Spi Clock 
);
assign data2_io = 1'b1;
assign data3_io = 1'b1;

wire capture;	// CAPTURE
wire update;	// UPDATE
wire tdi;	// TDI
wire tdo1;	// TDO1
reg [47:0] header;
reg [15:0]  len;
reg have_header = 0;

assign data0_o = cs_o ? 1'bZ : tdi;

wire sel1;	// SEL1
wire reset;	// RESET

reg cs_go_reg = 1'b0;		// CS_GO
reg cs_go_prep_reg = 1'b0;	// CS_GO_PREP
reg cs_stop_reg = 1'b0;		// CS_STOP
reg cs_stop_prep_reg = 1'b0;	// CS_STOP_PREP
reg [13:0] ram_read_addr_reg = 14'h0;	// RAM_RADDR
reg [13:0] ram_write_addr_reg = 14'h0;	// RAM_WADDR
wire clk_o_inv = !clk_o;	// DRCK1_INV
wire ram_data_out;		// RAM_DO
wire [15:0] ram_data_in;	// RAM_DI
reg  ram_we_reg = 1'b0;		// RAM_WE

////////////////////////////////////////////////
// Instance of a ram block                   //
//////////////////////////////////////////////
RAMB18E1 #(
	// Address Collision Mode: "PERFORMANCE" or "DELAYED_WRITE" 
	.RDADDR_COLLISION_HWCONFIG("PERFORMANCE"),
	// Collision check: Values ("ALL", "WARNING_ONLY", "GENERATE_X_ONLY" or "NONE")
	.SIM_COLLISION_CHECK("ALL"),
	// DOA_REG, DOB_REG: Optional output register (0 or 1)
	.DOA_REG(0),
      	.DOB_REG(0),
	// INIT_A, INIT_B: Initial values on output ports
	.INIT_A(1'b0),
	.INIT_B(1'b0),
	// Initialization File: RAM initialization file
	.INIT_FILE("NONE"),
	// RAM Mode: "SDP" or "TDP" 
	.RAM_MODE("TDP"),
	// READ_WIDTH_A/B, WRITE_WIDTH_A/B: Read/write width per port
	.READ_WIDTH_A(1),	// 0-72
	.READ_WIDTH_B(1),	// 0-18

	.WRITE_WIDTH_A(1),	// 0-18
	.WRITE_WIDTH_B(1),	// 0-72
	// RSTREG_PRIORITY_A, RSTREG_PRIORITY_B: Reset or enable priority ("RSTREG" or "REGCE")
	.RSTREG_PRIORITY_A("RSTREG"),
	.RSTREG_PRIORITY_B("RSTREG"),
	// SRVAL_A, SRVAL_B: Set/reset value for output
	.SRVAL_A(18'h00000),
	.SRVAL_B(18'h00000),
	// Simulation Device: Must be set to "7SERIES" for simulation behavior
	.SIM_DEVICE("7SERIES"),
	// WriteMode: Value on output upon a write ("WRITE_FIRST", "READ_FIRST", or "NO_CHANGE")
	.WRITE_MODE_A("WRITE_FIRST"),
	.WRITE_MODE_B("WRITE_FIRST") 
) ram_inst (
	// Port A Data: 16-bit (each) output: Port A data
	.DOADO		(ram_data_out),	// 16-bit output: A port  data/LSB data
	.DOPADOP	( ),		// 2-bit output: A port parity/LSB parity
	// Port B Data: 16-bit (each) output: Port B data
	.DOBDO		( ),	// 16-bit output: B port data/MSB data
	.DOPBDOP	( ),	// 2-bit output: B port parity/MSB parity
	// Port A Address/Control Signals: 14-bit (each) input:
	// Port A address and control signals (read port  when RAM_MODE="SDP")
	.ADDRARDADDR	( ram_read_addr_reg ),	// 14-bit input: A port address/Read address
	.CLKARDCLK	( clk_o_inv ),	// 1-bit input: A port clock/Read clock
	.ENARDEN	( 1'b1 ),	// 1-bit input: A port enable/Read enable
	.REGCEAREGCE	( 1'b0 ),	// 1-bit input: A port register enable/Register enable
	.RSTRAMARSTRAM	( 1'b0 ),	// 1-bit input: A port set/reset
	.RSTREGARSTREG	( 1'b0 ),	// 1-bit input: A port register set/reset
	.WEA		( 2'b00 ),	// 2-bit input: A port write enable
	// Port A Data: 16-bit  (each) input: Port A data
	.DIADI		( 16'h0000 ),	// 16-bit input: A port data/LSB data
	.DIPADIP	( 2'b00 ),	// 2-bit input: A port parity/LSB parity
	// Port B Address/Control Signals: 14-bit (each) input:
	// Port B address and control signals (write port  when RAM_MODE="SDP")
	.ADDRBWRADDR	( ram_write_addr_reg),	// 14-bit input: B port address/Write address
	.CLKBWRCLK	( clk_o ),	// 1-bit input: B port clock/Write clock
	.ENBWREN	( ram_we_reg ),	// 1-bit input: B port enable/Write enable
	.REGCEB		( 1'b0 ),	// 1-bit input: B port register enable
	.RSTRAMB	( 1'b0 ),	// 1-bit input: B port set/reset
	.RSTREGB	( 1'b0 ),	// 1-bit input: B port register set/reset
	.WEBWE		( 4'b0001 ), // 4-bit input: B port write enable/Write enable
	// Port B Data: 16-bit (each) input: Port B data
	.DIBDI		( ram_data_in ),// 16-bit input: B port data/MSB data
	.DIPBDIP	( 2'b00 )	// 2-bit input: B port parity/MSB parity
);
///////////////////////////////////////////////////
// Instance of the user boundary scan register  //
/////////////////////////////////////////////////
BSCANE2 #(
	.JTAG_CHAIN(1),	// Value for USER command.
	.DISABLE_JTAG ("FALSE")
) bscan_inst (
	.CAPTURE(capture), // 1-bit output: CAPTURE output from TAP controller.
	.DRCK(clk_o),	// 1-bit output: Gated TCK output. When SEL is asserted, 
			// DRCK toggles when CAPTURE or SHIFT are asserted.
	.RESET(reset),	// 1-bit output: Reset output for TAP controller.
	.RUNTEST( ), 	// 1-bit output: Output asserted when TAP controller is in Run Test/Idle state.
	.SEL(sel1),	// 1-bit output: USER instruction active output.
	.SHIFT(  ),	// 1-bit output: SHIFT output from TAP controller.
	.TCK(  ),	// 1-bit output: Test Clock output. Fabric connection to TAP Clock pin.
	.TDI(tdi),	// 1-bit output: Test Data Input (TDI) output from TAP controller.
	.TMS(  ),	// 1-bit output: Test Mode Select output. Fabric connection to TAP.
	.UPDATE(update),// 1-bit output: UPDATE output from TAP controller
	.TDO(tdo1)	// 1-bit input: Test Data Output (TDO) input for USER function.
);
//===================================================================//
// After this line, it's a common structure:

assign ram_data_in[0] 		= data1_i;
assign ram_data_in[15:1] 	= 15'h0;

assign tdo1 = ram_data_out;

wire rst = capture || reset || update || !sel1;

reg cs_go_sreg = 1'b0;
always @(negedge clk_o or posedge rst) begin
  if (rst) begin
	cs_go_sreg <= 1'b0;
  end else begin
  	cs_go_sreg <= cs_go_prep_reg;
  end
end

assign cs_o = !(cs_go_sreg && !cs_stop_reg);

always @(negedge clk_o or posedge rst)
  if (rst) begin
	have_header <= 1'b0;
	cs_go_prep_reg <= 1'b0;
	cs_stop_reg <= 1'b0;
  end else begin
	cs_stop_reg <= cs_stop_prep_reg;
	if (!have_header) begin
		if (header[46:15] == 32'h59a659a6) begin
			len <= {header [14:0],1'b0};
			have_header <= 1;
			if ({header [14:0],1'b0} != 0) begin
				cs_go_prep_reg <= 1;
			end
		end
	end else if (len != 0) begin
	len <= len - 1'b1;
	end // if (!have_header)
  end // if (rst)

reg reset_header = 0;
always @(posedge clk_o or posedge rst)
  if (rst) begin
	cs_go_reg <= 0;
	cs_stop_prep_reg <= 0;
	ram_write_addr_reg <= 0;
	ram_read_addr_reg <=0;
	ram_we_reg <= 0;
	reset_header <= 1;
  end else begin
	ram_read_addr_reg <= ram_read_addr_reg + 1'b1;
	ram_we_reg <= !cs_o;
	if(ram_we_reg) 
		ram_write_addr_reg <= ram_write_addr_reg + 1'b1;
	reset_header <= 0;
	// For the next if, the value of reset_header is probed at rising
	// clock, before "reset_header<=0" is executed:
	if(reset_header)
		header <={47'h000000000000, tdi};
	else
		header <= {header[46:0], tdi};
	cs_go_reg <= cs_go_prep_reg;
	if (cs_go_reg && (len == 0))
		cs_stop_prep_reg <= 1;
  end // if (rst)
endmodule /* bscan_spi */
