`timescale 1ps / 1ps

module tb_bscan_spi;

//////////////////////////////////////////////
// Parameters for cti and bte burst cycles //
////////////////////////////////////////////
/*
// bte:
parameter [1:0] linear = 2'b00,
		beat4  = 2'b01,
		beat8  = 2'b10,
		beat16 = 2'b11;
// cti:
parameter [2:0] classic = 3'b000,
		inc     = 3'b010,
		eob	= 3'b111;

*/
localparam JTAG_TMS = 0;
localparam JTAG_TCK = 1;
localparam JTAG_TDO = 2;

localparam JTAG_TMS_bit = 3'h1;
localparam JTAG_TCK_bit = 3'h2;
localparam JTAG_TDO_bit = 3'h4;

///////////////////////////////////////
// For bscan_spi                    //
/////////////////////////////////////
localparam SPI_PAGE_PROGRAM		= 8'h02;
localparam SPI_PAGE_READ		= 8'h03;
localparam SPI_READ_STATUS_REGISTER	= 8'h05;
localparam SPI_WRITE_ENABLE		= 8'h06;
localparam SPI_SECTOR_ERASE		= 8'hD8;
localparam SPI_READ_IDENTIFICATION	= 8'h9F;
localparam SPI_BULK_ERASE		= 8'hC7;

///////////////////////////////////////
//  Input system clock is 100 MHz   //
/////////////////////////////////////
reg sys_clk = 1'b0;

////////////////////////////////////////////
// Clocks                                //
// As they are generated in real design //
/////////////////////////////////////////
wire wb_clk;
wire wb_rst;

wire spi0_clk;

clkgen clkgen0(
  .adcs_clk_o( ), // Not using this

  .mems_clk_o (  ),
  .mems_rst_o (  ),

  .wb_clk_o ( wb_clk ),
  .wb_rst_o ( wb_rst ),

  .wbfast_clk_o (  ),
  .wbfast_rst_o (  ),

  .sys_clk_i( sys_clk ),

  .cclock_i (spi0_clk)
);

////////////////////////////////////////////////////
//  JTAG Simulation  Primitive                   //
//////////////////////////////////////////////////
reg jtag_tck_pad_i = 1'b0;
reg jtag_tms_pad_i = 1'b0;
reg jtag_tdi_pad_i = 1'b0;

wire jtag_tdo_pad_o;

JTAG_SIME2 #(
	.PART_NAME ("7K160T")
) jtag_port (
  .TDO ( jtag_tdo_pad_o ), // output
  .TCK ( jtag_tck_pad_i ), // input
  .TDI ( jtag_tdi_pad_i ), // input
  .TMS ( jtag_tms_pad_i )  // input
);

//////////////////////////////////////////////////
//  BSCANSPI  As it's in the real file         //
////////////////////////////////////////////////
wire spi0_data0_o;
wire spi0_data1_i;
wire spi0_data2_io;
wire spi0_data3_io;
wire spi0_cs_o;
wire spi0_reset_o;

bscan_spi spi0 (
	.data0_o(spi0_data0_o),         // MOSI
	.data1_i(spi0_data1_i),         // MISO
	.data2_io(spi0_data2_io),       // WP
	.data3_io(spi0_data3_io),       // HOLD
	.cs_o(spi0_cs_o),                // Select

	.clk_o( spi0_clk )
);
assign spi0_reset_o = 1'b1; // So that SPI is not held in reset mode

/////////////////////////////////////////////
//  Micron SPI Flash                      //
///////////////////////////////////////////
N25Qxxx spi0_chip (
	.S 		( spi0_cs_o), 
	.C_		( glbl.CCLKO_GLBL), 
	.HOLD_DQ3	( spi0_data3_io), 
	.DQ0		( spi0_data0_o ), 
	.DQ1		( spi0_data1_i ), 
	.Vcc		( 32'd3300 ), 
	.Vpp_W_DQ2	( spi0_data2_io )
//	.RESET2		( spi0_reset_o )	// We don't have a reset !!!
);

//===================  TASKS ========================================//
task spi_xfer_user1_header;
	input [15:0] stream_length;	// This includes SPI instruction (number of bits)

	reg [63:0] readdata;
	reg [63:0] writedata;
	integer ii;
begin
  $display("...spi_xfer_user1_header at time %t", $time);
  set_ir(6'h02);	// select User1 register
  #1000000; 		// wait 1usec
  write_bit(JTAG_TMS_bit);	// select_dr_scan
  write_bit(3'h0);		// capture_ir
  write_bit(3'h0);		// shift_ir
  // Everything needs to be bit-reversed
  // Write SPI magic Number 4bytes, 32bits (bit reversed)
  jtag_read_write_stream(64'h659A659A, 8'd32, 0, readdata); // write 4 bytes data
  // Write Message length in bits (bit reversed) (16bits, 2bytes)
  for (ii=15; ii >= 0; ii=ii-1) begin
	writedata[15-ii] = stream_length[ii];
  end
  // $display("...spi_xfer_user1_header length: 0x%x, ( bit-reversed: 0x%x)", stream_length, writedata);
  $display("...spi_xfer_user1_header recieved: 0x%x", readdata);
  jtag_read_write_stream(writedata , 8'd16, 0, readdata); // write 2 bytes data
end
endtask
//-------------------------------------------------------------------//
task spi_xfer_user1_byte;
	output [7:0] read_byte;
	input  [7:0] write_byte;
	input final_byte;

	reg [63:0] readdata;
	reg [63:0] writedata;
	integer ii;
begin
  $display("...spi_xfer_user1_body at time %t", $time);

  // MSB first
  for (ii=7; ii >= 0; ii=ii-1) begin
          writedata[7-ii] = write_byte[ii];
  end
  jtag_read_write_stream(writedata , 8'd8, final_byte, readdata); // transfer 8 bits
  for (ii=7; ii >= 0; ii=ii-1) begin
  	read_byte[7-ii] = readdata[ii];
  end
  if (final_byte) begin
	write_bit(JTAG_TMS_bit);      // update_dr
	write_bit(3'h0);              // idle
  end
end
endtask
//=================== Low Order Access functions ====================//
///////////////////////////////////////
// Check the TAP's ID code          //
/////////////////////////////////////
task check_idcode;
	reg [63:0] readdata;
	reg [31:0] idcode;
begin
$display("...check_idcode at time %t", $time);
set_ir(6'b001001); // 0x09
// Read the IDCODE in the DR
write_bit(JTAG_TMS_bit);  // select_dr_scan
write_bit(3'h0);           // capture_ir
write_bit(3'h0);           // shift_ir
jtag_read_write_stream(64'h0, 8'd32, 1, readdata); // write data, exit_1
write_bit(JTAG_TMS_bit);  // update_ir
write_bit(3'h0);           // idle
idcode = readdata[31:0];
$display("...Got TAP IDCODE 0x%x ", idcode);
end
endtask
////////////////////////////////
//  Set Instruction Register //
//////////////////////////////
// Puts a value in the TAP IR, assuming we start in IDLE state.
// Returns to IDLE state when finished
task set_ir;
	input [5:0] irval;
begin
write_bit(JTAG_TMS_bit);	// select_dr_scan
write_bit(JTAG_TMS_bit);	// select_ir_scan
write_bit(3'h0);		// capture_ir
write_bit(3'h0);		// shift_ir
jtag_write_stream({58'h0,irval}, 8'h6, 1);  // write data, exit_1
write_bit(JTAG_TMS_bit);	// update_ir
write_bit(3'h0);		// idle
end
endtask

/////////////////////////////////
//  Resets TAP                //
///////////////////////////////
task reset_jtag;
	integer i;
begin
for(i = 0; i < 8; i=i+1) begin
	write_bit(JTAG_TMS_bit);  // 5 TMS should put us in test_logic_reset mode
end
write_bit(3'h0);              // idle
end
endtask
////////////////////////////////////////////////////////////////////
//  Tasks to write or read-write a string of data                //
//////////////////////////////////////////////////////////////////
task jtag_write_stream;
	input [63:0] stream;
	input [7:0] len;
	input set_last_bit;
	integer i;
	integer databit;
	reg [2:0] bits;
begin
for(i = 0; i < (len-1); i=i+1) begin
	databit = (stream >> i) & 1'h1;
	bits = databit << JTAG_TDO;
	write_bit(bits);
end
databit = (stream >> i) & 1'h1;
bits = databit << JTAG_TDO;
if(set_last_bit) 
	bits = (bits | JTAG_TMS_bit);
write_bit(bits);

end
endtask
//-------------------------------------------------------------------//
task jtag_read_write_stream;
	input [63:0] stream;
	input [7:0] len;
	input set_last_bit;
	output [63:0] instream;
	integer i;
	integer databit;
	reg [2:0] bits;
	reg inbit;
begin
instream = 64'h0;
for(i = 0; i < (len-1); i=i+1) begin
	databit = (stream >> i) & 1'h1;
	bits = databit << JTAG_TDO;
	read_write_bit(bits, inbit);
	instream = (instream | (inbit << i));
end
databit = (stream >> i) & 1'h1;
bits = databit << JTAG_TDO;
if(set_last_bit) 
	bits = (bits | JTAG_TMS_bit);
read_write_bit(bits, inbit);
instream = (instream | (inbit << (len-1)));
end
endtask
/////////////////////////////////////////////////////////////////////////
//  Tasks which write or readwrite a single bit (including clocking)  //
///////////////////////////////////////////////////////////////////////
task write_bit;
	input [2:0] bitvals;
begin
// Set data
jtag_out(bitvals & ~(JTAG_TCK_bit));
#50000.0; // Wait 50nsecs
// Raise clock
jtag_out(bitvals | JTAG_TCK_bit);
#50000.0; // Wait 50nsecs
// drop clock (making output available in the SHIFT_xR states)
jtag_out(bitvals & ~(JTAG_TCK_bit));
#50000.0; // Wait 50nsecs
end
endtask
//-------------------------------------------------------------------//
task read_write_bit;
	input [2:0] bitvals;
	output l_tdi_val;
begin
// read bit state
l_tdi_val <= jtag_tdo_pad_o;
// Set data
jtag_out(bitvals & ~(JTAG_TCK_bit));
#50000.0; // Wait 50nsecs
// Raise clock
jtag_out(bitvals | JTAG_TCK_bit);
#50000.0; // Wait 50nsecs
// drop clock (making output available in the SHIFT_xR states)
jtag_out(bitvals & ~(JTAG_TCK_bit));
#50000.0; // Wait 50nsecs
end
endtask
///////////////////////////////////////
//  BASIC FUNCTIONS FOR JTAG        //
/////////////////////////////////////
task jtag_out;
	input   [2:0]   bitvals;
begin
jtag_tck_pad_i <= bitvals[JTAG_TCK];
jtag_tms_pad_i <= bitvals[JTAG_TMS];
jtag_tdi_pad_i <= bitvals[JTAG_TDO];
end
endtask
//-------------------------------------------------------------------//
task jtag_inout;
	input   [2:0]   bitvals;
	output l_tdi_val;
begin
jtag_tck_pad_i <= bitvals[JTAG_TCK];
jtag_tms_pad_i <= bitvals[JTAG_TMS];
jtag_tdi_pad_i <= bitvals[JTAG_TDO];

l_tdi_val <= jtag_tdo_pad_o;
end
endtask
//===================  SAVING TO THE FILE ===========================//
initial begin
  $dumpfile( "tb_bscan_spi.vcd" );
  $dumpvars;
end
//=================  Clocks =========================================//
always #5000.0 sys_clk = ~sys_clk; // 100 MHz

//=================== Main Test Tasks ===============================//
reg [7:0] write_byte;
reg [7:0] read_byte;

reg [7:0]  manuf_id_reg;
reg [15:0] device_id_reg;
reg [15:0] extended_device_id_reg;
initial begin
	@ (negedge wb_rst);
	$display("... Wishbone Reset Reached");
	reset_jtag;
	#10000;
	check_idcode;
	// If we've got IDCODE = 0x0364c093 then the tap is working
	// And we can test the Scan itself
	#10000;
	// Shifting header, we expect to transfer 1byte for instruction
	// and 20 bytes for SPI_ID = 21x8 = 168 bits.
	spi_xfer_user1_header(16'd168); // Parameter: length of stream
	// Parameters: output: read_byte, input: write_byte, input final_byte
	// Instruction
	spi_xfer_user1_byte(read_byte, SPI_READ_IDENTIFICATION, 0);
	// 20 bytes of data:
	spi_xfer_user1_byte(manuf_id_reg[7:0], 	8'h01, 0);
	spi_xfer_user1_byte(device_id_reg[15:8],8'h02, 0);
	spi_xfer_user1_byte(device_id_reg[7:0], 8'h03, 0);
	spi_xfer_user1_byte(read_byte, 		8'h04, 0);
	spi_xfer_user1_byte(read_byte, 		8'h05, 0);
	spi_xfer_user1_byte(read_byte, 		8'h06, 0);
	spi_xfer_user1_byte(read_byte, 		8'h07, 0);
	spi_xfer_user1_byte(read_byte, 		8'h08, 0);
	spi_xfer_user1_byte(read_byte, 		8'h09, 0);
	spi_xfer_user1_byte(read_byte, 		8'h0A, 0);
	spi_xfer_user1_byte(read_byte, 		8'h0B, 0);
	spi_xfer_user1_byte(read_byte, 		8'h0C, 0);
	spi_xfer_user1_byte(read_byte, 		8'h0D, 0);
	spi_xfer_user1_byte(read_byte, 		8'h0E, 0);
	spi_xfer_user1_byte(read_byte, 		8'h0F, 0);
	spi_xfer_user1_byte(read_byte, 		8'h10, 0);
	spi_xfer_user1_byte(read_byte, 		8'h11, 0);
	spi_xfer_user1_byte(read_byte, 		8'h12, 0);
	spi_xfer_user1_byte(read_byte, 		8'h13, 0);
	// Don't forget to indicate final byte
	spi_xfer_user1_byte(read_byte, 		8'h14, 1);

	$display("...Shifting out the data for ID");
	// Let's read it again
	spi_xfer_user1_header(16'd168);
	spi_xfer_user1_byte(read_byte, 		SPI_READ_IDENTIFICATION, 0);
	spi_xfer_user1_byte(manuf_id_reg[7:0],	8'h01, 0);
	spi_xfer_user1_byte(device_id_reg[15:8],8'h02, 0);
	spi_xfer_user1_byte(device_id_reg[7:0],	8'h03, 0);
	spi_xfer_user1_byte(read_byte,		8'h04, 0);
	spi_xfer_user1_byte(read_byte,		8'h05, 0);
	spi_xfer_user1_byte(read_byte,		8'h06, 0);
	spi_xfer_user1_byte(read_byte,		8'h07, 0);
	spi_xfer_user1_byte(read_byte,		8'h08, 0);
	spi_xfer_user1_byte(read_byte,		8'h09, 0);
	spi_xfer_user1_byte(read_byte,		8'h0A, 0);
	spi_xfer_user1_byte(read_byte,		8'h0B, 0);
	spi_xfer_user1_byte(read_byte,		8'h0C, 0);
	spi_xfer_user1_byte(read_byte,		8'h0D, 0);
	spi_xfer_user1_byte(read_byte,		8'h0E, 0);
	spi_xfer_user1_byte(read_byte,		8'h0F, 0);
	spi_xfer_user1_byte(read_byte,		8'h10, 0);
	spi_xfer_user1_byte(read_byte,		8'h11, 0);
	spi_xfer_user1_byte(read_byte,		8'h12, 0);
	spi_xfer_user1_byte(read_byte,		8'h13, 0);
	spi_xfer_user1_byte(read_byte,		8'h14, 1);

	$display("Resulting JTAG ID: 0x%x, 0x%x ", manuf_id_reg, device_id_reg);

	#10000000 $finish;
end

endmodule
