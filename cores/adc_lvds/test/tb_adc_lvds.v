`timescale 1ps / 1ps

module tb_adc_lvds;

//////////////////////////////////////////////
// Parameters for cti and bte burst cycles //
////////////////////////////////////////////

// bte:
parameter [1:0] linear = 2'b00,
		beat4  = 2'b01,
		beat8  = 2'b10,
		beat16 = 2'b11;
// cti:
parameter [2:0] classic = 3'b000,
		inc     = 3'b010,
		eob	= 3'b111;


///////////////////////////////////////
//  Input system clock is 100 MHz   //
/////////////////////////////////////
reg sys_clk = 1'b0;

////////////////////////////////////////////
// Clocks                                //
// As they are generated in real design //
/////////////////////////////////////////
wire adcs_clk;

wire mems_clk;
wire mems_rst;

wire wb_clk;
wire wb_rst;

wire wbfast_clk;
wire wbfast_rst;

wire spi0_clk; assign spi0_clk = 1'b0;
clkgen clkgen (
	// ADCs sampling clock
	.adcs_clk_o(adcs_clk),
	// Memories clock and reset
	.mems_clk_o( mems_clk ),
	.mems_rst_o( mems_rst ),
	// Normal wishbone clock and reset (CPU + Peripherials)
	.wb_clk_o(wb_clk),
	.wb_rst_o(wb_rst),
	// Fast wishbone bus clock and reset (ADCs + QSFPs)
	.wbfast_clk_o( wbfast_clk ),
	.wbfast_rst_o( wbfast_rst ),
	// Input system clock
        .sys_clk_i(sys_clk),
        // User Configuration Clock input (connected to SPI0 on the board)
        .cclock_i (spi0_clk)
);
////////////////////////////////////////////////////
//  Wishbone Master  (Substitute CPU)            //
//////////////////////////////////////////////////
reg [31:0]	wbm_adr_reg = 32'h0000_XXXX;
reg [1:0]	wbm_bte_reg = 2'bXX;
reg [2:0]	wbm_cti_reg = 3'bXXX;
reg		wbm_cyc_reg = 1'b0;
reg [31:0]	wbm_dat_reg = 32'hXXXX_XXXX;
reg [3:0]	wbm_sel_reg = 4'bxxxx;
reg 		wbm_stb_reg = 1'b0;
reg		wbm_we_reg  = 1'bX;

wire		wbm_ack_wire;
wire		wbm_err_wire;
wire		wbm_rty_wire;
wire [31:0]	wbm_dat_wire;

//---> Wires as defined by CPU
wire		wbm_d_cpu0_ack_i; assign wbm_ack_wire = wbm_d_cpu0_ack_i;
wire            wbm_d_cpu0_err_i; assign wbm_err_wire = wbm_d_cpu0_err_i;
wire            wbm_d_cpu0_rty_i; assign wbm_rty_wire = wbm_d_cpu0_rty_i;
wire [31:0]     wbm_d_cpu0_dat_i; assign wbm_dat_wire = wbm_d_cpu0_dat_i;

wire            wbm_d_cpu0_cyc_o; assign wbm_d_cpu0_cyc_o = wbm_cyc_reg;
wire [31:0]     wbm_d_cpu0_adr_o; assign wbm_d_cpu0_adr_o = wbm_adr_reg;
wire            wbm_d_cpu0_stb_o; assign wbm_d_cpu0_stb_o = wbm_stb_reg;
wire            wbm_d_cpu0_we_o;  assign wbm_d_cpu0_we_o = wbm_we_reg;
wire [3:0]      wbm_d_cpu0_sel_o; assign wbm_d_cpu0_sel_o = wbm_sel_reg;
wire [31:0]     wbm_d_cpu0_dat_o; assign wbm_d_cpu0_dat_o = wbm_dat_reg;
wire            wbm_d_cpu0_cab_o; assign wbm_d_cpu0_cab_o = 1'b0;
wire [2:0]      wbm_d_cpu0_cti_o; assign wbm_d_cpu0_cti_o = wbm_cti_reg;
wire [1:0]      wbm_d_cpu0_bte_o; assign wbm_d_cpu0_bte_o = wbm_bte_reg;


///////////////////////////////////////////////////
// Wishbone Slave (Will acknowledge everything)  //
// It substitutes MEMORY interface              //
/////////////////////////////////////////////////

wire [31:0]	wbs_adr_wire;
wire [1:0]	wbs_bte_wire;
wire [2:0]	wbs_cti_wire;
wire		wbs_cyc_wire;
wire [31:0]	wbs_dat_wire;
wire [3:0]	wbs_sel_wire;
wire		wbs_stb_wire;
wire		wbs_we_wire;

reg		wbs_ack_reg = 1'b0;
reg		wbs_err_reg = 1'b0;
reg		wbs_rty_reg = 1'b0;
reg [31:0]	wbs_dat_reg = 32'hXXXX_XXXX;

always @ (posedge wbs_clk) begin
	if (wbs_cyc_wire & wbs_stb_wire) begin
		wbs_ack_reg <= 1'b1;
		wbs_dat_reg <= 32'hDEAD_BEEF;
		if (wbs_we_wire) begin
			$display("==>WB Slave Write to\tAddr:0x%H \t Data:0x%H \t Mask:0x%H",
				wbs_adr_wire, wbs_dat_wire, wbs_sel_wire);
		end
	end else begin
		wbs_ack_reg <= 1'b0;
		wbs_dat_reg <= 32'hXXXX_XXXX;
	end
end
//---> Wires as defined by Memory
wire		wbs_a_mem1_ack_o; assign wbs_a_mem1_ack_o = wbs_ack_reg;
wire		wbs_a_mem1_err_o; assign wbs_a_mem1_err_o = wbs_err_reg;
wire		wbs_a_mem1_rty_o; assign wbs_a_mem1_rty_o = wbs_rty_reg;
wire [31:0]	wbs_a_mem1_dat_o; assign wbs_a_mem1_dat_o = wbs_dat_reg;

wire		wbs_a_mem1_cyc_i; assign wbs_cyc_wire = wbs_a_mem1_cyc_i;
wire [31:0]	wbs_a_mem1_adr_i; assign wbs_adr_wire = wbs_a_mem1_adr_i;
wire		wbs_a_mem1_stb_i; assign wbs_stb_wire = wbs_a_mem1_stb_i;
wire		wbs_a_mem1_we_i;  assign wbs_we_wire  = wbs_a_mem1_we_i;
wire [3:0]	wbs_a_mem1_sel_i; assign wbs_sel_wire = wbs_a_mem1_sel_i;
wire [31:0]	wbs_a_mem1_dat_i; assign wbs_dat_wire = wbs_a_mem1_dat_i;
wire		wbs_a_mem1_cab_i; // No cab
wire [2:0]	wbs_a_mem1_cti_i; assign wbs_cti_wire = wbs_a_mem1_cti_i;
wire [1:0]	wbs_a_mem1_bte_i; assign wbs_bte_wire = wbs_a_mem1_bte_i;

//===================================================================//
// Physical connections to the Chip
// Data
wire [8:1] adc1_d_i_p;
wire [8:1] adc1_d_i_n;
wire adc1_clk_o_p;
wire adc1_clk_o_n;
wire adc1_fco_i_p;
wire adc1_fco_i_n;
wire adc1_dco_i_p;
wire adc1_dco_i_n;
// SPI
wire adc1_spi_clk_o;
wire adc1_spi_dat_io;
wire adc1_spi_csb_o;
// Power
wire pow_adc1_vdd_enable_o;
wire pow_adc1_g1_enable_o;
wire pow_adc1_g2_enable_o;
wire pow_adc1_g3_enable_o;
wire pow_adc1_g4_enable_o;

///////////////////////////////////////////////
// One Pulse Per Second signal generation   //
/////////////////////////////////////////////
// For the testing purposes it's really not
// 1 pulse per second, it's faster
wire one_pulse_per_sec_clk;
reg [31:0] wb_cycles_counter = 32'h0000_0000;
always@(posedge wb_clk) begin
  if ( wb_cycles_counter == 32'h0000_0780 ) // 50MhZ
	wb_cycles_counter <= 32'h0000_0000;
  else
	wb_cycles_counter <= wb_cycles_counter + 1'b1;
end

reg one_pulse_per_sec_reg = 1'b0;
always@(posedge wb_clk) begin
  if ( wb_cycles_counter == 32'h0000_0340 ) // Half of 1sec
	one_pulse_per_sec_reg <= 1'b0;
  else if ( wb_cycles_counter == 32'h0000_0000 )
	one_pulse_per_sec_reg <= 1'b1;
  end
assign one_pulse_per_sec_clk = one_pulse_per_sec_reg;

/////////////////////////////////////////////
//   ADC1                                 //
///////////////////////////////////////////
wire            adc1_interrupt;
// Wishbone slave signals
wire [31:0]     wbs_c_adc1_adr_i;
wire [3:0]      wbs_c_adc1_sel_i;
wire            wbs_c_adc1_we_i;
wire [31:0]     wbs_c_adc1_dat_i;
wire            wbs_c_adc1_cyc_i;
wire            wbs_c_adc1_stb_i;
wire [2:0]      wbs_c_adc1_cti_i;
wire [1:0]      wbs_c_adc1_bte_i;

wire [31:0]     wbs_c_adc1_dat_o;
wire            wbs_c_adc1_ack_o;
wire            wbs_c_adc1_err_o;
wire            wbs_c_adc1_rty_o;

// Wishbone Master signals
wire [31:0]     wbm_a_adc1_adr_o;
wire [3:0]      wbm_a_adc1_sel_o;
wire            wbm_a_adc1_we_o;
wire [31:0]     wbm_a_adc1_dat_o;
wire            wbm_a_adc1_cyc_o;
wire            wbm_a_adc1_stb_o;
wire [2:0]      wbm_a_adc1_cti_o;
wire [1:0]      wbm_a_adc1_bte_o;

wire [31:0]     wbm_a_adc1_dat_i;
wire            wbm_a_adc1_ack_i;
wire            wbm_a_adc1_err_i;
wire            wbm_a_adc1_rty_i;

adc_lvds #(
	.D1_IDELAY(15),
	.D2_IDELAY(15),
	.D3_IDELAY(15),
	.D4_IDELAY(15),
	.D5_IDELAY(15),
	.D6_IDELAY(15),
	.D7_IDELAY(15),
	.D8_IDELAY(15),
	.FC_IDELAY(15),
	.DC_IDELAY(15)
) adc1 (
//---> Master Wishbone connections
  .wbm_adr_o( wbm_a_adc1_adr_o ),
  .wbm_bte_o( wbm_a_adc1_bte_o ),
  .wbm_cti_o( wbm_a_adc1_cti_o ),
  .wbm_cyc_o( wbm_a_adc1_cyc_o ),
  .wbm_dat_o( wbm_a_adc1_dat_o ),
  .wbm_sel_o( wbm_a_adc1_sel_o ),
  .wbm_stb_o( wbm_a_adc1_stb_o ),
  .wbm_we_o ( wbm_a_adc1_we_o  ),
  .wbm_ack_i( wbm_a_adc1_ack_i ),
  .wbm_err_i( wbm_a_adc1_err_i ),
  .wbm_rty_i( wbm_a_adc1_rty_i ),
  .wbm_dat_i( wbm_a_adc1_dat_i ),
  .wbm_clk_i( wbfast_clk ),
  .wbm_rst_i( wbfast_rst ),
//---> Slave Wishbone connections
  .wbs_adr_i( wbs_c_adc1_adr_i ),
  .wbs_bte_i( wbs_c_adc1_bte_i ),
  .wbs_cti_i( wbs_c_adc1_cti_i ),
  .wbs_cyc_i( wbs_c_adc1_cyc_i ),
  .wbs_dat_i( wbs_c_adc1_dat_i ),
  .wbs_sel_i( wbs_c_adc1_sel_i ),
  .wbs_stb_i( wbs_c_adc1_stb_i ),
  .wbs_we_i ( wbs_c_adc1_we_i  ),
  .wbs_ack_o( wbs_c_adc1_ack_o ),
  .wbs_err_o( wbs_c_adc1_err_o ),
  .wbs_rty_o( wbs_c_adc1_rty_o ),
  .wbs_dat_o( wbs_c_adc1_dat_o ),
  .wbs_clk_i( wb_clk ),
  .wbs_rst_i( wb_rst ),
//---> Connection to the ADC Chip
  .data_i_p		(adc1_d_i_p),
  .data_i_n		(adc1_d_i_n),
  .frame_clk_i_p	(adc1_fco_i_p),
  .frame_clk_i_n	(adc1_fco_i_n),
  .data_clk_i_p		(adc1_dco_i_p),
  .data_clk_i_n		(adc1_dco_i_n),
  .sample_clk_o_p	(adc1_clk_o_p),
  .sample_clk_o_n	(adc1_clk_o_n),
  .spi_cs_o		(adc1_spi_csb_o),
  .spi_dat_io		(adc1_spi_dat_io),
  .spi_clk_o		(adc1_spi_clk_o),
//---> Power Management
  .pow_adc_en_o	( pow_adc1_vdd_enable_o ),
  .pow_gr1_en_o	( pow_adc1_g1_enable_o  ),
  .pow_gr2_en_o	( pow_adc1_g2_enable_o  ),
  .pow_gr3_en_o	( pow_adc1_g3_enable_o  ),
  .pow_gr4_en_o	( pow_adc1_g4_enable_o  ),
//---> Interrupt
  .interrupt_o	( adc1_interrupt ),
//---> For Timestamp:
  .time_pulse_i ( one_pulse_per_sec_clk ),
//---> Input Clock for Sample Frequency
  .adc_clk_i	( adcs_clk )
);
//-------------------------------------------------------------------//
AD9637 #(
	.SAMPLING_SPEED (40)
) adc1_chip (
  .PDWN	(1'b0),
  .SYNC	(1'b0),

  .CSB		( adc1_spi_csb_o),
  .SDIO_DFS	( adc1_spi_dat_io ),
  .SCLK_DTP	( adc1_spi_clk_o ),

  .D1_P	(adc1_d_i_p[1]),
  .D1_N	(adc1_d_i_n[1]),
  .D2_P (adc1_d_i_p[2]),
  .D2_N (adc1_d_i_n[2]),
  .D3_P (adc1_d_i_p[3]),
  .D3_N (adc1_d_i_n[3]),
  .D4_P (adc1_d_i_p[4]),
  .D4_N (adc1_d_i_n[4]),
  .D5_P (adc1_d_i_p[5]),
  .D5_N (adc1_d_i_n[5]),
  .D6_P (adc1_d_i_p[6]),
  .D6_N (adc1_d_i_n[6]),
  .D7_P (adc1_d_i_p[7]),
  .D7_N (adc1_d_i_n[7]),
  .D8_P (adc1_d_i_p[8]),
  .D8_N (adc1_d_i_n[8]),

  .FCO_P(adc1_fco_i_p),
  .FCO_N(adc1_fco_i_n),
  .DCO_P(adc1_dco_i_p),
  .DCO_N(adc1_dco_i_n),

  .CLK_P(adc1_clk_o_p),
  .CLK_N(adc1_clk_o_n)
);
//-------------------------------------------------------------------//
////////////////////////////////////////////////
// We'll need 3 Buses: Dbus, Cbus and Abus   //
//////////////////////////////////////////////
// From dbus to cbus
wire		wbs_d_cbus_ack_o;
wire		wbs_d_cbus_err_o;
wire		wbs_d_cbus_rty_o;
wire [31:0]	wbs_d_cbus_dat_o;
wire		wbs_d_cbus_cyc_i;
wire [31:0]	wbs_d_cbus_adr_i;
wire		wbs_d_cbus_stb_i;
wire		wbs_d_cbus_we_i;
wire [3:0]	wbs_d_cbus_sel_i;
wire [31:0]	wbs_d_cbus_dat_i;
wire [2:0]	wbs_d_cbus_cti_i;
wire [1:0]	wbs_d_cbus_bte_i;
////////////////////////////////////////
// Wishbone data bus arbiter (DBUS)  //
//////////////////////////////////////
// 3 Masters, 5 Slaves
// Bus is 32bit wide
wb_switch_b3 # (
        .dw ( 32 ),
        .aw ( 32 ),
        .slave0_sel_width( 8 ),     // Width of selection bits 8 == [31:24]
        .slave0_sel_addr ( 8'h00 ), // Matching value of the sel_width bits
        .slave0_sel_mask ( 8'hFF ),
        .slave1_sel_width( 8 ),
        .slave1_sel_addr ( 8'h01 ),
        .slave1_sel_mask ( 8'hFF ),
        .slave2_sel_width( 8 ),
        .slave2_sel_addr ( 8'b1000_0000),
        .slave2_sel_mask ( 8'b1100_1111 ), // When mask is 0 -> don't care
        .slave3_sel_width( 4 ),
        .slave3_sel_addr ( 4'h7 ),
        .slave3_sel_mask ( 4'hF ),
        .slave4_sel_width( 8 ),
        .slave4_sel_addr ( 8'h92 ),
        .slave4_sel_mask ( 8'hFF )
) dbus (
  //----------- Masters ----------->
  // Master 0 == CPU0
  .wbm0_adr_i( wbm_d_cpu0_adr_o ),
  .wbm0_bte_i( wbm_d_cpu0_bte_o ),
  .wbm0_cti_i( wbm_d_cpu0_cti_o ),
  .wbm0_cyc_i( wbm_d_cpu0_cyc_o ),
  .wbm0_dat_i( wbm_d_cpu0_dat_o ),
  .wbm0_sel_i( wbm_d_cpu0_sel_o ),
  .wbm0_stb_i( wbm_d_cpu0_stb_o ),
  .wbm0_we_i ( wbm_d_cpu0_we_o ),
  .wbm0_ack_o( wbm_d_cpu0_ack_i ),
  .wbm0_err_o( wbm_d_cpu0_err_i ),
  .wbm0_rty_o( wbm_d_cpu0_rty_i ),
  .wbm0_dat_o( wbm_d_cpu0_dat_i ),
  // NOT Connected: wbm_d_cpu0_cab_o
  // Master 1 == DBG0
  .wbm1_adr_i( 32'h0000_0000 ),
  .wbm1_bte_i( 2'b00 ),
  .wbm1_cti_i( 3'b000 ),
  .wbm1_cyc_i( 1'b0 ),
  .wbm1_dat_i( 32'h0000_0000 ),
  .wbm1_sel_i( 4'b0000 ),
  .wbm1_stb_i( 1'b0 ),
  .wbm1_we_i ( 1'b0 ),
  .wbm1_ack_o( ),
  .wbm1_err_o( ),
  .wbm1_rty_o( ),
  .wbm1_dat_o( ),
  // NOT Connected: wbm_d_dbg0_cab_o
  // Master 2 == ETH0, Master
  .wbm2_adr_i( 32'h0000_0000 ),
  .wbm2_bte_i( 2'b00 ),
  .wbm2_cti_i( 3'b000 ),
  .wbm2_cyc_i( 1'b0 ),
  .wbm2_dat_i( 32'h0000_0000 ),
  .wbm2_sel_i( 4'b0000 ),
  .wbm2_stb_i( 1'b0 ),
  .wbm2_we_i ( 1'b0 ),
  .wbm2_ack_o( ),
  .wbm2_err_o( ),
  .wbm2_rty_o( ),
  .wbm2_dat_o( ),
  //------------ Slaves ----------->
  // Slave 0 == MEM0, Port 1
  .wbs0_adr_o( ),
  .wbs0_bte_o( ),
  .wbs0_cti_o( ),
  .wbs0_cyc_o( ),
  .wbs0_dat_o( ),
  .wbs0_sel_o( ),
  .wbs0_stb_o( ),
  .wbs0_we_o ( ),
  .wbs0_ack_i( 1'b0 ),
  .wbs0_err_i( 1'b0 ),
  .wbs0_rty_i( 1'b0 ),
  .wbs0_dat_i( 32'h0000_0000 ),
  // NOT CONNECTED:  wbs_d_mem0_cab_i
  // Slave 1 == MEM1, Port 0
  .wbs1_adr_o( ),
  .wbs1_bte_o( ),
  .wbs1_cti_o( ),
  .wbs1_cyc_o( ),
  .wbs1_dat_o( ),
  .wbs1_sel_o( ),
  .wbs1_stb_o( ),
  .wbs1_we_o ( ),
  .wbs1_ack_i( 1'b0 ),
  .wbs1_err_i( 1'b0 ),
  .wbs1_rty_i( 1'b0 ),
  .wbs1_dat_i( 32'h0000_0000 ),
  // NOT CONNECTED: wbs_d_mem1_cab_i
  // Slave 2 == Bridge to BBUS0
  .wbs2_adr_o( ),
  .wbs2_bte_o( ),
  .wbs2_cti_o( ),
  .wbs2_cyc_o( ),
  .wbs2_dat_o( ),
  .wbs2_sel_o( ),
  .wbs2_stb_o( ),
  .wbs2_we_o ( ),
  .wbs2_ack_i( 1'b0 ),
  .wbs2_err_i( 1'b0 ),
  .wbs2_rty_i( 1'b0 ),
  .wbs2_dat_i( 32'h0000_0000 ),
  // Slave 3 == Bridge to CBUS
  .wbs3_adr_o( wbs_d_cbus_adr_i ),
  .wbs3_bte_o( wbs_d_cbus_bte_i ),
  .wbs3_cti_o( wbs_d_cbus_cti_i ),
  .wbs3_cyc_o( wbs_d_cbus_cyc_i ),
  .wbs3_dat_o( wbs_d_cbus_dat_i ),
  .wbs3_sel_o( wbs_d_cbus_sel_i ),
  .wbs3_stb_o( wbs_d_cbus_stb_i ),
  .wbs3_we_o ( wbs_d_cbus_we_i ),
  .wbs3_ack_i( wbs_d_cbus_ack_o ),
  .wbs3_err_i( wbs_d_cbus_err_o ),
  .wbs3_rty_i( wbs_d_cbus_rty_o ),
  .wbs3_dat_i( wbs_d_cbus_dat_o ),
  // Slave 4 == ETH0, Slave
  .wbs4_adr_o( ),
  .wbs4_bte_o( ),
  .wbs4_cti_o( ),
  .wbs4_cyc_o( ),
  .wbs4_dat_o( ),
  .wbs4_sel_o( ),
  .wbs4_stb_o( ),
  .wbs4_we_o ( ),
  .wbs4_ack_i( 1'b0 ),
  .wbs4_err_i( 1'b0 ),
  .wbs4_rty_i( 1'b0 ),
  .wbs4_dat_i( 32'h0000_0000 ),
  //------------ Clock, reset ----->
  .wb_clk ( wb_clk ),
  .wb_rst ( wb_rst )
);
//-------------------------------------------------------------------//
//////////////////////////////////////////////////////////////
// Wishbone communication bus arbitrer for ADCs CBUS       //
////////////////////////////////////////////////////////////
// 1 Master, 8 Slaves
// Bus is 32bit wide
arbiter_cbus cbus(
  //----------- Masters ----------->
  // Master 0 == Bridge from DBUS, Slave 3
  .wbm0_adr_i( wbs_d_cbus_adr_i ),
  .wbm0_dat_i( wbs_d_cbus_dat_i ),
  .wbm0_sel_i( wbs_d_cbus_sel_i ),
  .wbm0_we_i ( wbs_d_cbus_we_i ),
  .wbm0_cyc_i( wbs_d_cbus_cyc_i ),
  .wbm0_stb_i( wbs_d_cbus_stb_i ),
  .wbm0_cti_i( wbs_d_cbus_cti_i ),
  .wbm0_bte_i( wbs_d_cbus_bte_i ),
  .wbm0_dat_o( wbs_d_cbus_dat_o ),
  .wbm0_ack_o( wbs_d_cbus_ack_o ),
  .wbm0_err_o( wbs_d_cbus_err_o ),
  .wbm0_rty_o( wbs_d_cbus_rty_o ),
  //------------ Slaves ----------->
  // Slave 0 == QSFP0, Slave
  .wbs0_adr_o( ),
  .wbs0_sel_o( ),
  .wbs0_dat_o( ),
  .wbs0_we_o ( ),
  .wbs0_cyc_o( ),
  .wbs0_stb_o( ),
  .wbs0_cti_o( ),
  .wbs0_bte_o( ),
  .wbs0_dat_i( 32'h0000_0000 ),
  .wbs0_ack_i( 1'b0 ),
  .wbs0_err_i( 1'b0 ),
  .wbs0_rty_i( 1'b0 ),
  // Slave 1 == QSFP1, Slave
  .wbs1_adr_o( ),
  .wbs1_sel_o( ),
  .wbs1_dat_o( ),
  .wbs1_we_o ( ),
  .wbs1_cyc_o( ),
  .wbs1_stb_o( ),
  .wbs1_cti_o( ),
  .wbs1_bte_o( ),
  .wbs1_dat_i( 32'h0000_0000 ),
  .wbs1_ack_i( 1'b0 ),
  .wbs1_err_i( 1'b0 ),
  .wbs1_rty_i( 1'b0 ),
  // Slave 2 == ADC1, Slave
  .wbs2_adr_o( wbs_c_adc1_adr_i ),
  .wbs2_sel_o( wbs_c_adc1_sel_i ),
  .wbs2_dat_o( wbs_c_adc1_dat_i ),
  .wbs2_we_o ( wbs_c_adc1_we_i  ),
  .wbs2_cyc_o( wbs_c_adc1_cyc_i ),
  .wbs2_stb_o( wbs_c_adc1_stb_i ),
  .wbs2_cti_o( wbs_c_adc1_cti_i ),
  .wbs2_bte_o( wbs_c_adc1_bte_i ),
  .wbs2_dat_i( wbs_c_adc1_dat_o ),
  .wbs2_ack_i( wbs_c_adc1_ack_o ),
  .wbs2_err_i( wbs_c_adc1_err_o ),
  .wbs2_rty_i( wbs_c_adc1_rty_o ),
  // Slave 3 == ADC2, Slave
  .wbs3_adr_o( ),
  .wbs3_sel_o( ),
  .wbs3_dat_o( ),
  .wbs3_we_o ( ),
  .wbs3_cyc_o( ),
  .wbs3_stb_o( ),
  .wbs3_cti_o( ),
  .wbs3_bte_o( ),
  .wbs3_dat_i( 32'h0000_0000 ),
  .wbs3_ack_i( 1'b0 ),
  .wbs3_err_i( 1'b0 ),
  .wbs3_rty_i( 1'b0 ),
  // Slave 4 == ADC3, Slave
  .wbs4_adr_o( ),
  .wbs4_sel_o( ),
  .wbs4_dat_o( ),
  .wbs4_we_o ( ),
  .wbs4_cyc_o( ),
  .wbs4_stb_o( ),
  .wbs4_cti_o( ),
  .wbs4_bte_o( ),
  .wbs4_dat_i( 32'h0000_0000 ),
  .wbs4_ack_i( 1'b0 ),
  .wbs4_err_i( 1'b0 ),
  .wbs4_rty_i( 1'b0 ),
  // Slave 5 == ADC4, Slave
  .wbs5_adr_o( ),
  .wbs5_sel_o( ),
  .wbs5_dat_o( ),
  .wbs5_we_o ( ),
  .wbs5_cyc_o( ),
  .wbs5_stb_o( ),
  .wbs5_cti_o( ),
  .wbs5_bte_o( ),
  .wbs5_dat_i( 32'h0000_0000 ),
  .wbs5_ack_i( 1'b0 ),
  .wbs5_err_i( 1'b0 ),
  .wbs5_rty_i( 1'b0 ),
  // Slave 6 == ADC5, Slave
  .wbs6_adr_o( ),
  .wbs6_sel_o( ),
  .wbs6_dat_o( ),
  .wbs6_we_o ( ),
  .wbs6_cyc_o( ),
  .wbs6_stb_o( ),
  .wbs6_cti_o( ),
  .wbs6_bte_o( ),
  .wbs6_dat_i( 32'h0000_0000 ),
  .wbs6_ack_i( 1'b0 ),
  .wbs6_err_i( 1'b0 ),
  .wbs6_rty_i( 1'b0 ),
  // Slave 7 == ADC6, Slave
  .wbs7_adr_o( ),
  .wbs7_sel_o( ),
  .wbs7_dat_o( ),
  .wbs7_we_o ( ),
  .wbs7_cyc_o( ),
  .wbs7_stb_o( ),
  .wbs7_cti_o( ),
  .wbs7_bte_o( ),
  .wbs7_dat_i( 32'h0000_0000 ),
  .wbs7_ack_i( 1'b0 ),
  .wbs7_err_i( 1'b0 ),
  .wbs7_rty_i( 1'b0 ),
  //------------ Clock, reset ----->
  .wb_clk_i ( wb_clk ),
  .wb_rst_i ( wb_rst )
);
  // See also comments on DBUS if you want to change numbers here
  defparam cbus.wb_addr_match_width = 8;
  defparam cbus.slave0_adr = 8'h71; // QSFP0
  defparam cbus.slave1_adr = 8'h72; // QSFP1
  defparam cbus.slave2_adr = 8'h7A; // ADC1
  defparam cbus.slave3_adr = 8'h7B; // ADC2
  defparam cbus.slave4_adr = 8'h7C; // ADC3
  defparam cbus.slave5_adr = 8'h7D; // ADC4
  defparam cbus.slave6_adr = 8'h7E; // ADC5
  defparam cbus.slave7_adr = 8'h7F; // ADC6
//-------------------------------------------------------------------//
//////////////////////////////////////////////////////////////////
// Wishbone bus arbitrer for ADCs to MEM connectiob ABUS       //
////////////////////////////////////////////////////////////////
// This one is special, it can be a separate clock region
// 8 Masters, 1 Slave
// Bus is 32bit wide
arbiter_abus abus(
  //----------- Masters ----------->
  // Master 0 == QSFP0, Master
  .wbm0_adr_i( 32'h0000_0000 ),
  .wbm0_dat_i( 32'h0000_0000 ),
  .wbm0_sel_i( 4'b0000 ),
  .wbm0_we_i ( 1'b0 ),
  .wbm0_cyc_i( 1'b0 ),
  .wbm0_stb_i( 1'b0 ),
  .wbm0_cti_i( 3'b000 ),
  .wbm0_bte_i( 2'b00 ),
  .wbm0_dat_o(  ),
  .wbm0_ack_o(  ),
  .wbm0_err_o(  ),
  .wbm0_rty_o(  ),
  // Master 1 == QSFP1, Master
  .wbm1_adr_i( 32'h0000_0000 ),
  .wbm1_dat_i( 32'h0000_0000 ),
  .wbm1_sel_i( 4'b0000 ),
  .wbm1_we_i ( 1'b0 ),
  .wbm1_cyc_i( 1'b0 ),
  .wbm1_stb_i( 1'b0 ),
  .wbm1_cti_i( 3'b000 ),
  .wbm1_bte_i( 2'b00 ),
  .wbm1_dat_o(  ),
  .wbm1_ack_o(  ),
  .wbm1_err_o(  ),
  .wbm1_rty_o(  ),
  // Master 2 == ADC1, Master
  .wbm2_adr_i( wbm_a_adc1_adr_o ),
  .wbm2_dat_i( wbm_a_adc1_dat_o ),
  .wbm2_sel_i( wbm_a_adc1_sel_o ),
  .wbm2_we_i ( wbm_a_adc1_we_o  ),
  .wbm2_cyc_i( wbm_a_adc1_cyc_o ),
  .wbm2_stb_i( wbm_a_adc1_stb_o ),
  .wbm2_cti_i( wbm_a_adc1_cti_o ),
  .wbm2_bte_i( wbm_a_adc1_bte_o ),
  .wbm2_dat_o( wbm_a_adc1_dat_i ),
  .wbm2_ack_o( wbm_a_adc1_ack_i ),
  .wbm2_err_o( wbm_a_adc1_err_i ),
  .wbm2_rty_o( wbm_a_adc1_rty_i ),
  // Master 3 == ADC2, Master
  .wbm3_adr_i( 32'h0000_0000 ),
  .wbm3_dat_i( 32'h0000_0000 ),
  .wbm3_sel_i( 4'b0000 ),
  .wbm3_we_i ( 1'b0 ),
  .wbm3_cyc_i( 1'b0 ),
  .wbm3_stb_i( 1'b0 ),
  .wbm3_cti_i( 3'b000 ),
  .wbm3_bte_i( 2'b00 ),
  .wbm3_dat_o( ),
  .wbm3_ack_o( ),
  .wbm3_err_o( ),
  .wbm3_rty_o( ),
  // Master 4 == ADC3, Master
  .wbm4_adr_i( 32'h0000_0000 ),
  .wbm4_dat_i( 32'h0000_0000 ),
  .wbm4_sel_i( 4'b0000 ),
  .wbm4_we_i ( 1'b0 ),
  .wbm4_cyc_i( 1'b0 ),
  .wbm4_stb_i( 1'b0 ),
  .wbm4_cti_i( 3'b000 ),
  .wbm4_bte_i( 2'b00 ),
  .wbm4_dat_o( ),
  .wbm4_ack_o( ),
  .wbm4_err_o( ),
  .wbm4_rty_o( ),
  // Master 5 == ADC4, Master
  .wbm5_adr_i( 32'h0000_0000 ),
  .wbm5_dat_i( 32'h0000_0000 ),
  .wbm5_sel_i( 4'b0000 ),
  .wbm5_we_i ( 1'b0 ),
  .wbm5_cyc_i( 1'b0 ),
  .wbm5_stb_i( 1'b0 ),
  .wbm5_cti_i( 3'b000 ),
  .wbm5_bte_i( 2'b00 ),
  .wbm5_dat_o( ),
  .wbm5_ack_o( ),
  .wbm5_err_o( ),
  .wbm5_rty_o( ),
  // Master 6 == ADC5, Master
  .wbm6_adr_i( 32'h0000_0000 ),
  .wbm6_dat_i( 32'h0000_0000 ),
  .wbm6_sel_i( 4'b0000 ),
  .wbm6_we_i ( 1'b0 ),
  .wbm6_cyc_i( 1'b0 ),
  .wbm6_stb_i( 1'b0 ),
  .wbm6_cti_i( 3'b000 ),
  .wbm6_bte_i( 2'b00 ),
  .wbm6_dat_o( ),
  .wbm6_ack_o( ),
  .wbm6_err_o( ),
  .wbm6_rty_o( ),
  // Master 7 == ADC6, Master
  .wbm7_adr_i( 32'h0000_0000 ),
  .wbm7_dat_i( 32'h0000_0000 ),
  .wbm7_sel_i( 4'b0000 ),
  .wbm7_we_i ( 1'b0  ),
  .wbm7_cyc_i( 1'b0 ),
  .wbm7_stb_i( 1'b0 ),
  .wbm7_cti_i( 3'b000 ),
  .wbm7_bte_i( 2'b00 ),
  .wbm7_dat_o( ),
  .wbm7_ack_o( ),
  .wbm7_err_o( ),
  .wbm7_rty_o( ),
  //------------ Slaves ----------->
  // Slave 0 == MEM1, Port 1
  .wbs0_adr_o( wbs_a_mem1_adr_i ),
  .wbs0_dat_o( wbs_a_mem1_dat_i ),
  .wbs0_sel_o( wbs_a_mem1_sel_i ),
  .wbs0_we_o ( wbs_a_mem1_we_i ),
  .wbs0_cyc_o( wbs_a_mem1_cyc_i ),
  .wbs0_stb_o( wbs_a_mem1_stb_i ),
  .wbs0_cti_o( wbs_a_mem1_cti_i ),
  .wbs0_bte_o( wbs_a_mem1_bte_i ),
  .wbs0_dat_i( wbs_a_mem1_dat_o ),
  .wbs0_ack_i( wbs_a_mem1_ack_o ),
  .wbs0_err_i( wbs_a_mem1_err_o ),
  .wbs0_rty_i( wbs_a_mem1_rty_o ),
  // NOT CONNECTED: wbs_a_mem1_cab_i
  //------------ Clock, reset ----->
  .wb_clk_i ( wbfast_clk ),
  .wb_rst_i ( wbfast_rst )
);
  // With one slave the address assignment is useless
  // So these parameters are not even defined for abus
  // defparam abus.wb_addr_match_width = 1;
  // defparam abus.slave0_adr = XXX;
//===================  TASKS ========================================//
wire wbs_clk; assign wbs_clk = wbfast_clk;
wire wbs_rst; assign wbs_rst = wbfast_rst;

wire wbm_clk; assign wbm_clk = wb_clk;
wire wbm_rst; assign wbm_rst = wb_rst;
///////////////////////////////////////
//  Wishbone Write                  //
/////////////////////////////////////
task wbm_write;
	input [31:0] 	addr;
	input  [3:0] 	wsel;
	input [31:0] 	data;
	input [2:0]	cti;
	input [1:0]	bte;
	input		cyc;
	input		stb;
begin
  @ (negedge wbm_clk);
  wbm_adr_reg <= addr;
  wbm_sel_reg <= wsel;
  wbm_we_reg  <= 1'b1;
  wbm_stb_reg <= 1'b1;
  wbm_cyc_reg <= 1'b1;
  wbm_dat_reg <= data;
  wbm_cti_reg <= cti;
  wbm_bte_reg <= bte;
  @ (posedge wbm_clk);
  while (wbm_ack_wire == 1'b0) begin
	@(posedge wbm_clk);
  end
  $display("   WBM Write  \t Addr:0x%H \t Data:0x%H \t Mask:0x%H", 
	wbm_adr_reg, wbm_dat_reg, wbm_sel_reg);
  wbm_adr_reg <= 'hX;
  wbm_sel_reg <= 'hX;
  wbm_dat_reg <= 'hX;
  wbm_stb_reg <= stb;
  wbm_cyc_reg <= cyc;
  wbm_we_reg  <= 1'bX;
end
endtask
//-------------------------------------------------------------------//
///////////////////////////////////////
//  Wishbone Read                   //
/////////////////////////////////////
task wbm_read;
	input  [31:0] 	addr;
	input  [3:0]  	wsel;
	output [31:0] 	data;
	input  [2:0] 	cti;
	input  [1:0] 	bte;
	input	     	cyc;
	input	     	stb;
begin
  @ (negedge wbm_clk);
  wbm_adr_reg <= addr;
  wbm_sel_reg <= wsel;
  wbm_we_reg  <= 1'b0;
  wbm_stb_reg <= 1'b1;
  wbm_cyc_reg <= 1'b1;
  wbm_cti_reg <= cti;
  wbm_bte_reg <= bte;
  @ (posedge wbm_clk);
  while ((wbm_ack_wire == 1'b0)) begin
	@(posedge wbm_clk);
  end
  data <= wbm_dat_wire;
  $display("...WBM Read  \t Addr:0x%H \t Data:0x%H", wbm_adr_reg, wbm_dat_wire);
  wbm_we_reg  <= 1'bX;
  wbm_cyc_reg <= cyc;
  wbm_stb_reg <= stb;
  wbm_adr_reg <= 'hX;
  wbm_sel_reg <= 'hx;
  wbm_dat_reg <= 'hX;
end
endtask
//-------------------------------------------------------------------//
//===================  SAVING TO THE FILE ===========================//
initial begin
  $dumpfile( "tb_adc_lvds.vcd" );
  $dumpvars;
end
//=================  Clocks ==========================================//
always #5000.0 sys_clk = ~sys_clk; // 100 MHz
//============= WBS0 Tasks =============//
reg [31:0] wbm_readout_data;
initial begin // Interrupt handling
	@ (posedge adc1_interrupt);
	$display("...Interrupt Reached, Let's Acknowledge it ");
	wbm_read (32'h7A00_01FC, 4'hF,  wbm_readout_data,       classic,        linear, 1'b0, 1'b0);
	wbm_read (32'h7A00_01FC, 4'hF,  wbm_readout_data,       classic,        linear, 1'b0, 1'b0);
	wbm_write(32'h7A00_01FC, 4'hF,  wbm_readout_data,       eob,            linear, 1'b0, 1'b0);
	wbm_read (32'h7A00_01FC, 4'hF,  wbm_readout_data,       classic,        linear, 1'b0, 1'b0);
	wbm_write(32'h7A00_01FC, 4'hF,  32'h0000_00FF,          eob,            linear, 1'b0, 1'b0);
	wbm_write(32'h7A00_01FC, 4'hF,  32'h0000_00FF,          eob,            linear, 1'b0, 1'b0);

end
// ADC1 is at the offset 7A
initial begin
	@ (negedge wbm_rst);
	$display("... Reset reached");
	/*  							cti,  		bte,	cyc,  stb 
	 *  cti:	classic,  inc,  eob	
	 *  bte:	linear, beat4, beat8, beat16  
	 */
	$display("... Writing Power Enable ");
	wbm_write(32'h7A00_0000, 4'hF,	32'h0100_0000,		eob, 		linear, 1'b0, 1'b0);
	wbm_write(32'h7A00_0000, 4'hF,  32'h0300_0000,          eob,            linear, 1'b0, 1'b0);
	wbm_write(32'h7A00_0000, 4'hF,  32'h0700_0000,          eob,            linear, 1'b0, 1'b0);
	wbm_write(32'h7A00_0000, 4'hF,  32'h0F00_0000,          eob,            linear, 1'b0, 1'b0);
	wbm_write(32'h7A00_0000, 4'hF,  32'h1F00_0000,          eob,            linear, 1'b0, 1'b0);
	wbm_read (32'h7A00_0000, 4'hF,	wbm_readout_data,	classic,	linear, 1'b0, 1'b0);
	$display("... Reading Physical Status Register ");
	wbm_read (32'h7A00_0004, 4'hF,	wbm_readout_data,	classic,	linear, 1'b0, 1'b0);
	#100000.0; // Wait 0.1usec

	$display("... Set Averaging Registers for CHAN 1 ");
	wbm_write(32'h7A00_001C, 4'hF,	32'h0000_0111,		classic,	linear, 1'b0, 1'b0);
	wbm_read (32'h7A00_001C, 4'hF,  wbm_readout_data,       classic,        linear, 1'b0, 1'b0);

	#100000.0; // Wait 0.1 usec
	$display("... Reading Pedestals From All ADCs ");
	wbm_read (32'h7A00_0020, 4'hF,  wbm_readout_data,       classic,        linear, 1'b0, 1'b0);
	wbm_read (32'h7A00_0040, 4'hF,  wbm_readout_data,       classic,        linear, 1'b0, 1'b0);
	wbm_read (32'h7A00_0060, 4'hF,  wbm_readout_data,       classic,        linear, 1'b0, 1'b0);
	wbm_read (32'h7A00_0080, 4'hF,  wbm_readout_data,       classic,        linear, 1'b0, 1'b0);
	wbm_read (32'h7A00_00A0, 4'hF,  wbm_readout_data,       classic,        linear, 1'b0, 1'b0);
	wbm_read (32'h7A00_00C0, 4'hF,  wbm_readout_data,       classic,        linear, 1'b0, 1'b0);
	wbm_read (32'h7A00_00E0, 4'hF,  wbm_readout_data,       classic,        linear, 1'b0, 1'b0);
	wbm_read (32'h7A00_0100, 4'hF,  wbm_readout_data,       classic,        linear, 1'b0, 1'b0);
	#100000.0; // Wait 0.1 usec

	$display("... Setting different Base Addresses for all ADCs ");
	wbm_write(32'h7A00_0018, 4'hF,  32'h0000_0000,          eob,            linear, 1'b0, 1'b0);
	wbm_write(32'h7A00_0038, 4'hF,  32'h0000_1000,          eob,            linear, 1'b0, 1'b0);
	wbm_write(32'h7A00_0058, 4'hF,  32'h0000_2000,          eob,            linear, 1'b0, 1'b0);
	wbm_write(32'h7A00_0078, 4'hF,  32'h0000_3000,          eob,            linear, 1'b0, 1'b0);
	wbm_write(32'h7A00_0098, 4'hF,  32'h0000_4000,          eob,            linear, 1'b0, 1'b0);
	wbm_write(32'h7A00_00B8, 4'hF,  32'h0000_5000,          eob,            linear, 1'b0, 1'b0);
	wbm_write(32'h7A00_00D8, 4'hF,  32'h0000_6000,          eob,            linear, 1'b0, 1'b0);
	wbm_write(32'h7A00_00F8, 4'hF,  32'h0000_7000,          eob,            linear, 1'b0, 1'b0);
	#100000.0; // Wait 0.1 usec

	$display("...Enabling Interrupts ");
	wbm_write(32'h7A00_01FC, 4'hF,  32'h0000_00FF,		eob,		linear, 1'b0, 1'b0);
	wbm_read (32'h7A00_01FC, 4'hF,  wbm_readout_data,       classic,        linear, 1'b0, 1'b0);
	#100000.0; // Wait 0.1 usec

	$display("... Sending Trigger to ALL Channels ");
	wbm_write(32'h7A00_0008, 4'hF,  32'h0000_00FF,          classic,        linear, 1'b0, 1'b0);
	wbm_write(32'h7A00_0008, 4'hF,  32'h0000_0000,          classic,        linear, 1'b0, 1'b0);
	#100000.0; // Wait 0.1 usec
	$display("... Sending Trigger to CHAN1");
	wbm_write(32'h7A00_0008, 4'hF,  32'h0000_0001,          classic,        linear, 1'b0, 1'b0);
	wbm_write(32'h7A00_0008, 4'hF,  32'h0000_0000,          classic,        linear, 1'b0, 1'b0);
	#300000.0; // Wait 0.3 usec
	wbm_write(32'h7A00_0008, 4'hF,  32'h0000_0001,          classic,        linear, 1'b0, 1'b0);
	wbm_write(32'h7A00_0008, 4'hF,  32'h0000_0000,          classic,        linear, 1'b0, 1'b0);
	#1000000.0; // Wait 1.0 usec
	wbm_write(32'h7A00_0008, 4'hF,  32'h0000_0001,          classic,        linear, 1'b0, 1'b0);
	wbm_write(32'h7A00_0008, 4'hF,  32'h0000_0000,          classic,        linear, 1'b0, 1'b0);
	#300000.0; // Wait 0.3 usec

	
	$display("... Sending Bitslip Pulse ");
	wbm_read (32'h7A00_0004, 4'hF,  wbm_readout_data,       classic,        linear, 1'b0, 1'b0);
	wbm_write(32'h7A00_0004, 4'hF,  32'h1000_0000,          eob,            linear, 1'b0, 1'b0);
	wbm_read (32'h7A00_0004, 4'hF,  wbm_readout_data,       classic,        linear, 1'b0, 1'b0);
	#100000.0  // Wait 0.1 usec
	wbm_read (32'h7A00_0004, 4'hF,  wbm_readout_data,       classic,        linear, 1'b0, 1'b0);
	#100000.0; // Wait 0.1 usec

	$display("... Sending SPI Write Command (testing) ");
	// 1 st 2 bytes is Address (R/nW , W1, W0, 12 Addres), last byte is data
	wbm_write(32'h7A00_01F0, 4'hF,  32'h0123_CDAB,		eob,		linear, 1'b0, 1'b0);
	#10000.0; // Wait
	wbm_read (32'h7A00_01F0, 4'hF,  wbm_readout_data,       eob,		linear, 1'b0, 1'b0);
	#3000000.0; // Wait 3usecs
	wbm_read (32'h7A00_01F0, 4'hF,  wbm_readout_data,       eob,            linear, 1'b0, 1'b0);

	$display("... Sending SPI Read Command (testing) ");
	wbm_write(32'h7A00_01F0, 4'hF,  32'h8123_DEAD,          eob,            linear, 1'b0, 1'b0);
	#5000000.0; // Wait 5usecs
	wbm_read (32'h7A00_01F0, 4'hF,  wbm_readout_data,       eob,            linear, 1'b0, 1'b0);
	/*
	$display("... Writing Control Register ");
	wbm_write(32'h7A00_0008, 4'hF,  32'h99AA_BBCC, 		classic, 	linear, 1'b0, 1'b0);
	wbm_read (32'h7A00_0008, 4'hF,  wbm_readout_data, 	eob,  		linear, 1'b0, 1'b0);
	*/
	#60000000.0;

	wbm_write(32'h7A00_0008, 4'hF,  32'h0000_00FF,          classic,        linear, 1'b0, 1'b0);
	wbm_write(32'h7A00_0008, 4'hF,  32'h0000_0000,          classic,        linear, 1'b0, 1'b0);
	#60000000.0;
	wbm_write(32'h7A00_0008, 4'hF,  32'h0000_00FF,          classic,        linear, 1'b0, 1'b0);
	wbm_write(32'h7A00_0008, 4'hF,  32'h0000_0000,          classic,        linear, 1'b0, 1'b0);
	#60000000.0 $finish;
end
endmodule
