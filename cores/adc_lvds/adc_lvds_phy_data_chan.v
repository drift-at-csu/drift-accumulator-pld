module adc_lvds_phy_data_chan
#(parameter	IDELAY_VALUE	= 15,	// Initial input delay
		IDELAY_NAME 	= "adcX_delay_group" // Name of the delay group
)(
  input wire data_i_p, 		// These 2 should come directly
  input wire data_i_n, 		// from the input pad

  output wire [11:0] data_o,
  input wire data_clk_i, 	// These 2 are alligned at the higher
  input wire data_div_clk_i, 	// level

  input wire bitslip_i,		// Bitslip signal
  input wire slow_rst_i,	// Reset for ISERDESEs
  input wire delay_inc_i	// Delay Increment for idelay
);


/////////////////////////////////////////////////////////////
//  Differential buffer for input data                    //
///////////////////////////////////////////////////////////
wire data_i;
IBUFDS #(
	.DIFF_TERM("TRUE"),	// Differential Termination
	.IBUF_LOW_PWR("TRUE"),	// Low power="TRUE", Highest performance="FALSE" 
	.IOSTANDARD("DEFAULT")	// Specify the input I/O standard
) data_buffer (
	.O(data_i),	// Buffer output
	.I(data_i_p),	// Diff_p buffer input (connect directly to top-level port)
	.IB(data_i_n)	// Diff_n buffer input (connect directly to top-level port)
);
/////////////////////////
// IDELAY             //
///////////////////////
wire delayed_data_i;
(* IODELAY_GROUP = IDELAY_NAME *)
IDELAYE2 #(
	.CINVCTRL_SEL("FALSE"),         // Enable dynamic clock inversion (FALSE, TRUE)
	.DELAY_SRC("IDATAIN"),          // Delay input (IDATAIN, DATAIN)
	.HIGH_PERFORMANCE_MODE("FALSE"),// Reduced jitter ("TRUE"), Reduced power ("FALSE")
	.IDELAY_TYPE("VARIABLE"),       // FIXED, VARIABLE, VAR_LOAD, VAR_LOAD_PIPE
	// 5 taps means 0.39nsec
	.IDELAY_VALUE( IDELAY_VALUE ),      // Input delay tap setting (0-31)
	.PIPE_SEL("FALSE"),             // Select pipelined mode, FALSE, TRUE
	.REFCLK_FREQUENCY(200.0),       // IDELAYCTRL clock input frequency in MHz (190.0-210.0).
	.SIGNAL_PATTERN("DATA")         // DATA, CLOCK input signal
) idelay (
  .CNTVALUEOUT(  ),       	// 5-bit output: Counter value output
  .DATAOUT( delayed_data_i ),	// 1-bit output: Delayed data output
  .C      ( data_div_clk_i ),	// 1-bit input: Clock input (VARIABLE, VAR_LOAD, VAR_LOAD_PIPE)
  .CE     ( delay_inc_i ),	// 1-bit input: Active high enable increment/decrement input
  .CINVCTRL( 1'b0 ),		// 1-bit input: Dynamic clock inversion input
  .CNTVALUEIN( 5'b00000 ),	// 5-bit input: Counter value input
  .DATAIN ( 1'b0 ),		// 1-bit input: Internal delay data input
  .IDATAIN( data_i ),		// 1-bit input: Data input from the I/O
  .INC    ( 1'b1 ),		// 1-bit input: Increment (1'b1) / Decrement (1'b0)  tap delay input
  .LD     ( 1'b0 ),		// 1-bit input: Load IDELAY_VALUE input
  .LDPIPEEN( 1'b0 ),		// 1-bit input: Enable PIPELINE register to load data input
  .REGRST ( 1'b0 )		// 1-bit input: Active-high reset tap-delay input (VAR_LOAD_PIPE)
);
//////////////////////////////////////////
// ISERDES                             //
////////////////////////////////////////
wire combinat_data_i;
reg [5:0] buffered_data = 6'b000000;
// Unbelievable, but 12bits is not possible. So we'll take
// 6bits and regenerate the clocks accordingly
ISERDESE2 #(
	.DATA_RATE("DDR"),	// DDR, SDR
	.DATA_WIDTH( 6 ),	// Parallel data width (2-8,10,14)
	.DYN_CLKDIV_INV_EN("FALSE"),	// Enable DYNCLKDIVINVSEL inversion (FALSE, TRUE)
	.DYN_CLK_INV_EN("FALSE"),	// Enable DYNCLKINVSEL inversion (FALSE, TRUE)
	// INIT_Q1 - INIT_Q4: Initial value on the  Q outputs (0/1)
	.INIT_Q1(1'b0),
	.INIT_Q2(1'b0),
	.INIT_Q3(1'b0),
	.INIT_Q4(1'b0),
	.INTERFACE_TYPE("NETWORKING"),	// MEMORY, MEMORY_DDR3, MEMORY_QDR, NETWORKING, OVERSAMPLE
	.IOBDELAY("IFD"),	// NONE, BOTH, IBUF, IFD
	.NUM_CE( 1 ),		// Number of clock enables (1,2)
	.OFB_USED("FALSE"),	// Select OFB path (FALSE, TRUE)
	.SERDES_MODE("MASTER"),	// MASTER, SLAVE
        // SRVAL_Q1 - SRVAL_Q4: Q output  values when  SR is used  (0/1)
	.SRVAL_Q1(1'b0),
	.SRVAL_Q2(1'b0),
	.SRVAL_Q3(1'b0),
	.SRVAL_Q4(1'b0)

) iserdes (
        .O ( ),	// 1-bit output: Combinatorial output
        // Q1 - Q8: 1-bit (each)  output: Registered data outputs
        .Q1( data_o[0] ),
        .Q2( data_o[1] ),
        .Q3( data_o[2] ),
        .Q4( data_o[3] ),
        .Q5( data_o[4] ),
        .Q6( data_o[5] ),
        .Q7(  ),
        .Q8(  ),
        // SHIFTOUT1-SHIFTOUT2: 1-bit (each)  output: Data width  expansion output  ports
        .SHIFTOUT1(  ),
        .SHIFTOUT2(  ),
        // 1-bit input: The BITSLIP pin performs a Bitslip operation synchronous to
        // CLKDIV when asserted (active High). Subsequently, the data seen on the Q1
        // to Q8 output ports will shift, as in a barrel-shifter operation, one
        // position every time Bitslip is invoked (DDR operation is different from
        // SDR).
        .BITSLIP( bitslip_i ),
        // CE1, CE2: 1-bit (each) input: Data register clock enable inputs
        .CE1	( 1'b1 ),
        .CE2	( ),
        .CLKDIVP(  ),	// 1-bit input: TBD
        .CLKDIV	(  data_div_clk_i  ), 	// 1-bit input: Divided clock
        .OCLK	(  data_clk_i  ), 	// 1-bit input: High speed output clock
        .OCLKB  ( !data_clk_i  ),	// 1-bit input: High speed negative edge output clock
        // Clocks: 1-bit (each) input: ISERDESE2 clock input ports
	.CLK	(  data_clk_i ),       // 1-bit input: High-speed clock
        .CLKB	( !data_clk_i ),       // 1-bit input: High-speed secondary clock
        // Dynamic Clock Inversions: 1-bit (each) input: Dynamic clock inversion pins to
        // switch clock polarity
        .DYNCLKDIVSEL( 1'b0 ),  	// 1-bit input: Dynamic CLKDIV inversion
        .DYNCLKSEL   ( 1'b0 ),  	// 1-bit input: Dynamic CLK/CLKB inversion
        // Input Data: 1-bit (each) input: ISERDESE2 data  input ports
        .D    	( data_i ),		// 1-bit input: Data input
        .DDLY 	( delayed_data_i ),	// 1-bit input: Serial data from IDELAYE2
        .OFB  	( 1'b0  ),		// 1-bit input: Data feedback from OSERDESE2
        .RST  	( slow_rst_i  ),		// 1-bit input: Active high asynchronous reset
        // SHIFTIN1-SHIFTIN2: 1-bit (each) input: Data width expansion  input ports
        .SHIFTIN1( 1'b0 ),
        .SHIFTIN2( 1'b0 )
);
/////////////////////////////////////////////
// Let's Put a buffer register for MSB    //
///////////////////////////////////////////
always @ (posedge data_div_clk_i) begin
	buffered_data <= data_o [5:0];
end
assign data_o[11:6] = buffered_data;
endmodule
