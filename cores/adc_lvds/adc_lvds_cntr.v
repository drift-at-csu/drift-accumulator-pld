module adc_lvds_cntr
#(parameter	NOF_ADCS	= 8, // Number of ADCs
		ADC_BIT_WIDTH	= 12,
		OUT_DATA_WIDTH	= 32,
		OUT_ADDR_WIDTH	= 10
)(
//---> Data input
  input wire  [ADC_BIT_WIDTH-1:0]	adc1_data_i,
  input wire  [ADC_BIT_WIDTH-1:0]	adc2_data_i,
  input wire  [ADC_BIT_WIDTH-1:0]	adc3_data_i,
  input wire  [ADC_BIT_WIDTH-1:0]	adc4_data_i,
  input wire  [ADC_BIT_WIDTH-1:0]	adc5_data_i,
  input wire  [ADC_BIT_WIDTH-1:0]	adc6_data_i,
  input wire  [ADC_BIT_WIDTH-1:0]	adc7_data_i,
  input wire  [ADC_BIT_WIDTH-1:0]	adc8_data_i,

  input wire				inp_clk_i,
//---> Timestamp input
  input wire	[31:0]			timestamp_seconds_i,
  input wire	[31:0]			timestamp_quants_persec_i,
  input wire	[31:0]			timestamp_quants_i,
//---> Data output ( Wishbone Master Interface )
  // CHANNEL 1
  output wire [OUT_DATA_WIDTH-1:0]	adc1_buf0_data_o,
  output wire				adc1_buf0_ready_o,
  input  wire				adc1_buf0_read_i,
  output wire [31:0]	adc1_buf0_monit_seconds_o,
  output wire [31:0]	adc1_buf0_monit_quants_o,
  output wire [31:0]	adc1_buf0_monit_quants_persec_o,
  output wire [31:0]	adc1_buf0_monit_status_o,

  output wire [OUT_DATA_WIDTH-1:0]	adc1_buf1_data_o,
  output wire				adc1_buf1_ready_o,
  input  wire				adc1_buf1_read_i,
  output wire [31:0]	adc1_buf1_monit_seconds_o,
  output wire [31:0]	adc1_buf1_monit_quants_o,
  output wire [31:0]	adc1_buf1_monit_quants_persec_o,
  output wire [31:0]	adc1_buf1_monit_status_o,

  // CHANNEL 2
  output wire [OUT_DATA_WIDTH-1:0]	adc2_buf0_data_o,
  output wire				adc2_buf0_ready_o,
  input  wire				adc2_buf0_read_i,
  output wire [31:0]	adc2_buf0_monit_seconds_o,
  output wire [31:0]	adc2_buf0_monit_quants_o,
  output wire [31:0]	adc2_buf0_monit_quants_persec_o,
  output wire [31:0]	adc2_buf0_monit_status_o,

  output wire [OUT_DATA_WIDTH-1:0]	adc2_buf1_data_o,
  output wire				adc2_buf1_ready_o,
  input  wire				adc2_buf1_read_i,
  output wire [31:0]	adc2_buf1_monit_seconds_o,
  output wire [31:0]	adc2_buf1_monit_quants_o,
  output wire [31:0]	adc2_buf1_monit_quants_persec_o,
  output wire [31:0]	adc2_buf1_monit_status_o,

  // CHANNEL 3
  output wire [OUT_DATA_WIDTH-1:0]	adc3_buf0_data_o,
  output wire				adc3_buf0_ready_o,
  input  wire				adc3_buf0_read_i,
  output wire [31:0]	adc3_buf0_monit_seconds_o,
  output wire [31:0]	adc3_buf0_monit_quants_o,
  output wire [31:0]	adc3_buf0_monit_quants_persec_o,
  output wire [31:0]	adc3_buf0_monit_status_o,

  output wire [OUT_DATA_WIDTH-1:0]	adc3_buf1_data_o,
  output wire				adc3_buf1_ready_o,
  input  wire				adc3_buf1_read_i,
  output wire [31:0]	adc3_buf1_monit_seconds_o,
  output wire [31:0]	adc3_buf1_monit_quants_o,
  output wire [31:0]	adc3_buf1_monit_quants_persec_o,
  output wire [31:0]	adc3_buf1_monit_status_o,

  // CHANNEL 4
  output wire [OUT_DATA_WIDTH-1:0]	adc4_buf0_data_o,
  output wire				adc4_buf0_ready_o,
  input  wire				adc4_buf0_read_i,
  output wire [31:0]	adc4_buf0_monit_seconds_o,
  output wire [31:0]	adc4_buf0_monit_quants_o,
  output wire [31:0]	adc4_buf0_monit_quants_persec_o,
  output wire [31:0]	adc4_buf0_monit_status_o,

  output wire [OUT_DATA_WIDTH-1:0]	adc4_buf1_data_o,
  output wire				adc4_buf1_ready_o,
  input  wire				adc4_buf1_read_i,
  output wire [31:0]	adc4_buf1_monit_seconds_o,
  output wire [31:0]	adc4_buf1_monit_quants_o,
  output wire [31:0]	adc4_buf1_monit_quants_persec_o,
  output wire [31:0]	adc4_buf1_monit_status_o,

  // CHANNEL 5
  output wire [OUT_DATA_WIDTH-1:0]	adc5_buf0_data_o,
  output wire				adc5_buf0_ready_o,
  input  wire				adc5_buf0_read_i,
  output wire [31:0]	adc5_buf0_monit_seconds_o,
  output wire [31:0]	adc5_buf0_monit_quants_o,
  output wire [31:0]	adc5_buf0_monit_quants_persec_o,
  output wire [31:0]	adc5_buf0_monit_status_o,

  output wire [OUT_DATA_WIDTH-1:0]	adc5_buf1_data_o,
  output wire				adc5_buf1_ready_o,
  input  wire				adc5_buf1_read_i,
  output wire [31:0]    adc5_buf1_monit_seconds_o,
  output wire [31:0]    adc5_buf1_monit_quants_o,
  output wire [31:0]    adc5_buf1_monit_quants_persec_o,
  output wire [31:0]    adc5_buf1_monit_status_o,

  // CHANNEL 6
  output wire [OUT_DATA_WIDTH-1:0]	adc6_buf0_data_o,
  output wire				adc6_buf0_ready_o,
  input  wire				adc6_buf0_read_i,
  output wire [31:0]	adc6_buf0_monit_seconds_o,
  output wire [31:0]	adc6_buf0_monit_quants_o,
  output wire [31:0]	adc6_buf0_monit_quants_persec_o,
  output wire [31:0]	adc6_buf0_monit_status_o,

  output wire [OUT_DATA_WIDTH-1:0]	adc6_buf1_data_o,
  output wire				adc6_buf1_ready_o,
  input  wire				adc6_buf1_read_i,
  output wire [31:0]    adc6_buf1_monit_seconds_o,
  output wire [31:0]    adc6_buf1_monit_quants_o,
  output wire [31:0]    adc6_buf1_monit_quants_persec_o,
  output wire [31:0]    adc6_buf1_monit_status_o,

  // CHANNEL 7
  output wire [OUT_DATA_WIDTH-1:0]	adc7_buf0_data_o,
  output wire				adc7_buf0_ready_o,
  input  wire				adc7_buf0_read_i,
  output wire [31:0]	adc7_buf0_monit_seconds_o,
  output wire [31:0]	adc7_buf0_monit_quants_o,
  output wire [31:0]	adc7_buf0_monit_quants_persec_o,
  output wire [31:0]	adc7_buf0_monit_status_o,

  output wire [OUT_DATA_WIDTH-1:0]	adc7_buf1_data_o,
  output wire				adc7_buf1_ready_o,
  input  wire				adc7_buf1_read_i,
  output wire [31:0]	adc7_buf1_monit_seconds_o,
  output wire [31:0]	adc7_buf1_monit_quants_o,
  output wire [31:0]	adc7_buf1_monit_quants_persec_o,
  output wire [31:0]	adc7_buf1_monit_status_o,

  // CHANNEL 8
  output wire [OUT_DATA_WIDTH-1:0]	adc8_buf0_data_o,
  output wire				adc8_buf0_ready_o,
  input  wire				adc8_buf0_read_i,
  output wire [31:0]	adc8_buf0_monit_seconds_o,
  output wire [31:0]	adc8_buf0_monit_quants_o,
  output wire [31:0]	adc8_buf0_monit_quants_persec_o,
  output wire [31:0]	adc8_buf0_monit_status_o,

  output wire [OUT_DATA_WIDTH-1:0]	adc8_buf1_data_o,
  output wire				adc8_buf1_ready_o,
  input  wire				adc8_buf1_read_i,
  output wire [31:0]	adc8_buf1_monit_seconds_o,
  output wire [31:0]	adc8_buf1_monit_quants_o,
  output wire [31:0]	adc8_buf1_monit_quants_persec_o,
  output wire [31:0]	adc8_buf1_monit_status_o,

  input wire				out_clk_i,

//---> Status Register interface
  output wire [27:0]	sreg_adc1_pedestal_o,
  output wire [27:0]	sreg_adc2_pedestal_o,
  output wire [27:0]	sreg_adc3_pedestal_o,
  output wire [27:0]	sreg_adc4_pedestal_o,
  output wire [27:0]	sreg_adc5_pedestal_o,
  output wire [27:0]	sreg_adc6_pedestal_o,
  output wire [27:0]	sreg_adc7_pedestal_o,
  output wire [27:0]	sreg_adc8_pedestal_o,

//---> Control Register interface
  input wire [2:0]	creg_adc1_avg_waveform_i,
  input wire [2:0]      creg_adc1_avg_trigger_i,
  input wire [2:0]      creg_adc1_avg_pedestal_i,
  input wire [27:0]	creg_adc1_trig_hig_i,
  input wire [27:0]     creg_adc1_trig_low_i,
  input wire		creg_adc1_trig_ext_i,

  input wire [2:0]      creg_adc2_avg_waveform_i,
  input wire [2:0]      creg_adc2_avg_trigger_i,
  input wire [2:0]      creg_adc2_avg_pedestal_i,
  input wire [27:0]	creg_adc2_trig_hig_i,
  input wire [27:0]	creg_adc2_trig_low_i,
  input wire		creg_adc2_trig_ext_i,

  input wire [2:0]      creg_adc3_avg_waveform_i,
  input wire [2:0]      creg_adc3_avg_trigger_i,
  input wire [2:0]      creg_adc3_avg_pedestal_i,
  input wire [27:0]	creg_adc3_trig_hig_i,
  input wire [27:0]	creg_adc3_trig_low_i,
  input wire		creg_adc3_trig_ext_i,

  input wire [2:0]      creg_adc4_avg_waveform_i,
  input wire [2:0]      creg_adc4_avg_trigger_i,
  input wire [2:0]      creg_adc4_avg_pedestal_i,
  input wire [27:0]	creg_adc4_trig_hig_i,
  input wire [27:0]	creg_adc4_trig_low_i,
  input wire		creg_adc4_trig_ext_i,

  input wire [2:0]      creg_adc5_avg_waveform_i,
  input wire [2:0]      creg_adc5_avg_trigger_i,
  input wire [2:0]      creg_adc5_avg_pedestal_i,
  input wire [27:0]	creg_adc5_trig_hig_i,
  input wire [27:0]	creg_adc5_trig_low_i,
  input wire		creg_adc5_trig_ext_i,

  input wire [2:0]      creg_adc6_avg_waveform_i,
  input wire [2:0]      creg_adc6_avg_trigger_i,
  input wire [2:0]      creg_adc6_avg_pedestal_i,
  input wire [27:0]	creg_adc6_trig_hig_i,
  input wire [27:0]	creg_adc6_trig_low_i,
  input wire		creg_adc6_trig_ext_i,

  input wire [2:0]      creg_adc7_avg_waveform_i,
  input wire [2:0]      creg_adc7_avg_trigger_i,
  input wire [2:0]      creg_adc7_avg_pedestal_i,
  input wire [27:0]	creg_adc7_trig_hig_i,
  input wire [27:0]	creg_adc7_trig_low_i,
  input wire		creg_adc7_trig_ext_i,

  input wire [2:0]      creg_adc8_avg_waveform_i,
  input wire [2:0]      creg_adc8_avg_trigger_i,
  input wire [2:0]      creg_adc8_avg_pedestal_i,
  input wire [27:0]	creg_adc8_trig_hig_i,
  input wire [27:0]	creg_adc8_trig_low_i,
  input wire		creg_adc8_trig_ext_i
);
/////////////////////////////////
// ADC1 Controller            //
///////////////////////////////
adc_lvds_cntr_chan #(
	.ADC_BIT_WIDTH	(ADC_BIT_WIDTH),
	.OUT_DATA_WIDTH	(OUT_DATA_WIDTH),
	.OUT_ADDR_WIDTH	(OUT_ADDR_WIDTH)
) adc1 (
  .data_i		( adc1_data_i ),
  .inp_clk_i		( inp_clk_i ),

  .mon_sec_i	( timestamp_seconds_i ),
  .mon_qnt_i	( timestamp_quants_i ),
  .mon_qps_i	( timestamp_quants_persec_i ),

  .buf0_data_o		( adc1_buf0_data_o ),
  .buf0_ready_o		( adc1_buf0_ready_o ),
  .buf0_read_i		( adc1_buf0_read_i ),
  .buf0_mon_sec_o	( adc1_buf0_monit_seconds_o ),
  .buf0_mon_qnt_o	( adc1_buf0_monit_quants_o ),
  .buf0_mon_qps_o	( adc1_buf0_monit_quants_persec_o ),
  .buf0_mon_stat_o	( adc1_buf0_monit_status_o ),
  .buf1_data_o		( adc1_buf1_data_o ),
  .buf1_ready_o		( adc1_buf1_ready_o ),
  .buf1_read_i		( adc1_buf1_read_i ),
  .buf1_mon_sec_o	( adc1_buf1_monit_seconds_o ),
  .buf1_mon_qnt_o	( adc1_buf1_monit_quants_o ),
  .buf1_mon_qps_o	( adc1_buf1_monit_quants_persec_o ),
  .buf1_mon_stat_o	( adc1_buf1_monit_status_o ),
  .out_clk_i		( out_clk_i ),

  .sreg_pedestal_o	( sreg_adc1_pedestal_o ),

  .creg_avg_waveform	( creg_adc1_avg_waveform_i ),
  .creg_avg_trigger	( creg_adc1_avg_trigger_i ),
  .creg_avg_pedestal	( creg_adc1_avg_pedestal_i ),
  .creg_trig_low_i	( creg_adc1_trig_low_i ),
  .creg_trig_hig_i	( creg_adc1_trig_hig_i ),
  .creg_trig_ext_i	( creg_adc1_trig_ext_i )
);
/////////////////////////////////
// ADC2 Controller            //
///////////////////////////////
adc_lvds_cntr_chan #(
	.ADC_BIT_WIDTH	(ADC_BIT_WIDTH),
	.OUT_DATA_WIDTH	(OUT_DATA_WIDTH),
	.OUT_ADDR_WIDTH	(OUT_ADDR_WIDTH)
) adc2 (
  .data_i		( adc2_data_i ),
  .inp_clk_i		( inp_clk_i ),

  .mon_sec_i	( timestamp_seconds_i ),
  .mon_qnt_i	( timestamp_quants_i ),
  .mon_qps_i	( timestamp_quants_persec_i ),

  .buf0_data_o		( adc2_buf0_data_o ),
  .buf0_ready_o		( adc2_buf0_ready_o ),
  .buf0_read_i		( adc2_buf0_read_i ),
  .buf0_mon_sec_o	( adc2_buf0_monit_seconds_o ),
  .buf0_mon_qnt_o	( adc2_buf0_monit_quants_o ),
  .buf0_mon_qps_o	( adc2_buf0_monit_quants_persec_o ),
  .buf0_mon_stat_o	( adc2_buf0_monit_status_o ),
  .buf1_data_o		( adc2_buf1_data_o ),
  .buf1_ready_o		( adc2_buf1_ready_o ),
  .buf1_read_i		( adc2_buf1_read_i ),
  .buf1_mon_sec_o       ( adc2_buf1_monit_seconds_o ),
  .buf1_mon_qnt_o       ( adc2_buf1_monit_quants_o ),
  .buf1_mon_qps_o       ( adc2_buf1_monit_quants_persec_o ),
  .buf1_mon_stat_o      ( adc2_buf1_monit_status_o ),
  .out_clk_i		( out_clk_i ),

  .sreg_pedestal_o	( sreg_adc2_pedestal_o ),

  .creg_avg_waveform	( creg_adc2_avg_waveform_i ),
  .creg_avg_trigger	( creg_adc2_avg_trigger_i ),
  .creg_avg_pedestal	( creg_adc2_avg_pedestal_i ),
  .creg_trig_low_i	( creg_adc2_trig_low_i ),
  .creg_trig_hig_i	( creg_adc2_trig_hig_i ),
  .creg_trig_ext_i	( creg_adc2_trig_ext_i )
);
/////////////////////////////////
// ADC3 Controller            //
///////////////////////////////
adc_lvds_cntr_chan #(
	.ADC_BIT_WIDTH  (ADC_BIT_WIDTH),
	.OUT_DATA_WIDTH (OUT_DATA_WIDTH),
	.OUT_ADDR_WIDTH (OUT_ADDR_WIDTH)
) adc3 (
  .data_i		( adc3_data_i ),
  .inp_clk_i		( inp_clk_i ),

  .mon_sec_i	( timestamp_seconds_i ),
  .mon_qnt_i	( timestamp_quants_i ),
  .mon_qps_i	( timestamp_quants_persec_i ),

  .buf0_data_o		( adc3_buf0_data_o ),
  .buf0_ready_o		( adc3_buf0_ready_o ),
  .buf0_read_i		( adc3_buf0_read_i ),
  .buf0_mon_sec_o	( adc3_buf0_monit_seconds_o ),
  .buf0_mon_qnt_o	( adc3_buf0_monit_quants_o ),
  .buf0_mon_qps_o	( adc3_buf0_monit_quants_persec_o ),
  .buf0_mon_stat_o	( adc3_buf0_monit_status_o ),
  .buf1_data_o		( adc3_buf1_data_o ),
  .buf1_ready_o		( adc3_buf1_ready_o ),
  .buf1_read_i		( adc3_buf1_read_i ),
  .buf1_mon_sec_o	( adc3_buf1_monit_seconds_o ),
  .buf1_mon_qnt_o	( adc3_buf1_monit_quants_o ),
  .buf1_mon_qps_o	( adc3_buf1_monit_quants_persec_o ),
  .buf1_mon_stat_o	( adc3_buf1_monit_status_o ),
  .out_clk_i		( out_clk_i ),

  .sreg_pedestal_o	( sreg_adc3_pedestal_o ),

  .creg_avg_waveform	( creg_adc3_avg_waveform_i ),
  .creg_avg_trigger	( creg_adc3_avg_trigger_i ),
  .creg_avg_pedestal	( creg_adc3_avg_pedestal_i ),
  .creg_trig_low_i	( creg_adc3_trig_low_i ),
  .creg_trig_hig_i	( creg_adc3_trig_hig_i ),
  .creg_trig_ext_i	( creg_adc3_trig_ext_i )
);
/////////////////////////////////
// ADC4 Controller            //
///////////////////////////////
adc_lvds_cntr_chan #(
	.ADC_BIT_WIDTH	(ADC_BIT_WIDTH),
	.OUT_DATA_WIDTH	(OUT_DATA_WIDTH),
	.OUT_ADDR_WIDTH	(OUT_ADDR_WIDTH)
) adc4 (
  .data_i		( adc4_data_i ),
  .inp_clk_i		( inp_clk_i ),

  .mon_sec_i	( timestamp_seconds_i ),
  .mon_qnt_i	( timestamp_quants_i ),
  .mon_qps_i	( timestamp_quants_persec_i ),

  .buf0_data_o		( adc4_buf0_data_o ),
  .buf0_ready_o		( adc4_buf0_ready_o ),
  .buf0_read_i		( adc4_buf0_read_i ),
  .buf0_mon_sec_o	( adc4_buf0_monit_seconds_o ),
  .buf0_mon_qnt_o	( adc4_buf0_monit_quants_o ),
  .buf0_mon_qps_o	( adc4_buf0_monit_quants_persec_o ),
  .buf0_mon_stat_o	( adc4_buf0_monit_status_o ),
  .buf1_data_o		( adc4_buf1_data_o ),
  .buf1_ready_o		( adc4_buf1_ready_o ),
  .buf1_read_i		( adc4_buf1_read_i ),
  .buf1_mon_sec_o	( adc4_buf1_monit_seconds_o ),
  .buf1_mon_qnt_o	( adc4_buf1_monit_quants_o ),
  .buf1_mon_qps_o	( adc4_buf1_monit_quants_persec_o ),
  .buf1_mon_stat_o	( adc4_buf1_monit_status_o ),
  .out_clk_i		( out_clk_i ),

  .sreg_pedestal_o	( sreg_adc4_pedestal_o ),

  .creg_avg_waveform	( creg_adc4_avg_waveform_i  ),
  .creg_avg_trigger	( creg_adc4_avg_trigger_i ),
  .creg_avg_pedestal	( creg_adc4_avg_pedestal_i ),
  .creg_trig_low_i	( creg_adc4_trig_low_i ),
  .creg_trig_hig_i	( creg_adc4_trig_hig_i ),
  .creg_trig_ext_i	( creg_adc4_trig_ext_i )
);
/////////////////////////////////
// ADC5 Controller            //
///////////////////////////////
adc_lvds_cntr_chan #(
	.ADC_BIT_WIDTH	(ADC_BIT_WIDTH),
	.OUT_DATA_WIDTH	(OUT_DATA_WIDTH),
	.OUT_ADDR_WIDTH	(OUT_ADDR_WIDTH)
) adc5 (
  .data_i		( adc5_data_i ),
  .inp_clk_i		( inp_clk_i ),

  .mon_sec_i	( timestamp_seconds_i ),
  .mon_qnt_i	( timestamp_quants_i ),
  .mon_qps_i	( timestamp_quants_persec_i ),

  .buf0_data_o		( adc5_buf0_data_o ),
  .buf0_ready_o		( adc5_buf0_ready_o ),
  .buf0_read_i		( adc5_buf0_read_i ),
  .buf0_mon_sec_o	( adc5_buf0_monit_seconds_o ),
  .buf0_mon_qnt_o	( adc5_buf0_monit_quants_o ),
  .buf0_mon_qps_o	( adc5_buf0_monit_quants_persec_o ),
  .buf0_mon_stat_o	( adc5_buf0_monit_status_o ),
  .buf1_data_o		( adc5_buf1_data_o ),
  .buf1_ready_o		( adc5_buf1_ready_o ),
  .buf1_read_i		( adc5_buf1_read_i ),
  .buf1_mon_sec_o       ( adc5_buf1_monit_seconds_o ),
  .buf1_mon_qnt_o       ( adc5_buf1_monit_quants_o ),
  .buf1_mon_qps_o       ( adc5_buf1_monit_quants_persec_o ),
  .buf1_mon_stat_o      ( adc5_buf1_monit_status_o ),
  .out_clk_i		( out_clk_i ),

  .sreg_pedestal_o	( sreg_adc5_pedestal_o ),

  .creg_avg_waveform	( creg_adc5_avg_waveform_i ),
  .creg_avg_trigger     ( creg_adc5_avg_trigger_i ),
  .creg_avg_pedestal    ( creg_adc5_avg_pedestal_i ),
  .creg_trig_low_i	( creg_adc5_trig_low_i ),
  .creg_trig_hig_i	( creg_adc5_trig_hig_i ),
  .creg_trig_ext_i	( creg_adc5_trig_ext_i )
);
/////////////////////////////////
// ADC6 Controller            //
///////////////////////////////
adc_lvds_cntr_chan #(
	.ADC_BIT_WIDTH	(ADC_BIT_WIDTH),
	.OUT_DATA_WIDTH	(OUT_DATA_WIDTH),
	.OUT_ADDR_WIDTH	(OUT_ADDR_WIDTH)
) adc6 (
  .data_i		( adc6_data_i ),
  .inp_clk_i		( inp_clk_i ),

  .mon_sec_i	( timestamp_seconds_i ),
  .mon_qnt_i	( timestamp_quants_i ),
  .mon_qps_i	( timestamp_quants_persec_i ),

  .buf0_data_o		( adc6_buf0_data_o ),
  .buf0_ready_o		( adc6_buf0_ready_o ),
  .buf0_read_i		( adc6_buf0_read_i ),
  .buf0_mon_sec_o       ( adc6_buf0_monit_seconds_o ),
  .buf0_mon_qnt_o       ( adc6_buf0_monit_quants_o ),
  .buf0_mon_qps_o       ( adc6_buf0_monit_quants_persec_o ),
  .buf0_mon_stat_o      ( adc6_buf0_monit_status_o ),
  .buf1_data_o		( adc6_buf1_data_o ),
  .buf1_ready_o		( adc6_buf1_ready_o ),
  .buf1_read_i		( adc6_buf1_read_i ),
  .buf1_mon_sec_o       ( adc6_buf1_monit_seconds_o ),
  .buf1_mon_qnt_o       ( adc6_buf1_monit_quants_o ),
  .buf1_mon_qps_o       ( adc6_buf1_monit_quants_persec_o ),
  .buf1_mon_stat_o      ( adc6_buf1_monit_status_o ),
  .out_clk_i		( out_clk_i ),

  .sreg_pedestal_o	( sreg_adc6_pedestal_o ),

  .creg_avg_waveform	( creg_adc6_avg_waveform_i ),
  .creg_avg_trigger	( creg_adc6_avg_trigger_i ),
  .creg_avg_pedestal	( creg_adc6_avg_pedestal_i ),
  .creg_trig_low_i	( creg_adc6_trig_low_i ),
  .creg_trig_hig_i	( creg_adc6_trig_hig_i ),
  .creg_trig_ext_i	( creg_adc6_trig_ext_i )
);
/////////////////////////////////
// ADC7 Controller            //
///////////////////////////////
adc_lvds_cntr_chan #(
	.ADC_BIT_WIDTH	(ADC_BIT_WIDTH),
	.OUT_DATA_WIDTH	(OUT_DATA_WIDTH),
	.OUT_ADDR_WIDTH	(OUT_ADDR_WIDTH)
) adc7 (
  .data_i		( adc7_data_i ),
  .inp_clk_i		( inp_clk_i ),

  .mon_sec_i	( timestamp_seconds_i ),
  .mon_qnt_i	( timestamp_quants_i ),
  .mon_qps_i	( timestamp_quants_persec_i ),

  .buf0_data_o          ( adc7_buf0_data_o ),
  .buf0_ready_o         ( adc7_buf0_ready_o ),
  .buf0_read_i          ( adc7_buf0_read_i ),
  .buf0_mon_sec_o       ( adc7_buf0_monit_seconds_o ),
  .buf0_mon_qnt_o       ( adc7_buf0_monit_quants_o ),
  .buf0_mon_qps_o       ( adc7_buf0_monit_quants_persec_o ),
  .buf0_mon_stat_o      ( adc7_buf0_monit_status_o ),
  .buf1_data_o          ( adc7_buf1_data_o ),
  .buf1_ready_o         ( adc7_buf1_ready_o ),
  .buf1_read_i          ( adc7_buf1_read_i ),
  .buf1_mon_sec_o       ( adc7_buf1_monit_seconds_o ),
  .buf1_mon_qnt_o       ( adc7_buf1_monit_quants_o ),
  .buf1_mon_qps_o       ( adc7_buf1_monit_quants_persec_o ),
  .buf1_mon_stat_o      ( adc7_buf1_monit_status_o ),
  .out_clk_i		( out_clk_i ),

  .sreg_pedestal_o	( sreg_adc7_pedestal_o ),

  .creg_avg_waveform	( creg_adc7_avg_waveform_i ),
  .creg_avg_trigger	( creg_adc7_avg_trigger_i ),
  .creg_avg_pedestal	( creg_adc7_avg_pedestal_i ),
  .creg_trig_low_i	( creg_adc7_trig_low_i ),
  .creg_trig_hig_i	( creg_adc7_trig_hig_i ),
  .creg_trig_ext_i	( creg_adc7_trig_ext_i )
);
/////////////////////////////////
// ADC8 Controller            //
///////////////////////////////
adc_lvds_cntr_chan #(
	.ADC_BIT_WIDTH  (ADC_BIT_WIDTH),
	.OUT_DATA_WIDTH (OUT_DATA_WIDTH),
	.OUT_ADDR_WIDTH (OUT_ADDR_WIDTH)
) adc8 (
  .data_i		( adc8_data_i ),
  .inp_clk_i		( inp_clk_i ),

  .mon_sec_i	( timestamp_seconds_i ),
  .mon_qnt_i	( timestamp_quants_i ),
  .mon_qps_i	( timestamp_quants_persec_i ),

  .buf0_data_o          ( adc8_buf0_data_o ),
  .buf0_ready_o         ( adc8_buf0_ready_o ),
  .buf0_read_i          ( adc8_buf0_read_i ),
  .buf0_mon_sec_o	( adc8_buf0_monit_seconds_o ),
  .buf0_mon_qnt_o	( adc8_buf0_monit_quants_o ),
  .buf0_mon_qps_o	( adc8_buf0_monit_quants_persec_o ),
  .buf0_mon_stat_o	( adc8_buf0_monit_status_o ),
  .buf1_data_o          ( adc8_buf1_data_o ),
  .buf1_ready_o         ( adc8_buf1_ready_o ),
  .buf1_read_i          ( adc8_buf1_read_i ),
  .buf1_mon_sec_o       ( adc8_buf1_monit_seconds_o ),
  .buf1_mon_qnt_o       ( adc8_buf1_monit_quants_o ),
  .buf1_mon_qps_o       ( adc8_buf1_monit_quants_persec_o ),
  .buf1_mon_stat_o      ( adc8_buf1_monit_status_o ),
  .out_clk_i		( out_clk_i ),

  .sreg_pedestal_o	( sreg_adc8_pedestal_o ),

  .creg_avg_waveform	( creg_adc8_avg_waveform_i ),
  .creg_avg_trigger	( creg_adc8_avg_trigger_i ),
  .creg_avg_pedestal	( creg_adc8_avg_pedestal_i ),
  .creg_trig_low_i	( creg_adc8_trig_low_i ),
  .creg_trig_hig_i	( creg_adc8_trig_hig_i ),
  .creg_trig_ext_i	( creg_adc8_trig_ext_i )
);

endmodule
