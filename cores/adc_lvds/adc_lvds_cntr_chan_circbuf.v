/* 
 * Circular Buffer Should accept data until 
 * the input trigger is activated, then
 * it goes into the additional writing mode
 * for half of the buffer.
 *
 * After the buffer is full, inp_ready_o <= 1'b0
 * and out_ready_o <= 1'b1 are asserted, indicating
 * that one can't write to buffer and it's ready
 * for readout.
 *
 * It waits in this state untill the whole buffer is
 * read, and then inp_ready_o <= 1'b1
 * and out_ready_o <= 1'b0 are asserted, indicating
 * that one can write to the buffer and it's not
 * ready for readout.
 *
 *
 */
module adc_lvds_cntr_chan_circbuf
#(parameter	DATA_WIDTH	= 32 	// This is an incoming and outgoing Data Width
)(
//---> Input Data Path
  input wire  [DATA_WIDTH-1:0]	inp_data_i,
  input wire			inp_wren_i,

  output wire			inp_ready_o,

  input wire			inp_clk_i,

  input wire			inp_trig_i,
//---> Inputs for Monitoring Signals
  input wire  [DATA_WIDTH-1:0]	inp_mon_sec_i,
  input wire  [DATA_WIDTH-1:0]	inp_mon_qnt_i,
  input wire  [DATA_WIDTH-1:0]	inp_mon_qps_i,
  input wire  [DATA_WIDTH-1:0]	inp_mon_stat_i,
//---> Output Data Path
  output wire [DATA_WIDTH-1:0]	out_data_o,
  input wire			out_rden_i,
  output reg			out_ready_o = 1'b0,
  input wire			out_clk_i,
//---> Outputs for Monitoring Signals
  output reg  [DATA_WIDTH-1:0]	out_mon_sec_o,
  output reg  [DATA_WIDTH-1:0]	out_mon_qnt_o,
  output reg  [DATA_WIDTH-1:0]	out_mon_qps_o,
  output reg  [DATA_WIDTH-1:0]	out_mon_stat_o
);

localparam	FULL_BUF_LENGTH	= 9'h1FF; // 9'h1FF;
localparam	HALF_BUF_LENGTH = 9'h0FF; // 9'h0FF;

//////////////////////////////////////////////////
// Let's Limit writing only to the cases when  //
// inp_ready_o == 1'b1                        //
///////////////////////////////////////////////
wire gated_wren_i; 
assign gated_wren_i = ( inp_wren_i & inp_ready_o );


////////////////////////////////////////////////////////////////
// Trigger Recieved indicator register                       //
//////////////////////////////////////////////////////////////
reg trig_recieved_reg = 1'b0;

always@(posedge inp_clk_i) begin
  if ( inp_ready_o & inp_trig_i ) begin
	trig_recieved_reg <= 1'b1;
  end else begin
  	if ( inp_ready_out_goes_low ) begin
		trig_recieved_reg <= 1'b0;
	end else begin
		trig_recieved_reg <= trig_recieved_reg;
	end
  end
end

//////////////////////////////////////
// Synchronization Of ready signal //
////////////////////////////////////
wire inp_ready_out_goes_low;
reg  [1:0] inp_ready_sreg = 2'b00;
always@(posedge inp_clk_i) begin
  inp_ready_sreg <= {inp_ready_sreg[0], out_ready_o };
end
assign inp_ready_out_goes_low = (inp_ready_sreg == 2'b10);

////////////////////////////////////
// Write Counter                 //
//////////////////////////////////
reg [8:0] writes_before_trig_counter = FULL_BUF_LENGTH; // Maximum
always@(posedge inp_clk_i) begin
  if ( gated_wren_i ) begin
  	// There is a write happening when the buffer was ready
	if ( !trig_recieved_reg ) begin
		// This write was happening before trigger was recieved
		if (writes_before_trig_counter == HALF_BUF_LENGTH ) begin
			// We want to stop the counter in the middle of the
			// buffer
			writes_before_trig_counter <= writes_before_trig_counter;
		end else begin
			// Trigger is not recieved just decrement the counter
			writes_before_trig_counter <= writes_before_trig_counter - 1'b1;
		end
	end else begin
		// This write is happening after the trigger was recieved
		// but when the buffer is not complete yet
		if (writes_before_trig_counter == 9'h000 ) begin
			// we reached the zero and still writing, in principle
			// it should never happen
			writes_before_trig_counter <= writes_before_trig_counter;
		end else begin
			// we didn't reach the zero, so keep decrementing the
			// counter
			writes_before_trig_counter <= writes_before_trig_counter - 1'b1;
		end
	end
  end else begin
	if ( inp_ready_out_goes_low ) begin
		writes_before_trig_counter <= FULL_BUF_LENGTH;
	end else begin
		writes_before_trig_counter <= writes_before_trig_counter;
	end
  end
end

////////////////////////////////////////////////////////////
//  Input Ready is Based on writes_before_trig_counter   //
//////////////////////////////////////////////////////////
assign inp_ready_o = ( writes_before_trig_counter == 9'h000 ) ? 1'b0 : 1'b1;


/////////////////////////////////////////////////////////////////////////////
// Synchronize Output Monitoring registers on the last bin in the buffer  //
///////////////////////////////////////////////////////////////////////////
always@(posedge inp_clk_i) begin
  if ((writes_before_trig_counter == 9'h001)&gated_wren_i) begin
	out_mon_sec_o <= inp_mon_sec_i;
	out_mon_qnt_o <= inp_mon_qnt_i;
	out_mon_qps_o <= inp_mon_qps_i;
	out_mon_stat_o <= inp_mon_stat_i;
  end
end

//===================================================================//
// Output Clock Domain                                              //
//=================================================================//
reg [8:0] reads_after_ready_counter = FULL_BUF_LENGTH;

////////////////////////////////////////////////
// Output Ready Sould be set When Recieving  //
// inp_ready_o goes from high to low        //
/////////////////////////////////////////////
wire out_ready_inp_goes_low;
wire out_ready_inp_goes_high;
reg  [1:0] out_ready_sreg = 2'b00;
always@(posedge out_clk_i) begin
  out_ready_sreg <= {out_ready_sreg[0], inp_ready_o };
end
assign out_ready_inp_goes_low =  ( out_ready_sreg == 2'b10 );
assign out_ready_inp_goes_high = ( out_ready_sreg == 2'b01 );

always@(posedge out_clk_i) begin
  if (out_ready_inp_goes_low) begin
	out_ready_o <= 1'b1;
  end else begin
  	// We should wait for reads_after_ready_counter to go to zero
	if ( reads_after_ready_counter == 9'h000 ) begin
		out_ready_o <= 1'b0;
	end
  end
end

/////////////////////////////////////////////////////////////
// We do need a counter of how many samples we have read  //
///////////////////////////////////////////////////////////
wire gated_out_ready; 
assign gated_out_ready = out_ready_o & out_rden_i;

always@(posedge out_clk_i) begin
  if (out_ready_inp_goes_high)
	reads_after_ready_counter <= FULL_BUF_LENGTH;
  if ( gated_out_ready ) begin
	if ( reads_after_ready_counter == 9'h000 ) begin
		// We do have a read, but our counter is already at 0
		// in principle should never happen, but just in case
		reads_after_ready_counter <= reads_after_ready_counter;
	end else begin
		// We do have a gated read, decrement the counter
		reads_after_ready_counter <= reads_after_ready_counter - 1'b1;
	end
  end
end


///////////////////////////////////////////////
// Let's create a gated read enable         //
/////////////////////////////////////////////
wire gated_rden_i;
assign gated_rden_i = ( out_rden_i & out_ready_o );

//===== After This we can initite Memory and ADDresses =====//
///////////////////////////////////////////
// Registers for storing the Addresses  //
/////////////////////////////////////////
reg [8:0]      inp_addr_i = 9'h000;
reg [8:0]      out_addr_o = 9'h000;
always@(posedge inp_clk_i) begin
  if ( gated_wren_i ) begin
  	if (inp_addr_i == FULL_BUF_LENGTH)
		inp_addr_i <= 9'h000;
	else
		inp_addr_i <= inp_addr_i + 1'b1;
  end else begin
  	inp_addr_i <= inp_addr_i;
  end
end
/////////////////////////////////////////////
// For output Address we want to sync it  //
// only when we're not reading           //
//////////////////////////////////////////
always@(posedge out_clk_i) begin
  if (out_ready_o ) begin
	// We're reading it out' so increase it by 1
	if ( out_rden_i ) begin
		if (out_addr_o == FULL_BUF_LENGTH)
			out_addr_o <= 12'h000;
		else
			out_addr_o <= out_addr_o + 1'b1;
	end else begin
		out_addr_o <= out_addr_o;
	end
  end else begin
  	// We're waiting for an address, so update it
  	out_addr_o <= inp_addr_i;
  end
end


////////////////////////////////////////////
// Double Port Memory Instance           //
//////////////////////////////////////////
// Write(Input Side): PortB		Read(Output Side): PortA
RAMB18E1 #(
	// Address Collision Mode: "PERFORMANCE" or "DELAYED_WRITE" 
	.RDADDR_COLLISION_HWCONFIG("PERFORMANCE"),
	// Collision check: Values ("ALL", "WARNING_ONLY", "GENERATE_X_ONLY" or "NONE")
	.SIM_COLLISION_CHECK("ALL"),
	// DOA_REG, DOB_REG: Optional output register (0 or 1)
	.DOA_REG(0),
	.DOB_REG(0),
	// INIT_A, INIT_B: Initial values on output ports
	.INIT_A(32'h0000_0000),
	.INIT_B(),
	// Initialization File: RAM initialization file
	.INIT_FILE("NONE"),
	// RAM Mode: "SDP" or "TDP" 
	.RAM_MODE("SDP"),
	// READ_WIDTH_A/B, WRITE_WIDTH_A/B: Read/write width per port
	.READ_WIDTH_A(36),	// 0-72
	.READ_WIDTH_B(0),	// 0-18
	.WRITE_WIDTH_A(0),	// 0-18
	.WRITE_WIDTH_B(36),	// 0-72
	// RSTREG_PRIORITY_A, RSTREG_PRIORITY_B: Reset or enable priority ("RSTREG" or "REGCE")
	.RSTREG_PRIORITY_A("RSTREG"),
	.RSTREG_PRIORITY_B("RSTREG"),
	// SRVAL_A, SRVAL_B: Set/reset value for output
	.SRVAL_A(),
	.SRVAL_B(32'h0000_0000),
	// Simulation Device: Must be set to "7SERIES" for simulation behavior
	.SIM_DEVICE("7SERIES"),
	// WriteMode: Value on output upon a write ("WRITE_FIRST", "READ_FIRST", or "NO_CHANGE")
	.WRITE_MODE_A("WRITE_FIRST"),
	.WRITE_MODE_B("WRITE_FIRST") 
) buff_mem (
  // Port A Data: 16-bit (each) output: Port A data
  .DOADO	( out_data_o [15:0] ),	// 16-bit output: A port data/LSB data
  .DOPADOP	(),	// 2-bit output: A port parity/LSB parity
  // Port B Data: 16-bit (each) output: Port B data
  .DOBDO	( out_data_o [31:16] ),	// 16-bit output: B port data/MSB data
  .DOPBDOP	(),	// 2-bit output: B port parity/MSB parity
  // Port A Address/Control Signals: 14-bit (each) input: Port A address and control signals (read port
  // when RAM_MODE="SDP")
  .ADDRARDADDR	( {out_addr_o, 5'h0} ),	// 14-bit input: A port address/Read address
  .CLKARDCLK	( out_clk_i  ),	// 1-bit input: A port clock/Read clock
  .ENARDEN	( gated_rden_i ),	// 1-bit input: A port enable/Read enable
  .REGCEAREGCE	( 1'b0 ),	// 1-bit input: A port register enable/Register enable
  .RSTRAMARSTRAM( 1'b0 ),	// 1-bit input: A port set/reset
  .RSTREGARSTREG( 1'b0 ),	// 1-bit input: A port register set/reset
  .WEA		( 2'b00 ),	// 2-bit input: A port write enable
  // Port A Data: 16-bit (each) input: Port A data
  .DIADI	( inp_data_i [15:0]),	// 16-bit input: A port data/LSB data
  .DIPADIP	( 2'b00 ),	// 2-bit input: A port parity/LSB parity
  // Port B Address/Control Signals: 14-bit (each) input: Port B address and control signals (write port
  // when RAM_MODE="SDP")
  .ADDRBWRADDR	( {inp_addr_i, 5'h0} ),	// 14-bit input: B port address/Write address
  .CLKBWRCLK	( inp_clk_i  ),	// 1-bit input: B port clock/Write clock
  .ENBWREN	( gated_wren_i ),	// 1-bit input: B port enable/Write enable
  .REGCEB	( 1'b0 ),	// 1-bit input: B port register enable
  .RSTRAMB	( 1'b0 ),	// 1-bit input: B port set/reset
  .RSTREGB	( 1'b0 ),	// 1-bit input: B port register set/reset
  .WEBWE	( 4'b1111 ),	// 4-bit input: B port write enable/Write enable
  // Port B Data: 16-bit (each) input: Port B data
  .DIBDI	( inp_data_i[31:16] ),	// 16-bit input: B port data/MSB data
  .DIPBDIP	( 2'b00 )	// 2-bit input: B port parity/MSB parity
);

endmodule
