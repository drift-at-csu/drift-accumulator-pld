module adc_lvds
#(parameter	MDW	= 32, 	// Data Width of master wishbone bus
                MAW	= 32,	// Address Width of master wishbone bus
		MBL 	= 9,	// Burst Length of master wishbone bus (means nothing so far)
		SDW	= 32,	// Data Width of slave wishbone bus
		SAW	= 32,	// Address Width of slave wishbone bus

		ADC_BIT_WIDTH	= 12,	// Number of Bits in the ADC

		D1_IDELAY	= 15,	// Data lines I-Delay values
		D2_IDELAY	= 15,
		D3_IDELAY	= 15,
		D4_IDELAY	= 15,
		D5_IDELAY	= 15,
		D6_IDELAY	= 15,
		D7_IDELAY	= 15,
		D8_IDELAY	= 15,

		FC_IDELAY	= 15, // Frame Clock I-Delay value
		DC_IDELAY	= 15 // Data Clock I-Delay value
)(
//---> Master Wishbone connections
  output wire [MAW-1:0]		wbm_adr_o,
  output wire [1:0]		wbm_bte_o,
  output wire [2:0]		wbm_cti_o,
  output wire			wbm_cyc_o,
  output wire [MDW-1:0]		wbm_dat_o,
  output wire [(MDW/8)-1:0]	wbm_sel_o,
  output wire			wbm_stb_o,
  output wire			wbm_we_o,

  input wire			wbm_ack_i,
  input wire			wbm_err_i,
  input wire			wbm_rty_i,
  input wire  [MDW-1:0]		wbm_dat_i,
  
  input wire			wbm_clk_i,
  input wire			wbm_rst_i,
  
//---> Slave Wishbone connections
  input wire [SAW-1:0]		wbs_adr_i,
  input wire [1:0]		wbs_bte_i,
  input wire [2:0]		wbs_cti_i,
  input wire			wbs_cyc_i,
  input wire [SDW-1:0]		wbs_dat_i,
  input wire [(SDW/8)-1:0]	wbs_sel_i,
  input wire			wbs_stb_i,
  input wire			wbs_we_i,
 
  output wire			wbs_ack_o,
  output wire			wbs_err_o,
  output wire			wbs_rty_o,
  output wire [SDW-1:0]		wbs_dat_o,

  input wire			wbs_clk_i,
  input wire			wbs_rst_i,

//---> Connection to the ADC Chip
  // Data Lines and clock signals
  input wire [8:1] 	data_i_p,
  input wire [8:1]	data_i_n,
  
  input wire 		frame_clk_i_p,
  input wire 		frame_clk_i_n,

  input wire 		data_clk_i_p,
  input wire 		data_clk_i_n,

  // Sample Clocks
  output wire 		sample_clk_o_p,
  output wire 		sample_clk_o_n,

  // SPI Interface
  output wire	spi_cs_o,
  inout  wire	spi_dat_io,
  output wire	spi_clk_o,

//---> Power Management (These are in Wishbone Slave Clock Domain)
  output wire	pow_adc_en_o,
  output wire	pow_gr1_en_o,
  output wire	pow_gr2_en_o,
  output wire	pow_gr3_en_o,
  output wire	pow_gr4_en_o,
//---> Interrupt (again in Wishbone Slave Clock Domain)
  output wire	interrupt_o,
//---> For Timestamp: ADC controller expects to recieve 1pulse per second
  input wire	time_pulse_i,

//---> Input Clock for sampling and reset
  input wire adc_clk_i
);
//-------------------------------------------------------------------//
/////////////////////////////
// SPI Interface          //
///////////////////////////
wire 		wbs_spi_transfer;
wire [15:0]	wbs_spi_addr;
wire [15:0]	wbs_spi_data;
wire [16:0]	spi_wbs_data;
wire		spi_wbs_ready;

// Maximum Speed for this ADC is 25MHz,
// therefore it is 2 times smaller than wishbone clk
adc_lvds_spi #(
	.DATA_WIDTH (16),
	.ADDR_WIDTH (16)
) spi (
//---> Input Control lines
  .transfer_i	(wbs_spi_transfer),
  .data_i	(wbs_spi_data),
  .addr_i	(wbs_spi_addr),
//---> Output Status Lines
  .data_o	(spi_wbs_data),
  .ready_o	(spi_wbs_ready),
//---> SPI Physical Connections to pads
  .spi_cs_o	(spi_cs_o  ),
  .spi_dat_io	(spi_dat_io),
  .spi_clk_o	(spi_clk_o ),
//---> Clock input
  .clk_i	(wbs_clk_i),
  .rst_i	(wbs_rst_i)
);
//-------------------------------------------------------------------//
///////////////////////////
// Physical Interface   //
/////////////////////////
wire [ADC_BIT_WIDTH-1:0] phy_cntr_adc1_data;
wire [ADC_BIT_WIDTH-1:0] phy_cntr_adc2_data;
wire [ADC_BIT_WIDTH-1:0] phy_cntr_adc3_data;
wire [ADC_BIT_WIDTH-1:0] phy_cntr_adc4_data;
wire [ADC_BIT_WIDTH-1:0] phy_cntr_adc5_data;
wire [ADC_BIT_WIDTH-1:0] phy_cntr_adc6_data;
wire [ADC_BIT_WIDTH-1:0] phy_cntr_adc7_data;
wire [ADC_BIT_WIDTH-1:0] phy_cntr_adc8_data;

wire	phy_cntr_adc_clk;

wire 			 wbs_phy_bitslip;
wire			 wbs_phy_reset;

wire [ADC_BIT_WIDTH-1:0] phy_wbslave_sreg_fco;
wire [ADC_BIT_WIDTH-1:0] phy_wbslave_sreg_dco;

adc_lvds_phy #(
	.ADC_BIT_WIDTH	(ADC_BIT_WIDTH),
	.D1_IDELAY	(D1_IDELAY),
	.D2_IDELAY	(D2_IDELAY),
	.D3_IDELAY	(D3_IDELAY),
	.D4_IDELAY	(D4_IDELAY),
	.D5_IDELAY	(D5_IDELAY),
	.D6_IDELAY	(D6_IDELAY),
	.D7_IDELAY	(D7_IDELAY),
	.D8_IDELAY	(D8_IDELAY),
	.FC_IDELAY	(FC_IDELAY),
	.DC_IDELAY	(DC_IDELAY)
) phy (
//---> The Parallel ADC data output
  .adc1_data_o 		( phy_cntr_adc1_data ),
  .adc2_data_o 		( phy_cntr_adc2_data ),
  .adc3_data_o		( phy_cntr_adc3_data ),
  .adc4_data_o		( phy_cntr_adc4_data ),
  .adc5_data_o		( phy_cntr_adc5_data ),
  .adc6_data_o		( phy_cntr_adc6_data ),
  .adc7_data_o		( phy_cntr_adc7_data ),
  .adc8_data_o		( phy_cntr_adc8_data ),

  .adcs_data_clk_o	( phy_cntr_adc_clk ),

//---> Status Registers outputs
  .sreg_fco_o		( phy_wbslave_sreg_fco ),
  .sreg_dco_o		( phy_wbslave_sreg_dco ),
//---> Control Registers inputs
  .creg_bitslip_i	( wbs_phy_bitslip ),
  .creg_reset_i		( wbs_phy_reset ),
  .creg_delay_inc_i	( wbs_phy_delay_inc ),
//---> Connection to the ADC Chip that should go directly to the pads
  .data_i_p 		( data_i_p ),
  .data_i_n 		( data_i_n ),
  .frame_clk_i_p	( frame_clk_i_p ),
  .frame_clk_i_n	( frame_clk_i_n ),
  .data_clk_i_p 	( data_clk_i_p ),
  .data_clk_i_n 	( data_clk_i_n ),

//---> Sample Clocks
  .sample_clk_o_p 	( sample_clk_o_p ),
  .sample_clk_o_n 	( sample_clk_o_n ),

  .adc_clk_i		( adc_clk_i )
);
//-------------------------------------------------------------------//
////////////////////////////////
// Wishbone Slave Interface  //
//////////////////////////////
wire [31:0] wbs_cntr_all;

assign pow_adc_en_o = wbs_cntr_all[24];
assign pow_gr1_en_o = wbs_cntr_all[25];
assign pow_gr2_en_o = wbs_cntr_all[26];
assign pow_gr3_en_o = wbs_cntr_all[27];
assign pow_gr4_en_o = wbs_cntr_all[28];

wire [8:0] wbs_cntr_adc1_average;
wire [8:0] wbs_cntr_adc2_average;
wire [8:0] wbs_cntr_adc3_average;
wire [8:0] wbs_cntr_adc4_average;
wire [8:0] wbs_cntr_adc5_average;
wire [8:0] wbs_cntr_adc6_average;
wire [8:0] wbs_cntr_adc7_average;
wire [8:0] wbs_cntr_adc8_average;

wire [27:0]	cntr_wbs_adc1_pedestal;
wire [27:0]	cntr_wbs_adc2_pedestal;
wire [27:0]	cntr_wbs_adc3_pedestal;
wire [27:0]	cntr_wbs_adc4_pedestal;
wire [27:0]	cntr_wbs_adc5_pedestal;
wire [27:0]	cntr_wbs_adc6_pedestal;
wire [27:0]	cntr_wbs_adc7_pedestal;
wire [27:0]	cntr_wbs_adc8_pedestal;

wire [27:0]	wbs_cntr_adc1_trig_low;
wire [27:0]     wbs_cntr_adc2_trig_low;
wire [27:0]	wbs_cntr_adc3_trig_low;
wire [27:0]	wbs_cntr_adc4_trig_low;
wire [27:0]	wbs_cntr_adc5_trig_low;
wire [27:0]	wbs_cntr_adc6_trig_low;
wire [27:0]	wbs_cntr_adc7_trig_low;
wire [27:0]	wbs_cntr_adc8_trig_low;

wire [27:0]	wbs_cntr_adc1_trig_hig;
wire [27:0]	wbs_cntr_adc2_trig_hig;
wire [27:0]	wbs_cntr_adc3_trig_hig;
wire [27:0]	wbs_cntr_adc4_trig_hig;
wire [27:0]	wbs_cntr_adc5_trig_hig;
wire [27:0]	wbs_cntr_adc6_trig_hig;
wire [27:0]	wbs_cntr_adc7_trig_hig;
wire [27:0]	wbs_cntr_adc8_trig_hig;

wire	wbs_cntr_adc1_trig_ext;
wire    wbs_cntr_adc2_trig_ext;
wire    wbs_cntr_adc3_trig_ext;
wire    wbs_cntr_adc4_trig_ext;
wire    wbs_cntr_adc5_trig_ext;
wire    wbs_cntr_adc6_trig_ext;
wire    wbs_cntr_adc7_trig_ext;
wire    wbs_cntr_adc8_trig_ext;

wire [31:0] wbs_wbm_adc1_base_addr;
wire [31:0] wbs_wbm_adc2_base_addr;
wire [31:0] wbs_wbm_adc3_base_addr;
wire [31:0] wbs_wbm_adc4_base_addr;
wire [31:0] wbs_wbm_adc5_base_addr;
wire [31:0] wbs_wbm_adc6_base_addr;
wire [31:0] wbs_wbm_adc7_base_addr;
wire [31:0] wbs_wbm_adc8_base_addr;

// Timestamp wires goes here, since Wishbone slave is the first one that
// uses them
wire [MDW-1:0]	tms_cntr_seconds;
wire [MDW-1:0]	tms_cntr_quant_per_sec;

wire 		wbs_tms_seconds_reset;

// Wires that eventually generate interrupts 
wire wbm_wbs_chan1_transfered;
wire wbm_wbs_chan2_transfered;
wire wbm_wbs_chan3_transfered;
wire wbm_wbs_chan4_transfered;
wire wbm_wbs_chan5_transfered;
wire wbm_wbs_chan6_transfered;
wire wbm_wbs_chan7_transfered;
wire wbm_wbs_chan8_transfered;

wire wbs_wbm_chan1_resume;
wire wbs_wbm_chan2_resume;
wire wbs_wbm_chan3_resume;
wire wbs_wbm_chan4_resume;
wire wbs_wbm_chan5_resume;
wire wbs_wbm_chan6_resume;
wire wbs_wbm_chan7_resume;
wire wbs_wbm_chan8_resume;


adc_lvds_wbslave #(
        .DATA_WIDTH	(SDW),
	.ADDRESS_WIDTH	(SAW)
) wbslave (
//---> Slave Wishbone connections
  .wb_adr_i	(wbs_adr_i),
  .wb_bte_i	(wbs_bte_i),
  .wb_cti_i	(wbs_cti_i),
  .wb_cyc_i	(wbs_cyc_i),
  .wb_dat_i	(wbs_dat_i),
  .wb_sel_i	(wbs_sel_i),
  .wb_stb_i	(wbs_stb_i),
  .wb_we_i 	(wbs_we_i),

  .wb_ack_o	(wbs_ack_o),
  .wb_err_o	(wbs_err_o),
  .wb_rty_o	(wbs_rty_o),
  .wb_dat_o	(wbs_dat_o),

  .wb_clk_i	(wbs_clk_i),
  .wb_rst_i	(wbs_rst_i),
//---> Control Registers output
  .creg_control_o	( wbs_cntr_all ),

  .creg_phy_bitslip_o	( wbs_phy_bitslip ),
  .creg_phy_reset_o	( wbs_phy_reset ),
  .creg_phy_delay_inc_o	( wbs_phy_delay_inc ),

  .creg_timestamp_reset_o ( wbs_tms_seconds_reset ),

  .creg_adc1_trig_low_o	( wbs_cntr_adc1_trig_low ),
  .creg_adc1_trig_hig_o	( wbs_cntr_adc1_trig_hig ),
  .creg_adc1_trig_ext_o ( wbs_cntr_adc1_trig_ext ),
  .creg_adc1_base_addr_o( wbs_wbm_adc1_base_addr ),
  .creg_adc1_average_o  ( wbs_cntr_adc1_average  ),
  .creg_adc1_resume_o	( wbs_wbm_chan1_resume   ),

  .creg_adc2_trig_low_o	( wbs_cntr_adc2_trig_low ),
  .creg_adc2_trig_hig_o	( wbs_cntr_adc2_trig_hig ),
  .creg_adc2_trig_ext_o ( wbs_cntr_adc2_trig_ext ),
  .creg_adc2_base_addr_o( wbs_wbm_adc2_base_addr ),
  .creg_adc2_average_o  ( wbs_cntr_adc2_average  ),
  .creg_adc2_resume_o	( wbs_wbm_chan2_resume   ),

  .creg_adc3_trig_low_o	( wbs_cntr_adc3_trig_low ),
  .creg_adc3_trig_hig_o	( wbs_cntr_adc3_trig_hig ),
  .creg_adc3_trig_ext_o ( wbs_cntr_adc3_trig_ext ),
  .creg_adc3_base_addr_o( wbs_wbm_adc3_base_addr ),
  .creg_adc3_average_o  ( wbs_cntr_adc3_average  ),
  .creg_adc3_resume_o	( wbs_wbm_chan3_resume   ),

  .creg_adc4_trig_low_o	( wbs_cntr_adc4_trig_low ),
  .creg_adc4_trig_hig_o	( wbs_cntr_adc4_trig_hig ),
  .creg_adc4_trig_ext_o ( wbs_cntr_adc4_trig_ext ),
  .creg_adc4_base_addr_o( wbs_wbm_adc4_base_addr ),
  .creg_adc4_average_o  ( wbs_cntr_adc4_average  ),
  .creg_adc4_resume_o	( wbs_wbm_chan4_resume   ),

  .creg_adc5_trig_low_o	( wbs_cntr_adc5_trig_low ),
  .creg_adc5_trig_hig_o	( wbs_cntr_adc5_trig_hig ),
  .creg_adc5_trig_ext_o ( wbs_cntr_adc5_trig_ext ),
  .creg_adc5_base_addr_o( wbs_wbm_adc5_base_addr ),
  .creg_adc5_average_o  ( wbs_cntr_adc5_average  ),
  .creg_adc5_resume_o   ( wbs_wbm_chan5_resume   ),

  .creg_adc6_trig_low_o	( wbs_cntr_adc6_trig_low ),
  .creg_adc6_trig_hig_o	( wbs_cntr_adc6_trig_hig ),
  .creg_adc6_trig_ext_o ( wbs_cntr_adc6_trig_ext ),
  .creg_adc6_base_addr_o( wbs_wbm_adc6_base_addr ),
  .creg_adc6_average_o  ( wbs_cntr_adc6_average  ),
  .creg_adc6_resume_o	( wbs_wbm_chan6_resume   ),

  .creg_adc7_trig_low_o	( wbs_cntr_adc7_trig_low ),
  .creg_adc7_trig_hig_o	( wbs_cntr_adc7_trig_hig ),
  .creg_adc7_trig_ext_o ( wbs_cntr_adc7_trig_ext ),
  .creg_adc7_base_addr_o( wbs_wbm_adc7_base_addr ),
  .creg_adc7_average_o  ( wbs_cntr_adc7_average  ),
  .creg_adc7_resume_o	( wbs_wbm_chan7_resume   ),

  .creg_adc8_trig_low_o	( wbs_cntr_adc8_trig_low ),
  .creg_adc8_trig_hig_o	( wbs_cntr_adc8_trig_hig ),
  .creg_adc8_trig_ext_o ( wbs_cntr_adc8_trig_ext ),
  .creg_adc8_base_addr_o( wbs_wbm_adc8_base_addr ),
  .creg_adc8_average_o  ( wbs_cntr_adc8_average  ),
  .creg_adc8_resume_o   ( wbs_wbm_chan8_resume   ),

  // SPI Control
  .creg_spi_transfer_o	( wbs_spi_transfer ),
  .creg_spi_addr_reg_o	( wbs_spi_addr ),
  .creg_spi_data_reg_o	( wbs_spi_data ),

//---> Status Registers Input
  // TIMESTAMP
  .sreg_timestamp_sec_i	( tms_cntr_seconds ),
  .sreg_timestamp_qps_i	( tms_cntr_quant_per_sec ),

  // PHY Interface
  .sreg_phy_fco_i	( phy_wbslave_sreg_fco ),
  .sreg_phy_dco_i	( phy_wbslave_sreg_dco ),

  // CONTROLLER Interface
  .sreg_cnt_adc1_pedestal_i	( cntr_wbs_adc1_pedestal ),
  .sreg_cnt_adc2_pedestal_i     ( cntr_wbs_adc2_pedestal ),
  .sreg_cnt_adc3_pedestal_i	( cntr_wbs_adc3_pedestal ),
  .sreg_cnt_adc4_pedestal_i	( cntr_wbs_adc4_pedestal ),
  .sreg_cnt_adc5_pedestal_i     ( cntr_wbs_adc5_pedestal ),
  .sreg_cnt_adc6_pedestal_i     ( cntr_wbs_adc6_pedestal ),
  .sreg_cnt_adc7_pedestal_i	( cntr_wbs_adc7_pedestal ),
  .sreg_cnt_adc8_pedestal_i	( cntr_wbs_adc8_pedestal ),

  .sreg_cnt_adc1_transfered_i	( wbm_wbs_chan1_transfered ),
  .sreg_cnt_adc2_transfered_i   ( wbm_wbs_chan2_transfered ),
  .sreg_cnt_adc3_transfered_i   ( wbm_wbs_chan3_transfered ),
  .sreg_cnt_adc4_transfered_i   ( wbm_wbs_chan4_transfered ),
  .sreg_cnt_adc5_transfered_i   ( wbm_wbs_chan5_transfered ),
  .sreg_cnt_adc6_transfered_i   ( wbm_wbs_chan6_transfered ),
  .sreg_cnt_adc7_transfered_i   ( wbm_wbs_chan7_transfered ),
  .sreg_cnt_adc8_transfered_i   ( wbm_wbs_chan8_transfered ),

  // SPI Status
  .sreg_spi_ready_i	( spi_wbs_ready ),
  .sreg_spi_data_reg_i	( spi_wbs_data ),

  .interrupt_o	( interrupt_o )
);
//-------------------------------------------------------------------//
////////////////////////////////
// Wishbone Master Interface //
//////////////////////////////
wire [MDW-1:0]	cntr_wbm_adc1_buf0_data;
wire		cntr_wbm_adc1_buf0_ready;
wire		wbm_cntr_adc1_buf0_read;
wire [MDW-1:0]	cntr_wbm_adc1_buf0_monit_seconds;
wire [MDW-1:0]	cntr_wbm_adc1_buf0_monit_quants;
wire [MDW-1:0]	cntr_wbm_adc1_buf0_monit_quants_persec;
wire [MDW-1:0]	cntr_wbm_adc1_buf0_monit_status;
wire [MDW-1:0]	cntr_wbm_adc1_buf1_data;
wire		cntr_wbm_adc1_buf1_ready;
wire		wbm_cntr_adc1_buf1_read;
wire [MDW-1:0]	cntr_wbm_adc1_buf1_monit_seconds;
wire [MDW-1:0]	cntr_wbm_adc1_buf1_monit_quants;
wire [MDW-1:0]	cntr_wbm_adc1_buf1_monit_quants_persec;
wire [MDW-1:0]	cntr_wbm_adc1_buf1_monit_status;

wire [MDW-1:0]	cntr_wbm_adc2_buf0_data;
wire		cntr_wbm_adc2_buf0_ready;
wire		wbm_cntr_adc2_buf0_read;
wire [MDW-1:0]	cntr_wbm_adc2_buf0_monit_seconds;
wire [MDW-1:0]	cntr_wbm_adc2_buf0_monit_quants;
wire [MDW-1:0]	cntr_wbm_adc2_buf0_monit_quants_persec;
wire [MDW-1:0]	cntr_wbm_adc2_buf0_monit_status;
wire [MDW-1:0]	cntr_wbm_adc2_buf1_data;
wire		cntr_wbm_adc2_buf1_ready;
wire		wbm_cntr_adc2_buf1_read;
wire [MDW-1:0]	cntr_wbm_adc2_buf1_monit_seconds;
wire [MDW-1:0]	cntr_wbm_adc2_buf1_monit_quants;
wire [MDW-1:0]	cntr_wbm_adc2_buf1_monit_quants_persec;
wire [MDW-1:0]	cntr_wbm_adc2_buf1_monit_status;

wire [MDW-1:0]	cntr_wbm_adc3_buf0_data;
wire		cntr_wbm_adc3_buf0_ready;
wire		wbm_cntr_adc3_buf0_read;
wire [MDW-1:0]	cntr_wbm_adc3_buf0_monit_seconds;
wire [MDW-1:0]	cntr_wbm_adc3_buf0_monit_quants;
wire [MDW-1:0]	cntr_wbm_adc3_buf0_monit_quants_persec;
wire [MDW-1:0]	cntr_wbm_adc3_buf0_monit_status;
wire [MDW-1:0]	cntr_wbm_adc3_buf1_data;
wire		cntr_wbm_adc3_buf1_ready;
wire		wbm_cntr_adc3_buf1_read;
wire [MDW-1:0]	cntr_wbm_adc3_buf1_monit_seconds;
wire [MDW-1:0]	cntr_wbm_adc3_buf1_monit_quants;
wire [MDW-1:0]	cntr_wbm_adc3_buf1_monit_quants_persec;
wire [MDW-1:0]	cntr_wbm_adc3_buf1_monit_status;

wire [MDW-1:0]	cntr_wbm_adc4_buf0_data;
wire		cntr_wbm_adc4_buf0_ready;
wire		wbm_cntr_adc4_buf0_read;
wire [MDW-1:0]	cntr_wbm_adc4_buf0_monit_seconds;
wire [MDW-1:0]	cntr_wbm_adc4_buf0_monit_quants;
wire [MDW-1:0]	cntr_wbm_adc4_buf0_monit_quants_persec;
wire [MDW-1:0]	cntr_wbm_adc4_buf0_monit_status;
wire [MDW-1:0]	cntr_wbm_adc4_buf1_data;
wire		cntr_wbm_adc4_buf1_ready;
wire		wbm_cntr_adc4_buf1_read;
wire [MDW-1:0]	cntr_wbm_adc4_buf1_monit_seconds;
wire [MDW-1:0]	cntr_wbm_adc4_buf1_monit_quants;
wire [MDW-1:0]	cntr_wbm_adc4_buf1_monit_quants_persec;
wire [MDW-1:0]	cntr_wbm_adc4_buf1_monit_status;

wire [MDW-1:0]	cntr_wbm_adc5_buf0_data;
wire		cntr_wbm_adc5_buf0_ready;
wire		wbm_cntr_adc5_buf0_read;
wire [MDW-1:0]	cntr_wbm_adc5_buf0_monit_seconds;
wire [MDW-1:0]	cntr_wbm_adc5_buf0_monit_quants;
wire [MDW-1:0]	cntr_wbm_adc5_buf0_monit_quants_persec;
wire [MDW-1:0]	cntr_wbm_adc5_buf0_monit_status;
wire [MDW-1:0]	cntr_wbm_adc5_buf1_data;
wire		cntr_wbm_adc5_buf1_ready;
wire		wbm_cntr_adc5_buf1_read;
wire [MDW-1:0]	cntr_wbm_adc5_buf1_monit_seconds;
wire [MDW-1:0]	cntr_wbm_adc5_buf1_monit_quants;
wire [MDW-1:0]	cntr_wbm_adc5_buf1_monit_quants_persec;
wire [MDW-1:0]	cntr_wbm_adc5_buf1_monit_status;

wire [MDW-1:0]	cntr_wbm_adc6_buf0_data;
wire		cntr_wbm_adc6_buf0_ready;
wire		wbm_cntr_adc6_buf0_read;
wire [MDW-1:0]	cntr_wbm_adc6_buf0_monit_seconds;
wire [MDW-1:0]	cntr_wbm_adc6_buf0_monit_quants;
wire [MDW-1:0]	cntr_wbm_adc6_buf0_monit_quants_persec;
wire [MDW-1:0]	cntr_wbm_adc6_buf0_monit_status;
wire [MDW-1:0]	cntr_wbm_adc6_buf1_data;
wire		cntr_wbm_adc6_buf1_ready;
wire		wbm_cntr_adc6_buf1_read;
wire [MDW-1:0]	cntr_wbm_adc6_buf1_monit_seconds;
wire [MDW-1:0]	cntr_wbm_adc6_buf1_monit_quants;
wire [MDW-1:0]	cntr_wbm_adc6_buf1_monit_quants_persec;
wire [MDW-1:0]	cntr_wbm_adc6_buf1_monit_status;

wire [MDW-1:0]	cntr_wbm_adc7_buf0_data;
wire		cntr_wbm_adc7_buf0_ready;
wire		wbm_cntr_adc7_buf0_read;
wire [MDW-1:0]	cntr_wbm_adc7_buf0_monit_seconds;
wire [MDW-1:0]	cntr_wbm_adc7_buf0_monit_quants;
wire [MDW-1:0]	cntr_wbm_adc7_buf0_monit_quants_persec;
wire [MDW-1:0]	cntr_wbm_adc7_buf0_monit_status;
wire [MDW-1:0]	cntr_wbm_adc7_buf1_data;
wire		cntr_wbm_adc7_buf1_ready;
wire		wbm_cntr_adc7_buf1_read;
wire [MDW-1:0]	cntr_wbm_adc7_buf1_monit_seconds;
wire [MDW-1:0]	cntr_wbm_adc7_buf1_monit_quants;
wire [MDW-1:0]	cntr_wbm_adc7_buf1_monit_quants_persec;
wire [MDW-1:0]	cntr_wbm_adc7_buf1_monit_status;

wire [MDW-1:0]	cntr_wbm_adc8_buf0_data;
wire		cntr_wbm_adc8_buf0_ready;
wire		wbm_cntr_adc8_buf0_read;
wire [MDW-1:0]	cntr_wbm_adc8_buf0_monit_seconds;
wire [MDW-1:0]	cntr_wbm_adc8_buf0_monit_quants;
wire [MDW-1:0]	cntr_wbm_adc8_buf0_monit_quants_persec;
wire [MDW-1:0]	cntr_wbm_adc8_buf0_monit_status;
wire [MDW-1:0]	cntr_wbm_adc8_buf1_data;
wire		cntr_wbm_adc8_buf1_ready;
wire		wbm_cntr_adc8_buf1_read;
wire [MDW-1:0]	cntr_wbm_adc8_buf1_monit_seconds;
wire [MDW-1:0]	cntr_wbm_adc8_buf1_monit_quants;
wire [MDW-1:0]	cntr_wbm_adc8_buf1_monit_quants_persec;
wire [MDW-1:0]	cntr_wbm_adc8_buf1_monit_status;

adc_lvds_wbmaster #(
	.DATA_WIDTH     (MDW),
	.ADDRESS_WIDTH  (MAW),
	.BURST_LENGTH	(MBL)
) wbmaster (
//---> Master Wishbone connections
  .wb_adr_o	(wbm_adr_o),
  .wb_bte_o	(wbm_bte_o),
  .wb_cti_o	(wbm_cti_o),
  .wb_cyc_o	(wbm_cyc_o),
  .wb_dat_o	(wbm_dat_o),
  .wb_sel_o	(wbm_sel_o),
  .wb_stb_o	(wbm_stb_o),
  .wb_we_o	(wbm_we_o),

  .wb_ack_i	(wbm_ack_i),
  .wb_err_i	(wbm_err_i),
  .wb_rty_i	(wbm_rty_i),
  .wb_dat_i	(wbm_dat_i),

  .wb_clk_i	(wbm_clk_i),
  .wb_rst_i	(wbm_rst_i),

//---> Circulating Buffer Inputs
  // CHANNEL 1
  .chan1_buf0_data_i	( cntr_wbm_adc1_buf0_data  ),
  .chan1_buf0_ready_i	( cntr_wbm_adc1_buf0_ready ),
  .chan1_buf0_read_o	( wbm_cntr_adc1_buf0_read  ),
  .chan1_buf0_mon_sec_i	( cntr_wbm_adc1_buf0_monit_seconds ),
  .chan1_buf0_mon_qnt_i ( cntr_wbm_adc1_buf0_monit_quants ),
  .chan1_buf0_mon_qps_i ( cntr_wbm_adc1_buf0_monit_quants_persec ),
  .chan1_buf0_mon_sts_i ( cntr_wbm_adc1_buf0_monit_status ),
  .chan1_buf1_data_i	( cntr_wbm_adc1_buf1_data  ),
  .chan1_buf1_ready_i	( cntr_wbm_adc1_buf1_ready ),
  .chan1_buf1_read_o	( wbm_cntr_adc1_buf1_read  ),
  .chan1_buf1_mon_sec_i	( cntr_wbm_adc1_buf1_monit_seconds ),
  .chan1_buf1_mon_qnt_i	( cntr_wbm_adc1_buf1_monit_quants ),
  .chan1_buf1_mon_qps_i	( cntr_wbm_adc1_buf1_monit_quants_persec ),
  .chan1_buf1_mon_sts_i	( cntr_wbm_adc1_buf1_monit_status ),
  // CHANNEL 2
  .chan2_buf0_data_i	( cntr_wbm_adc2_buf0_data  ),
  .chan2_buf0_ready_i	( cntr_wbm_adc2_buf0_ready ),
  .chan2_buf0_read_o	( wbm_cntr_adc2_buf0_read  ),
  .chan2_buf0_mon_sec_i	( cntr_wbm_adc2_buf0_monit_seconds ),
  .chan2_buf0_mon_qnt_i	( cntr_wbm_adc2_buf0_monit_quants ),
  .chan2_buf0_mon_qps_i	( cntr_wbm_adc2_buf0_monit_quants_persec ),
  .chan2_buf0_mon_sts_i	( cntr_wbm_adc2_buf0_monit_status ),
  .chan2_buf1_data_i	( cntr_wbm_adc2_buf1_data  ),
  .chan2_buf1_ready_i	( cntr_wbm_adc2_buf1_ready ),
  .chan2_buf1_read_o	( wbm_cntr_adc2_buf1_read  ),
  .chan2_buf1_mon_sec_i	( cntr_wbm_adc2_buf1_monit_seconds ),
  .chan2_buf1_mon_qnt_i	( cntr_wbm_adc2_buf1_monit_quants ),
  .chan2_buf1_mon_qps_i	( cntr_wbm_adc2_buf1_monit_quants_persec ),
  .chan2_buf1_mon_sts_i	( cntr_wbm_adc2_buf1_monit_status ),
  // CHANNEL 3
  .chan3_buf0_data_i	( cntr_wbm_adc3_buf0_data  ),
  .chan3_buf0_ready_i	( cntr_wbm_adc3_buf0_ready ),
  .chan3_buf0_read_o	( wbm_cntr_adc3_buf0_read  ),
  .chan3_buf0_mon_sec_i	( cntr_wbm_adc3_buf0_monit_seconds ),
  .chan3_buf0_mon_qnt_i	( cntr_wbm_adc3_buf0_monit_quants ),
  .chan3_buf0_mon_qps_i	( cntr_wbm_adc3_buf0_monit_quants_persec ),
  .chan3_buf0_mon_sts_i	( cntr_wbm_adc3_buf0_monit_status ),
  .chan3_buf1_data_i	( cntr_wbm_adc3_buf1_data  ),
  .chan3_buf1_ready_i	( cntr_wbm_adc3_buf1_ready ),
  .chan3_buf1_read_o	( wbm_cntr_adc3_buf1_read  ),
  .chan3_buf1_mon_sec_i	( cntr_wbm_adc3_buf1_monit_seconds ),
  .chan3_buf1_mon_qnt_i	( cntr_wbm_adc3_buf1_monit_quants ),
  .chan3_buf1_mon_qps_i	( cntr_wbm_adc3_buf1_monit_quants_persec ),
  .chan3_buf1_mon_sts_i	( cntr_wbm_adc3_buf1_monit_status ),
  // CHANNEL 4
  .chan4_buf0_data_i	( cntr_wbm_adc4_buf0_data  ),
  .chan4_buf0_ready_i	( cntr_wbm_adc4_buf0_ready ),
  .chan4_buf0_read_o	( wbm_cntr_adc4_buf0_read  ),
  .chan4_buf0_mon_sec_i	( cntr_wbm_adc4_buf0_monit_seconds ),
  .chan4_buf0_mon_qnt_i	( cntr_wbm_adc4_buf0_monit_quants ),
  .chan4_buf0_mon_qps_i	( cntr_wbm_adc4_buf0_monit_quants_persec ),
  .chan4_buf0_mon_sts_i	( cntr_wbm_adc4_buf0_monit_status ),
  .chan4_buf1_data_i	( cntr_wbm_adc4_buf1_data  ),
  .chan4_buf1_ready_i	( cntr_wbm_adc4_buf1_ready ),
  .chan4_buf1_read_o	( wbm_cntr_adc4_buf1_read  ),
  .chan4_buf1_mon_sec_i ( cntr_wbm_adc4_buf1_monit_seconds ),
  .chan4_buf1_mon_qnt_i ( cntr_wbm_adc4_buf1_monit_quants ),
  .chan4_buf1_mon_qps_i ( cntr_wbm_adc4_buf1_monit_quants_persec ),
  .chan4_buf1_mon_sts_i ( cntr_wbm_adc4_buf1_monit_status ),
  // CHANNEL 5
  .chan5_buf0_data_i	( cntr_wbm_adc5_buf0_data  ),
  .chan5_buf0_ready_i	( cntr_wbm_adc5_buf0_ready ),
  .chan5_buf0_read_o	( wbm_cntr_adc5_buf0_read  ),
  .chan5_buf0_mon_sec_i ( cntr_wbm_adc5_buf0_monit_seconds ),
  .chan5_buf0_mon_qnt_i ( cntr_wbm_adc5_buf0_monit_quants ),
  .chan5_buf0_mon_qps_i ( cntr_wbm_adc5_buf0_monit_quants_persec ),
  .chan5_buf0_mon_sts_i ( cntr_wbm_adc5_buf0_monit_status ),
  .chan5_buf1_data_i	( cntr_wbm_adc5_buf1_data  ),
  .chan5_buf1_ready_i	( cntr_wbm_adc5_buf1_ready ),
  .chan5_buf1_read_o	( wbm_cntr_adc5_buf1_read  ),
  .chan5_buf1_mon_sec_i	( cntr_wbm_adc5_buf1_monit_seconds ),
  .chan5_buf1_mon_qnt_i	( cntr_wbm_adc5_buf1_monit_quants ),
  .chan5_buf1_mon_qps_i	( cntr_wbm_adc5_buf1_monit_quants_persec ),
  .chan5_buf1_mon_sts_i	( cntr_wbm_adc5_buf1_monit_status ),
  // CHANNEL 6
  .chan6_buf0_data_i	( cntr_wbm_adc6_buf0_data  ),
  .chan6_buf0_ready_i	( cntr_wbm_adc6_buf0_ready ),
  .chan6_buf0_read_o	( wbm_cntr_adc6_buf0_read  ),
  .chan6_buf0_mon_sec_i ( cntr_wbm_adc6_buf0_monit_seconds ),
  .chan6_buf0_mon_qnt_i ( cntr_wbm_adc6_buf0_monit_quants ),
  .chan6_buf0_mon_qps_i ( cntr_wbm_adc6_buf0_monit_quants_persec ),
  .chan6_buf0_mon_sts_i ( cntr_wbm_adc6_buf0_monit_status ),
  .chan6_buf1_data_i	( cntr_wbm_adc6_buf1_data  ),
  .chan6_buf1_ready_i	( cntr_wbm_adc6_buf1_ready ),
  .chan6_buf1_read_o	( wbm_cntr_adc6_buf1_read  ),
  .chan6_buf1_mon_sec_i	( cntr_wbm_adc6_buf1_monit_seconds ),
  .chan6_buf1_mon_qnt_i	( cntr_wbm_adc6_buf1_monit_quants ),
  .chan6_buf1_mon_qps_i	( cntr_wbm_adc6_buf1_monit_quants_persec ),
  .chan6_buf1_mon_sts_i	( cntr_wbm_adc6_buf1_monit_status ),
  // CHANNEL 7
  .chan7_buf0_data_i	( cntr_wbm_adc7_buf0_data  ),
  .chan7_buf0_ready_i	( cntr_wbm_adc7_buf0_ready ),
  .chan7_buf0_read_o	( wbm_cntr_adc7_buf0_read  ),
  .chan7_buf0_mon_sec_i ( cntr_wbm_adc7_buf0_monit_seconds ),
  .chan7_buf0_mon_qnt_i ( cntr_wbm_adc7_buf0_monit_quants ),
  .chan7_buf0_mon_qps_i ( cntr_wbm_adc7_buf0_monit_quants_persec ),
  .chan7_buf0_mon_sts_i ( cntr_wbm_adc7_buf0_monit_status ),
  .chan7_buf1_data_i	( cntr_wbm_adc7_buf1_data  ),
  .chan7_buf1_ready_i	( cntr_wbm_adc7_buf1_ready ),
  .chan7_buf1_read_o	( wbm_cntr_adc7_buf1_read  ),
  .chan7_buf1_mon_sec_i ( cntr_wbm_adc7_buf1_monit_seconds ),
  .chan7_buf1_mon_qnt_i ( cntr_wbm_adc7_buf1_monit_quants ),
  .chan7_buf1_mon_qps_i ( cntr_wbm_adc7_buf1_monit_quants_persec ),
  .chan7_buf1_mon_sts_i ( cntr_wbm_adc7_buf1_monit_status ),
  // CHANNEL 8
  .chan8_buf0_data_i	( cntr_wbm_adc8_buf0_data  ),
  .chan8_buf0_ready_i	( cntr_wbm_adc8_buf0_ready ),
  .chan8_buf0_read_o	( wbm_cntr_adc8_buf0_read  ),
  .chan8_buf0_mon_sec_i ( cntr_wbm_adc8_buf0_monit_seconds ),
  .chan8_buf0_mon_qnt_i ( cntr_wbm_adc8_buf0_monit_quants ),
  .chan8_buf0_mon_qps_i ( cntr_wbm_adc8_buf0_monit_quants_persec ),
  .chan8_buf0_mon_sts_i ( cntr_wbm_adc8_buf0_monit_status ),
  .chan8_buf1_data_i	( cntr_wbm_adc8_buf1_data  ),
  .chan8_buf1_ready_i	( cntr_wbm_adc8_buf1_ready ),
  .chan8_buf1_read_o	( wbm_cntr_adc8_buf1_read  ),
  .chan8_buf1_mon_sec_i ( cntr_wbm_adc8_buf1_monit_seconds ),
  .chan8_buf1_mon_qnt_i ( cntr_wbm_adc8_buf1_monit_quants ),
  .chan8_buf1_mon_qps_i ( cntr_wbm_adc8_buf1_monit_quants_persec ),
  .chan8_buf1_mon_sts_i ( cntr_wbm_adc8_buf1_monit_status ),
//---> Status  Registers outputs
  .sreg_chan1_transfered_o	( wbm_wbs_chan1_transfered ),
  .sreg_chan2_transfered_o	( wbm_wbs_chan2_transfered ),
  .sreg_chan3_transfered_o	( wbm_wbs_chan3_transfered ),
  .sreg_chan4_transfered_o	( wbm_wbs_chan4_transfered ),
  .sreg_chan5_transfered_o	( wbm_wbs_chan5_transfered ),
  .sreg_chan6_transfered_o	( wbm_wbs_chan6_transfered ),
  .sreg_chan7_transfered_o	( wbm_wbs_chan7_transfered ),
  .sreg_chan8_transfered_o	( wbm_wbs_chan8_transfered ),
//---> Control Registers input
  .creg_chan1_baseaddr_i	( wbs_wbm_adc1_base_addr ),
  .creg_chan1_resume_i		( wbs_wbm_chan1_resume   ),
  .creg_chan2_baseaddr_i	( wbs_wbm_adc2_base_addr ),
  .creg_chan2_resume_i		( wbs_wbm_chan2_resume   ),
  .creg_chan3_baseaddr_i	( wbs_wbm_adc3_base_addr ),
  .creg_chan3_resume_i		( wbs_wbm_chan3_resume   ),
  .creg_chan4_baseaddr_i	( wbs_wbm_adc4_base_addr ),
  .creg_chan4_resume_i		( wbs_wbm_chan4_resume   ),
  .creg_chan5_baseaddr_i	( wbs_wbm_adc5_base_addr ),
  .creg_chan5_resume_i		( wbs_wbm_chan5_resume   ),
  .creg_chan6_baseaddr_i	( wbs_wbm_adc6_base_addr ),
  .creg_chan6_resume_i		( wbs_wbm_chan6_resume   ),
  .creg_chan7_baseaddr_i	( wbs_wbm_adc7_base_addr ),
  .creg_chan7_resume_i		( wbs_wbm_chan7_resume   ),
  .creg_chan8_baseaddr_i	( wbs_wbm_adc8_base_addr ),
  .creg_chan8_resume_i  	( wbs_wbm_chan8_resume   )
);
//-------------------------------------------------------------------//
////////////////////////////////
// Controller interface      //
//////////////////////////////
// Timestamp wires
wire [MDW-1:0] tms_cntr_quant_counter;

adc_lvds_cntr # (
	.NOF_ADCS (8),
	.ADC_BIT_WIDTH(ADC_BIT_WIDTH),
	.OUT_DATA_WIDTH(MDW),
	.OUT_ADDR_WIDTH(10)
) cntr (
//---> Data input
  .adc1_data_i	( phy_cntr_adc1_data ),
  .adc2_data_i	( phy_cntr_adc2_data ),
  .adc3_data_i	( phy_cntr_adc3_data ),
  .adc4_data_i	( phy_cntr_adc4_data ),
  .adc5_data_i	( phy_cntr_adc5_data ),
  .adc6_data_i	( phy_cntr_adc6_data ),
  .adc7_data_i	( phy_cntr_adc7_data ),
  .adc8_data_i	( phy_cntr_adc8_data ),

  .inp_clk_i	( phy_cntr_adc_clk ),
//---> Timestamp
  .timestamp_seconds_i		( tms_cntr_seconds ),
  .timestamp_quants_persec_i 	( tms_cntr_quant_per_sec ),
  .timestamp_quants_i		( tms_cntr_quant_counter ),
//---> Data output ( Wishbone Master Interface )
  .adc1_buf0_data_o	( cntr_wbm_adc1_buf0_data  ),
  .adc1_buf0_ready_o	( cntr_wbm_adc1_buf0_ready ),
  .adc1_buf0_read_i	( wbm_cntr_adc1_buf0_read  ),
  .adc1_buf0_monit_seconds_o		( cntr_wbm_adc1_buf0_monit_seconds ),
  .adc1_buf0_monit_quants_o		( cntr_wbm_adc1_buf0_monit_quants ),
  .adc1_buf0_monit_quants_persec_o 	( cntr_wbm_adc1_buf0_monit_quants_persec ),
  .adc1_buf0_monit_status_o		( cntr_wbm_adc1_buf0_monit_status ),
  .adc1_buf1_data_o	( cntr_wbm_adc1_buf1_data  ),
  .adc1_buf1_ready_o	( cntr_wbm_adc1_buf1_ready ),
  .adc1_buf1_read_i	( wbm_cntr_adc1_buf1_read  ),
  .adc1_buf1_monit_seconds_o		( cntr_wbm_adc1_buf1_monit_seconds ),
  .adc1_buf1_monit_quants_o		( cntr_wbm_adc1_buf1_monit_quants ),
  .adc1_buf1_monit_quants_persec_o	( cntr_wbm_adc1_buf1_monit_quants_persec ),
  .adc1_buf1_monit_status_o		( cntr_wbm_adc1_buf1_monit_status ),

  .adc2_buf0_data_o	( cntr_wbm_adc2_buf0_data  ),
  .adc2_buf0_ready_o	( cntr_wbm_adc2_buf0_ready ),
  .adc2_buf0_read_i	( wbm_cntr_adc2_buf0_read  ),
  .adc2_buf0_monit_seconds_o		( cntr_wbm_adc2_buf0_monit_seconds ),
  .adc2_buf0_monit_quants_o		( cntr_wbm_adc2_buf0_monit_quants ),
  .adc2_buf0_monit_quants_persec_o	( cntr_wbm_adc2_buf0_monit_quants_persec ),
  .adc2_buf0_monit_status_o		( cntr_wbm_adc2_buf0_monit_status ),
  .adc2_buf1_data_o	( cntr_wbm_adc2_buf1_data  ),
  .adc2_buf1_ready_o	( cntr_wbm_adc2_buf1_ready ),
  .adc2_buf1_read_i	( wbm_cntr_adc2_buf1_read  ),
  .adc2_buf1_monit_seconds_o		( cntr_wbm_adc2_buf1_monit_seconds ),
  .adc2_buf1_monit_quants_o		( cntr_wbm_adc2_buf1_monit_quants ),
  .adc2_buf1_monit_quants_persec_o	( cntr_wbm_adc2_buf1_monit_quants_persec ),
  .adc2_buf1_monit_status_o		( cntr_wbm_adc2_buf1_monit_status ),

  .adc3_buf0_data_o	( cntr_wbm_adc3_buf0_data  ),
  .adc3_buf0_ready_o	( cntr_wbm_adc3_buf0_ready ),
  .adc3_buf0_read_i	( wbm_cntr_adc3_buf0_read  ),
  .adc3_buf0_monit_seconds_o		( cntr_wbm_adc3_buf0_monit_seconds ),
  .adc3_buf0_monit_quants_o		( cntr_wbm_adc3_buf0_monit_quants ),
  .adc3_buf0_monit_quants_persec_o	( cntr_wbm_adc3_buf0_monit_quants_persec ),
  .adc3_buf0_monit_status_o		( cntr_wbm_adc3_buf0_monit_status ),
  .adc3_buf1_data_o	( cntr_wbm_adc3_buf1_data  ),
  .adc3_buf1_ready_o	( cntr_wbm_adc3_buf1_ready ),
  .adc3_buf1_read_i	( wbm_cntr_adc3_buf1_read  ),
  .adc3_buf1_monit_seconds_o		( cntr_wbm_adc3_buf1_monit_seconds ),
  .adc3_buf1_monit_quants_o		( cntr_wbm_adc3_buf1_monit_quants ),
  .adc3_buf1_monit_quants_persec_o	( cntr_wbm_adc3_buf1_monit_quants_persec ),
  .adc3_buf1_monit_status_o		( cntr_wbm_adc3_buf1_monit_status ),

  .adc4_buf0_data_o	( cntr_wbm_adc4_buf0_data  ),
  .adc4_buf0_ready_o	( cntr_wbm_adc4_buf0_ready ),
  .adc4_buf0_read_i	( wbm_cntr_adc4_buf0_read  ),
  .adc4_buf0_monit_seconds_o		( cntr_wbm_adc4_buf0_monit_seconds ),
  .adc4_buf0_monit_quants_o		( cntr_wbm_adc4_buf0_monit_quants ),
  .adc4_buf0_monit_quants_persec_o	( cntr_wbm_adc4_buf0_monit_quants_persec ),
  .adc4_buf0_monit_status_o		( cntr_wbm_adc4_buf0_monit_status ),
  .adc4_buf1_data_o	( cntr_wbm_adc4_buf1_data  ),
  .adc4_buf1_ready_o	( cntr_wbm_adc4_buf1_ready ),
  .adc4_buf1_read_i	( wbm_cntr_adc4_buf1_read  ),
  .adc4_buf1_monit_seconds_o		( cntr_wbm_adc4_buf1_monit_seconds ),
  .adc4_buf1_monit_quants_o		( cntr_wbm_adc4_buf1_monit_quants ),
  .adc4_buf1_monit_quants_persec_o	( cntr_wbm_adc4_buf1_monit_quants_persec ),
  .adc4_buf1_monit_status_o		( cntr_wbm_adc4_buf1_monit_status ),

  .adc5_buf0_data_o     ( cntr_wbm_adc5_buf0_data  ),
  .adc5_buf0_ready_o    ( cntr_wbm_adc5_buf0_ready ),
  .adc5_buf0_read_i     ( wbm_cntr_adc5_buf0_read  ),
  .adc5_buf0_monit_seconds_o		( cntr_wbm_adc5_buf0_monit_seconds ),
  .adc5_buf0_monit_quants_o		( cntr_wbm_adc5_buf0_monit_quants ),
  .adc5_buf0_monit_quants_persec_o	( cntr_wbm_adc5_buf0_monit_quants_persec ),
  .adc5_buf0_monit_status_o		( cntr_wbm_adc5_buf0_monit_status ),
  .adc5_buf1_data_o     ( cntr_wbm_adc5_buf1_data  ),
  .adc5_buf1_ready_o    ( cntr_wbm_adc5_buf1_ready ),
  .adc5_buf1_read_i     ( wbm_cntr_adc5_buf1_read  ),
  .adc5_buf1_monit_seconds_o		( cntr_wbm_adc5_buf1_monit_seconds ),
  .adc5_buf1_monit_quants_o		( cntr_wbm_adc5_buf1_monit_quants ),
  .adc5_buf1_monit_quants_persec_o	( cntr_wbm_adc5_buf1_monit_quants_persec ),
  .adc5_buf1_monit_status_o		( cntr_wbm_adc5_buf1_monit_status ),

  .adc6_buf0_data_o     ( cntr_wbm_adc6_buf0_data  ),
  .adc6_buf0_ready_o    ( cntr_wbm_adc6_buf0_ready ),
  .adc6_buf0_read_i     ( wbm_cntr_adc6_buf0_read  ),
  .adc6_buf0_monit_seconds_o		( cntr_wbm_adc6_buf0_monit_seconds ),
  .adc6_buf0_monit_quants_o		( cntr_wbm_adc6_buf0_monit_quants ),
  .adc6_buf0_monit_quants_persec_o	( cntr_wbm_adc6_buf0_monit_quants_persec ),
  .adc6_buf0_monit_status_o		( cntr_wbm_adc6_buf0_monit_status ),
  .adc6_buf1_data_o     ( cntr_wbm_adc6_buf1_data  ),
  .adc6_buf1_ready_o    ( cntr_wbm_adc6_buf1_ready ),
  .adc6_buf1_read_i     ( wbm_cntr_adc6_buf1_read  ),
  .adc6_buf1_monit_seconds_o		( cntr_wbm_adc6_buf1_monit_seconds ),
  .adc6_buf1_monit_quants_o		( cntr_wbm_adc6_buf1_monit_quants ),
  .adc6_buf1_monit_quants_persec_o	( cntr_wbm_adc6_buf1_monit_quants_persec ),
  .adc6_buf1_monit_status_o		( cntr_wbm_adc6_buf1_monit_status ),

  .adc7_buf0_data_o     ( cntr_wbm_adc7_buf0_data  ),
  .adc7_buf0_ready_o    ( cntr_wbm_adc7_buf0_ready ),
  .adc7_buf0_read_i     ( wbm_cntr_adc7_buf0_read  ),
  .adc7_buf0_monit_seconds_o		( cntr_wbm_adc7_buf0_monit_seconds ),
  .adc7_buf0_monit_quants_o		( cntr_wbm_adc7_buf0_monit_quants ),
  .adc7_buf0_monit_quants_persec_o	( cntr_wbm_adc7_buf0_monit_quants_persec ),
  .adc7_buf0_monit_status_o		( cntr_wbm_adc7_buf0_monit_status ),
  .adc7_buf1_data_o     ( cntr_wbm_adc7_buf1_data  ),
  .adc7_buf1_ready_o    ( cntr_wbm_adc7_buf1_ready ),
  .adc7_buf1_read_i     ( wbm_cntr_adc7_buf1_read  ),
  .adc7_buf1_monit_seconds_o		( cntr_wbm_adc7_buf1_monit_seconds ),
  .adc7_buf1_monit_quants_o		( cntr_wbm_adc7_buf1_monit_quants ),
  .adc7_buf1_monit_quants_persec_o	( cntr_wbm_adc7_buf1_monit_quants_persec ),
  .adc7_buf1_monit_status_o		( cntr_wbm_adc7_buf1_monit_status ),

  .adc8_buf0_data_o     ( cntr_wbm_adc8_buf0_data  ),
  .adc8_buf0_ready_o    ( cntr_wbm_adc8_buf0_ready ),
  .adc8_buf0_read_i     ( wbm_cntr_adc8_buf0_read  ),
  .adc8_buf0_monit_seconds_o		( cntr_wbm_adc8_buf0_monit_seconds ),
  .adc8_buf0_monit_quants_o		( cntr_wbm_adc8_buf0_monit_quants ),
  .adc8_buf0_monit_quants_persec_o	( cntr_wbm_adc8_buf0_monit_quants_persec ),
  .adc8_buf0_monit_status_o		( cntr_wbm_adc8_buf0_monit_status ),
  .adc8_buf1_data_o     ( cntr_wbm_adc8_buf1_data  ),
  .adc8_buf1_ready_o    ( cntr_wbm_adc8_buf1_ready ),
  .adc8_buf1_read_i     ( wbm_cntr_adc8_buf1_read  ),
  .adc8_buf1_monit_seconds_o		( cntr_wbm_adc8_buf1_monit_seconds ),
  .adc8_buf1_monit_quants_o		( cntr_wbm_adc8_buf1_monit_quants ),
  .adc8_buf1_monit_quants_persec_o	( cntr_wbm_adc8_buf1_monit_quants_persec ),
  .adc8_buf1_monit_status_o		( cntr_wbm_adc8_buf1_monit_status ),

  .out_clk_i	( wbm_clk_i ),
//---> Status Register interface
  .sreg_adc1_pedestal_o		( cntr_wbs_adc1_pedestal ),
  .sreg_adc2_pedestal_o		( cntr_wbs_adc2_pedestal ),
  .sreg_adc3_pedestal_o		( cntr_wbs_adc3_pedestal ),
  .sreg_adc4_pedestal_o		( cntr_wbs_adc4_pedestal ),
  .sreg_adc5_pedestal_o		( cntr_wbs_adc5_pedestal ),
  .sreg_adc6_pedestal_o		( cntr_wbs_adc6_pedestal ),
  .sreg_adc7_pedestal_o		( cntr_wbs_adc7_pedestal ),
  .sreg_adc8_pedestal_o		( cntr_wbs_adc8_pedestal ),

//---> Control Register interface
  .creg_adc1_avg_waveform_i	( wbs_cntr_adc1_average[2:0] ),
  .creg_adc1_avg_trigger_i	( wbs_cntr_adc1_average[5:3] ),
  .creg_adc1_avg_pedestal_i	( wbs_cntr_adc1_average[8:6] ),
  .creg_adc1_trig_hig_i		( wbs_cntr_adc1_trig_hig ),
  .creg_adc1_trig_low_i		( wbs_cntr_adc1_trig_low ),
  .creg_adc1_trig_ext_i		( wbs_cntr_adc1_trig_ext ),

  .creg_adc2_avg_waveform_i	( wbs_cntr_adc2_average[2:0] ),
  .creg_adc2_avg_trigger_i	( wbs_cntr_adc2_average[5:3] ),
  .creg_adc2_avg_pedestal_i	( wbs_cntr_adc2_average[8:6] ),
  .creg_adc2_trig_hig_i		( wbs_cntr_adc2_trig_hig ),
  .creg_adc2_trig_low_i		( wbs_cntr_adc2_trig_low ),
  .creg_adc2_trig_ext_i		( wbs_cntr_adc2_trig_ext ),

  .creg_adc3_avg_waveform_i	( wbs_cntr_adc3_average[2:0] ),
  .creg_adc3_avg_trigger_i	( wbs_cntr_adc3_average[5:3] ),
  .creg_adc3_avg_pedestal_i	( wbs_cntr_adc3_average[8:6] ),
  .creg_adc3_trig_hig_i		( wbs_cntr_adc3_trig_hig ),
  .creg_adc3_trig_low_i		( wbs_cntr_adc3_trig_low ),
  .creg_adc3_trig_ext_i		( wbs_cntr_adc3_trig_ext ),

  .creg_adc4_avg_waveform_i	( wbs_cntr_adc4_average[2:0] ),
  .creg_adc4_avg_trigger_i	( wbs_cntr_adc4_average[5:3] ),
  .creg_adc4_avg_pedestal_i	( wbs_cntr_adc4_average[8:6] ),
  .creg_adc4_trig_hig_i		( wbs_cntr_adc4_trig_hig ),
  .creg_adc4_trig_low_i		( wbs_cntr_adc4_trig_low ),
  .creg_adc4_trig_ext_i		( wbs_cntr_adc4_trig_ext ),

  .creg_adc5_avg_waveform_i	( wbs_cntr_adc5_average[2:0] ),
  .creg_adc5_avg_trigger_i	( wbs_cntr_adc5_average[5:3] ),
  .creg_adc5_avg_pedestal_i	( wbs_cntr_adc5_average[8:6] ),
  .creg_adc5_trig_hig_i		( wbs_cntr_adc5_trig_hig ),
  .creg_adc5_trig_low_i		( wbs_cntr_adc5_trig_low ),
  .creg_adc5_trig_ext_i		( wbs_cntr_adc5_trig_ext ),

  .creg_adc6_avg_waveform_i	( wbs_cntr_adc6_average[2:0] ),
  .creg_adc6_avg_trigger_i	( wbs_cntr_adc6_average[5:3] ),
  .creg_adc6_avg_pedestal_i	( wbs_cntr_adc6_average[8:6] ),
  .creg_adc6_trig_hig_i		( wbs_cntr_adc6_trig_hig ),
  .creg_adc6_trig_low_i		( wbs_cntr_adc6_trig_low ),
  .creg_adc6_trig_ext_i		( wbs_cntr_adc6_trig_ext ),

  .creg_adc7_avg_waveform_i	( wbs_cntr_adc7_average[2:0] ),
  .creg_adc7_avg_trigger_i	( wbs_cntr_adc7_average[5:3] ),
  .creg_adc7_avg_pedestal_i	( wbs_cntr_adc7_average[8:6] ),
  .creg_adc7_trig_hig_i		( wbs_cntr_adc7_trig_hig ),
  .creg_adc7_trig_low_i		( wbs_cntr_adc7_trig_low ),
  .creg_adc7_trig_ext_i		( wbs_cntr_adc7_trig_ext ),

  .creg_adc8_avg_waveform_i	( wbs_cntr_adc8_average[2:0] ),
  .creg_adc8_avg_trigger_i	( wbs_cntr_adc8_average[5:3] ),
  .creg_adc8_avg_pedestal_i	( wbs_cntr_adc8_average[8:6] ),
  .creg_adc8_trig_hig_i		( wbs_cntr_adc8_trig_hig ),
  .creg_adc8_trig_low_i		( wbs_cntr_adc8_trig_low ),
  .creg_adc8_trig_ext_i		( wbs_cntr_adc8_trig_ext )
);
//-------------------------------------------------------------------//
/////////////////////////////
// Time Managing Module   //
///////////////////////////

adc_lvds_timestamp # (
	.OUT_DATA_WIDTH(MDW)
) timestamp (
  .pulse_per_second_i 	( time_pulse_i ),	// 1pps input, asynchronous
//---> The input clock is our regenerated data clock
  .seconds_counter_o	( tms_cntr_seconds ), // How many seconds passed since creg_reset_seconds_i
  .quantum_per_sec_o 	( tms_cntr_quant_per_sec ),	// How many quantums was in 1sec for prev second
  .quantum_counter_o	( tms_cntr_quant_counter ),	// Current Cuantum value

  .sample_clock_i	( phy_cntr_adc_clk ), // This is our incoming data Clock
//---> Control Registers from wishbone slave
  // This resets seconds counter to 0.
  .creg_reset_seconds_i	( wbs_tms_seconds_reset )
);
//-------------------------------------------------------------------//
endmodule
