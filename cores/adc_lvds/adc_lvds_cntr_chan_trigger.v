module adc_lvds_cntr_chan_trigger
#(parameter	INP_DATA_WIDTH	= 28 // This is an incoming Data Width
)(
//---> Input Data Path
  input wire  [INP_DATA_WIDTH-1:0]	trigg_data_i,
  input wire  [INP_DATA_WIDTH-1:0]	pdest_data_i,
  input wire				inp_ready_i,
  input wire 				clk_i,
//--> Control Register Inputs for threshold and external trigger
  input wire  [INP_DATA_WIDTH-1:0]	creg_trig_hig_i,
  input wire  [INP_DATA_WIDTH-1:0]	creg_trig_low_i,
  input wire				creg_trig_ext_i,

//--> Trigger Output
  output wire				trigg_o
);
///////////////////////////////////////////////////////
// Synchronize External trigger to the rising edge  //
/////////////////////////////////////////////////////
  reg [1:0]  ext_trig_sreg = 2'b00;
  wire external_trigger;
  always@(posedge clk_i) begin
	ext_trig_sreg <= {ext_trig_sreg[0], creg_trig_ext_i};
  end
  assign external_trigger = (ext_trig_sreg == 2'b01);

////////////////////////////////////////////////////
//  High Trigger                                 //
//////////////////////////////////////////////////
  reg hig_trig_reg = 1'b0;
  always@(posedge clk_i) begin
	if ( trigg_data_i > (pdest_data_i + creg_trig_hig_i)) begin
		hig_trig_reg <= 1'b1;
	end else begin
		hig_trig_reg <= 1'b0;
	end
  end

/////////////////////////////////////////////////
// Low Trigger                                //
///////////////////////////////////////////////
  reg low_trig_reg = 1'b0;
  always@(posedge clk_i) begin
  	if ( pdest_data_i > (trigg_data_i + creg_trig_low_i) ) begin
		low_trig_reg <= 1'b1;
	end else begin
		low_trig_reg <= 1'b0;
	end
  end

////////////////////////////////////////////////
// The output is the combined trigger        //
//////////////////////////////////////////////
  assign trigg_o = external_trigger | hig_trig_reg | low_trig_reg;

endmodule
