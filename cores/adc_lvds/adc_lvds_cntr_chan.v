module adc_lvds_cntr_chan
#(parameter	ADC_BIT_WIDTH   = 12,	// Data Width of ADC
		OUT_DATA_WIDTH	= 32,	// Circulating Buffers output width
		OUT_ADDR_WIDTH	= 10	// Addresable width of Circulating buffers
)(
//---> Data Input Path
  input wire  [ADC_BIT_WIDTH-1:0]	data_i,
  input wire 				inp_clk_i,

//---> Input Clock Statistcs
  input wire [OUT_DATA_WIDTH-1:0]	mon_sec_i,
  input wire [OUT_DATA_WIDTH-1:0]	mon_qnt_i,
  input wire [OUT_DATA_WIDTH-1:0]	mon_qps_i,

//---> Data Output Path
  output wire [OUT_DATA_WIDTH-1:0]	buf0_data_o,
  output wire				buf0_ready_o, // Ready for readout
  input  wire				buf0_read_i,
  output wire [OUT_DATA_WIDTH-1:0]	buf0_mon_sec_o,
  output wire [OUT_DATA_WIDTH-1:0]	buf0_mon_qnt_o,
  output wire [OUT_DATA_WIDTH-1:0]	buf0_mon_qps_o,
  output wire [OUT_DATA_WIDTH-1:0]	buf0_mon_stat_o,

  output wire [OUT_DATA_WIDTH-1:0]	buf1_data_o,
  output wire				buf1_ready_o, // Ready for readout
  input  wire				buf1_read_i,
  output wire [OUT_DATA_WIDTH-1:0]	buf1_mon_sec_o,
  output wire [OUT_DATA_WIDTH-1:0]	buf1_mon_qnt_o,
  output wire [OUT_DATA_WIDTH-1:0]	buf1_mon_qps_o,
  output wire [OUT_DATA_WIDTH-1:0]	buf1_mon_stat_o,

  input wire 				out_clk_i,
//---> Status registers output
  output wire [27:0]	sreg_pedestal_o,

//---> Registers Interface input, exposed to WBSlave
  // Averaging control registers
  input wire [2:0]	creg_avg_waveform,
  input wire [2:0] 	creg_avg_trigger,
  input wire [2:0] 	creg_avg_pedestal,
  // Value for triggering
  input wire [27:0]	creg_trig_low_i,
  input wire [27:0]	creg_trig_hig_i,
  input wire		creg_trig_ext_i
);
////////////////////////////////////////////////////////
// All the logic is operating in inp_clk_i  domain   //
//////////////////////////////////////////////////////
//=== Register should be synchronized to inp_clk_i domain
  reg [2:0] creg_sync_avg_waveform = 3'b000;
  reg [2:0] creg_sync_avg_trigger  = 3'b000;
  reg [2:0] creg_sync_avg_pedestal = 3'b000;

  always@(posedge inp_clk_i) begin
	creg_sync_avg_waveform <= creg_avg_waveform;
	creg_sync_avg_trigger  <= creg_avg_trigger;
	creg_sync_avg_pedestal <= creg_avg_pedestal;
  end
//=== The Data is distributed into the 3 input average registers
/*
 *                                        Exposed to 
 *                                        monitoring
 *                                        registers
 *                                            /\
 *                                            ||                                                  
 *         -----------    -----------    -----------
 * [data]=>| AVGWFM  |===>| AVGTRIG |===>| AVGPED  |
 *         -----------    -----------    -----------
 *              U             U  ------------  U
 *    --------  U  --------   \=>| TRIGGER  |<=/
 *    | CIRC |<=Y=>| CIRC |      ------------
 *    | BUF0 |     | BUF1 |         |    |
 *    |      |<----|---<--|<----<---/    \---<--<  Ext
 *    --------     --------                        Trigger
 *       U            U
 *       Y            Y
 *
 *     To Wishbone Master 
 *          Interface
 *
 */
wire trace_is_triggered;

////////////////////////
//  Average Waveform //
//////////////////////
wire [ADC_BIT_WIDTH+8-1:0] 	avg_wfm_data;
wire				avg_wfm_data_ready;
adc_lvds_cntr_chan_avgwfm #(
	.INP_DATA_WIDTH(ADC_BIT_WIDTH),
	.AVG_DATA_INCR(8)
) avg_waveform (
  .data_i	(data_i),
  .data_o	(avg_wfm_data),
  .ready_o	(avg_wfm_data_ready),

  .avg_value_i	(creg_sync_avg_waveform),
  .clk_i	(inp_clk_i)
);
/////////////////////////
//  Average Trigger   //
///////////////////////
wire [ADC_BIT_WIDTH+8+8-1:0]	avg_trig_data; // 27 to 0
wire				avg_trig_data_ready;
adc_lvds_cntr_chan_avgtrig #(
	.INP_DATA_WIDTH(ADC_BIT_WIDTH + 8),
	.AVG_DATA_INCR(8)
) avg_trigger (
  .data_i	(avg_wfm_data),
  .inp_ready_i	(avg_wfm_data_ready),
  .data_o	(avg_trig_data),
  .out_ready_o	(avg_trig_data_ready),

  .avg_value_i	(creg_sync_avg_trigger),
  .clk_i	(inp_clk_i)
);
////////////////////////
// Average Pedestal  //
//////////////////////
wire [ADC_BIT_WIDTH+8+8-1:0]	avg_pedest_data;
adc_lvds_cntr_chan_avgped #(
	.INP_DATA_WIDTH(ADC_BIT_WIDTH + 8 + 8),
	.AVG_DATA_INCR(8)
) avg_pedestal (
  .data_i	(avg_trig_data),
  .inp_ready_i	(avg_trig_data_ready),
  .data_o	(avg_pedest_data),

  .avg_value_i	(creg_sync_avg_pedestal),
  .clk_i	(inp_clk_i)
);

assign sreg_pedestal_o = avg_pedest_data;

/////////////////////////////
// Trigger Block itself   //
///////////////////////////
adc_lvds_cntr_chan_trigger #(
	.INP_DATA_WIDTH(ADC_BIT_WIDTH + 8 + 8)
) trigger (
//---> Input Data Path
  .trigg_data_i ( avg_trig_data ),
  .pdest_data_i ( avg_pedest_data ),
  .inp_ready_i	( avg_trig_data_ready ),
  .clk_i	( inp_clk_i ),
//--> Control Register Inputs for threshold and external trigger
//	(they should be synchronized inside the module)
  .creg_trig_hig_i	( creg_trig_hig_i ),
  .creg_trig_low_i	( creg_trig_low_i ),
  .creg_trig_ext_i	( creg_trig_ext_i ),
//--> Trigger Output
  .trigg_o		( trace_is_triggered )
  
);

///////////////////////////////
// Two Circulating Buffers  //
/////////////////////////////
wire buf0_inp_ready;
wire buf1_inp_ready;
// The logic that selects the buffer (for trigger and write enable)
// goes here too
reg current_buffer_reg = 1'b0; // the default is buffer 0
always @(posedge inp_clk_i) begin
  // We should change the buffer only if the other buffer is ready
  if (current_buffer_reg == 1'b0) begin
	// Current buffer is 0, let's assign 1 if if it's not ready
	current_buffer_reg <= !buf0_inp_ready; // should be 1 if buffer 0 is busy
  end else begin
	// Current buffer is 1, 
	current_buffer_reg <= buf1_inp_ready; // should stay 1 if the buffer is ready
  end
end

adc_lvds_cntr_chan_circbuf #(
	.DATA_WIDTH (32)
) circ_buf0 (
//---> Input Data Path
  .inp_data_i 	(avg_wfm_data),
  .inp_wren_i 	(avg_wfm_data_ready&(current_buffer_reg == 1'b0)),
  .inp_ready_o	(buf0_inp_ready ),
  .inp_clk_i	(inp_clk_i),

  .inp_trig_i	(trace_is_triggered & (current_buffer_reg == 1'b0) ),

//---> Inputs for Monitoring Signals
  .inp_mon_sec_i	( mon_sec_i ),
  .inp_mon_qnt_i	( mon_qnt_i ),
  .inp_mon_qps_i	( mon_qps_i ),
  .inp_mon_stat_i	( {
	16'h0000,
  	4'b0000,
  	1'b0, creg_avg_pedestal,    // 4bits
	1'b0, creg_avg_trigger,	    // 4bits
	1'b0, creg_avg_waveform} ), // 4bits

//---> Output Data Path
  .out_data_o	(buf0_data_o),
  .out_rden_i	(buf0_read_i),
  .out_ready_o	(buf0_ready_o),
  .out_clk_i	(out_clk_i),

//---> Outputs for Monitoring Signals
  .out_mon_sec_o	( buf0_mon_sec_o ),
  .out_mon_qnt_o        ( buf0_mon_qnt_o ),
  .out_mon_qps_o        ( buf0_mon_qps_o ),
  .out_mon_stat_o	( buf0_mon_stat_o )
);

adc_lvds_cntr_chan_circbuf #(
	.DATA_WIDTH (32)
) circ_buf1 (
//---> Input Data Path
  .inp_data_i   (avg_wfm_data),
  .inp_wren_i   (avg_wfm_data_ready&(current_buffer_reg == 1'b1)),
  .inp_ready_o  (buf1_inp_ready ),
  .inp_clk_i    (inp_clk_i),

  .inp_trig_i	(trace_is_triggered & (current_buffer_reg == 1'b1) ),

//---> Inputs for Monitoring Signals
  .inp_mon_sec_i	( mon_sec_i ),
  .inp_mon_qnt_i	( mon_qnt_i ),
  .inp_mon_qps_i	( mon_qps_i ),
  .inp_mon_stat_i	( {     
  	16'h0000,
	4'b0000,
        1'b0, creg_avg_pedestal,    // 4bits
        1'b0, creg_avg_trigger,     // 4bits
        1'b0, creg_avg_waveform} ), // 4bits
//---> Output Data Path
  .out_data_o   (buf1_data_o),
  .out_rden_i   (buf1_read_i),
  .out_ready_o  (buf1_ready_o),
  .out_clk_i    (out_clk_i),
//---> Outputs for Monitoring Signals
  .out_mon_sec_o	( buf1_mon_sec_o ),
  .out_mon_qnt_o	( buf1_mon_qnt_o ),
  .out_mon_qps_o	( buf1_mon_qps_o ),
  .out_mon_stat_o	( buf1_mon_stat_o )
);
//===================================================================//
endmodule
