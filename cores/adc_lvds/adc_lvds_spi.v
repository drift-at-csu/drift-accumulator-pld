module adc_lvds_spi
#(parameter	DATA_WIDTH	= 16,  // This is an incoming Data Width
		ADDR_WIDTH	= 16 // This is an incoming address Width
)(
//---> Input Control lines
  input wire				transfer_i,
  input wire	[DATA_WIDTH-1:0]	data_i,
  input wire	[ADDR_WIDTH-1:0]	addr_i,
//---> Output Status Lines
  output reg	[DATA_WIDTH-1:0]	data_o,
  output reg				ready_o = 1'b1,
//---> SPI Physical Connections to pads
  output reg				spi_cs_o = 1'b1,
  inout  wire				spi_dat_io,
  output wire				spi_clk_o,
//---> Clock input (Wishbone Slave)
  input wire 				clk_i,
  input wire				rst_i
);
  reg spi_clk_o_reg = 1'b0;
  assign spi_clk_o = spi_cs_o ? 1'bZ : spi_clk_o_reg;

  reg spi_data_direction_reg = 1'b0; // 0 for reading, 1 for writing
  wire spi_data_out;

  assign spi_dat_io = spi_data_direction_reg ? spi_data_out : 1'bZ;

  // Let's create 1 pulse for incoming transfer control line
  reg [1:0] transfer_sreg = 2'b00;
  wire start_transfer;
  always@(posedge clk_i) begin
  	transfer_sreg <= {transfer_sreg[0], transfer_i};
  end
  assign start_transfer = (transfer_sreg == 2'b01);

  // Small State Machine for clarity of the process
  reg [7:0] bit_address_reg;
  reg [31:0] bit_transfer_sreg = 32'hDEAD_BEEF;
  (* FSM_ENCODING="SEQUENTIAL", SAFE_IMPLEMENTATION="NO" *) reg [1:0] spi_fsm_state = 2'b00;
  always@(posedge clk_i)
  if (rst_i) begin
  	spi_fsm_state <= 2'b00;
	bit_address_reg <= 8'h00;
	spi_data_direction_reg <= 1'b0;
	spi_cs_o <= 1'b1;
  end else
  case (spi_fsm_state)
  2'b00 : begin // Idle
	spi_fsm_state <= start_transfer ? 2'b10 : 2'b00;
	ready_o <= !start_transfer;
	bit_address_reg <= 8'h00;
	spi_cs_o <= 1'b1;
	if (start_transfer) begin
		bit_transfer_sreg <= {addr_i[15], 2'b01, addr_i[12:0], data_i};
		spi_data_direction_reg <= 1'b1;
	end 
  end
  2'b01 : begin // before SPI Clock rising
  	spi_cs_o <= 1'b0;
	spi_fsm_state <= 2'b10;
	spi_clk_o_reg <= 1'b1;
	bit_address_reg <= bit_address_reg + 1'b1;
	// Let's shift in the data
	data_o <= {data_o[DATA_WIDTH-2:0], spi_dat_io};
  end
  2'b10 : begin // after the SPI Clock rising
	spi_fsm_state <= (bit_address_reg == 8'd32) ? 2'b11 : 2'b01;
	if ((addr_i[15])&(bit_address_reg == 8'd16)) begin
		// If we are reading, we want to reverse the direction
		spi_data_direction_reg <= 1'b0;
	end
	bit_transfer_sreg <= {bit_transfer_sreg[30:0],bit_transfer_sreg[31]};
	spi_clk_o_reg <= 1'b0;
	spi_cs_o <= 1'b0;
  end
  2'b11 : begin // Finalize
  	spi_fsm_state <= 2'b00;
	spi_cs_o <= 1'b0;
	spi_data_direction_reg <= 1'b0;
	ready_o <= 1'b1;
  end
  endcase

  assign spi_data_out = bit_transfer_sreg[0];

endmodule
