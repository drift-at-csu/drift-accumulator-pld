module adc_lvds_cntr_chan_avgped
#(parameter	INP_DATA_WIDTH	= 28, // This is an incoming Data Width
		AVG_DATA_INCR	= 8  // This is an increment to the Data Width
)(
//---> Input Data Path
  input wire  [INP_DATA_WIDTH-1:0]		data_i,
  input wire					inp_ready_i,
//---> Output Data Path
  output reg  [INP_DATA_WIDTH-1:0]		data_o,
//---> Input Registers For averaging
  input wire [2:0]	avg_value_i,	 
//---> Clock input
  input wire clk_i
);
  // Logical Shifter:
  reg [7:0] avg_value_reg;
  always @* begin
	case (avg_value_i)
		3'b000 : avg_value_reg = avg_value_i;
		3'b001 : avg_value_reg = (1 << avg_value_i) - 1;
		3'b010 : avg_value_reg = (1 << avg_value_i) - 1;
		3'b011 : avg_value_reg = (1 << avg_value_i) - 1;
		3'b100 : avg_value_reg = (1 << avg_value_i) - 1;
		3'b101 : avg_value_reg = (1 << avg_value_i) - 1;
		3'b110 : avg_value_reg = (1 << avg_value_i) - 1;
		default: avg_value_reg = (1 << avg_value_i) - 1;
	endcase
  end

  // Down Counter
  reg [AVG_DATA_INCR-1:0] avg_bin_counter_reg = 0;
  assign ready_o = (avg_bin_counter_reg == 0);
  always@(posedge clk_i) begin
  	if (inp_ready_i) begin
		if (ready_o) begin
			avg_bin_counter_reg <= avg_value_reg;
		end else begin
			avg_bin_counter_reg <= avg_bin_counter_reg - 1'b1;
		end
	end
  end

  // Summing register
  reg [INP_DATA_WIDTH+AVG_DATA_INCR-1:0] summing_reg = 0;
  always@(posedge clk_i) begin
  	if (inp_ready_i) begin
  		if (ready_o) begin
			summing_reg <= data_i;
		end else begin
			summing_reg <= summing_reg + data_i;
		end
	end
  end

  // Synchronize output to the Summing register
  always@(posedge clk_i) begin
  	if (inp_ready_i) begin
  		if (ready_o) begin
			case (avg_value_i)
				3'b000 : data_o <= summing_reg;
				3'b001 : data_o <= summing_reg >> avg_value_i;
				3'b010 : data_o <= summing_reg >> avg_value_i;
				3'b011 : data_o <= summing_reg >> avg_value_i;
				3'b100 : data_o <= summing_reg >> avg_value_i;
				3'b101 : data_o <= summing_reg >> avg_value_i;
				3'b110 : data_o <= summing_reg >> avg_value_i;
				default: data_o <= summing_reg >> avg_value_i;
			endcase
		end
	end
  end

endmodule
