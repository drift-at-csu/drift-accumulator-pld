module adc_lvds_phy
#(parameter	ADC_BIT_WIDTH	= 12, // Data Width of ADC
		IDELAY_NAME	= "adcX_delay_group",
		D1_IDELAY	= 15,
		D2_IDELAY	= 15,
		D3_IDELAY	= 15,
		D4_IDELAY	= 15,
		D5_IDELAY	= 15,
		D6_IDELAY	= 15,
		D7_IDELAY	= 15,
		D8_IDELAY	= 15,
		FC_IDELAY	= 15, // Frame Clock I-Delay value
		DC_IDELAY	= 15  // Data Clock I-Delay value
)(
//---> The Parallel ADC data output
  // These wires are in adcs_clk domain
  output wire [ADC_BIT_WIDTH-1:0]	adc1_data_o,
  output wire [ADC_BIT_WIDTH-1:0]	adc2_data_o,
  output wire [ADC_BIT_WIDTH-1:0]	adc3_data_o,
  output wire [ADC_BIT_WIDTH-1:0]	adc4_data_o,
  output wire [ADC_BIT_WIDTH-1:0]	adc5_data_o,
  output wire [ADC_BIT_WIDTH-1:0]	adc6_data_o,
  output wire [ADC_BIT_WIDTH-1:0]	adc7_data_o,
  output wire [ADC_BIT_WIDTH-1:0]	adc8_data_o,

  output wire	adcs_data_clk_o, // (data sample clock)
//---> Control Registers inputs
  input wire	creg_bitslip_i,
  input wire	creg_reset_i,
  input wire	creg_delay_inc_i,
//---> Status registers output
  output reg [ADC_BIT_WIDTH-1:0]	sreg_fco_o = 'hEAD,
  output reg [ADC_BIT_WIDTH-1:0]	sreg_dco_o = 'hDEA,
//---> Connection to the ADC Chip that should go directly to the pads
  input wire [8:1] data_i_p, // Data from ADC Chip
  input wire [8:1] data_i_n,
  
  input wire frame_clk_i_p, // Slow Clock for data
  input wire frame_clk_i_n,
  
  input wire data_clk_i_p, // Fast clock
  input wire data_clk_i_n,

//---> Sample Clocks
  output wire sample_clk_o_p, // Sample Clocks ( Directly to the pad )
  output wire sample_clk_o_n,

  input wire adc_clk_i
);

wire clk_fast;
wire clk_slow;

///////////////////////////////////////////////////////
// Finetuning of the clock delay is happening here  //
/////////////////////////////////////////////////////
  reg [1:0] creg_delay_inc_sync_reg = 2'b00;
  wire all_data_delay_increment;
  always@(negedge clk_slow) begin
	creg_delay_inc_sync_reg <= {creg_delay_inc_sync_reg[0], creg_delay_inc_i};
  end
  assign all_data_delay_increment = ( creg_delay_inc_sync_reg == 2'b01 );
//////////////////////////////////////////////////
// Synchronization of input control registers  //
////////////////////////////////////////////////
  reg [1:0] creg_bitslip_sync_reg = 2'b00;
  wire all_data_bitslip;
  always@(negedge clk_slow) begin
	creg_bitslip_sync_reg <= {creg_bitslip_sync_reg[0], creg_bitslip_i};
  end
  assign all_data_bitslip = ( creg_bitslip_sync_reg == 2'b01);

///////////////////////////////////////////////////////
//  Reset is mandatory for multi-bit Structure      //
/////////////////////////////////////////////////////
  reg [1:0] creg_reset_sync_reg = 2'b00;
  wire all_data_slow_reset;
  always@(negedge clk_slow) begin
  	creg_reset_sync_reg <=  {creg_reset_sync_reg[0], creg_reset_i};
  end
  reg creg_reset_sync_delayed_reg = 1'b0;
  always@(negedge clk_slow) begin
  	creg_reset_sync_delayed_reg <= ( creg_reset_sync_reg == 2'b01);
  end
  assign all_data_slow_reset = ( creg_reset_sync_reg == 2'b01) | creg_reset_sync_delayed_reg;

//////////////////////////////////////////////////////
// We want to directly propagate input adc_clk_i   //
// to the sample_clk_o, it's a free running clock //
///////////////////////////////////////////////////
OBUFDS #(
	.IOSTANDARD("DEFAULT"),	// Specify the output I/O standard
	.SLEW("SLOW")		// Specify the output slew rate
) sample_clk_buf (
	.O ( sample_clk_o_p),	// Diff_p output (connect directly to top-level port)
	.OB( sample_clk_o_n),	// Diff_n output (connect directly to top-level port)
	.I ( adc_clk_i )	// Buffer input 
);
/////////////////////
// Clock Channel  //
///////////////////
wire [ADC_BIT_WIDTH-1:0] mon_data_clk_o;

adc_lvds_phy_clk_chan #(
	.IDELAY_VALUE(DC_IDELAY),
	.IDELAY_NAME(IDELAY_NAME)
) data_clock (
  .data_i_p	 (data_clk_i_p),
  .data_i_n	 (data_clk_i_n),

  .data_o	 (mon_data_clk_o),
  .data_clk_o	 (clk_fast),
  .data_div_clk_o(clk_slow)
);
// Synchronization of monitoring clock pattern
always@(posedge adcs_data_clk_o) begin
  sreg_dco_o <= mon_data_clk_o;
end
//////////////////////////////
// Frame Clock Channel     //
////////////////////////////
wire [ADC_BIT_WIDTH-1:0] mon_frame_clk_o;
adc_lvds_phy_data_chan #(
	.IDELAY_VALUE(FC_IDELAY),
	.IDELAY_NAME(IDELAY_NAME)
) frame_clock (
  .data_i_p	 ( frame_clk_i_p ),
  .data_i_n	 ( frame_clk_i_n ),

  .data_o	 ( mon_frame_clk_o ),
  .data_clk_i	 ( clk_fast ),
  .data_div_clk_i( clk_slow ),

  .bitslip_i	 ( all_data_bitslip ),
  .slow_rst_i	 ( all_data_slow_reset ),
  .delay_inc_i	 ( all_data_delay_increment )
);
// let's sync mon_frame_clk_o to sreg_fco_o
always@(posedge adcs_data_clk_o) begin
  sreg_fco_o <= mon_frame_clk_o;
end


//////////////////////////////
// Data Channels           //
////////////////////////////
// ADC 1
adc_lvds_phy_data_chan #(
	.IDELAY_VALUE(D1_IDELAY),
        .IDELAY_NAME(IDELAY_NAME)
) adc1 (
  .data_i_p	 (data_i_p[1]),
  .data_i_n	 (data_i_n[1]),

  .data_o	 (adc1_data_o),
  .data_clk_i	 (clk_fast),
  .data_div_clk_i(clk_slow),

  .bitslip_i	 (all_data_bitslip),
  .slow_rst_i	 ( all_data_slow_reset ),
  .delay_inc_i   ( all_data_delay_increment )
);
// ADC 2
adc_lvds_phy_data_chan #(
	.IDELAY_VALUE(D2_IDELAY),
	.IDELAY_NAME(IDELAY_NAME)
) adc2 (
  .data_i_p	 (data_i_p[2]),
  .data_i_n	 (data_i_n[2]),

  .data_o	 (adc2_data_o),
  .data_clk_i	 (clk_fast),
  .data_div_clk_i(clk_slow),

  .bitslip_i	 (all_data_bitslip),
  .slow_rst_i	 ( all_data_slow_reset ),
  .delay_inc_i   ( all_data_delay_increment )
);
// ADC 3
adc_lvds_phy_data_chan #(
	.IDELAY_VALUE(D3_IDELAY),
	.IDELAY_NAME(IDELAY_NAME)
) adc3 (
  .data_i_p	 (data_i_p[3]),
  .data_i_n	 (data_i_n[3]),

  .data_o	 (adc3_data_o),
  .data_clk_i	 (clk_fast),
  .data_div_clk_i(clk_slow),

  .bitslip_i	 (all_data_bitslip),
  .slow_rst_i	 ( all_data_slow_reset ),
  .delay_inc_i   ( all_data_delay_increment )
);
// ADC 4
adc_lvds_phy_data_chan #(
	.IDELAY_VALUE(D4_IDELAY),
	.IDELAY_NAME(IDELAY_NAME)
) adc4 (
  .data_i_p	 (data_i_p[4]),
  .data_i_n	 (data_i_n[4]),

  .data_o	 (adc4_data_o),
  .data_clk_i	 (clk_fast),
  .data_div_clk_i(clk_slow),

  .bitslip_i	 (all_data_bitslip),
  .slow_rst_i	 ( all_data_slow_reset ),
  .delay_inc_i   ( all_data_delay_increment )
);
// ADC 5
adc_lvds_phy_data_chan #(
	.IDELAY_VALUE(D5_IDELAY),
	.IDELAY_NAME(IDELAY_NAME)
) adc5 (
  .data_i_p	 (data_i_p[5]),
  .data_i_n	 (data_i_n[5]),

  .data_o	 (adc5_data_o),
  .data_clk_i	 (clk_fast),
  .data_div_clk_i(clk_slow),

  .bitslip_i	 (all_data_bitslip),
  .slow_rst_i	 ( all_data_slow_reset ),
  .delay_inc_i   ( all_data_delay_increment )
);
// ADC 6
adc_lvds_phy_data_chan #(
	.IDELAY_VALUE(D6_IDELAY),
	.IDELAY_NAME(IDELAY_NAME)
) adc6 (
  .data_i_p	 (data_i_p[6]),
  .data_i_n	 (data_i_n[6]),

  .data_o	 (adc6_data_o),
  .data_clk_i	 (clk_fast),
  .data_div_clk_i(clk_slow),

  .bitslip_i	 (all_data_bitslip),
  .slow_rst_i	 ( all_data_slow_reset ),
  .delay_inc_i   ( all_data_delay_increment )
);
// ADC 7
adc_lvds_phy_data_chan #(
	.IDELAY_VALUE(D7_IDELAY),
	.IDELAY_NAME(IDELAY_NAME)
) adc7 (
  .data_i_p	 (data_i_p[7]),
  .data_i_n	 (data_i_n[7]),

  .data_o	 (adc7_data_o),
  .data_clk_i	 (clk_fast),
  .data_div_clk_i(clk_slow),

  .bitslip_i	 (all_data_bitslip),
  .slow_rst_i	 ( all_data_slow_reset ),
  .delay_inc_i   ( all_data_delay_increment )
);
// ADC 8
adc_lvds_phy_data_chan #(
	.IDELAY_VALUE(D8_IDELAY),
	.IDELAY_NAME(IDELAY_NAME)
) adc8 (
  .data_i_p	 (data_i_p[8]),
  .data_i_n	 (data_i_n[8]),

  .data_o	 (adc8_data_o),
  .data_clk_i	 (clk_fast),
  .data_div_clk_i(clk_slow),

  .bitslip_i	 (all_data_bitslip),
  .slow_rst_i	 ( all_data_slow_reset ),
  .delay_inc_i   ( all_data_delay_increment )
);

//////////////////////////////////////////////////////
// Last Thing: we want to generate the data clock  //
////////////////////////////////////////////////////
wire adcs_data_clk_o_inv;
BUFR #(
	.BUFR_DIVIDE("2"), // Values: "BYPASS, 1, 2, 3, 4, 5, 6, 7, 8" 
	.SIM_DEVICE("7SERIES")  // Must be set to "7SERIES" 
) output_clk_buf (
	.O    (adcs_data_clk_o_inv),// 1-bit output: Clock output port
	.CE   (1'b1),         	// 1-bit input: Active high, clock enable (Divided modes only)
	.CLR  (1'b0),         	// 1-bit input: Active high, asynchronous clear (Divided modes only)
	.I    (~clk_slow)	// 1-bit input: Clock buffer input
);

assign adcs_data_clk_o = ~adcs_data_clk_o_inv;
endmodule
