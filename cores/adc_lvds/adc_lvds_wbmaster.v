/*
 * The Wishbone Master will produce an ethernet packet ready
 * chunk of Memory , based on the control data coming out of the 
 * FIFO.
 *
 * It's operating in wb_clk_i clock domain.
 *
 */

module adc_lvds_wbmaster
#(parameter     DATA_WIDTH    =  32, 	// Data Width of wishbone bus
                ADDRESS_WIDTH  = 32, 	// Address Width of wishbone bus
		BURST_LENGTH = 9	// Currently means nothing
)(
//---> Master Wishbone connections
  output reg [ADDRESS_WIDTH-1:0]	wb_adr_o,
  output wire [1:0]			wb_bte_o,
  output wire [2:0]			wb_cti_o,
  output reg				wb_cyc_o = 1'b0,
  output wire [DATA_WIDTH-1:0]		wb_dat_o,
  output wire [(DATA_WIDTH/8)-1:0]	wb_sel_o,
  output reg				wb_stb_o = 1'b0,
  output wire				wb_we_o,
 
  input wire				wb_ack_i,
  input wire				wb_err_i,
  input wire				wb_rty_i,
  input wire [DATA_WIDTH-1:0]		wb_dat_i,

  input wire	wb_clk_i,
  input wire	wb_rst_i,

//---> Circulating Buffer Inputs
  // CHANNEL 1
  input wire [DATA_WIDTH-1:0]	chan1_buf0_data_i,
  input wire			chan1_buf0_ready_i,
  output wire			chan1_buf0_read_o,
  input wire [DATA_WIDTH-1:0]	chan1_buf0_mon_sec_i,
  input wire [DATA_WIDTH-1:0]	chan1_buf0_mon_qnt_i,
  input wire [DATA_WIDTH-1:0]	chan1_buf0_mon_qps_i,
  input wire [DATA_WIDTH-1:0]	chan1_buf0_mon_sts_i,
  input wire [DATA_WIDTH-1:0]	chan1_buf1_data_i,
  input wire			chan1_buf1_ready_i,
  output wire			chan1_buf1_read_o,
  input wire [DATA_WIDTH-1:0]	chan1_buf1_mon_sec_i,
  input wire [DATA_WIDTH-1:0]	chan1_buf1_mon_qnt_i,
  input wire [DATA_WIDTH-1:0]	chan1_buf1_mon_qps_i,
  input wire [DATA_WIDTH-1:0]	chan1_buf1_mon_sts_i,
  // CHANNEL 2
  input wire [DATA_WIDTH-1:0]	chan2_buf0_data_i,
  input wire			chan2_buf0_ready_i,
  output wire			chan2_buf0_read_o,
  input wire [DATA_WIDTH-1:0]	chan2_buf0_mon_sec_i,
  input wire [DATA_WIDTH-1:0]	chan2_buf0_mon_qnt_i,
  input wire [DATA_WIDTH-1:0]	chan2_buf0_mon_qps_i,
  input wire [DATA_WIDTH-1:0]	chan2_buf0_mon_sts_i,
  input wire [DATA_WIDTH-1:0]	chan2_buf1_data_i,
  input wire			chan2_buf1_ready_i,
  output wire			chan2_buf1_read_o,
  input wire [DATA_WIDTH-1:0]	chan2_buf1_mon_sec_i,
  input wire [DATA_WIDTH-1:0]	chan2_buf1_mon_qnt_i,
  input wire [DATA_WIDTH-1:0]	chan2_buf1_mon_qps_i,
  input wire [DATA_WIDTH-1:0]	chan2_buf1_mon_sts_i,
  // CHANNEL 3
  input wire [DATA_WIDTH-1:0]	chan3_buf0_data_i,
  input wire			chan3_buf0_ready_i,
  output wire			chan3_buf0_read_o,
  input wire [DATA_WIDTH-1:0]	chan3_buf0_mon_sec_i,
  input wire [DATA_WIDTH-1:0]	chan3_buf0_mon_qnt_i,
  input wire [DATA_WIDTH-1:0]	chan3_buf0_mon_qps_i,
  input wire [DATA_WIDTH-1:0]	chan3_buf0_mon_sts_i,
  input wire [DATA_WIDTH-1:0]	chan3_buf1_data_i,
  input wire			chan3_buf1_ready_i,
  output wire			chan3_buf1_read_o,
  input wire [DATA_WIDTH-1:0]	chan3_buf1_mon_sec_i,
  input wire [DATA_WIDTH-1:0]	chan3_buf1_mon_qnt_i,
  input wire [DATA_WIDTH-1:0]	chan3_buf1_mon_qps_i,
  input wire [DATA_WIDTH-1:0]	chan3_buf1_mon_sts_i,
  // CHANNEL 4
  input wire [DATA_WIDTH-1:0]	chan4_buf0_data_i,
  input wire			chan4_buf0_ready_i,
  output wire			chan4_buf0_read_o,
  input wire [DATA_WIDTH-1:0]	chan4_buf0_mon_sec_i,
  input wire [DATA_WIDTH-1:0]	chan4_buf0_mon_qnt_i,
  input wire [DATA_WIDTH-1:0]	chan4_buf0_mon_qps_i,
  input wire [DATA_WIDTH-1:0]	chan4_buf0_mon_sts_i,
  input wire [DATA_WIDTH-1:0]	chan4_buf1_data_i,
  input wire			chan4_buf1_ready_i,
  output wire			chan4_buf1_read_o,
  input wire [DATA_WIDTH-1:0]	chan4_buf1_mon_sec_i,
  input wire [DATA_WIDTH-1:0]	chan4_buf1_mon_qnt_i,
  input wire [DATA_WIDTH-1:0]	chan4_buf1_mon_qps_i,
  input wire [DATA_WIDTH-1:0]	chan4_buf1_mon_sts_i,
  // CHANNEL 5
  input wire [DATA_WIDTH-1:0]	chan5_buf0_data_i,
  input wire			chan5_buf0_ready_i,
  output wire			chan5_buf0_read_o,
  input wire [DATA_WIDTH-1:0]	chan5_buf0_mon_sec_i,
  input wire [DATA_WIDTH-1:0]	chan5_buf0_mon_qnt_i,
  input wire [DATA_WIDTH-1:0]	chan5_buf0_mon_qps_i,
  input wire [DATA_WIDTH-1:0]	chan5_buf0_mon_sts_i,
  input wire [DATA_WIDTH-1:0]	chan5_buf1_data_i,
  input wire			chan5_buf1_ready_i,
  output wire			chan5_buf1_read_o,
  input wire [DATA_WIDTH-1:0]   chan5_buf1_mon_sec_i,
  input wire [DATA_WIDTH-1:0]   chan5_buf1_mon_qnt_i,
  input wire [DATA_WIDTH-1:0]   chan5_buf1_mon_qps_i,
  input wire [DATA_WIDTH-1:0]   chan5_buf1_mon_sts_i,
  // CHANNEL 6
  input wire [DATA_WIDTH-1:0]	chan6_buf0_data_i,
  input wire			chan6_buf0_ready_i,
  output wire			chan6_buf0_read_o,
  input wire [DATA_WIDTH-1:0]	chan6_buf0_mon_sec_i,
  input wire [DATA_WIDTH-1:0]	chan6_buf0_mon_qnt_i,
  input wire [DATA_WIDTH-1:0]	chan6_buf0_mon_qps_i,
  input wire [DATA_WIDTH-1:0]	chan6_buf0_mon_sts_i,
  input wire [DATA_WIDTH-1:0]	chan6_buf1_data_i,
  input wire			chan6_buf1_ready_i,
  output wire			chan6_buf1_read_o,
  input wire [DATA_WIDTH-1:0]	chan6_buf1_mon_sec_i,
  input wire [DATA_WIDTH-1:0]	chan6_buf1_mon_qnt_i,
  input wire [DATA_WIDTH-1:0]	chan6_buf1_mon_qps_i,
  input wire [DATA_WIDTH-1:0]	chan6_buf1_mon_sts_i,
  // CHANNEL 7
  input wire [DATA_WIDTH-1:0]	chan7_buf0_data_i,
  input wire			chan7_buf0_ready_i,
  output wire			chan7_buf0_read_o,
  input wire [DATA_WIDTH-1:0]	chan7_buf0_mon_sec_i,
  input wire [DATA_WIDTH-1:0]	chan7_buf0_mon_qnt_i,
  input wire [DATA_WIDTH-1:0]	chan7_buf0_mon_qps_i,
  input wire [DATA_WIDTH-1:0]	chan7_buf0_mon_sts_i,
  input wire [DATA_WIDTH-1:0]	chan7_buf1_data_i,
  input wire			chan7_buf1_ready_i,
  output wire			chan7_buf1_read_o,
  input wire [DATA_WIDTH-1:0]	chan7_buf1_mon_sec_i,
  input wire [DATA_WIDTH-1:0]	chan7_buf1_mon_qnt_i,
  input wire [DATA_WIDTH-1:0]	chan7_buf1_mon_qps_i,
  input wire [DATA_WIDTH-1:0]	chan7_buf1_mon_sts_i,
  // CHANNEL 8
  input wire [DATA_WIDTH-1:0]	chan8_buf0_data_i,
  input wire			chan8_buf0_ready_i,
  output wire			chan8_buf0_read_o,
  input wire [DATA_WIDTH-1:0]	chan8_buf0_mon_sec_i,
  input wire [DATA_WIDTH-1:0]	chan8_buf0_mon_qnt_i,
  input wire [DATA_WIDTH-1:0]	chan8_buf0_mon_qps_i,
  input wire [DATA_WIDTH-1:0]	chan8_buf0_mon_sts_i,
  input wire [DATA_WIDTH-1:0]	chan8_buf1_data_i,
  input wire			chan8_buf1_ready_i,
  output wire			chan8_buf1_read_o,
  input wire [DATA_WIDTH-1:0]	chan8_buf1_mon_sec_i,
  input wire [DATA_WIDTH-1:0]	chan8_buf1_mon_qnt_i,
  input wire [DATA_WIDTH-1:0]	chan8_buf1_mon_qps_i,
  input wire [DATA_WIDTH-1:0]	chan8_buf1_mon_sts_i,
//---> Status  Registers outputs
  output reg	sreg_chan1_transfered_o = 1'b0,
  output reg	sreg_chan2_transfered_o = 1'b0,
  output reg	sreg_chan3_transfered_o = 1'b0,
  output reg	sreg_chan4_transfered_o = 1'b0,
  output reg	sreg_chan5_transfered_o = 1'b0,
  output reg	sreg_chan6_transfered_o = 1'b0,
  output reg	sreg_chan7_transfered_o = 1'b0,
  output reg	sreg_chan8_transfered_o = 1'b0,

//---> Control Registers input
  input wire [ADDRESS_WIDTH-1:0]	creg_chan1_baseaddr_i,
  input wire				creg_chan1_resume_i,
  input wire [ADDRESS_WIDTH-1:0]	creg_chan2_baseaddr_i,
  input wire				creg_chan2_resume_i,
  input wire [ADDRESS_WIDTH-1:0]	creg_chan3_baseaddr_i,
  input wire				creg_chan3_resume_i,
  input wire [ADDRESS_WIDTH-1:0]	creg_chan4_baseaddr_i,
  input wire				creg_chan4_resume_i,
  input wire [ADDRESS_WIDTH-1:0]	creg_chan5_baseaddr_i,
  input wire				creg_chan5_resume_i,
  input wire [ADDRESS_WIDTH-1:0]	creg_chan6_baseaddr_i,
  input wire				creg_chan6_resume_i,
  input wire [ADDRESS_WIDTH-1:0]	creg_chan7_baseaddr_i,
  input wire				creg_chan7_resume_i,
  input wire [ADDRESS_WIDTH-1:0]	creg_chan8_baseaddr_i,
  input wire				creg_chan8_resume_i
);
//===================================================================//
////////////////////////////////////////////////////
// Current Buffer and Current Channel registers  //
//////////////////////////////////////////////////
reg		current_buffer_reg  = 1'b0;
reg[2:0]	current_channel_reg = 3'b000;

/////////////////////////////////////////////////
// Select Inputs, based on current values     //
///////////////////////////////////////////////
wire [DATA_WIDTH-1:0]	chan1_data_i;
wire			chan1_ready_i;
wire [DATA_WIDTH-1:0]   chan1_mon_sec_i;
wire [DATA_WIDTH-1:0]   chan1_mon_qnt_i;
wire [DATA_WIDTH-1:0]   chan1_mon_qps_i;
wire [DATA_WIDTH-1:0]   chan1_mon_sts_i;
assign chan1_data_i    = current_buffer_reg ? chan1_buf1_data_i    : chan1_buf0_data_i;
assign chan1_ready_i   =(current_buffer_reg ? chan1_buf1_ready_i   : chan1_buf0_ready_i) &&
			(!sreg_chan1_transfered_o);
assign chan1_mon_sec_i = current_buffer_reg ? chan1_buf1_mon_sec_i : chan1_buf0_mon_sec_i;
assign chan1_mon_qnt_i = current_buffer_reg ? chan1_buf1_mon_qnt_i : chan1_buf0_mon_qnt_i;
assign chan1_mon_qps_i = current_buffer_reg ? chan1_buf1_mon_qps_i : chan1_buf0_mon_qps_i;
assign chan1_mon_sts_i = current_buffer_reg ? chan1_buf1_mon_sts_i : chan1_buf0_mon_sts_i;

wire [DATA_WIDTH-1:0]	chan2_data_i;
wire			chan2_ready_i;
wire [DATA_WIDTH-1:0]	chan2_mon_sec_i;
wire [DATA_WIDTH-1:0]	chan2_mon_qnt_i;
wire [DATA_WIDTH-1:0]	chan2_mon_qps_i;
wire [DATA_WIDTH-1:0]	chan2_mon_sts_i;
assign chan2_data_i    = current_buffer_reg ? chan2_buf1_data_i    : chan2_buf0_data_i;
assign chan2_ready_i   =(current_buffer_reg ? chan2_buf1_ready_i   : chan2_buf0_ready_i) &&
			(!sreg_chan2_transfered_o);
assign chan2_mon_sec_i = current_buffer_reg ? chan2_buf1_mon_sec_i : chan2_buf0_mon_sec_i;
assign chan2_mon_qnt_i = current_buffer_reg ? chan2_buf1_mon_qnt_i : chan2_buf0_mon_qnt_i;
assign chan2_mon_qps_i = current_buffer_reg ? chan2_buf1_mon_qps_i : chan2_buf0_mon_qps_i;
assign chan2_mon_sts_i = current_buffer_reg ? chan2_buf1_mon_sts_i : chan2_buf0_mon_sts_i;

wire [DATA_WIDTH-1:0]	chan3_data_i;
wire			chan3_ready_i;
wire [DATA_WIDTH-1:0]	chan3_mon_sec_i;
wire [DATA_WIDTH-1:0]	chan3_mon_qnt_i;
wire [DATA_WIDTH-1:0]	chan3_mon_qps_i;
wire [DATA_WIDTH-1:0]	chan3_mon_sts_i;
assign chan3_data_i    = current_buffer_reg ? chan3_buf1_data_i    : chan3_buf0_data_i;
assign chan3_ready_i   =(current_buffer_reg ? chan3_buf1_ready_i   : chan3_buf0_ready_i) &&
			(!sreg_chan3_transfered_o);
assign chan3_mon_sec_i = current_buffer_reg ? chan3_buf1_mon_sec_i : chan3_buf0_mon_sec_i;
assign chan3_mon_qnt_i = current_buffer_reg ? chan3_buf1_mon_qnt_i : chan3_buf0_mon_qnt_i;
assign chan3_mon_qps_i = current_buffer_reg ? chan3_buf1_mon_qps_i : chan3_buf0_mon_qps_i;
assign chan3_mon_sts_i = current_buffer_reg ? chan3_buf1_mon_sts_i : chan3_buf0_mon_sts_i;

wire [DATA_WIDTH-1:0]	chan4_data_i;
wire			chan4_ready_i;
wire [DATA_WIDTH-1:0]	chan4_mon_sec_i;
wire [DATA_WIDTH-1:0]	chan4_mon_qnt_i;
wire [DATA_WIDTH-1:0]	chan4_mon_qps_i;
wire [DATA_WIDTH-1:0]	chan4_mon_sts_i;
assign chan4_data_i    = current_buffer_reg ? chan4_buf1_data_i    : chan4_buf0_data_i;
assign chan4_ready_i   =(current_buffer_reg ? chan4_buf1_ready_i   : chan4_buf0_ready_i) &&
			(!sreg_chan4_transfered_o);
assign chan4_mon_sec_i = current_buffer_reg ? chan4_buf1_mon_sec_i : chan4_buf0_mon_sec_i;
assign chan4_mon_qnt_i = current_buffer_reg ? chan4_buf1_mon_qnt_i : chan4_buf0_mon_qnt_i;
assign chan4_mon_qps_i = current_buffer_reg ? chan4_buf1_mon_qps_i : chan4_buf0_mon_qps_i;
assign chan4_mon_sts_i = current_buffer_reg ? chan4_buf1_mon_sts_i : chan4_buf0_mon_sts_i;

wire [DATA_WIDTH-1:0]	chan5_data_i;
wire			chan5_ready_i;
wire [DATA_WIDTH-1:0]	chan5_mon_sec_i;
wire [DATA_WIDTH-1:0]	chan5_mon_qnt_i;
wire [DATA_WIDTH-1:0]	chan5_mon_qps_i;
wire [DATA_WIDTH-1:0]	chan5_mon_sts_i;
assign chan5_data_i    = current_buffer_reg ? chan5_buf1_data_i    : chan5_buf0_data_i;
assign chan5_ready_i   =(current_buffer_reg ? chan5_buf1_ready_i   : chan5_buf0_ready_i) &&
			(!sreg_chan5_transfered_o);
assign chan5_mon_sec_i = current_buffer_reg ? chan5_buf1_mon_sec_i : chan5_buf0_mon_sec_i;
assign chan5_mon_qnt_i = current_buffer_reg ? chan5_buf1_mon_qnt_i : chan5_buf0_mon_qnt_i;
assign chan5_mon_qps_i = current_buffer_reg ? chan5_buf1_mon_qps_i : chan5_buf0_mon_qps_i;
assign chan5_mon_sts_i = current_buffer_reg ? chan5_buf1_mon_sts_i : chan5_buf0_mon_sts_i;

wire [DATA_WIDTH-1:0]	chan6_data_i;
wire			chan6_ready_i;
wire [DATA_WIDTH-1:0]	chan6_mon_sec_i;
wire [DATA_WIDTH-1:0]	chan6_mon_qnt_i;
wire [DATA_WIDTH-1:0]	chan6_mon_qps_i;
wire [DATA_WIDTH-1:0]	chan6_mon_sts_i;
assign chan6_data_i    = current_buffer_reg ? chan6_buf1_data_i    : chan6_buf0_data_i;
assign chan6_ready_i   =(current_buffer_reg ? chan6_buf1_ready_i   : chan6_buf0_ready_i) &&
			(!sreg_chan6_transfered_o);
assign chan6_mon_sec_i = current_buffer_reg ? chan6_buf1_mon_sec_i : chan6_buf0_mon_sec_i;
assign chan6_mon_qnt_i = current_buffer_reg ? chan6_buf1_mon_qnt_i : chan6_buf0_mon_qnt_i;
assign chan6_mon_qps_i = current_buffer_reg ? chan6_buf1_mon_qps_i : chan6_buf0_mon_qps_i;
assign chan6_mon_sts_i = current_buffer_reg ? chan6_buf1_mon_sts_i : chan6_buf0_mon_sts_i;

wire [DATA_WIDTH-1:0]	chan7_data_i;
wire			chan7_ready_i;
wire [DATA_WIDTH-1:0]	chan7_mon_sec_i;
wire [DATA_WIDTH-1:0]	chan7_mon_qnt_i;
wire [DATA_WIDTH-1:0]	chan7_mon_qps_i;
wire [DATA_WIDTH-1:0]	chan7_mon_sts_i;
assign chan7_data_i    = current_buffer_reg ? chan7_buf1_data_i    : chan7_buf0_data_i;
assign chan7_ready_i   =(current_buffer_reg ? chan7_buf1_ready_i   : chan7_buf0_ready_i) &&
			(!sreg_chan7_transfered_o);
assign chan7_mon_sec_i = current_buffer_reg ? chan7_buf1_mon_sec_i : chan7_buf0_mon_sec_i;
assign chan7_mon_qnt_i = current_buffer_reg ? chan7_buf1_mon_qnt_i : chan7_buf0_mon_qnt_i;
assign chan7_mon_qps_i = current_buffer_reg ? chan7_buf1_mon_qps_i : chan7_buf0_mon_qps_i;
assign chan7_mon_sts_i = current_buffer_reg ? chan7_buf1_mon_sts_i : chan7_buf0_mon_sts_i;

wire [DATA_WIDTH-1:0]	chan8_data_i;
wire			chan8_ready_i;
wire [DATA_WIDTH-1:0]	chan8_mon_sec_i;
wire [DATA_WIDTH-1:0]	chan8_mon_qnt_i;
wire [DATA_WIDTH-1:0]	chan8_mon_qps_i;
wire [DATA_WIDTH-1:0]	chan8_mon_sts_i;
assign chan8_data_i    = current_buffer_reg ? chan8_buf1_data_i    : chan8_buf0_data_i;
assign chan8_ready_i   =(current_buffer_reg ? chan8_buf1_ready_i   : chan8_buf0_ready_i) &&
			(!sreg_chan8_transfered_o);
assign chan8_mon_sec_i = current_buffer_reg ? chan8_buf1_mon_sec_i : chan8_buf0_mon_sec_i;
assign chan8_mon_qnt_i = current_buffer_reg ? chan8_buf1_mon_qnt_i : chan8_buf0_mon_qnt_i;
assign chan8_mon_qps_i = current_buffer_reg ? chan8_buf1_mon_qps_i : chan8_buf0_mon_qps_i;
assign chan8_mon_sts_i = current_buffer_reg ? chan8_buf1_mon_sts_i : chan8_buf0_mon_sts_i;


reg [DATA_WIDTH-1:0]	current_data_i;
always @(current_channel_reg, 
	chan1_data_i, chan2_data_i, chan3_data_i, chan4_data_i,
	chan5_data_i, chan6_data_i, chan7_data_i, chan8_data_i)
case (current_channel_reg)
	3'b000: current_data_i = chan1_data_i;
	3'b001: current_data_i = chan2_data_i;
	3'b010: current_data_i = chan3_data_i;
	3'b011: current_data_i = chan4_data_i;
	3'b100: current_data_i = chan5_data_i;
	3'b101: current_data_i = chan6_data_i;
	3'b110: current_data_i = chan7_data_i;
	3'b111: current_data_i = chan8_data_i;
endcase

reg	current_ready_i;
always @(current_channel_reg,
	chan1_ready_i, chan2_ready_i, chan3_ready_i, chan4_ready_i,
	chan5_ready_i, chan6_ready_i, chan7_ready_i, chan8_ready_i)
case (current_channel_reg)
	3'b000: current_ready_i = chan1_ready_i;
	3'b001: current_ready_i = chan2_ready_i;
	3'b010: current_ready_i = chan3_ready_i;
	3'b011: current_ready_i = chan4_ready_i;
	3'b100: current_ready_i = chan5_ready_i;
	3'b101: current_ready_i = chan6_ready_i;
	3'b110: current_ready_i = chan7_ready_i;
	3'b111: current_ready_i = chan8_ready_i;
endcase

reg [ADDRESS_WIDTH-1:0]	current_baseaddr_i;
always @(current_channel_reg,
	creg_chan1_baseaddr_i, creg_chan2_baseaddr_i, 
	creg_chan3_baseaddr_i, creg_chan4_baseaddr_i,
	creg_chan5_baseaddr_i, creg_chan6_baseaddr_i, 
	creg_chan7_baseaddr_i, creg_chan8_baseaddr_i)
case (current_channel_reg)
	3'b000: current_baseaddr_i = creg_chan1_baseaddr_i;
	3'b001: current_baseaddr_i = creg_chan2_baseaddr_i;
	3'b010: current_baseaddr_i = creg_chan3_baseaddr_i;
	3'b011: current_baseaddr_i = creg_chan4_baseaddr_i;
	3'b100: current_baseaddr_i = creg_chan5_baseaddr_i;
	3'b101: current_baseaddr_i = creg_chan6_baseaddr_i;
	3'b110: current_baseaddr_i = creg_chan7_baseaddr_i;
	3'b111: current_baseaddr_i = creg_chan8_baseaddr_i;
endcase

reg [DATA_WIDTH-1:0]	current_mon_sec_i;
always @(current_channel_reg,
	chan1_mon_sec_i, chan2_mon_sec_i, chan3_mon_sec_i, chan4_mon_sec_i,
	chan5_mon_sec_i, chan6_mon_sec_i, chan7_mon_sec_i, chan8_mon_sec_i )
case (current_channel_reg)
	3'b000: current_mon_sec_i = chan1_mon_sec_i;
	3'b001: current_mon_sec_i = chan2_mon_sec_i;
	3'b010: current_mon_sec_i = chan3_mon_sec_i;
	3'b011: current_mon_sec_i = chan4_mon_sec_i;
	3'b100: current_mon_sec_i = chan5_mon_sec_i;
	3'b101: current_mon_sec_i = chan6_mon_sec_i;
	3'b110: current_mon_sec_i = chan7_mon_sec_i;
	3'b111: current_mon_sec_i = chan8_mon_sec_i;
endcase

reg [DATA_WIDTH-1:0]	current_mon_qnt_i;
always @(current_channel_reg,
	chan1_mon_qnt_i, chan2_mon_qnt_i, chan3_mon_qnt_i, chan4_mon_qnt_i,
	chan5_mon_qnt_i, chan6_mon_qnt_i, chan7_mon_qnt_i, chan8_mon_qnt_i )
case (current_channel_reg)
	3'b000: current_mon_qnt_i = chan1_mon_qnt_i;
	3'b001: current_mon_qnt_i = chan2_mon_qnt_i;
	3'b010: current_mon_qnt_i = chan3_mon_qnt_i;
	3'b011: current_mon_qnt_i = chan4_mon_qnt_i;
	3'b100: current_mon_qnt_i = chan5_mon_qnt_i;
	3'b101: current_mon_qnt_i = chan6_mon_qnt_i;
	3'b110: current_mon_qnt_i = chan7_mon_qnt_i;
	3'b111: current_mon_qnt_i = chan8_mon_qnt_i;
endcase

reg [DATA_WIDTH-1:0]	current_mon_qps_i;
always @(current_channel_reg,
	chan1_mon_qps_i, chan2_mon_qps_i, chan3_mon_qps_i, chan4_mon_qps_i,
	chan5_mon_qps_i, chan6_mon_qps_i, chan7_mon_qps_i, chan8_mon_qps_i )
case (current_channel_reg)
	3'b000: current_mon_qps_i = chan1_mon_qps_i;
	3'b001: current_mon_qps_i = chan2_mon_qps_i;
	3'b010: current_mon_qps_i = chan3_mon_qps_i;
	3'b011: current_mon_qps_i = chan4_mon_qps_i;
	3'b100: current_mon_qps_i = chan5_mon_qps_i;
	3'b101: current_mon_qps_i = chan6_mon_qps_i;
	3'b110: current_mon_qps_i = chan7_mon_qps_i;
	3'b111: current_mon_qps_i = chan8_mon_qps_i;
endcase

reg [DATA_WIDTH-1:0]	current_mon_sts_i;
always @(current_channel_reg,
	chan1_mon_sts_i, chan2_mon_sts_i, chan3_mon_sts_i, chan4_mon_sts_i,
	chan5_mon_sts_i, chan6_mon_sts_i, chan7_mon_sts_i, chan8_mon_sts_i )
case (current_channel_reg)
	3'b000: current_mon_sts_i = chan1_mon_sts_i;
	3'b001: current_mon_sts_i = chan2_mon_sts_i;
	3'b010: current_mon_sts_i = chan3_mon_sts_i;
	3'b011: current_mon_sts_i = chan4_mon_sts_i;
	3'b100: current_mon_sts_i = chan5_mon_sts_i;
	3'b101: current_mon_sts_i = chan6_mon_sts_i;
	3'b110: current_mon_sts_i = chan7_mon_sts_i;
	3'b111: current_mon_sts_i = chan8_mon_sts_i;
endcase
///////////////////////////////////////////////////
//  Assign Outputs, based on current values     //
/////////////////////////////////////////////////
reg	current_read_o = 1'b0;

wire	chan1_read_o;
wire    chan2_read_o;
wire    chan3_read_o;
wire    chan4_read_o;
wire    chan5_read_o;
wire    chan6_read_o;
wire    chan7_read_o;
wire    chan8_read_o;

assign chan1_buf0_read_o = current_buffer_reg ? 1'b0  : chan1_read_o;
assign chan1_buf1_read_o = current_buffer_reg ? chan1_read_o  : 1'b0;
assign chan2_buf0_read_o = current_buffer_reg ? 1'b0  : chan2_read_o;
assign chan2_buf1_read_o = current_buffer_reg ? chan2_read_o  : 1'b0;
assign chan3_buf0_read_o = current_buffer_reg ? 1'b0  : chan3_read_o;
assign chan3_buf1_read_o = current_buffer_reg ? chan3_read_o  : 1'b0;
assign chan4_buf0_read_o = current_buffer_reg ? 1'b0  : chan4_read_o;
assign chan4_buf1_read_o = current_buffer_reg ? chan4_read_o  : 1'b0;
assign chan5_buf0_read_o = current_buffer_reg ? 1'b0  : chan5_read_o;
assign chan5_buf1_read_o = current_buffer_reg ? chan5_read_o  : 1'b0;
assign chan6_buf0_read_o = current_buffer_reg ? 1'b0  : chan6_read_o;
assign chan6_buf1_read_o = current_buffer_reg ? chan6_read_o  : 1'b0;
assign chan7_buf0_read_o = current_buffer_reg ? 1'b0  : chan7_read_o;
assign chan7_buf1_read_o = current_buffer_reg ? chan7_read_o  : 1'b0;
assign chan8_buf0_read_o = current_buffer_reg ? 1'b0  : chan8_read_o;
assign chan8_buf1_read_o = current_buffer_reg ? chan8_read_o  : 1'b0;

assign chan1_read_o = (current_channel_reg == 3'b000 ) ? current_read_o : 1'b0;
assign chan2_read_o = (current_channel_reg == 3'b001 ) ? current_read_o : 1'b0;
assign chan3_read_o = (current_channel_reg == 3'b010 ) ? current_read_o : 1'b0;
assign chan4_read_o = (current_channel_reg == 3'b011 ) ? current_read_o : 1'b0;
assign chan5_read_o = (current_channel_reg == 3'b100 ) ? current_read_o : 1'b0;
assign chan6_read_o = (current_channel_reg == 3'b101 ) ? current_read_o : 1'b0;
assign chan7_read_o = (current_channel_reg == 3'b110 ) ? current_read_o : 1'b0;
assign chan8_read_o = (current_channel_reg == 3'b111 ) ? current_read_o : 1'b0;

//============= After this we can operate on state Machine ==========//
// List of Registers:
// 	current_read_o
// 	current_buffer_reg
// 	current_channel_reg
// List of Input wires:
// 	current_data_i
//	current_ready_i

//////////////////////////////////////////////////
// Round - Robin Arbiter FSM :                 //
// If we find a buffer with  a non zero       //
// current_ready_i dump the buffer containt  //
// to the memory                            //
/////////////////////////////////////////////
reg [1:0] header_counter = 2'b00;
reg 	data_o_is_in_the_header = 1'b0;
wire [DATA_WIDTH-1:0]	header_data_o;

assign header_data_o = header_counter[0]? (
	header_counter[1] ?	current_mon_sts_i  :	// 2'b11
				current_mon_qps_i ):(	// 2'b01
	header_counter[1] ? 	current_mon_qnt_i  :	// 2'b10
		 		current_mon_sec_i );	// 2'b00

reg buffer_transfered = 1'b0;
(* FSM_ENCODING="SEQUENTIAL", SAFE_IMPLEMENTATION="NO" *) reg [1:0] wbm_fsm_state = 2'b00;
always@(posedge wb_clk_i)
if (wb_rst_i) begin
	wbm_fsm_state <= 2'b00;
end else
case (wbm_fsm_state)
  2'b00 : begin // rotate, looking for non-empty buffer
	if ( current_ready_i ) begin
		// The buffer is ready, let's write it to mem
		wb_adr_o <= current_baseaddr_i;
		data_o_is_in_the_header <= 1'b1;
		wbm_fsm_state <= 2'b01;
		current_read_o <= 1'b1;
		wb_cyc_o <= 1'b1;
		wb_stb_o <= 1'b1;
	end else begin
		{current_buffer_reg, current_channel_reg} <= 
			{current_buffer_reg, current_channel_reg} + 1'b1;
		current_read_o <= 1'b0;
		data_o_is_in_the_header <= 1'b0;
	end
	buffer_transfered <= 1'b0;
  end
  2'b01 : begin // Wait until acknowledge is recieved and download header
  	if (wb_ack_i) begin
		if (header_counter == 2'b11) begin
			data_o_is_in_the_header <= 1'b0;
			wbm_fsm_state <= 2'b10;
			current_read_o <= 1'b1;
		end else begin
			header_counter <= header_counter + 1'b1;
			current_read_o <= 1'b0;
		end
		// we don't want to increment address
		wb_adr_o[31:2] <= wb_adr_o[31:2] + 1'b1;
	end else begin
		// should we do anything?
		current_read_o <= 1'b0;
	end
  end
  2'b10 : begin // Wait until acknowledge is recieved and download the trace
  	if (wb_ack_i) begin
		// Acknowledge recieved
		if (current_read_o) begin
  			wb_adr_o[31:2] <= wb_adr_o[31:2] + 1'b1;
		end else begin
			wb_adr_o[31:2] <= wb_adr_o[31:2];
		end
		// wbm_fsm_state <= 2'b01;
		current_read_o <= 1'b1;
	end else begin
		// Acknowledge is not recieved, so don't 
		// load the next data
		current_read_o <= 1'b0;
	end
	//--> Checking if we still do have the data from buffers
	if (current_ready_i) begin
		wb_cyc_o <= 1'b1;
		wb_stb_o <= 1'b1;
	end else begin
		wb_cyc_o <= 1'b0;
		wb_stb_o <= 1'b0;
		wbm_fsm_state <= 2'b00;
		buffer_transfered <= 1'b1;
	end
	header_counter <= 2'b00;
  end
  2'b11 : begin
  end
endcase // FSM

//===================================================================//
// We want to synchronize resume registers from wishbone slave to wihbone
// master clocks
reg [1:0] chan1_resume_sreg = 2'b00;
reg [1:0] chan2_resume_sreg = 2'b00;
reg [1:0] chan3_resume_sreg = 2'b00;
reg [1:0] chan4_resume_sreg = 2'b00;
reg [1:0] chan5_resume_sreg = 2'b00;
reg [1:0] chan6_resume_sreg = 2'b00;
reg [1:0] chan7_resume_sreg = 2'b00;
reg [1:0] chan8_resume_sreg = 2'b00;
always@(posedge wb_clk_i) begin
  chan1_resume_sreg <= {chan1_resume_sreg[0], creg_chan1_resume_i};
  chan2_resume_sreg <= {chan2_resume_sreg[0], creg_chan2_resume_i};
  chan3_resume_sreg <= {chan3_resume_sreg[0], creg_chan3_resume_i};
  chan4_resume_sreg <= {chan4_resume_sreg[0], creg_chan4_resume_i};
  chan5_resume_sreg <= {chan5_resume_sreg[0], creg_chan5_resume_i};
  chan6_resume_sreg <= {chan6_resume_sreg[0], creg_chan6_resume_i};
  chan7_resume_sreg <= {chan7_resume_sreg[0], creg_chan7_resume_i};
  chan8_resume_sreg <= {chan8_resume_sreg[0], creg_chan8_resume_i};
end
// We want to be able to tell which channel we just transfered:
// transfered register is reset by resume falling edge.
always@(posedge wb_clk_i) begin
  if ( buffer_transfered && (current_channel_reg == 3'b000) )
  	sreg_chan1_transfered_o <= 1'b1;
  else if (chan1_resume_sreg == 2'b10) // falling edge
  	sreg_chan1_transfered_o <= 1'b0;

  if ( buffer_transfered && (current_channel_reg == 3'b001) )
	sreg_chan2_transfered_o <= 1'b1;
  else if (chan2_resume_sreg == 2'b10) // falling edge
  	sreg_chan2_transfered_o <= 1'b0;

  if ( buffer_transfered && (current_channel_reg == 3'b010) )
	sreg_chan3_transfered_o <= 1'b1;
  else if (chan3_resume_sreg == 2'b10) // falling edge
	sreg_chan3_transfered_o <= 1'b0;

  if ( buffer_transfered && (current_channel_reg == 3'b011) )
	sreg_chan4_transfered_o <= 1'b1;
  else if (chan4_resume_sreg == 2'b10) // falling edge
	sreg_chan4_transfered_o <= 1'b0;

  if ( buffer_transfered && (current_channel_reg == 3'b100) )
	sreg_chan5_transfered_o <= 1'b1;
  else if (chan5_resume_sreg == 2'b10) // falling edge
	sreg_chan5_transfered_o <= 1'b0;

  if ( buffer_transfered && (current_channel_reg == 3'b101) )
	sreg_chan6_transfered_o <= 1'b1;
  else if (chan6_resume_sreg == 2'b10) // falling edge
	sreg_chan6_transfered_o <= 1'b0;

  if ( buffer_transfered && (current_channel_reg == 3'b110) )
	sreg_chan7_transfered_o <= 1'b1;
  else if (chan7_resume_sreg == 2'b10) // falling edge
	sreg_chan7_transfered_o <= 1'b0;

  if ( buffer_transfered && (current_channel_reg == 3'b111) )
	sreg_chan8_transfered_o <= 1'b1;
  else if (chan8_resume_sreg == 2'b10) // falling edge
	sreg_chan8_transfered_o <= 1'b0;
end
//===================================================================//
/////////////////////////////////////////////////////
// For Now Let's Assign quet values on the bus    //
///////////////////////////////////////////////////
assign wb_bte_o = 2'b00;	// Classic
assign wb_cti_o = 3'b000; 	// Classic
assign wb_we_o  = 1'b1;		// We are always writing
assign wb_dat_o = data_o_is_in_the_header ? header_data_o : current_data_i;
assign wb_sel_o = 4'b1111;      // We're always writing full word

endmodule
