module adc_lvds_cntr_chan_avgwfm
#(parameter	INP_DATA_WIDTH	= 12, // This is an incoming Data Width
		AVG_DATA_INCR	= 8  // This is an increment to the Data Width
)(
//---> Input Data Path
  input wire [INP_DATA_WIDTH-1:0]			data_i,
//---> Output Data Path
  output reg  [INP_DATA_WIDTH+AVG_DATA_INCR-1:0]	data_o,
  output wire						ready_o,
//---> Input Registers For averaging
  input wire [2:0]	avg_value_i, 
//---> Clock input
  input wire clk_i
);
  // Logical Shifter:
  reg [7:0] avg_value_reg;
  always @* begin
	case (avg_value_i)
		3'b000 : avg_value_reg = avg_value_i;
		3'b001 : avg_value_reg = (1 << avg_value_i) - 1;
		3'b010 : avg_value_reg = (1 << avg_value_i) - 1;
		3'b011 : avg_value_reg = (1 << avg_value_i) - 1;
		3'b100 : avg_value_reg = (1 << avg_value_i) - 1;
		3'b101 : avg_value_reg = (1 << avg_value_i) - 1;
		3'b110 : avg_value_reg = (1 << avg_value_i) - 1;
		default: avg_value_reg = (1 << avg_value_i) - 1;
	endcase
  end

  reg [AVG_DATA_INCR-1:0] avg_bin_counter_reg = 0;
  reg ready_reg = 1'b0;
  assign ready_o = (avg_value_reg == 0) ? 1'b1 : (avg_bin_counter_reg == 0);
  // Down Counter
  always@(posedge clk_i) begin
	if (ready_o) begin
		avg_bin_counter_reg <= avg_value_reg;
	end else begin
		avg_bin_counter_reg <= avg_bin_counter_reg - 1'b1;
	end
  end

  // Summing register
  reg [INP_DATA_WIDTH+AVG_DATA_INCR-1:0] summing_reg = 0;
  always@(posedge clk_i) begin
  	if (ready_o) begin
		summing_reg <= data_i;
	end else begin
		summing_reg <= summing_reg + data_i;
	end
	
  end

  // Synchronize output to the Summing register
  always@(posedge clk_i) begin
  	if (ready_o) begin
		data_o <= summing_reg;
	end
  end

endmodule
