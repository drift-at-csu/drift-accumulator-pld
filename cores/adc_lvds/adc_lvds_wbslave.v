/*
 * The Wishbone Slave is accepting slow control signals
 * like Threshold settings, averaging, power on etc.
 *
 */

module adc_lvds_wbslave
#(parameter     DATA_WIDTH    =  32, 	// Data Width of wishbone bus
                ADDRESS_WIDTH  = 32 	// Address Width of wishbone bus
)(
//---> Slave Wishbone connections
  input wire [ADDRESS_WIDTH-1:0]	wb_adr_i,
  input wire [1:0]			wb_bte_i,
  input wire [2:0]			wb_cti_i,
  input wire				wb_cyc_i,
  input wire [DATA_WIDTH-1:0]		wb_dat_i,
  input wire [(DATA_WIDTH/8)-1:0]	wb_sel_i,
  input wire				wb_stb_i,
  input wire				wb_we_i,
 
  output wire				wb_ack_o,
  output wire				wb_err_o,
  output wire				wb_rty_o,
  output reg [DATA_WIDTH-1:0]		wb_dat_o,

  input wire	wb_clk_i,
  input wire	wb_rst_i,

//---> Control Registers output
  /* bits [31:24] -> Power Control 
   * bits [23:16] -> External Trigger
   * bits [15:8]  -> ????
   * bits [7:0]	  -> Enable for channels */
  output reg  [31:0]	creg_control_o		= 32'h0000_0000,

  output reg		creg_phy_bitslip_o	= 1'b0,
  output reg		creg_phy_reset_o	= 1'b0,
  output reg		creg_phy_delay_inc_o	= 1'b0,

  output reg		creg_timestamp_reset_o  = 1'b0,

//---> Channel Specyfic registers:
  /* Value for the trigger, when the signal is lower than a pedestal */
  output reg  [27:0]	creg_adc1_trig_low_o	= 28'h3FF_FFFF,
  /* Value for the trigger, when the signal is higher than a pedestal */
  output reg  [27:0]	creg_adc1_trig_hig_o	= 28'h3FF_FFFF,
  output reg		creg_adc1_trig_ext_o	= 1'b0,
  /* Base Address where to download the triggered waveform */
  output reg  [31:0]	creg_adc1_base_addr_o	= 32'h0000_0000,
  /* Averaging for waveform and trigger (for 40 MSPS 1bin = 25ns):
   * bits [8:6] -> Averaging Base for Pedestal  
   * 			(1, 2, 4, 8, 16, 32, 64, 128) trigger bins (Max = 52.4msec)
   * bits [5:3] -> Averaging Base for Trigger   
   * 			(1, 2, 4, 8, 16, 32, 64, 128) waveform bins (Max=409.6usec)
   * bits [2:0] -> Averaging Base for Waveforms 
   * 			(1, 2, 4, 8, 16, 32, 64, 128) time bins	(Max=3.2usec) 
   *
   * So for example 100 100 101 (  16  16  32 ) would result in 
   *	0.8usec sampling, 12.8usec Trigger trace and 204.8usec Pedestal
   *	1Buffer (500Samples) = 400usec
   * The other realistic settings will be 110 011 110  ( 64   8  64 )
   * 	1.6usec sampling, 12.8usec Trigger trace and 819.2usec Pedestal
   * 	1 Buffer (500 Samples) = 800usec
   * For testing: 011 010 001  ( 8  4  2 ) would result in
   * 	50nsec sampling, 0.2usec Trigger trace and 1.6usec for Pedestal
   * 	1 Buffer (500 Samples) = 25usec
   */
  output reg  [8:0]	creg_adc1_average_o  	= 9'h000,
  output reg		creg_adc1_resume_o	= 1'b0,


  output reg  [27:0]	creg_adc2_trig_low_o	= 28'h3FF_FFFF,
  output reg  [27:0]	creg_adc2_trig_hig_o	= 28'h3FF_FFFF,
  output reg		creg_adc2_trig_ext_o	= 1'b0,
  output reg  [31:0]	creg_adc2_base_addr_o	= 32'h0000_0000,
  output reg   [8:0]	creg_adc2_average_o	= 9'h000,
  output reg		creg_adc2_resume_o	= 1'b0,

  output reg  [27:0]	creg_adc3_trig_low_o	= 28'h3FF_FFFF,
  output reg  [27:0]	creg_adc3_trig_hig_o	= 28'h3FF_FFFF,
  output reg		creg_adc3_trig_ext_o	= 1'b0,
  output reg  [31:0]	creg_adc3_base_addr_o	= 32'h0000_0000,
  output reg   [8:0]	creg_adc3_average_o	= 9'h000,
  output reg		creg_adc3_resume_o	= 1'b0,

  output reg  [27:0]	creg_adc4_trig_low_o	= 28'h3FF_FFFF,
  output reg  [27:0]	creg_adc4_trig_hig_o	= 28'h3FF_FFFF,
  output reg		creg_adc4_trig_ext_o	= 1'b0,
  output reg  [31:0]	creg_adc4_base_addr_o	= 32'h0000_0000,
  output reg   [8:0]	creg_adc4_average_o	= 9'h000,
  output reg		creg_adc4_resume_o	= 1'b0,

  output reg  [27:0]	creg_adc5_trig_low_o	= 28'h3FF_FFFF,
  output reg  [27:0]	creg_adc5_trig_hig_o	= 28'h3FF_FFFF,
  output reg		creg_adc5_trig_ext_o	= 1'b0,
  output reg  [31:0]	creg_adc5_base_addr_o	= 32'h0000_0000,
  output reg   [8:0]	creg_adc5_average_o	= 9'h000,
  output reg		creg_adc5_resume_o	= 1'b0,

  output reg  [27:0]	creg_adc6_trig_low_o	= 28'h3FF_FFFF,
  output reg  [27:0]	creg_adc6_trig_hig_o	= 28'h3FF_FFFF,
  output reg		creg_adc6_trig_ext_o	= 1'b0,
  output reg  [31:0]	creg_adc6_base_addr_o	= 32'h0000_0000,
  output reg   [8:0]	creg_adc6_average_o	= 9'h000,
  output reg		creg_adc6_resume_o	= 1'b0,

  output reg  [27:0]	creg_adc7_trig_low_o	= 28'h3FF_FFFF,
  output reg  [27:0]	creg_adc7_trig_hig_o	= 28'h3FF_FFFF,
  output reg		creg_adc7_trig_ext_o	= 1'b0,
  output reg  [31:0]	creg_adc7_base_addr_o	= 32'h0000_0000,
  output reg   [8:0]	creg_adc7_average_o	= 9'h000,
  output reg		creg_adc7_resume_o	= 1'b0,

  output reg  [27:0]	creg_adc8_trig_low_o	= 28'h3FF_FFFF,
  output reg  [27:0]	creg_adc8_trig_hig_o	= 28'h3FF_FFFF,
  output reg		creg_adc8_trig_ext_o	= 1'b0,
  output reg  [31:0]	creg_adc8_base_addr_o	= 32'h0000_0000,
  output reg  [ 8:0]	creg_adc8_average_o	= 9'h000,
  output reg		creg_adc8_resume_o	= 1'b0,
  // SPI Control
  output reg  		creg_spi_transfer_o	= 1'b0,
  output reg  [15:0]	creg_spi_addr_reg_o	= 16'h0000,
  output reg  [15:0]	creg_spi_data_reg_o	= 16'h0000,
//---> Status Registers Input
  // TIMESTAMP
  input wire  [31:0]	sreg_timestamp_sec_i,
  input wire  [31:0]	sreg_timestamp_qps_i,

  // PHY Status
  input wire  [11:0]	sreg_phy_fco_i,
  input wire  [11:0]	sreg_phy_dco_i,

  // CONTROLLER Status
  input wire  [27:0]	sreg_cnt_adc1_pedestal_i,
  input wire  [27:0]	sreg_cnt_adc2_pedestal_i,
  input wire  [27:0]	sreg_cnt_adc3_pedestal_i,
  input wire  [27:0]	sreg_cnt_adc4_pedestal_i,
  input wire  [27:0]	sreg_cnt_adc5_pedestal_i,
  input wire  [27:0]	sreg_cnt_adc6_pedestal_i,
  input wire  [27:0]	sreg_cnt_adc7_pedestal_i,
  input wire  [27:0]	sreg_cnt_adc8_pedestal_i,

  input wire		sreg_cnt_adc1_transfered_i,
  input wire		sreg_cnt_adc2_transfered_i,
  input wire		sreg_cnt_adc3_transfered_i,
  input wire		sreg_cnt_adc4_transfered_i,
  input wire		sreg_cnt_adc5_transfered_i,
  input wire		sreg_cnt_adc6_transfered_i,
  input wire		sreg_cnt_adc7_transfered_i,
  input wire		sreg_cnt_adc8_transfered_i,

  // SPI Status
  input wire		sreg_spi_ready_i,
  input wire  [16:0]	sreg_spi_data_reg_i,

  output wire		interrupt_o
);
//-------------------------------------------------------------------//
// Internal interrupt registers
  reg [7:0]	irq_chan_enable = 8'h00;
  reg [7:0]	irq_chan_pending = 8'h00;
always@(posedge wb_clk_i) begin
  irq_chan_pending[0] <= sreg_cnt_adc1_transfered_i;
  irq_chan_pending[1] <= sreg_cnt_adc2_transfered_i;
  irq_chan_pending[2] <= sreg_cnt_adc3_transfered_i;
  irq_chan_pending[3] <= sreg_cnt_adc4_transfered_i;
  irq_chan_pending[4] <= sreg_cnt_adc5_transfered_i;
  irq_chan_pending[5] <= sreg_cnt_adc6_transfered_i;
  irq_chan_pending[6] <= sreg_cnt_adc7_transfered_i;
  irq_chan_pending[7] <= sreg_cnt_adc8_transfered_i;
end

assign interrupt_o = | (irq_chan_enable & irq_chan_pending);
//-------------------------------------------------------------------//

///////////////////
// Read Section //
/////////////////
  wire wb_single_bus_access = ((wb_cti_i == 3'b111) | (wb_cti_i == 3'b000)) &
  	wb_cyc_i & wb_stb_i;

  // We do acknowledge the single write right away
  wire wb_single_write_access = wb_single_bus_access & wb_we_i;

  reg wb_ack_reg = 1'b0; assign wb_ack_o = wb_single_write_access ? 1'b1 : wb_ack_reg;
  reg [ADDRESS_WIDTH-1:0] burst_addr;

  // State Machine for Read and Write
  (* FSM_ENCODING="SEQUENTIAL", SAFE_IMPLEMENTATION="NO" *) reg [1:0] wb_fsm_state = 2'b00;
  always@(posedge wb_clk_i) 
  if (wb_rst_i) begin
	wb_fsm_state <= 2'b00; // Idle state
	burst_addr <= 0;
	wb_ack_reg <= 1'b0;
  end else
  case (wb_fsm_state)
  2'b00 : begin // Idle
	if ( wb_cyc_i & wb_stb_i ) begin
		// Read or write cycle
		if (wb_we_i) begin
			// Write Specyfics
			case (wb_adr_i[8:0])
			9'h000: begin
				creg_control_o	<= wb_dat_i;
			end
			9'h004: begin // Writing to PHY Status register
				creg_phy_reset_o   <= wb_dat_i[29];
				creg_phy_bitslip_o <= wb_dat_i[28];
				creg_phy_delay_inc_o <= wb_dat_i[12];
			end
			9'h008: begin
				creg_adc8_trig_ext_o <= wb_dat_i[7];
				creg_adc7_trig_ext_o <= wb_dat_i[6];
				creg_adc6_trig_ext_o <= wb_dat_i[5];
				creg_adc5_trig_ext_o <= wb_dat_i[4];
				creg_adc4_trig_ext_o <= wb_dat_i[3];
				creg_adc3_trig_ext_o <= wb_dat_i[2];
				creg_adc2_trig_ext_o <= wb_dat_i[1];
				creg_adc1_trig_ext_o <= wb_dat_i[0];
			end

			//---> ADCs Specyfic Registers
			9'h010: creg_adc1_trig_low_o 	<= wb_dat_i[27:0];
			9'h014: creg_adc1_trig_hig_o 	<= wb_dat_i[27:0];
			9'h018: begin
				creg_adc1_resume_o	<= 1'b0;
				creg_adc1_base_addr_o 	<= wb_dat_i;
			end
			9'h01C: begin
				// We are skipping 1 bit
				creg_adc1_average_o[2:0] <= wb_dat_i [2:0];
				creg_adc1_average_o[5:3] <= wb_dat_i [6:4];
				creg_adc1_average_o[8:6] <= wb_dat_i[10:8];
			end
			// 9'h020: Reserved for ADC1 Pedestal, readonly

			9'h030: creg_adc2_trig_low_o	<= wb_dat_i[27:0];
			9'h034: creg_adc2_trig_hig_o	<= wb_dat_i[27:0];
			9'h038: begin
				creg_adc2_resume_o	<= 1'b0;
				creg_adc2_base_addr_o	<= wb_dat_i;
			end
			9'h03C: begin
				creg_adc2_average_o[2:0] <= wb_dat_i [2:0];
				creg_adc2_average_o[5:3] <= wb_dat_i [6:4];
				creg_adc2_average_o[8:6] <= wb_dat_i[10:8];
			end
			// 9'h040

			9'h050: creg_adc3_trig_low_o	<= wb_dat_i[27:0];
			9'h054: creg_adc3_trig_hig_o	<= wb_dat_i[27:0];
			9'h058: begin
				creg_adc3_resume_o	<= 1'b0;
				creg_adc3_base_addr_o	<= wb_dat_i;
			end
			9'h05C: begin
				creg_adc3_average_o[2:0] <= wb_dat_i [2:0];
				creg_adc3_average_o[5:3] <= wb_dat_i [6:4];
				creg_adc3_average_o[8:6] <= wb_dat_i[10:8];
			end
			// 9'h060

			9'h070: creg_adc4_trig_low_o	<= wb_dat_i[27:0];
			9'h074: creg_adc4_trig_hig_o	<= wb_dat_i[27:0];
			9'h078: begin
				creg_adc4_resume_o	<= 1'b0;
				creg_adc4_base_addr_o	<= wb_dat_i;
			end
			9'h07C: begin
				creg_adc4_average_o[2:0] <= wb_dat_i [2:0];
				creg_adc4_average_o[5:3] <= wb_dat_i [6:4];
				creg_adc4_average_o[8:6] <= wb_dat_i[10:8];
			end
			// 9'h080

			9'h090: creg_adc5_trig_low_o	<= wb_dat_i[27:0];
			9'h094: creg_adc5_trig_hig_o	<= wb_dat_i[27:0];
			9'h098: begin 
				creg_adc5_resume_o	<= 1'b0;
				creg_adc5_base_addr_o	<= wb_dat_i;
			end
			9'h09C: begin
				creg_adc5_average_o[2:0] <= wb_dat_i [2:0];
				creg_adc5_average_o[5:3] <= wb_dat_i [6:4];
				creg_adc5_average_o[8:6] <= wb_dat_i[10:8];
			end
			// 9'h0A0

			9'h0B0: creg_adc6_trig_low_o     <= wb_dat_i[27:0];
			9'h0B4: creg_adc6_trig_hig_o     <= wb_dat_i[27:0];
			9'h0B8: begin
				creg_adc6_resume_o	<= 1'b0;
				creg_adc6_base_addr_o	<= wb_dat_i;
			end
			9'h0BC: begin
				creg_adc6_average_o[2:0] <= wb_dat_i [2:0];
				creg_adc6_average_o[5:3] <= wb_dat_i [6:4];
				creg_adc6_average_o[8:6] <= wb_dat_i[10:8];
			end
			// 9'h0C0

			9'h0D0: creg_adc7_trig_low_o	<= wb_dat_i[27:0];
			9'h0D4: creg_adc7_trig_hig_o	<= wb_dat_i[27:0];
			9'h0D8: begin
				creg_adc7_resume_o	<= 1'b0;
				creg_adc7_base_addr_o	<= wb_dat_i;
			end
			9'h0DC: begin
				creg_adc7_average_o[2:0] <= wb_dat_i [2:0];
				creg_adc7_average_o[5:3] <= wb_dat_i [6:4];
				creg_adc7_average_o[8:6] <= wb_dat_i[10:8];
			end
			// 9'h0E0

			9'h0F0: creg_adc8_trig_low_o	<= wb_dat_i[27:0];
			9'h0F4: creg_adc8_trig_hig_o	<= wb_dat_i[27:0];
			9'h0F8: begin
				creg_adc8_resume_o	<= 1'b0;
				creg_adc8_base_addr_o	<= wb_dat_i;
			end
			9'h0FC: begin
				creg_adc8_average_o[2:0] <= wb_dat_i [2:0];
				creg_adc8_average_o[5:3] <= wb_dat_i [6:4];
				creg_adc8_average_o[8:6] <= wb_dat_i[10:8];
			end
			// 9'h100
			9'h1F0: begin // Writing to SPI Register
			  if ((creg_spi_transfer_o == 1'b0) & (sreg_spi_ready_i)) begin
				creg_spi_transfer_o     <= 1'b1;
				creg_spi_addr_reg_o     <= wb_dat_i[31:16];
				creg_spi_data_reg_o     <= wb_dat_i[15:0];
			  end
			end
			9'h1F4: begin // Writing to Seconds Register (reset)
				creg_timestamp_reset_o <= wb_dat_i [31];
			end
			// 9'h1F8
			9'h1FC: begin // Interrupt Register
				creg_adc8_resume_o <= wb_dat_i [15];
				creg_adc7_resume_o <= wb_dat_i [14];
				creg_adc6_resume_o <= wb_dat_i [13];
				creg_adc5_resume_o <= wb_dat_i [12];
				creg_adc4_resume_o <= wb_dat_i [11];
				creg_adc3_resume_o <= wb_dat_i [10];
				creg_adc2_resume_o <= wb_dat_i [9];
				creg_adc1_resume_o <= wb_dat_i [8];				
				irq_chan_enable <= wb_dat_i [7:0];
			end
			endcase // Write Address Decoding
		end else begin
			// Read Specyfic
			case (wb_adr_i[8:0])
			9'h000: begin // Control Register Readout
				wb_dat_o <= creg_control_o;
			end

			9'h004: begin // Reading Physical Status
				creg_phy_bitslip_o <= 1'b0; // Reading Register resets bitslip.
				creg_phy_reset_o <= 1'b0; // Reading register resets reset.
				creg_phy_delay_inc_o <= 1'b0; // Reading register resets delay increment.

				wb_dat_o [31:30] <= 2'b00;
				wb_dat_o [29]	 <= creg_phy_reset_o;
				wb_dat_o [28]    <= creg_phy_bitslip_o;
				wb_dat_o [27:16] <= sreg_phy_fco_i;
				wb_dat_o [15:13] <= 3'b000;
				wb_dat_o [12]	 <= creg_phy_delay_inc_o;
				wb_dat_o [11:0]  <= sreg_phy_dco_i;
			end
			9'h008: begin
				creg_adc8_trig_ext_o <= 1'b0;
				creg_adc7_trig_ext_o <= 1'b0;
				creg_adc6_trig_ext_o <= 1'b0;
				creg_adc5_trig_ext_o <= 1'b0;
				creg_adc4_trig_ext_o <= 1'b0;
				creg_adc3_trig_ext_o <= 1'b0;
				creg_adc2_trig_ext_o <= 1'b0;
				creg_adc1_trig_ext_o <= 1'b0;

				wb_dat_o [31:8] <= 24'h000000;
				wb_dat_o [7] <= creg_adc8_trig_ext_o;
				wb_dat_o [6] <= creg_adc7_trig_ext_o;
				wb_dat_o [5] <= creg_adc6_trig_ext_o;
				wb_dat_o [4] <= creg_adc5_trig_ext_o;
				wb_dat_o [3] <= creg_adc4_trig_ext_o;
				wb_dat_o [2] <= creg_adc3_trig_ext_o;
				wb_dat_o [1] <= creg_adc2_trig_ext_o;
				wb_dat_o [0] <= creg_adc1_trig_ext_o;
			end

			9'h01C: begin // Reading Average register for CHAN 1
				wb_dat_o[31:12] <= 20'h00000;
				wb_dat_o[11]	<= 1'b0;
				wb_dat_o[10:8]	<= creg_adc1_average_o[8:6];
				wb_dat_o[7]	<= 1'b0;
				wb_dat_o[6:4]	<= creg_adc1_average_o[5:3];
				wb_dat_o[3]	<= 1'b0;
				wb_dat_o[2:0]	<= creg_adc1_average_o[2:0];
                        end
			9'h020: begin // Reading Pedestal for CHAN 1
				wb_dat_o[31:28] <= 4'h0;
				wb_dat_o[27:0]	<= sreg_cnt_adc1_pedestal_i;
			end

			9'h03C: begin // Reading Average register for CHAN 2
				wb_dat_o[31:12] <= 20'h00000;
				wb_dat_o[11]    <= 1'b0;
				wb_dat_o[10:8]  <= creg_adc2_average_o[8:6];
				wb_dat_o[7]     <= 1'b0;
				wb_dat_o[6:4]   <= creg_adc2_average_o[5:3];
				wb_dat_o[3]     <= 1'b0;
				wb_dat_o[2:0]   <= creg_adc2_average_o[2:0];
			end
			9'h040: begin // Reading Pedestal for CHAN 2
				wb_dat_o[31:28] <= 4'h0;
				wb_dat_o[27:0]  <= sreg_cnt_adc2_pedestal_i;
			end

			9'h05C: begin // Reading Average register for CHAN 3
				wb_dat_o[31:12] <= 20'h00000;
				wb_dat_o[11]    <= 1'b0;
				wb_dat_o[10:8]  <= creg_adc3_average_o[8:6];
				wb_dat_o[7]     <= 1'b0;
				wb_dat_o[6:4]   <= creg_adc3_average_o[5:3];
				wb_dat_o[3]     <= 1'b0;
				wb_dat_o[2:0]   <= creg_adc3_average_o[2:0];
			end
			9'h060: begin // Reading Pedestal for CHAN 3
				wb_dat_o[31:28] <= 4'h0;
				wb_dat_o[27:0]  <= sreg_cnt_adc3_pedestal_i;
			end

			9'h07C: begin // Reading Average register for CHAN 4
				wb_dat_o[31:12] <= 20'h00000;
				wb_dat_o[11]    <= 1'b0;
				wb_dat_o[10:8]  <= creg_adc4_average_o[8:6];
				wb_dat_o[7]     <= 1'b0;
				wb_dat_o[6:4]   <= creg_adc4_average_o[5:3];
				wb_dat_o[3]     <= 1'b0;
				wb_dat_o[2:0]   <= creg_adc4_average_o[2:0];
			end
			9'h080: begin // Reading Pedestal for CHAN 4
				wb_dat_o[31:28] <= 4'h0;
				wb_dat_o[27:0]  <= sreg_cnt_adc4_pedestal_i;
			end

			9'h09C: begin // Reading Average register for CHAN 5
				wb_dat_o[31:12] <= 20'h00000;
				wb_dat_o[11]    <= 1'b0;
				wb_dat_o[10:8]  <= creg_adc5_average_o[8:6];
				wb_dat_o[7]     <= 1'b0;
				wb_dat_o[6:4]   <= creg_adc5_average_o[5:3];
				wb_dat_o[3]     <= 1'b0;
				wb_dat_o[2:0]   <= creg_adc5_average_o[2:0];
			end
			9'h0A0: begin // Reading Pedestal for CHAN 5
				wb_dat_o[31:28] <= 4'h0;
				wb_dat_o[27:0]  <= sreg_cnt_adc5_pedestal_i;
			end

			9'h0BC: begin // Reading Average register for CHAN 6
				wb_dat_o[31:12] <= 20'h00000;
				wb_dat_o[11]    <= 1'b0;
				wb_dat_o[10:8]  <= creg_adc6_average_o[8:6];
				wb_dat_o[7]     <= 1'b0;
				wb_dat_o[6:4]   <= creg_adc6_average_o[5:3];
				wb_dat_o[3]     <= 1'b0;
				wb_dat_o[2:0]   <= creg_adc6_average_o[2:0];
			end
			9'h0C0: begin // Reading Pedestal for CHAN 6
				wb_dat_o[31:28] <= 4'h0;
				wb_dat_o[27:0]  <= sreg_cnt_adc6_pedestal_i;
			end

			9'h0DC: begin // Reading Average register for CHAN 7
				wb_dat_o[31:12] <= 20'h00000;
				wb_dat_o[11]    <= 1'b0;
				wb_dat_o[10:8]  <= creg_adc7_average_o[8:6];
				wb_dat_o[7]     <= 1'b0;
				wb_dat_o[6:4]   <= creg_adc7_average_o[5:3];
				wb_dat_o[3]     <= 1'b0;
				wb_dat_o[2:0]   <= creg_adc7_average_o[2:0];
			end
			9'h0E0: begin // Reading Pedestal for CHAN 7
				wb_dat_o[31:28] <= 4'h0;
				wb_dat_o[27:0]  <= sreg_cnt_adc7_pedestal_i;
			end

			9'h0FC: begin // Reading Average register for CHAN 8
				wb_dat_o[31:12] <= 20'h00000;
				wb_dat_o[11]    <= 1'b0;
				wb_dat_o[10:8]  <= creg_adc8_average_o[8:6];
				wb_dat_o[7]     <= 1'b0;
				wb_dat_o[6:4]   <= creg_adc8_average_o[5:3];
				wb_dat_o[3]     <= 1'b0;
				wb_dat_o[2:0]   <= creg_adc8_average_o[2:0];
			end
			9'h100: begin // Reading Pedestal for CHAN 8
				wb_dat_o[31:28] <= 4'h0;
				wb_dat_o[27:0]  <= sreg_cnt_adc8_pedestal_i;
			end

			9'h1F0: begin // Reading from SPI Register
				creg_spi_transfer_o     <= 1'b0; // Reading resets transfer Req.
				wb_dat_o[31]		<= creg_spi_addr_reg_o[15];
				wb_dat_o[30]		<= creg_spi_transfer_o;
				wb_dat_o[29]		<= sreg_spi_ready_i;
				wb_dat_o[28:16]		<= creg_spi_addr_reg_o[12:0];
				wb_dat_o[15:0]		<= sreg_spi_data_reg_i;
			end
			9'h1F4: begin // Reading from Timestamp Seconds register
				creg_timestamp_reset_o	<= 1'b0; // Reading resets reset
				wb_dat_o		<= sreg_timestamp_sec_i;
			end
			9'h1F8: begin // Reading from quanta per second register
				wb_dat_o		<= sreg_timestamp_qps_i;
			end
			9'h1FC: begin // Interrupt Register
				wb_dat_o [31:16]	<= 16'h0000;
				wb_dat_o [15:8]		<= irq_chan_pending;
				wb_dat_o [7:0]		<= irq_chan_enable;
			end
			endcase // Read Address Decoding
		end

		if (wb_single_bus_access) begin
			// Single Operation
			wb_ack_reg 	<= wb_single_write_access ? 1'b0 : // Write auto acknowledged 
						1'b1; // Acknowledge the read
			wb_fsm_state 	<= wb_single_write_access ? 2'b00 : // Go to Idle
						2'b11; // Go to finalize procedure if it's read
		end else begin
			// Burst Access
		end
	end else begin
		wb_fsm_state <= 2'b00;
		wb_ack_reg   <= 1'b0;
	end
  	
  end
  2'b01 : begin // Read Burst
	if (wb_cti_i == 3'b111) begin
		wb_fsm_state    <= 2'b11; // Go to finalize procedure
	end else begin
		wb_fsm_state    <= 2'b01; 
	end
  end
  2'b10 : begin // Write Burst
	if (wb_cti_i == 3'b111) begin
		wb_fsm_state    <= 2'b11; // Go to finalize procedure
	end else begin
		wb_fsm_state    <= 2'b10;
	end

  end
  2'b11 : begin // Finalize Transaction
  	// Here we just waiting for Stb and cycle to go down
	/*
	if (wb_cyc_i == 1'b0 && wb_stb_i == 1'b0 ) begin
		wb_ack_reg <= 1'b0;
		wb_fsm_state <= 2'b00;
	end */
	wb_ack_reg <= 1'b0;
	wb_fsm_state <= 2'b00;
  end
  endcase
//-------------------------------------------------------------------//
assign wb_err_o = 1'b0;
assign wb_rty_o = 1'b0;
//-------------------------------------------------------------------//
endmodule
