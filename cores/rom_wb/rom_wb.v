module rom_wb #(
	parameter addr_offset = 32'h0000_0100
)(
  output reg [31:0] 	wb_dat_o = 32'h1500_0000,
  output reg 		wb_ack_o = 1'b0,
  input wire [31:0]	wb_adr_i,
  input wire		wb_stb_i,
  input wire	  	wb_cyc_i,
  input wire [2:0]	wb_cti_i,
  input wire [1:0]	wb_bte_i,
  output wire		wb_err_o,
  output wire		wb_rty_o,

  input wire	wb_clk_i,
  input wire	wb_rst_i
);
   reg [29:0]	adr_reg;
   reg		wb_stb_reg;
   wire 	new_access;
   reg 		new_access_reg;   
   wire		burst;
   reg		burst_reg;
   wire		new_burst;
  
assign wb_err_o = 0;
assign wb_rty_o = 0;

always @ (posedge wb_clk_i) begin
  if (wb_rst_i)
	wb_dat_o <= 32'h1500_0000;
  else
	case (adr_reg)
`include "bootrom.v"
	default:
		wb_dat_o <= 32'h1500_0000;
	endcase // case (wb_adr_i)
end

reg wb_stb_i_reg;
always @(posedge wb_clk_i)
     wb_stb_i_reg <= wb_stb_i;

assign new_access = (wb_stb_i & !wb_stb_i_reg);
always @(posedge wb_clk_i)
     new_access_reg <= new_access;

always @(posedge wb_clk_i)
     burst_reg <= burst;
assign new_burst = (burst & !burst_reg);

always @(posedge wb_clk_i)
  if (wb_rst_i)
	adr_reg <= 30'h0000_0000;
  else if (new_access)
	adr_reg <= wb_adr_i[31:2] - addr_offset[31:2];
  else if (burst) begin
	if (wb_cti_i == 3'b010)
		case (wb_bte_i)
			2'b00: adr_reg <= adr_reg + 1'b1;
			2'b01: adr_reg[1:0] <= adr_reg[1:0] + 1'b1;
			2'b10: adr_reg[2:0] <= adr_reg[2:0] + 1'b1;
			2'b11: adr_reg[3:0] <= adr_reg[3:0] + 1'b1;
		endcase
	else
		adr_reg <= wb_adr_i[31:2] - addr_offset[31:2];
  end

always @(posedge wb_clk_i)
	if (wb_rst_i)
        	wb_ack_o <= 1'b0;
	else if (wb_ack_o & (!burst | (wb_cti_i == 3'b111)))
		wb_ack_o <= 1'b0;
	else if (wb_stb_i & ((!burst & !new_access & new_access_reg) | (burst & burst_reg)))
		wb_ack_o <= 1'b1;
	else
		wb_ack_o <= 1'b0;

assign burst = wb_cyc_i & (!(wb_cti_i == 3'b000)) & (!(wb_cti_i == 3'b111));

endmodule 
