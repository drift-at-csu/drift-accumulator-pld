`timescale 1ps / 1ps

module tb_rom;

//////////////////////////////////////////////
// Parameters for cti and bte burst cycles //
////////////////////////////////////////////

// bte:
parameter [1:0] linear = 2'b00,
		beat4  = 2'b01,
		beat8  = 2'b10,
		beat16 = 2'b11;
// cti:
parameter [2:0] classic = 3'b000,
		inc     = 3'b010,
		eob	= 3'b111;


///////////////////////////////////////
//  Input system clock is 100 MHz   //
/////////////////////////////////////
reg sys_clk = 1'b0;

////////////////////////////////////////////
// WB  Clocks                            //
// As they are generated in real design //
/////////////////////////////////////////
wire wb_clk;
wire wb_rst;

clkgen clkgen0(
  .adcs_clk_o( ), // Not using this

  .mems_clk_o ( ),
  .mems_rst_o ( ),

  .wb_clk_o ( wb_clk ),
  .wb_rst_o ( wb_rst ),

  .wbfast_clk_o (  ),
  .wbfast_rst_o (  ),

  .sys_clk_i( sys_clk )
);

////////////////////////////////////////////////////
//  Wishbone things                              //
//////////////////////////////////////////////////
reg [31:0]	wbm_i_cpu0_adr_o = 32'hXXXX_XXXX;
reg [1:0]	wbm_i_cpu0_bte_o = 2'b00;
reg [2:0]	wbm_i_cpu0_cti_o = 3'b000;
reg		wbm_i_cpu0_cyc_o = 1'b0;
reg [31:0]	wbm_i_cpu0_dat_o = 32'hXXXX_XXXX;
reg [3:0]	wbm_i_cpu0_sel_o = 4'bxxxx;
reg 		wbm_i_cpu0_stb_o = 1'b0;
reg		wbm_i_cpu0_we_o  = 1'bX;
wire		wbm_i_cpu0_ack_i;
wire		wbm_i_cpu0_err_i;
wire		wbm_i_cpu0_rty_i;
wire [31:0]	wbm_i_cpu0_dat_i;

wire            wbs_i_mem0_ack_o;
wire            wbs_i_mem0_err_o;
wire            wbs_i_mem0_rty_o;
wire [31:0]     wbs_i_mem0_dat_o;
wire            wbs_i_mem0_cyc_i;
wire [31:0]     wbs_i_mem0_adr_i;
wire            wbs_i_mem0_stb_i;
wire            wbs_i_mem0_we_i;
wire [3:0]      wbs_i_mem0_sel_i;
wire [31:0]     wbs_i_mem0_dat_i;
wire [2:0]      wbs_i_mem0_cti_i;
wire [1:0]      wbs_i_mem0_bte_i;

wire            wbs_i_rom0_ack_o;
wire            wbs_i_rom0_err_o;
wire            wbs_i_rom0_rty_o;
wire [31:0]     wbs_i_rom0_dat_o;
wire            wbs_i_rom0_cyc_i;
wire [31:0]     wbs_i_rom0_adr_i;
wire            wbs_i_rom0_stb_i;
wire            wbs_i_rom0_we_i;
wire [3:0]      wbs_i_rom0_sel_i;
wire [31:0]     wbs_i_rom0_dat_i;
wire [2:0]      wbs_i_rom0_cti_i;
wire [1:0]      wbs_i_rom0_bte_i;

arbiter_ibus #(
  .wb_addr_match_width ( 4 ),
  .slave0_adr(4'h0), // MEM0: Resulting Address: 0x0000_0000
  .slave1_adr(4'hf)  // ROM0: 0xF000_0000
)ibus(
  //----------- Masters ----------->
  // Master 0 == CPU0 (OR1200)
  .wbm0_adr_i( wbm_i_cpu0_adr_o ),
  .wbm0_dat_i( wbm_i_cpu0_dat_o ),
  .wbm0_sel_i( wbm_i_cpu0_sel_o ),
  .wbm0_we_i ( wbm_i_cpu0_we_o ),
  .wbm0_cyc_i( wbm_i_cpu0_cyc_o ),
  .wbm0_stb_i( wbm_i_cpu0_stb_o ),
  .wbm0_cti_i( wbm_i_cpu0_cti_o ),
  .wbm0_bte_i( wbm_i_cpu0_bte_o ),
  .wbm0_dat_o( wbm_i_cpu0_dat_i),
  .wbm0_ack_o( wbm_i_cpu0_ack_i ),
  .wbm0_err_o( wbm_i_cpu0_err_i ),
  .wbm0_rty_o( wbm_i_cpu0_rty_i ),
  //------------ Slaves ----------->
  // Slave 0 == MEM0, Port 0
  .wbs0_adr_o( wbs_i_mem0_adr_i ),
  .wbs0_dat_o( wbs_i_mem0_dat_i ),
  .wbs0_sel_o( wbs_i_mem0_sel_i ),
  .wbs0_we_o ( wbs_i_mem0_we_i ),
  .wbs0_cyc_o( wbs_i_mem0_cyc_i ),
  .wbs0_stb_o( wbs_i_mem0_stb_i ),
  .wbs0_cti_o( wbs_i_mem0_cti_i ),
  .wbs0_bte_o( wbs_i_mem0_bte_i ),
  .wbs0_dat_i( wbs_i_mem0_dat_o ),
  .wbs0_ack_i( wbs_i_mem0_ack_o ),
  .wbs0_err_i( wbs_i_mem0_err_o ),
  .wbs0_rty_i( wbs_i_mem0_rty_o ),
  // Slave 1 == ROM0
  .wbs1_adr_o( wbs_i_rom0_adr_i ),
  .wbs1_dat_o(  ), // Read-only
  .wbs1_sel_o(  ), // Read-only
  .wbs1_we_o (  ), // Read-only
  .wbs1_cyc_o( wbs_i_rom0_cyc_i ),
  .wbs1_stb_o( wbs_i_rom0_stb_i ),
  .wbs1_cti_o( wbs_i_rom0_cti_i ),
  .wbs1_bte_o( wbs_i_rom0_bte_i ),
  .wbs1_dat_i( wbs_i_rom0_dat_o ),
  .wbs1_ack_i( wbs_i_rom0_ack_o ),
  .wbs1_err_i( wbs_i_rom0_err_o ),
  .wbs1_rty_i( wbs_i_rom0_rty_o ),
  //------------ Clock, reset ----->
  .wb_clk_i ( wb_clk ),
  .wb_rst_i ( wb_rst )
);
//---------------------- Instances ----------------------------------//
///////////////////////////////////
//  ROM 0                       //
/////////////////////////////////
rom_wb # (
        .addr_offset (32'hF000_0100)  // The OR1200 boots from 0xF000_0100, so if the bootrom.v
		// doesn't include this 0x100 shift, we have to include it here
) rom0 (
  .wb_dat_o ( wbs_i_rom0_dat_o ),
  .wb_ack_o ( wbs_i_rom0_ack_o ),
  .wb_adr_i ( wbs_i_rom0_adr_i ),
  .wb_stb_i ( wbs_i_rom0_stb_i ),
  .wb_cyc_i ( wbs_i_rom0_cyc_i ),
  .wb_cti_i ( wbs_i_rom0_cti_i ),
  .wb_bte_i ( wbs_i_rom0_bte_i ),
  .wb_err_o ( wbs_i_rom0_err_o ),
  .wb_rty_o ( wbs_i_rom0_rty_o ),

  .wb_clk_i (wb_clk),
  .wb_rst_i (wb_rst)
);
//===================  TASKS ========================================//
///////////////////////////////////////
//  Wishbone Write                  //
/////////////////////////////////////
task wbm_write;
	input [31:0] addr;
	input  [3:0] wsel;
	input [31:0] data;
	input	  cyc_end;
begin
  @ (negedge wb_clk);
  wbm_i_cpu0_adr_o <= addr;
  wbm_i_cpu0_sel_o <= wsel;
  wbm_i_cpu0_we_o  <= 1'b1;
  wbm_i_cpu0_stb_o <= 1'b1;
  wbm_i_cpu0_cyc_o <= 1'b1;
  wbm_i_cpu0_dat_o <= data;
  wbm_i_cpu0_cti_o <= classic;
  wbm_i_cpu0_bte_o <= linear;
  @ (posedge ( wbm_i_cpu0_ack_i | wbm_i_cpu0_err_i | wbm_i_cpu0_rty_i));
  $display("WBM0 Write  \t Addr:0x%H \t Data:0x%H \t Mask:0x%H", 
  	wbm_i_cpu0_adr_o, wbm_i_cpu0_dat_o, wbm_i_cpu0_sel_o);
  @ (posedge wb_clk); #1
  wbm_i_cpu0_adr_o <= 'hX;
  wbm_i_cpu0_sel_o <= 'hx;
  wbm_i_cpu0_dat_o <= 'hX;
  wbm_i_cpu0_stb_o <= cyc_end;
  wbm_i_cpu0_cyc_o <= cyc_end;
  wbm_i_cpu0_we_o  <= 1'bX;
end
endtask
//-------------------------------------------------------------------//
///////////////////////////////////////
//  Wishbone Read                   //
/////////////////////////////////////
task wbm_read;
	input  [31:0] addr;
	input  [3:0]  wsel;
	output [31:0] data;
	input  [2:0] cti;
	input  [1:0] bte;
	input	   cyc_end;
begin
  @ (negedge wb_clk);
  wbm_i_cpu0_adr_o <= addr;
  wbm_i_cpu0_sel_o <= wsel;
  wbm_i_cpu0_we_o  <= 1'b0;
  wbm_i_cpu0_stb_o <= 1'b1;
  wbm_i_cpu0_cyc_o <= 1'b1;
  wbm_i_cpu0_cti_o <= cti;
  wbm_i_cpu0_bte_o <= bte;
  @ (posedge wb_clk);
  while (wbm_i_cpu0_ack_i == 1'b0) begin
	@(posedge wb_clk);
  end
  data <= wbm_i_cpu0_dat_i;
  $display("WBS0 Read  \t Addr:0x%H \t Data:0x%H",
  	wbm_i_cpu0_adr_o, wbm_i_cpu0_dat_i);
  // @ (negedge wbs0_clk);
  wbm_i_cpu0_stb_o <= cyc_end;
  wbm_i_cpu0_cyc_o <= cyc_end;
  wbm_i_cpu0_we_o  <= 1'bX;
end
endtask
//-------------------------------------------------------------------//
/////////////////////////////////
//  Wishbone Burst Read       //
///////////////////////////////
task wbm_burst_read;
	input  [31:0] addr;
	input  [7 :0] bl;
	input         cyc_end;
	reg 	[8:0] ii;
begin
  @ (negedge wb_clk);
  for (ii = 0; ii < bl; ii = ii + 1) begin
	wbm_i_cpu0_stb_o <= 1'b1; 
	wbm_i_cpu0_cyc_o <= 1'b1;
	wbm_i_cpu0_we_o  <= 1'b0; 
	wbm_i_cpu0_adr_o <= addr+4*ii;
	if ((ii+1) == bl) 
		wbm_i_cpu0_cti_o <= 3'b111; 
	else 
		wbm_i_cpu0_cti_o <= 3'b010;
	wbm_i_cpu0_bte_o <= 2'b00;

	@ (posedge wb_clk);
	while (wbm_i_cpu0_ack_i == 1'b0) begin 
		@(posedge wb_clk); 
	end
	$display("---WBM Burst Read #%d \t Addr:0x%H \t Data:0x%H",
		ii+1, wbm_i_cpu0_adr_o, wbm_i_cpu0_dat_i);
  end 
  wbm_i_cpu0_stb_o <= cyc_end; 
  wbm_i_cpu0_cyc_o <= cyc_end;
  wbm_i_cpu0_we_o  <= 1'b0; 
  wbm_i_cpu0_adr_o <= 'hx;
end
endtask
//-------------------------------------------------------------------//
//===================  SAVING TO THE FILE ===========================//
initial begin
  $dumpfile( "tb_rom.vcd" );
  $dumpvars;
end
//=================  Clocks ==========================================//
always #5000.0 sys_clk = ~sys_clk; // 100 MHz
//============= WBS0 Tasks =============//
reg [31:0] wbm_readout_data;  
initial begin
	@ (negedge wb_rst);
	$display("... Reset reached");
	$display("... Burst Read of 18 words from ROM ...");
	wbm_burst_read(32'hF000_0100, 18, 1'b1);
	$display("... Single Read ...");
	wbm_read(32'hF000_0100, 4'hF,	wbm_readout_data, eob, linear, 1'b0);
	$display("... Burst Read of 18 words outside of ROM ...");
	wbm_burst_read(32'hF000_0000, 18, 1'b1);
	$display("... 4 Single classic reads ...");
	wbm_read(32'hF000_0100,	4'hF,	wbm_readout_data, classic, linear, 1'b1);
	wbm_read(32'hF000_0104,	4'hF,	wbm_readout_data, classic, linear, 1'b1);
	wbm_read(32'hF000_0108,	4'hF,	wbm_readout_data, classic, linear, 1'b0);
	#100000
	wbm_read(32'hF000_0144, 4'hF,   wbm_readout_data, classic, linear, 1'b1);
	$display("...Burst Read of 38 words from ROM ...");
	wbm_burst_read(32'hF000_0100, 38, 1'b0);
	#100000 $finish;
end
endmodule
