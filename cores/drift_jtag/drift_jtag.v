/*
 * Top Module for DRIFT Jtag controller (OR1200, Wishbone Bus)
 */

/* The Idea Here is to use BSCANE2 primitives
 *
 * USER1 register --> Reserved
 * USER2 register for wishbone Master
 * USER3 register for CPU communications
 * USER4 register --> Reserved for Serial
 *
 * The Idea is to decouple jtag clocks
 * from Wishbone and CPU clocks at the top level through FIFOs
 * and then have a separate modules for cpu and wbm
 * which operate in their clock domains.
 *
 *
 */

module drift_jtag #(
  parameter JTAG_REG_WBM = 2, // USER2 Register
  parameter JTAG_REG_CPU = 3  // USER3 Register
)(
  //---> Wishbone Master Signals
  output wire [31:0]	wbm_adr_o, 
  output wire [1:0] 	wbm_bte_o, 
  output wire [2:0]	wbm_cti_o, 
  output wire		wbm_cyc_o, 
  output wire [31:0]	wbm_dat_o, 
  output wire [3:0]	wbm_sel_o, 
  output wire		wbm_stb_o, 
  output wire		wbm_we_o,
  input  wire		wbm_ack_i, 
  input  wire		wbm_err_i, 
  input  wire		wbm_rty_i, 
  input  wire [31:0]	wbm_dat_i,

  input wire 		wbm_clk_i,
  input wire 		wbm_rst_i,

  //---> OR1200 control Signals ( Clock can be different than WB clock)
  input  wire	cpu_bp_i,
  output wire	cpu_stall_o,

  output wire [31:0]	cpu_adr_o,
  output wire [31:0]	cpu_dat_o,
  output wire		cpu_stb_o,
  output wire		cpu_we_o,
  input  wire [31:0]	cpu_dat_i,
  input  wire		cpu_ack_i,

  input  wire	cpu_clk_i,
  input  wire	cpu_rst_i
);
 
////////////////////////////////
//  WBM BSCAN                //
//////////////////////////////
wire jtag_wbm_capture;
wire jtag_wbm_clk;
wire jtag_wbm_reset;
wire jtag_wbm_sel;
wire jtag_wbm_update;
wire jtag_wbm_tdi_o;
wire jtag_wbm_tdo_i;
wire jtag_wbm_tck;
wire jtag_wbm_shift;
BSCANE2 #(
	.JTAG_CHAIN(JTAG_REG_WBM), // Value for USER command.
	.DISABLE_JTAG ("FALSE")
) bscan_wbm (
  .CAPTURE( jtag_wbm_capture ),	// 1-bit output: CAPTURE output from TAP controller.
  .DRCK   ( jtag_wbm_clk ),	// 1-bit output: Gated TCK output. When SEL is asserted, 
				// DRCK toggles when CAPTURE or SHIFT  are asserted.
  .RESET  ( jtag_wbm_reset ),	// 1-bit output: Reset output for TAP controller.
  .RUNTEST( ),		// 1-bit output: Output asserted when TAP controller is in Run Test/Idle state.
  .SEL	  ( jtag_wbm_sel ),     // 1-bit output: USER instruction active output.
  .SHIFT  ( jtag_wbm_shift ),	// 1-bit output: SHIFT output from TAP controller.
  .TCK	  ( jtag_wbm_tck ),	// 1-bit output: Test Clock output. Fabric connection to TAP Clock pin.
  .TDI	  ( jtag_wbm_tdi_o ),	// 1-bit output: Test Data Input (TDI) output from TAP controller.
  .TMS	  (  ),		// 1-bit output: Test Mode Select output. Fabric connection to TAP.
  .UPDATE ( jtag_wbm_update ),	// 1-bit output: UPDATE output from TAP controller
  .TDO	  ( jtag_wbm_tdo_i )	// 1-bit input: Test Data Output (TDO) input for USER function.
);
wire jtag_wbm_shift_op = jtag_wbm_shift & jtag_wbm_sel;
wire jtag_wbm_capture_op = jtag_wbm_capture & jtag_wbm_sel;
reg [15:0] jtag_wbm_pos_bit_counter;
always @( posedge jtag_wbm_clk ) begin
	if (!(jtag_wbm_shift_op)) begin
		jtag_wbm_pos_bit_counter <= 16'h0000;
	end else begin
		jtag_wbm_pos_bit_counter <= jtag_wbm_pos_bit_counter + 1'b1;
		
	end
end
reg [15:0] jtag_wbm_neg_bit_counter;
always @( negedge jtag_wbm_tck ) begin
	if (!(jtag_wbm_shift_op || jtag_wbm_capture_op )) begin
		jtag_wbm_neg_bit_counter <= 16'h0000;
	end else begin
		jtag_wbm_neg_bit_counter <= jtag_wbm_neg_bit_counter + 1'b1;
	end
end
// the data tdi_o is clocked in on the rising jtag clock
// and clocked out (tdo_i) on the falling edge of the jtag clock

wire   [71:0] jtag_wbm_inp_reg;
reg jtag_wbm_tdo_reg;
assign jtag_wbm_tdo_i = jtag_wbm_tdo_reg;
always @( posedge jtag_wbm_clk ) begin
	jtag_wbm_tdo_reg <= jtag_wbm_inp_reg[jtag_wbm_neg_bit_counter];
end

reg  [71:0] jtag_wbm_out_reg; // = {8'hCD, 32'hA5A6_A7A8, 32'hD5D6_D7D8};
always @( posedge jtag_wbm_clk ) begin
	jtag_wbm_out_reg[jtag_wbm_pos_bit_counter] <= jtag_wbm_tdi_o;
end

// We want to update these two registers on the rising edge of jtag_wbm_tck
reg jtag_wbm_push_pull;
always @ (posedge jtag_wbm_tck) begin
	if (jtag_wbm_update & jtag_wbm_sel)
		jtag_wbm_push_pull <= 1'b1;
	else
		jtag_wbm_push_pull <= 1'b0;
end

//---> So after this if jtag_wbm_push_pull goes high this indicates that
// the data from jtag_wbm_tdo_reg is avaiable for wishbone master, and
// after the operation described in this register, the response should be put
// to jtag_wbm_inp_reg.
drift_jtag_wb_master #(
	.AW ( 32 ),
	.DW ( 32 ),
	.HW ( 8 )
) wbm_bui (
  //---> Async JTAG interface
  .jtag_inp_reg_i( jtag_wbm_out_reg ),
  .jtag_out_reg_o( jtag_wbm_inp_reg ),
  .jtag_reg_trn_i( jtag_wbm_push_pull ),
  //---> Wishbone Master Signals
  .wbm_adr_o( wbm_adr_o ),
  .wbm_bte_o( wbm_bte_o ),
  .wbm_cti_o( wbm_cti_o ),
  .wbm_cyc_o( wbm_cyc_o ),
  .wbm_dat_o( wbm_dat_o ),
  .wbm_sel_o( wbm_sel_o ),
  .wbm_stb_o( wbm_stb_o ),
  .wbm_we_o ( wbm_we_o ),
  .wbm_ack_i( wbm_ack_i ),
  .wbm_err_i( wbm_err_i ),
  .wbm_rty_i( wbm_rty_i ),
  .wbm_dat_i( wbm_dat_i ),

  .wb_clk_i( wbm_clk_i ),
  .wb_rst_i( wbm_rst_i )
);


//===================================================================//
//////////////////////////////
// CPU BSCAN               //
////////////////////////////
wire jtag_cpu_capture;
wire jtag_cpu_clk;
wire jtag_cpu_reset;
wire jtag_cpu_sel;
wire jtag_cpu_update;
wire jtag_cpu_tdi_o;
wire jtag_cpu_tdo_i;
wire jtag_cpu_shift;
wire jtag_cpu_tck;
BSCANE2 #(
	.JTAG_CHAIN(JTAG_REG_CPU), // Value for USER command.
	.DISABLE_JTAG ("FALSE")
) bscan_cpu (
  .CAPTURE( jtag_cpu_capture ),	// 1-bit output: CAPTURE output from TAP controller.
  .DRCK	  ( jtag_cpu_clk ),	// 1-bit output: Gated TCK output. When SEL is asserted,
				// DRCK toggles when CAPTURE or SHIFT are asserted.
  .RESET  ( jtag_cpu_reset ),	// 1-bit output: Reset output for TAP controller.
  .RUNTEST( ),	// 1-bit output: Output asserted when TAP controller is in Run Test/Idle state.
  .SEL	  ( jtag_cpu_sel ),	// 1-bit output: USER instruction active output.
  .SHIFT  ( jtag_cpu_shift ),	// 1-bit output: SHIFT output from TAP controller.
  .TCK	  ( jtag_cpu_tck ),	// 1-bit output: Test Clock output. Fabric connection to TAP Clock pin.
  .TDI	  ( jtag_cpu_tdi_o ),	// 1-bit output: Test Data Input (TDI) output from TAP controller.
  .TMS	  (  ),   // 1-bit output: Test Mode Select output. Fabric connection to TAP.
  .UPDATE ( jtag_cpu_update ),	// 1-bit output: UPDATE output from TAP controller
  .TDO	  ( jtag_cpu_tdo_i )	// 1-bit input: Test Data Output (TDO) input for USER function.
);
wire jtag_cpu_shift_op = jtag_cpu_shift & jtag_cpu_sel;
wire jtag_cpu_capture_op = jtag_cpu_capture & jtag_cpu_sel;
reg [15:0] jtag_cpu_pos_bit_counter;
always @( posedge jtag_cpu_clk ) begin
  if (!(jtag_cpu_shift_op)) begin
	jtag_cpu_pos_bit_counter <= 16'h0000;
  end else begin
	jtag_cpu_pos_bit_counter <= jtag_cpu_pos_bit_counter + 1'b1;
  end
end
reg [15:0] jtag_cpu_neg_bit_counter;
always @( negedge jtag_cpu_tck ) begin
  if (!(jtag_cpu_shift_op || jtag_cpu_capture_op )) begin
	jtag_cpu_neg_bit_counter <= 16'h0000;
  end else begin
	jtag_cpu_neg_bit_counter <= jtag_cpu_neg_bit_counter + 1'b1;
  end
end
// the data tdi_o is clocked in on the rising jtag clock
// and clocked out (tdo_i) on the falling edge of the jtag clock
wire   [71:0] jtag_cpu_inp_reg;
reg jtag_cpu_tdo_reg;
assign jtag_cpu_tdo_i = jtag_cpu_tdo_reg;
always @( posedge jtag_cpu_clk ) begin
	jtag_cpu_tdo_reg <= jtag_cpu_inp_reg[jtag_cpu_neg_bit_counter];
end

reg  [71:0] jtag_cpu_out_reg; // = {8'hCD, 32'hA5A6_A7A8, 32'hD5D6_D7D8};
always @( posedge jtag_cpu_clk ) begin
	jtag_cpu_out_reg[jtag_cpu_pos_bit_counter] <= jtag_cpu_tdi_o;
end
// We want to update these two registers on the rising edge of jtag_wbm_tck
reg jtag_cpu_push_pull;
always @ (posedge jtag_cpu_tck) begin
  if (jtag_cpu_update & jtag_cpu_sel)
	jtag_cpu_push_pull <= 1'b1;
  else
	jtag_cpu_push_pull <= 1'b0;
end
//---> So after this if jtag_cpu_push_pull goes high this indicates that
// the data from jtag_cpu_tdo_reg is avaiable for cpu master, and
// after the operation described in this register, the response should be
// put to jtag_cpu_inp_reg.
drift_jtag_cpu #(
	.AW ( 32 ),
	.DW ( 32 ),
	.HW ( 8 )
) cpu_bui (
  //---> Async JTAG interface
  .jtag_inp_reg_i( jtag_cpu_out_reg ),
  .jtag_out_reg_o( jtag_cpu_inp_reg ),
  .jtag_reg_trn_i( jtag_cpu_push_pull ),
  //---> CPU Master Signals
  .cpu_adr_o( cpu_adr_o ),
  .cpu_dat_o( cpu_dat_o ),
  .cpu_stb_o( cpu_stb_o ),
  .cpu_we_o ( cpu_we_o ),
  .cpu_ack_i( cpu_ack_i ),
  .cpu_dat_i( cpu_dat_i ),
  //---> Slow Control Signals
  .cpu_stall_o ( cpu_stall_o ),
  .cpu_bp_i	( cpu_bp_i ),
  //---> Clocks and reset
  .cpu_clk_i( cpu_clk_i ),
  .cpu_rst_i( cpu_rst_i )
);
//===================================================================//

endmodule
