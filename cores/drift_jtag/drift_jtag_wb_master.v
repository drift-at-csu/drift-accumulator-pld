module drift_jtag_wb_master #(
	parameter AW = 32,
	parameter DW = 32,
	parameter HW = 8
)(
  //---> Async JTAG interface
  input wire [HW+AW+DW-1:0] 	jtag_inp_reg_i,
  output reg [HW+AW+DW-1:0] 	jtag_out_reg_o,
  input wire 			jtag_reg_trn_i,
  //---> Wishbone Master Signals
  output reg [AW-1:0]		wbm_adr_o,
  output reg [1:0]		wbm_bte_o,
  output reg [2:0]		wbm_cti_o,
  output reg			wbm_cyc_o,
  output reg [AW-1:0]		wbm_dat_o,
  output reg [(DW/8)-1:0]	wbm_sel_o,
  output reg			wbm_stb_o,
  output reg			wbm_we_o,
  input  wire			wbm_ack_i,
  input  wire			wbm_err_i,
  input  wire			wbm_rty_i,
  input  wire [DW-1:0]		wbm_dat_i,

  input wire	wb_clk_i,
  input wire    wb_rst_i
);

// Let's first synchronize the transfer signal:
wire wbm_transfer_req;
reg [1:0] wbm_transfer_req_sreg;
always @(posedge wb_clk_i) begin
	if (wb_rst_i)
		wbm_transfer_req_sreg <= 2'b00;
	else
		wbm_transfer_req_sreg <= {wbm_transfer_req_sreg[0], 
			jtag_reg_trn_i};
end
assign wbm_transfer_req = (wbm_transfer_req_sreg == 2'b01);

// State Machine for wishbone operations
(* FSM_ENCODING="SEQUENTIAL", SAFE_IMPLEMENTATION="NO" *) reg [1:0] wbm_fsm_state = 2'b00;
always@(posedge wb_clk_i)
  if (wb_rst_i) begin
	wbm_fsm_state <= 2'b00; // Idle state
	wbm_cyc_o <= 1'b0;
	wbm_stb_o <= 1'b0;
  end else
  case (wbm_fsm_state)
  2'b00 : begin // Idle state, waiting for read or write access
  	if (wbm_transfer_req) begin
		// We should start a transfer
		// HW(8bits) 		AW(32bits) 	DW(32bits)
		// [HW+AW+DW-1: AW+DW]	[AW+DW-1: DW]	[DW-1:0]
		// Header ( 8 bits )
		// 7bit: read(0) or write (1) operation
		// 6bit:  Error ( err_i )
		// 5bit:  Rty (rty_i)
		// 4bit:  Ack (ack_i)
		// 3-0bit: mask 
		wbm_adr_o <= jtag_inp_reg_i[AW+DW-1:DW];
		wbm_we_o  <= jtag_inp_reg_i[HW+AW+DW-1];
		wbm_bte_o <= 2'b00; // classic
		wbm_cti_o <= 3'b000; // classic
		wbm_cyc_o <= 1'b1;
		wbm_stb_o <= 1'b1;
		wbm_dat_o <= jtag_inp_reg_i[DW-1:0];
		wbm_sel_o <= jtag_inp_reg_i[AW+DW+(DW/8):AW+DW];

		if (jtag_inp_reg_i[HW+AW+DW-1] == 1'b1) begin
			wbm_fsm_state <= 2'b10; // Write
		end else begin
			wbm_fsm_state <= 2'b01; // Read
		end
	end else begin
		wbm_cyc_o <= 1'b0;
		wbm_stb_o <= 1'b0;
	end
  end
  2'b01 : begin // Bus read access
	if (wbm_ack_i || wbm_err_i || wbm_rty_i) begin
		wbm_fsm_state <= 2'b00; // Go to idle
		// Push data to the jtag output register
		jtag_out_reg_o[HW+AW+DW-1] <= 1'b0; // Read
		jtag_out_reg_o[HW+AW+DW-2] <= wbm_err_i;
		jtag_out_reg_o[HW+AW+DW-3] <= wbm_rty_i;
		jtag_out_reg_o[HW+AW+DW-4] <= wbm_ack_i;
		jtag_out_reg_o[AW+DW+(DW/8):AW+DW] <= wbm_sel_o;
		jtag_out_reg_o[AW+DW-1:DW] <= wbm_adr_o;
		jtag_out_reg_o[DW-1:0] 	   <= wbm_dat_i;
		// Don't Forget to drop Strobe and Cycle
		wbm_cyc_o <= 1'b0;
		wbm_stb_o <= 1'b0;
	end else begin
		wbm_fsm_state <= 2'b01; // Stay in Bus Read Cycle
	end
  end
  2'b10 : begin // Bus write access
	if (wbm_ack_i || wbm_err_i || wbm_rty_i) begin
		wbm_fsm_state <= 2'b00; // Go to idle
		// Push data to the jtag output register
		jtag_out_reg_o[HW+AW+DW-1] <= 1'b1; // Write
		jtag_out_reg_o[HW+AW+DW-2] <= wbm_err_i;
		jtag_out_reg_o[HW+AW+DW-3] <= wbm_rty_i;
		jtag_out_reg_o[HW+AW+DW-4] <= wbm_ack_i;
		jtag_out_reg_o[AW+DW+(DW/8):AW+DW] <= wbm_sel_o;
		jtag_out_reg_o[AW+DW-1:DW] <= wbm_adr_o;
		jtag_out_reg_o[DW-1:0]     <= wbm_dat_o;
		// Don't Forget to drop Strobe and Cycle
		wbm_cyc_o <= 1'b0;
		wbm_stb_o <= 1'b0;
	end else begin
		wbm_fsm_state  <= 2'b10; // Stay in Bus Write Cycle
	end
  end
  2'b11: begin // Cleaning up after the access? 
	wbm_fsm_state <= 2'b00; // For now just go to idle
  end
  endcase
endmodule
