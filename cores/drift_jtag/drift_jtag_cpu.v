module drift_jtag_cpu #(
	parameter AW = 32,
	parameter DW = 32,
	parameter HW = 8
)(
  //---> Async JTAG interface
  input wire [HW+AW+DW-1:0] 	jtag_inp_reg_i,
  output reg [HW+AW+DW-1:0] 	jtag_out_reg_o,
  input wire 			jtag_reg_trn_i,
  //---> CPU Master Signals
  output reg  [AW-1:0]	cpu_adr_o,
  output reg  [DW-1:0]	cpu_dat_o,
  output reg		cpu_stb_o = 1'b0,
  output reg		cpu_we_o  = 1'b0,
  input  wire		cpu_ack_i,
  input  wire [DW-1:0]	cpu_dat_i,
  //---> Slow Control Signals
  output reg  cpu_stall_o = 1'b0, 
  input  wire cpu_bp_i,
  //---> Clocks and reset
  input  wire  cpu_clk_i,
  input  wire  cpu_rst_i
);

// Let's first synchronize the transfer signal:
wire cpu_transfer_req;
reg [1:0] cpu_transfer_req_sreg;
always @(posedge cpu_clk_i) begin
	if (cpu_rst_i)
		cpu_transfer_req_sreg <= 2'b00;
	else
		cpu_transfer_req_sreg <= {cpu_transfer_req_sreg[0], jtag_reg_trn_i};
end
assign cpu_transfer_req = (cpu_transfer_req_sreg == 2'b01);

// State Machine for cpu operations
(* FSM_ENCODING="SEQUENTIAL", SAFE_IMPLEMENTATION="NO" *) reg [1:0] cpu_fsm_state = 2'b00;
always@(posedge cpu_clk_i)
  if (cpu_rst_i) begin
	cpu_fsm_state <= 2'b00; // Idle state
	cpu_stb_o <= 1'b0;
  end else
  case (cpu_fsm_state)
  2'b00 : begin // Idle state, waiting for read or write access
  	if ( cpu_transfer_req && (jtag_inp_reg_i[HW+AW+DW-2] == 1'b1) ) begin
		// We should start a transfer
		// HW(8bits) 		AW(32bits) 	DW(32bits)
		// [HW+AW+DW-1: AW+DW]	[AW+DW-1: DW]	[DW-1:0]
		// Header ( 8 bits )
		// 7bit: read(0) or write (1) operation
		// 6bit: Bus Operation indicator ( if 1 then reading or writing )
		// 5bit: Stall indicator
		// 4bit: Ack (ack_i) (BUS Mode) or BPI (Status Mode)
		// 3bit: 
		// 2-0bit: Reserved
		cpu_adr_o <= jtag_inp_reg_i[AW+DW-1:DW];
		cpu_we_o  <= jtag_inp_reg_i[HW+AW+DW-1];
		cpu_stb_o <= 1'b1;
		cpu_dat_o <= jtag_inp_reg_i[DW-1:0];
		if (jtag_inp_reg_i[HW+AW+DW-1] == 1'b1) begin
			cpu_fsm_state <= 2'b10; // Write
		end else begin
			cpu_fsm_state <= 2'b01; // Read
		end
	end else begin
		if (cpu_transfer_req) begin
			// We're in status update state
			cpu_stall_o <= jtag_inp_reg_i[HW+AW+DW-3]; // bit 5

			jtag_out_reg_o[HW+AW+DW-3] <= cpu_stall_o;
			jtag_out_reg_o[HW+AW+DW-4] <= cpu_bp_i; // bit 4
			jtag_out_reg_o[DW-1:0] <= 32'hDEADBEEF;
		end else begin
			// We're in idle state
			cpu_stb_o <= 1'b0;
		end
	end
  end
  2'b01 : begin // CPU Bus read access
	if (cpu_ack_i) begin
		cpu_fsm_state <= 2'b00; // Go to idle
		// Push data to the jtag output register
		jtag_out_reg_o[HW+AW+DW-1] <= 1'b0; // Read
		jtag_out_reg_o[HW+AW+DW-4] <= cpu_ack_i;
		jtag_out_reg_o[AW+DW-1:DW] <= cpu_adr_o;
		jtag_out_reg_o[DW-1:0] 	   <= cpu_dat_i;
		// Don't Forget to drop Strobe
		cpu_stb_o <= 1'b0;
	end else begin
		cpu_fsm_state <= 2'b01; // Stay in Bus Read Cycle
	end
  end
  2'b10 : begin // CPU Bus write access
	if (cpu_ack_i) begin
		cpu_fsm_state <= 2'b00; // Go to idle
		// Push data to the jtag output register
		jtag_out_reg_o[HW+AW+DW-1] <= 1'b1; // Write
		jtag_out_reg_o[HW+AW+DW-4] <= cpu_ack_i;

		// Address and Data
		jtag_out_reg_o[AW+DW-1:DW] <= cpu_adr_o;
		jtag_out_reg_o[DW-1:0]     <= cpu_dat_o;
		// Don't Forget to drop Strobe
		cpu_stb_o <= 1'b0;
	end else begin
		cpu_fsm_state  <= 2'b10; // Stay in Bus Write Cycle
	end
  end
  2'b11: begin // Cleaning up after the access? 
	cpu_fsm_state <= 2'b00; // For now just go to idle
  end
  endcase
endmodule
