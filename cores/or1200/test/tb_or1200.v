`include "drift_accum-defines.v"
`timescale 1ns / 1ps

module tb_or1200;

///////////////////////////////////////
// Local Parameters                 //
/////////////////////////////////////
localparam JTAG_TMS = 0;
localparam JTAG_TCK = 1;
localparam JTAG_TDO = 2;

localparam JTAG_TMS_bit = 3'h1;
localparam JTAG_TCK_bit = 3'h2;
localparam JTAG_TDO_bit = 3'h4;

//TODO: Don't forget to fill these
localparam ADBG_TOP_CPU0_DEBUG_MODULE = 2'h1;
localparam ADBG_TOP_WISHBONE_DEBUG_MODULE = 2'h0;
// This is already bit-reversed
localparam ADBG_CRC_POLY = 32'hedb88320;

localparam ADBG_WB_CMD_IREG_WR  = 4'h9;
localparam ADBG_WB_CMD_IREG_SEL = 4'hd;

localparam ADBG_WB_CMD_BWRITE8  = 4'h1;
localparam ADBG_WB_CMD_BWRITE16 = 4'h2;
localparam ADBG_WB_CMD_BWRITE32 = 4'h3;
localparam ADBG_WB_CMD_BREAD8  = 4'h5;
localparam ADBG_WB_CMD_BREAD16 = 4'h6;
localparam ADBG_WB_CMD_BREAD32 = 4'h7;

///////////////////////////////////////
//  Input system clock is 100 MHz   //
/////////////////////////////////////
reg sys_clk = 1'b0;

////////////////////////////////////////////
// QDR and WB Slave  Clocks              //
// As they are generated in real design //
/////////////////////////////////////////

wire mems_clk;
wire mems_rst;

wire wb_clk;
wire wb_rst;

wire spi0_clk;

clkgen clkgen0(
  .adcs_clk_o( ), // Not using this

  .mems_clk_o ( mems_clk ),
  .mems_rst_o ( mems_rst ),

  .wb_clk_o ( wb_clk ),
  .wb_rst_o ( wb_rst ),

  .wbfast_clk_o (  ),
  .wbfast_rst_o (  ),

  .sys_clk_i( sys_clk ),

  .cclock_i (spi0_clk)
);
///////////////////////////////////
//  Memory Bank 0   Chip Itself //
/////////////////////////////////

wire [8:0] mem1_data_i;
wire [8:0] mem1_data_o;
wire [21:0] mem1_addr_o;
wire mem1_bws_o;
wire mem1_wps_o;
wire mem1_rps_o;
wire mem1_doff_o;
wire mem1_wclk_o_p;
wire mem1_wclk_o_n;
wire mem1_rclk_o_p;
wire mem1_rclk_o_n;
wire mem1_rclk_i_p;
wire mem1_rclk_i_n;

cyqdr2_b2 mem0_chip (
  .TCK ( 1'b1 ),
  .TMS ( 1'b1 ),
  .TDI ( 1'b1 ),
  .TDO (  ),

  .D	 ( mem1_data_o ), 	// input data
  .K	 ( mem1_wclk_o_p ), 	// Main clock
  .K_n	 ( mem1_wclk_o_n ), 	// 	input
  .C	 ( mem1_rclk_o_p ), 	// Data Output
  .C_n	 ( mem1_rclk_o_n ), 	// 	clock input
  .RPS_n ( mem1_rps_o ), 		// Read Port Select input
  .WPS_n ( mem1_wps_o ), 	// Write port select input
  .BW0_n ( mem1_bws_o ), 	// Byte Write Select
  .ZQ	 ( 1'b0 ), 		// Programmable Impedance Pin input
  .DOFF_b( mem1_doff_o ), 		// Internal DLL off pin
  .A	 ( mem1_addr_o ), 	// Input Address bus
  .CQ	 ( mem1_rclk_i_p ), 	// Echo clock
  .CQ_n	 ( mem1_rclk_i_n ),	//	output
  .Q	 ( mem1_data_i )  	// Output data
);

wire            wbs_i_mem0_ack_o;
wire            wbs_i_mem0_err_o;
wire            wbs_i_mem0_rty_o;
wire [31:0]     wbs_i_mem0_dat_o;
wire            wbs_i_mem0_cyc_i;
wire [31:0]     wbs_i_mem0_adr_i;
wire            wbs_i_mem0_stb_i;
wire            wbs_i_mem0_we_i;
wire [3:0]      wbs_i_mem0_sel_i;
wire [31:0]     wbs_i_mem0_dat_i;
wire            wbs_i_mem0_cab_i; // Consecutive Address Burst Indicator
wire [2:0]      wbs_i_mem0_cti_i;
wire [1:0]      wbs_i_mem0_bte_i;

wire            wbs_d_mem0_ack_o;
wire            wbs_d_mem0_err_o;
wire            wbs_d_mem0_rty_o;
wire [31:0]     wbs_d_mem0_dat_o;
wire            wbs_d_mem0_cyc_i;
wire [31:0]     wbs_d_mem0_adr_i;
wire            wbs_d_mem0_stb_i;
wire            wbs_d_mem0_we_i;
wire [3:0]      wbs_d_mem0_sel_i;
wire [31:0]     wbs_d_mem0_dat_i;
wire            wbs_d_mem0_cab_i; // Consecutive Address Burst Indicator
wire [2:0]      wbs_d_mem0_cti_i;
wire [1:0]      wbs_d_mem0_bte_i;

qdrii_wb #(
	.iodelay_group_name ("mem0_delay_group"),
	.data0_delay (15),
	.data1_delay (15),
	.data2_delay (15),
	.data3_delay (15),
	.data4_delay (15),
	.data5_delay (15),
	.data6_delay (15),
	.data7_delay (15),
	.data8_delay (15),
	.echo_clk_p_delay ( 15 ),
	.echo_clk_n_delay ( 15 )
) mem0 (
  .wbs0_adr_i( wbs_i_mem0_adr_i ),
  .wbs0_bte_i( wbs_i_mem0_bte_i ),
  .wbs0_cti_i( wbs_i_mem0_cti_i ),
  .wbs0_cyc_i( wbs_i_mem0_cyc_i ),
  .wbs0_dat_i( wbs_i_mem0_dat_i ),
  .wbs0_sel_i( wbs_i_mem0_sel_i ),
  .wbs0_stb_i( wbs_i_mem0_stb_i ),
  .wbs0_we_i( wbs_i_mem0_we_i ),
  .wbs0_cab_i( wbs_i_mem0_cab_i ),
  .wbs0_ack_o( wbs_i_mem0_ack_o ),
  .wbs0_err_o( wbs_i_mem0_err_o ),
  .wbs0_rty_o( wbs_i_mem0_rty_o ),
  .wbs0_dat_o( wbs_i_mem0_dat_o ),
 
  .wbs0_clk_i( wb_clk ),
  .wbs0_rst_i( wb_rst ),

  .wbs1_adr_i( wbs_d_mem0_adr_i ),
  .wbs1_bte_i( wbs_d_mem0_bte_i ),
  .wbs1_cti_i( wbs_d_mem0_cti_i ),
  .wbs1_cyc_i( wbs_d_mem0_cyc_i ),
  .wbs1_dat_i( wbs_d_mem0_dat_i ),
  .wbs1_sel_i( wbs_d_mem0_sel_i ),
  .wbs1_stb_i( wbs_d_mem0_stb_i ),
  .wbs1_we_i ( wbs_d_mem0_we_i ),
  .wbs1_cab_i( wbs_d_mem0_cab_i ),
  .wbs1_ack_o( wbs_d_mem0_ack_o ),
  .wbs1_err_o( wbs_d_mem0_err_o ),
  .wbs1_rty_o( wbs_d_mem0_rty_o ),
  .wbs1_dat_o( wbs_d_mem0_dat_o ),
 
  .wbs1_clk_i( wb_clk ),
  .wbs1_rst_i( wb_rst ),

  .qdr_rps_o ( mem1_rps_o ),
  .qdr_wps_o ( mem1_wps_o ),
  .qdr_bws_o ( mem1_bws_o ),
  .qdr_addr_o( mem1_addr_o ),
  .qdr_data_o( mem1_data_o ),
  .qdr_data_i( mem1_data_i ),

  .qdr_read_clk_p_o     ( mem1_rclk_o_p ),
  .qdr_read_clk_n_o     ( mem1_rclk_o_n ),
 
  .qdr_write_clk_p_o    ( mem1_wclk_o_p ),
  .qdr_write_clk_n_o    ( mem1_wclk_o_n ),
 
  .qdr_echo_clk_p_i     ( mem1_rclk_i_p ),
  .qdr_echo_clk_n_i     ( mem1_rclk_i_n ),
 
  .qdr_clk_i( mems_clk ),
  .qdr_rst_i( mems_rst ),

  .pow_doff_o( mem1_doff_o )
);
///////////////////////////////////////////////////////
////  ROM0 -> CPU0 Boot instruction from it on reset  //
///////////////////////////////////////////////////////
wire [31:0]     wbs_i_rom0_dat_o;
wire            wbs_i_rom0_ack_o;
wire [23:0]     wbs_i_rom0_adr_i;
wire            wbs_i_rom0_stb_i;
wire            wbs_i_rom0_cyc_i;
wire [2:0]      wbs_i_rom0_cti_i;
wire [1:0]      wbs_i_rom0_bte_i;
wire            wbs_i_rom0_err_o;
wire            wbs_i_rom0_rty_o;

rom_wb # (
        .addr_offset (32'h0000_0100)
) rom0 (
  .wb_dat_o ( wbs_i_rom0_dat_o ),
  .wb_ack_o ( wbs_i_rom0_ack_o ),
  .wb_adr_i ( wbs_i_rom0_adr_i ),
  .wb_stb_i ( wbs_i_rom0_stb_i ),
  .wb_cyc_i ( wbs_i_rom0_cyc_i ),
  .wb_cti_i ( wbs_i_rom0_cti_i ),
  .wb_bte_i ( wbs_i_rom0_bte_i ),
  .wb_err_o ( wbs_i_rom0_err_o ),
  .wb_rty_o ( wbs_i_rom0_rty_o ),
  
  .wb_clk_i (wb_clk),
  .wb_rst_i (wb_rst)
);
////////////////////////////
// Ethernet              //
//////////////////////////

wire [31:0]     wbs_d_eth0_dat_i;
wire [31:0]     wbs_d_eth0_dat_o;
wire [31:0]     wbs_d_eth0_adr_i;
wire [3:0]      wbs_d_eth0_sel_i;
wire            wbs_d_eth0_we_i;
wire            wbs_d_eth0_cyc_i;
wire            wbs_d_eth0_stb_i;
wire            wbs_d_eth0_ack_o;
wire            wbs_d_eth0_err_o;

wire [31:0]     wbm_d_eth0_adr_o;
wire [3:0]      wbm_d_eth0_sel_o;
wire            wbm_d_eth0_we_o;
wire [31:0]     wbm_d_eth0_dat_o;
wire [31:0]     wbm_d_eth0_dat_i;
wire            wbm_d_eth0_cyc_o;
wire            wbm_d_eth0_stb_o;
wire            wbm_d_eth0_ack_i;
wire            wbm_d_eth0_err_i;
wire [2:0]      wbm_d_eth0_cti_o;
wire [1:0]      wbm_d_eth0_bte_o;

assign wbm_d_eth0_cyc_o = 1'b0;
assign wbm_d_eth0_stb_o = 1'b0;

//////////////////////////////
// SPI 0 - Config EEPROM   //
////////////////////////////
wire spi0_data0_o;
wire spi0_data1_i;
wire spi0_data2_io;
wire spi0_data3_io;

wire spi0_cs_o;
wire spi0_reset_o;
bscan_spi spi0 (
	.data0_o(spi0_data0_o),         // MOSI
	.data1_i(spi0_data1_i),         // MISO
	.data2_io(spi0_data2_io),       // WP
	.data3_io(spi0_data3_io),       // HOLD
	.cs_o(spi0_cs_o),		// Select

	.clk_o( spi0_clk )
);
assign spi0_reset_o = 1'b1; // So that SPI is not held in reset mode
assign spi0_data1_i = 1'b1;

///////////////////////
//  SPI 1 ////////////
/////////////////////

wire spi1_interrupt;
wire            wbs_b0_spi1_cyc_i;
wire            wbs_b0_spi1_stb_i;
wire [31:0]     wbs_b0_spi1_adr_i;
wire            wbs_b0_spi1_we_i;
wire [7:0]      wbs_b0_spi1_dat_i;
wire [7:0]      wbs_b0_spi1_dat_o;
wire            wbs_b0_spi1_ack_o;

simple_spi spi1 (
  .clk_i( wb_clk ),     // clock
  .rst_i( wb_rst ),     // reset (asynchronous active low)

  .cyc_i( wbs_b0_spi1_cyc_i ),  // cycle
  .stb_i( wbs_b0_spi1_stb_i ),  // strobe
  .adr_i( wbs_b0_spi1_adr_i ),  // address
  .we_i ( wbs_b0_spi1_we_i ),   // write enable
  .dat_i( wbs_b0_spi1_dat_i ),  // data input
  .dat_o( wbs_b0_spi1_dat_o ),  // data output
  .ack_o( wbs_b0_spi1_ack_o ),  // normal bus termination

  .inta_o( spi1_interrupt ),    // interrupt output
  
  .sck_o ( spi1_clk_o ),        // serial clock output
  .ss_o  ( spi1_cs_o  ),        // slave select
  .mosi_o( spi1_data0_io ),     // MasterOut SlaveIN
  .miso_i( spi1_data1_i )       // MasterIn SlaveOut
);
defparam spi1.slave_select_width = 1;
// Bring it out of the reset
assign spi1_reset_o = 1'b1;

////////////////////////////////////////////////////
//  JTAG Simulation  Primitive                   //
//////////////////////////////////////////////////
reg jtag_tck_pad_i = 1'b0;
reg jtag_tms_pad_i = 1'b0;
reg jtag_tdi_pad_i = 1'b0;

wire jtag_tdo_pad_o;

JTAG_SIME2 #(
	.PART_NAME ("7K160T")
) jtag_port (
  .TDO ( jtag_tdo_pad_o ), // output
  .TCK ( jtag_tck_pad_i ), // input
  .TDI ( jtag_tdi_pad_i ), // input
  .TMS ( jtag_tms_pad_i )  // input
);

/////////////////////////////////////
//  Debug Unit                  ////
///////////////////////////////////
wire cpu0_clk;
wire cpu0_rst;

wire [31:0]     wbm_d_dbg0_dat_i;
wire [31:0]     wbm_d_dbg0_adr_o;
wire [31:0]     wbm_d_dbg0_dat_o;
wire            wbm_d_dbg0_cyc_o;
wire            wbm_d_dbg0_stb_o;
wire [3:0]      wbm_d_dbg0_sel_o;
wire            wbm_d_dbg0_we_o;
wire            wbm_d_dbg0_ack_i;
wire            wbm_d_dbg0_err_i;
wire [2:0]      wbm_d_dbg0_cti_o;
wire [1:0]      wbm_d_dbg0_bte_o;

wire [31:0]     dbg0_cpu0_addr;
wire [31:0]     dbg0_cpu0_data;
wire            dbg0_cpu0_stb;
wire            dbg0_cpu0_we;
wire [31:0]     cpu0_dbg0_data;
wire            cpu0_dbg0_ack;
wire            dbg0_cpu0_stall;
wire            cpu0_dbg0_bp;

drift_jtag # (
	.JTAG_REG_WBM( 2 ),
	.JTAG_REG_CPU( 3 )
) cpu0_jtag_debug (
  // JTAG signals are hidden inside the FPGA

  //---> Wishbone Master Signals
  .wbm_adr_o( wbm_d_dbg0_adr_o ),
  .wbm_bte_o( wbm_d_dbg0_bte_o ),
  .wbm_cti_o( wbm_d_dbg0_cti_o ),
  .wbm_cyc_o( wbm_d_dbg0_cyc_o ),
  .wbm_dat_o( wbm_d_dbg0_dat_o ),
  .wbm_sel_o( wbm_d_dbg0_sel_o ),
  .wbm_stb_o( wbm_d_dbg0_stb_o ),
  .wbm_we_o ( wbm_d_dbg0_we_o ),
  .wbm_ack_i( wbm_d_dbg0_ack_i ),
  .wbm_err_i( wbm_d_dbg0_err_i ),
  .wbm_rty_i( 1'b0 ),
  .wbm_dat_i( wbm_d_dbg0_dat_i ),

  .wbm_clk_i( wb_clk ),
  .wbm_rst_i( wb_rst ),
  //---> OR1200 control Signals
  .cpu_bp_i	( cpu0_dbg0_bp ),
  .cpu_stall_o	( dbg0_cpu0_stall ),

  .cpu_adr_o ( dbg0_cpu0_addr ),
  .cpu_dat_o ( dbg0_cpu0_data ),
  .cpu_stb_o ( dbg0_cpu0_stb  ),
  .cpu_we_o  ( dbg0_cpu0_we   ),
  .cpu_dat_i ( cpu0_dbg0_data ),
  .cpu_ack_i ( cpu0_dbg0_ack ),

  .cpu_clk_i ( cpu0_clk ),
  .cpu_rst_i ( cpu0_rst )

);

/////////////////////////////////
//  OR1200                    //
///////////////////////////////
wire [19:0]     cpu0_interrupts;
assign cpu0_interrupts[ 0] = 0; // Non-maskable inside OR1200
assign cpu0_interrupts[ 1] = 0; // Non-maskable inside OR1200
assign cpu0_interrupts[ 2] = 0; // UART
assign cpu0_interrupts[ 3] = 0; // GPIO
assign cpu0_interrupts[ 4] = 0; // ETH0
assign cpu0_interrupts[ 5] = 0;
assign cpu0_interrupts[ 6] = 0; // SPI0
assign cpu0_interrupts[ 7] = 0;
assign cpu0_interrupts[ 8] = 0;
assign cpu0_interrupts[ 9] = 0;
assign cpu0_interrupts[10] = 0; // I2C
assign cpu0_interrupts[11] = 0; // DMA
assign cpu0_interrupts[12] = 0;
assign cpu0_interrupts[13] = 0;
assign cpu0_interrupts[14] = 0;
assign cpu0_interrupts[15] = 0;
assign cpu0_interrupts[16] = 0;
assign cpu0_interrupts[17] = 0;
assign cpu0_interrupts[18] = 0;
assign cpu0_interrupts[19] = 0;

wire            wbm_i_cpu0_ack_i;
wire            wbm_i_cpu0_err_i;
wire            wbm_i_cpu0_rty_i;
wire [31:0]     wbm_i_cpu0_dat_i;
wire            wbm_i_cpu0_cyc_o;
wire [31:0]     wbm_i_cpu0_adr_o;
wire            wbm_i_cpu0_stb_o;
wire            wbm_i_cpu0_we_o;
wire [3:0]      wbm_i_cpu0_sel_o;
wire [31:0]     wbm_i_cpu0_dat_o;
wire            wbm_i_cpu0_cab_o; // Consecutive Address Burst Indicator (Default Undefined)
wire [2:0]      wbm_i_cpu0_cti_o;
wire [1:0]      wbm_i_cpu0_bte_o;

wire            wbm_d_cpu0_ack_i;
wire            wbm_d_cpu0_err_i;
wire            wbm_d_cpu0_rty_i;
wire [31:0]     wbm_d_cpu0_dat_i;
wire            wbm_d_cpu0_cyc_o;
wire [31:0]     wbm_d_cpu0_adr_o;
wire            wbm_d_cpu0_stb_o;
wire            wbm_d_cpu0_we_o;
wire [3:0]      wbm_d_cpu0_sel_o;
wire [31:0]     wbm_d_cpu0_dat_o;
wire            wbm_d_cpu0_cab_o;
wire [2:0]      wbm_d_cpu0_cti_o;
wire [1:0]      wbm_d_cpu0_bte_o;

or1200_top cpu0 (
  .clk_i        ( cpu0_clk ),
  .rst_i        ( cpu0_rst ),
  .pic_ints_i   ( cpu0_interrupts ),
  .clmode_i     ( 2'b00 ),

  .iwb_clk_i ( wb_clk ),
  .iwb_rst_i ( wb_rst ),

  .iwb_ack_i ( wbm_i_cpu0_ack_i ),
  .iwb_err_i ( wbm_i_cpu0_err_i ),
  .iwb_rty_i ( wbm_i_cpu0_rty_i ),
  .iwb_dat_i ( wbm_i_cpu0_dat_i ),
  .iwb_cyc_o ( wbm_i_cpu0_cyc_o ),
  .iwb_adr_o ( wbm_i_cpu0_adr_o ),
  .iwb_stb_o ( wbm_i_cpu0_stb_o ),
  .iwb_we_o  ( wbm_i_cpu0_we_o ),
  .iwb_sel_o ( wbm_i_cpu0_sel_o ),
  .iwb_dat_o ( wbm_i_cpu0_dat_o ),
`ifdef OR1200_WB_CAB
  .iwb_cab_o ( wbm_i_cpu0_cab_o ),
`endif
`ifdef OR1200_WB_B3
  .iwb_cti_o ( wbm_i_cpu0_cti_o ),
  .iwb_bte_o ( wbm_i_cpu0_bte_o ),
`endif

  .dwb_clk_i ( wb_clk ),
  .dwb_rst_i ( wb_rst ),

  .dwb_ack_i ( wbm_d_cpu0_ack_i ),
  .dwb_err_i ( wbm_d_cpu0_err_i ),
  .dwb_rty_i ( wbm_d_cpu0_rty_i ),
  .dwb_dat_i ( wbm_d_cpu0_dat_i ),
  .dwb_cyc_o ( wbm_d_cpu0_cyc_o ),
  .dwb_adr_o ( wbm_d_cpu0_adr_o ),
  .dwb_stb_o ( wbm_d_cpu0_stb_o ),
  .dwb_we_o  ( wbm_d_cpu0_we_o ),
  .dwb_sel_o ( wbm_d_cpu0_sel_o ),
  .dwb_dat_o ( wbm_d_cpu0_dat_o ),
`ifdef OR1200_WB_CAB
  .dwb_cab_o ( wbm_d_cpu0_cab_o ),
`endif
`ifdef OR1200_WB_B3
  .dwb_cti_o ( wbm_d_cpu0_cti_o ),
  .dwb_bte_o ( wbm_d_cpu0_bte_o ),
`endif
  .dbg_stall_i ( dbg0_cpu0_stall ),
  .dbg_ewt_i   (1'b0),  /* External Watchpoint Trigger Input,   Not Connected */
  .dbg_lss_o   (  ),    /* External Load/Store Unit Status,     Not Connected */
  .dbg_is_o    (  ),    /* External Insn Fetch Status,          Not Connected */
  .dbg_wp_o    (  ),    /* Watchpoints Outputs,                 Not Connected */
  .dbg_bp_o    ( cpu0_dbg0_bp ),

  .dbg_adr_i   ( dbg0_cpu0_addr ),
  .dbg_we_i    ( dbg0_cpu0_we ),
  .dbg_stb_i   ( dbg0_cpu0_stb ),
  .dbg_dat_i   ( dbg0_cpu0_data ),
  .dbg_dat_o   ( cpu0_dbg0_data ),
  .dbg_ack_o   ( cpu0_dbg0_ack ),

  .pm_cpustall_i        ( 1'b0 ),

  .pm_clksd_o           (  ),
  .pm_dc_gate_o         (  ),
  .pm_ic_gate_o         (  ),
  .pm_dmmu_gate_o       (  ),
  .pm_immu_gate_o       (  ),
  .pm_tt_gate_o         (  ),
  .pm_cpu_gate_o        (  ),
  .pm_wakeup_o          (  ),
  .pm_lvolt_o           (  ),
 
  .sig_tick (  ) /* Output, Not Connected */
);
assign cpu0_clk = wb_clk;
assign cpu0_rst = wb_rst;

`ifndef OR1200_WB_CAB
assign wbm_i_cpu0_cab_o = 1'b0;
assign wbm_d_cpu0_cab_o = 1'b0;
`endif
`ifndef OR1200_WB_B3
assign wbm_i_cpu0_cti_o = 3'b000;
assign wbm_d_cpu0_cti_o = 3'b000;
assign wbm_i_cpu0_bte_o = 2'b00;
assign wbm_d_cpu0_bte_o = 2'b00;
`endif

arbiter_ibus #(
	.wb_addr_match_width ( 4 ),
	.slave0_adr(4'h0), // MEM0: Resulting Address: 0x0000_0000
	.slave1_adr(4'hf)  // ROM0: 0xF000_0000
)ibus(
  .wbm0_adr_i( wbm_i_cpu0_adr_o ),
  .wbm0_dat_i( wbm_i_cpu0_dat_o ),
  .wbm0_sel_i( wbm_i_cpu0_sel_o ),
  .wbm0_we_i ( wbm_i_cpu0_we_o ),
  .wbm0_cyc_i( wbm_i_cpu0_cyc_o ),
  .wbm0_stb_i( wbm_i_cpu0_stb_o ),
  .wbm0_cti_i( wbm_i_cpu0_cti_o ),
  .wbm0_bte_i( wbm_i_cpu0_bte_o ),
  .wbm0_dat_o( wbm_i_cpu0_dat_i ),
  .wbm0_ack_o( wbm_i_cpu0_ack_i ),
  .wbm0_err_o( wbm_i_cpu0_err_i ),
  .wbm0_rty_o( wbm_i_cpu0_rty_i ),

  .wbs0_adr_o( wbs_i_mem0_adr_i ),
  .wbs0_dat_o( wbs_i_mem0_dat_i ),
  .wbs0_sel_o( wbs_i_mem0_sel_i ),
  .wbs0_we_o ( wbs_i_mem0_we_i ),
  .wbs0_cyc_o( wbs_i_mem0_cyc_i ),
  .wbs0_stb_o( wbs_i_mem0_stb_i ),
  .wbs0_cti_o( wbs_i_mem0_cti_i ),
  .wbs0_bte_o( wbs_i_mem0_bte_i ),
  .wbs0_dat_i( wbs_i_mem0_dat_o ),
  .wbs0_ack_i( wbs_i_mem0_ack_o ),
  .wbs0_err_i( wbs_i_mem0_err_o ),
  .wbs0_rty_i( wbs_i_mem0_rty_o ),

  .wbs1_adr_o( wbs_i_rom0_adr_i ),
  .wbs1_dat_o(  ), // Read-only
  .wbs1_sel_o(  ), // Read-only
  .wbs1_we_o (  ), // Read-only
  .wbs1_cyc_o( wbs_i_rom0_cyc_i ),
  .wbs1_stb_o( wbs_i_rom0_stb_i ),
  .wbs1_cti_o( wbs_i_rom0_cti_i ),
  .wbs1_bte_o( wbs_i_rom0_bte_i ),
  .wbs1_dat_i( wbs_i_rom0_dat_o ),
  .wbs1_ack_i( wbs_i_rom0_ack_o ),
  .wbs1_err_i( wbs_i_rom0_err_o ),
  .wbs1_rty_i( wbs_i_rom0_rty_o ),

  .wb_clk_i ( wb_clk ),
  .wb_rst_i ( wb_rst )
);

wire            wbs_d_bbus0_ack_o;
wire            wbs_d_bbus0_err_o;
wire            wbs_d_bbus0_rty_o;
wire [31:0]     wbs_d_bbus0_dat_o;
wire            wbs_d_bbus0_cyc_i;
wire [31:0]     wbs_d_bbus0_adr_i;
wire            wbs_d_bbus0_stb_i;
wire            wbs_d_bbus0_we_i;
wire [3:0]      wbs_d_bbus0_sel_i;
wire [31:0]     wbs_d_bbus0_dat_i;
wire [2:0]      wbs_d_bbus0_cti_i;
wire [1:0]      wbs_d_bbus0_bte_i;

wire            wbs_d_bbus1_ack_o;
wire            wbs_d_bbus1_err_o;
wire            wbs_d_bbus1_rty_o;
wire [31:0]     wbs_d_bbus1_dat_o;
wire            wbs_d_bbus1_cyc_i;
wire [31:0]     wbs_d_bbus1_adr_i;
wire            wbs_d_bbus1_stb_i;
wire            wbs_d_bbus1_we_i;
wire [3:0]      wbs_d_bbus1_sel_i;
wire [31:0]     wbs_d_bbus1_dat_i;
wire [2:0]      wbs_d_bbus1_cti_i;
wire [1:0]      wbs_d_bbus1_bte_i;

wb_switch_b3 # (
	.dw ( 32 ),
	.aw ( 32 ),
	.slave0_sel_width( 8 ),     // Width of selection bits 8 == [31:24]
	.slave0_sel_addr ( 8'h00 ), // Matching value of the sel_width bits
	.slave0_sel_mask ( 8'hFF ),
	.slave1_sel_width( 8 ),
	.slave1_sel_addr ( 8'h01 ),
	.slave1_sel_mask ( 8'hFF ),
	.slave2_sel_width( 8 ),
	.slave2_sel_addr ( 8'b1000_0000),
	.slave2_sel_mask ( 8'b1100_1111 ), // When mask is 0 -> don't care
	.slave3_sel_width( 4 ),
	.slave3_sel_addr ( 4'h7 ),
	.slave3_sel_mask ( 4'hF ),
	.slave4_sel_width( 8 ),
	.slave4_sel_addr ( 8'h92 ),
	.slave4_sel_mask ( 8'hFF )
)dbus(
  .wbm0_adr_i( wbm_d_cpu0_adr_o ),
  .wbm0_bte_i( wbm_d_cpu0_bte_o ),
  .wbm0_cti_i( wbm_d_cpu0_cti_o ),
  .wbm0_cyc_i( wbm_d_cpu0_cyc_o ),
  .wbm0_dat_i( wbm_d_cpu0_dat_o ),
  .wbm0_sel_i( wbm_d_cpu0_sel_o ),
  .wbm0_stb_i( wbm_d_cpu0_stb_o ),
  .wbm0_we_i ( wbm_d_cpu0_we_o ),
  .wbm0_ack_o( wbm_d_cpu0_ack_i ),
  .wbm0_err_o( wbm_d_cpu0_err_i ),
  .wbm0_rty_o( wbm_d_cpu0_rty_i ),
  .wbm0_dat_o( wbm_d_cpu0_dat_i ),

  .wbm1_adr_i( wbm_d_dbg0_adr_o ),
  .wbm1_bte_i( wbm_d_dbg0_bte_o ),
  .wbm1_cti_i( wbm_d_dbg0_cti_o ),
  .wbm1_cyc_i( wbm_d_dbg0_cyc_o ),
  .wbm1_dat_i( wbm_d_dbg0_dat_o ),
  .wbm1_sel_i( wbm_d_dbg0_sel_o ),
  .wbm1_stb_i( wbm_d_dbg0_stb_o ),
  .wbm1_we_i ( wbm_d_dbg0_we_o ),
  .wbm1_ack_o( wbm_d_dbg0_ack_i ),
  .wbm1_err_o( wbm_d_dbg0_err_i ),
  .wbm1_rty_o(  ),
  .wbm1_dat_o( wbm_d_dbg0_dat_i ),

  .wbm2_adr_i( wbm_d_eth0_adr_o ),
  .wbm2_bte_i( wbm_d_eth0_bte_o ),
  .wbm2_cti_i( wbm_d_eth0_cti_o ),
  .wbm2_cyc_i( wbm_d_eth0_cyc_o ),
  .wbm2_dat_i( wbm_d_eth0_dat_o ),
  .wbm2_sel_i( wbm_d_eth0_sel_o ),
  .wbm2_stb_i( wbm_d_eth0_stb_o ),
  .wbm2_we_i ( wbm_d_eth0_we_o ),
  .wbm2_ack_o( wbm_d_eth0_ack_i ),
  .wbm2_err_o( wbm_d_eth0_err_i ),
  .wbm2_rty_o(  ),
  .wbm2_dat_o( wbm_d_eth0_dat_i ),

  .wbs0_adr_o( wbs_d_mem0_adr_i ),
  .wbs0_bte_o( wbs_d_mem0_bte_i ),
  .wbs0_cti_o( wbs_d_mem0_cti_i ),
  .wbs0_cyc_o( wbs_d_mem0_cyc_i ),
  .wbs0_dat_o( wbs_d_mem0_dat_i ),
  .wbs0_sel_o( wbs_d_mem0_sel_i ),
  .wbs0_stb_o( wbs_d_mem0_stb_i ),
  .wbs0_we_o ( wbs_d_mem0_we_i ),
  .wbs0_ack_i( wbs_d_mem0_ack_o ),
  .wbs0_err_i( wbs_d_mem0_err_o ),
  .wbs0_rty_i( wbs_d_mem0_rty_o ),
  .wbs0_dat_i( wbs_d_mem0_dat_o ),

  .wbs1_adr_o( wbs_d_mem1_adr_i ),
  .wbs1_bte_o( wbs_d_mem1_bte_i ),
  .wbs1_cti_o( wbs_d_mem1_cti_i ),
  .wbs1_cyc_o( wbs_d_mem1_cyc_i ),
  .wbs1_dat_o( wbs_d_mem1_dat_i ),
  .wbs1_sel_o( wbs_d_mem1_sel_i ),
  .wbs1_stb_o( wbs_d_mem1_stb_i ),
  .wbs1_we_o ( wbs_d_mem1_we_i ),
  .wbs1_ack_i( wbs_d_mem1_ack_o ),
  .wbs1_err_i( wbs_d_mem1_err_o ),
  .wbs1_rty_i( wbs_d_mem1_rty_o ),
  .wbs1_dat_i( wbs_d_mem1_dat_o ),
 
  .wbs2_adr_o( wbs_d_bbus0_adr_i ),
  .wbs2_bte_o( wbs_d_bbus0_bte_i ),
  .wbs2_cti_o( wbs_d_bbus0_cti_i ),
  .wbs2_cyc_o( wbs_d_bbus0_cyc_i ),
  .wbs2_dat_o( wbs_d_bbus0_dat_i ),
  .wbs2_sel_o( wbs_d_bbus0_sel_i ),
  .wbs2_stb_o( wbs_d_bbus0_stb_i ),
  .wbs2_we_o ( wbs_d_bbus0_we_i ),
  .wbs2_ack_i( wbs_d_bbus0_ack_o ),
  .wbs2_err_i( wbs_d_bbus0_err_o ),
  .wbs2_rty_i( wbs_d_bbus0_rty_o ),
  .wbs2_dat_i( wbs_d_bbus0_dat_o ),

  .wbs3_adr_o( wbs_d_bbus1_adr_i ),
  .wbs3_bte_o( wbs_d_bbus1_bte_i ),
  .wbs3_cti_o( wbs_d_bbus1_cti_i ),
  .wbs3_cyc_o( wbs_d_bbus1_cyc_i ),
  .wbs3_dat_o( wbs_d_bbus1_dat_i ),
  .wbs3_sel_o( wbs_d_bbus1_sel_i ),
  .wbs3_stb_o( wbs_d_bbus1_stb_i ),
  .wbs3_we_o ( wbs_d_bbus1_we_i ),
  .wbs3_ack_i( wbs_d_bbus1_ack_o ),
  .wbs3_err_i( wbs_d_bbus1_err_o ),
  .wbs3_rty_i( wbs_d_bbus1_rty_o ),
  .wbs3_dat_i( wbs_d_bbus1_dat_o ),

  .wbs4_adr_o( wbs_d_eth0_adr_i ),
  .wbs4_bte_o(  ),
  .wbs4_cti_o(  ),
  .wbs4_cyc_o( wbs_d_eth0_cyc_i ),
  .wbs4_dat_o( wbs_d_eth0_dat_i ),
  .wbs4_sel_o( wbs_d_eth0_sel_i ),
  .wbs4_stb_o( wbs_d_eth0_stb_i ),
  .wbs4_we_o ( wbs_d_eth0_we_i ),
  .wbs4_ack_i( wbs_d_eth0_ack_o ),
  .wbs4_err_i( wbs_d_eth0_err_o ),
  .wbs4_rty_i( 1'b0 ),
  .wbs4_dat_i( wbs_d_eth0_dat_o ),

  .wb_clk ( wb_clk ),
  .wb_rst ( wb_rst )
);
//--------------------------------------------------------------------//
arbiter_bbus#(
	.wb_addr_match_width (8),
	.slave0_adr(8'h90),
	.slave1_adr(8'hA0),
	.slave2_adr(8'hD0),
	.slave3_adr(8'hC0),
	.slave4_adr(8'hB0),
	.slave5_adr(8'h80),
	.slave6_adr(8'hE0),
	.slave7_adr(8'hF0)
) bbus0 (
  .wbm0_adr_i( wbs_d_bbus0_adr_i ),
  .wbm0_dat_i( wbs_d_bbus0_dat_i ),
  .wbm0_sel_i( wbs_d_bbus0_sel_i ),
  .wbm0_we_i ( wbs_d_bbus0_we_i ),
  .wbm0_cyc_i( wbs_d_bbus0_cyc_i ),
  .wbm0_stb_i( wbs_d_bbus0_stb_i ),
  .wbm0_cti_i( wbs_d_bbus0_cti_i ),
  .wbm0_bte_i( wbs_d_bbus0_bte_i ),
  .wbm0_dat_o( wbs_d_bbus0_dat_o ),
  .wbm0_ack_o( wbs_d_bbus0_ack_o ),
  .wbm0_err_o( wbs_d_bbus0_err_o ),
  .wbm0_rty_o( wbs_d_bbus0_rty_o ),

  .wbs0_adr_o( wbs_b0_uart0_adr_i ),
  .wbs0_dat_o( wbs_b0_uart0_dat_i ),
  .wbs0_we_o ( wbs_b0_uart0_we_i ),
  .wbs0_cyc_o( wbs_b0_uart0_cyc_i ),
  .wbs0_stb_o( wbs_b0_uart0_stb_i ),
  .wbs0_cti_o(  ),
  .wbs0_bte_o(  ),
  .wbs0_dat_i( wbs_b0_uart0_dat_o ),
  .wbs0_ack_i( wbs_b0_uart0_ack_o ),
  .wbs0_err_i( 1'b0 ),
  .wbs0_rty_i( 1'b0 ),

  .wbs1_adr_o(  ),
  .wbs1_dat_o(  ),
  .wbs1_we_o (  ),
  .wbs1_cyc_o(  ),
  .wbs1_stb_o(  ),
  .wbs1_cti_o(  ),
  .wbs1_bte_o(  ),
  .wbs1_dat_i( 8'h00 ),
  .wbs1_ack_i( 1'b0 ),
  .wbs1_err_i( 1'b0 ),
  .wbs1_rty_i( 1'b0 ),

  .wbs2_adr_o(  ),
  .wbs2_dat_o(  ),
  .wbs2_we_o (  ),
  .wbs2_cyc_o(  ),
  .wbs2_stb_o(  ),
  .wbs2_cti_o(  ),
  .wbs2_bte_o(  ),
  .wbs2_dat_i( 8'h00 ),
  .wbs2_ack_i( 1'b0 ),
  .wbs2_err_i( 1'b0 ),
  .wbs2_rty_i( 1'b0 ),

  .wbs3_adr_o(  ),
  .wbs3_dat_o(  ),
  .wbs3_we_o (  ),
  .wbs3_cyc_o(  ),
  .wbs3_stb_o(  ),
  .wbs3_cti_o(  ),
  .wbs3_bte_o(  ),
  .wbs3_dat_i( 8'h00 ),
  .wbs3_ack_i( 1'b0 ),
  .wbs3_err_i( 1'b0 ),
  .wbs3_rty_i( 1'b0 ),

  .wbs4_adr_o( wbs_b0_spi1_adr_i ),
  .wbs4_dat_o( wbs_b0_spi1_dat_i ),
  .wbs4_we_o ( wbs_b0_spi1_we_i ),
  .wbs4_cyc_o( wbs_b0_spi1_cyc_i ),
  .wbs4_stb_o( wbs_b0_spi1_stb_i ),
  .wbs4_cti_o(  ),
  .wbs4_bte_o(  ),
  .wbs4_dat_i( wbs_b0_spi1_dat_o ),
  .wbs4_ack_i( wbs_b0_spi1_ack_o ),
  .wbs4_err_i( 1'b0 ),
  .wbs4_rty_i( 1'b0 ),

  .wbs5_adr_o(  ),
  .wbs5_dat_o(  ),
  .wbs5_we_o (  ),
  .wbs5_cyc_o(  ),
  .wbs5_stb_o(  ),
  .wbs5_cti_o(  ),
  .wbs5_bte_o(  ),
  .wbs5_dat_i( 8'h00 ),
  .wbs5_ack_i( 1'b0 ),
  .wbs5_err_i( 1'b0 ),
  .wbs5_rty_i( 1'b0 ),

  .wbs6_adr_o(  ),
  .wbs6_dat_o(  ),
  .wbs6_we_o (  ),
  .wbs6_cyc_o(  ),
  .wbs6_stb_o(  ),
  .wbs6_cti_o(  ),
  .wbs6_bte_o(  ),
  .wbs6_dat_i( 8'h00 ),
  .wbs6_ack_i( 1'b0 ),
  .wbs6_err_i( 1'b0 ),
  .wbs6_rty_i( 1'b0 ),

  .wbs7_adr_o(  ),
  .wbs7_dat_o(  ),
  .wbs7_we_o (  ),
  .wbs7_cyc_o(  ),
  .wbs7_stb_o(  ),
  .wbs7_cti_o(  ),
  .wbs7_bte_o(  ),
  .wbs7_dat_i( 8'h00 ),
  .wbs7_ack_i( 1'b0 ),
  .wbs7_err_i( 1'b0 ),
  .wbs7_rty_i( 1'b0 ),

  .wb_clk_i ( wb_clk ),
  .wb_rst_i ( wb_rst )
);
//===================  SAVING TO THE FILE ===========================//
initial begin
  $dumpfile( "tb_or1200.vcd" );
  $dumpvars;
end
//=================  Clocks ==========================================//
always #5.0 sys_clk = ~sys_clk; // 100 MHz
//================ Adv. Debug High level Tasks =======================//
// Data which will be written to the WB interface
reg [31:0] adbg_static_data32 [0:15];
reg [15:0] adbg_static_data16 [0:15];
reg [7:0]  adbg_static_data8 [0:15];
// Arrays to hold data read back from the WB interface, for comparison
reg [31:0] adbg_input_data32 [0:15];
reg [15:0] adbg_input_data16 [0:15];
reg [7:0]  adbg_input_data8 [0:15];
//-------------------------------------------------------------------//
task adbg_compute_crc;
	input [31:0] crc_in;
	input [31:0] data_in;
	input [5:0] length_bits;
	output [31:0] crc_out;

	integer i;
	reg [31:0] d;
	reg [31:0] c;
begin
  crc_out = crc_in;
  for(i = 0; i < length_bits; i = i+1) begin
	d = (data_in[i]) ? 32'hffffffff : 32'h0;
	c = (crc_out[0]) ? 32'hffffffff : 32'h0;
	crc_out = crc_out >> 1;
	crc_out = crc_out ^ ((d ^ c) & ADBG_CRC_POLY);
  end
end
endtask
//-------------------------------------------------------------------//
task adbg_select_debug_module;
	input [1:0] moduleid;

	reg validid;
begin
  jtag_write_bit(JTAG_TMS_bit);	// select_dr_scan
  jtag_write_bit(3'h0);		// capture_ir
  jtag_write_bit(3'h0);		// shift_ir
  jtag_write_stream({1'b1,moduleid}, 8'h3, 1);  // write data, exit_1
  jtag_write_bit(JTAG_TMS_bit);	// update_dr
  jtag_write_bit(3'h0);		// idle
end
endtask
//-------------------------------------------------------------------//
task adbg_send_module_burst_command;
	input [3:0] opcode;
	input [31:0] address;
	input [15:0] burstlength;

	reg [63:0] streamdata;
begin
  streamdata = {11'h0,1'b0,opcode,address,burstlength};
  jtag_write_bit(JTAG_TMS_bit);	// select_dr_scan
  jtag_write_bit(3'h0);		// capture_ir
  jtag_write_bit(3'h0);		// shift_ir
  jtag_write_stream(streamdata, 8'd53, 1);	// write data, exit_1
  jtag_write_bit(JTAG_TMS_bit);	// update_dr
  jtag_write_bit(3'h0);		// idle
end
endtask
//-------------------------------------------------------------------//
task adbg_select_module_internal_register;  // Really just a read, with discarded data
	input [31:0] regidx;
	input [7:0] len;  // the length of the register index data, we assume not more than 32

	reg[63:0] streamdata;
begin
  streamdata = 64'h0;
  streamdata = streamdata | regidx;
  streamdata = streamdata | (ADBG_WB_CMD_IREG_SEL << len);
  jtag_write_bit(JTAG_TMS_bit);	// select_dr_scan
  jtag_write_bit(3'h0);		// capture_ir
  jtag_write_bit(3'h0);		// shift_ir
  jtag_write_stream(streamdata, (len+5), 1);  // write data, exit_1
  jtag_write_bit(JTAG_TMS_bit);	// update_dr
  jtag_write_bit(3'h0);		// idle
end
endtask
//-------------------------------------------------------------------//
task adbg_read_module_internal_register;  // We assume the register is already selected
	input [7:0] len;  // the length of the data desired, we assume a max of 64 bits
	output [63:0] instream;

	reg [63:0] bitmask;
begin
  instream = 64'h0;
  // We shift out all 0's, which is a NOP to the debug unit
  jtag_write_bit(JTAG_TMS_bit);	// select_dr_scan
  jtag_write_bit(3'h0);		// capture_ir
  jtag_write_bit(3'h0);	// shift_ir
  // Shift at least 5 bits, as this is the min, for a valid NOP
  jtag_read_write_stream(64'h0, len+4,1,instream);  // exit_1
  jtag_write_bit(JTAG_TMS_bit);  // update_dr
  jtag_write_bit(3'h0);           // idle
  bitmask = 64'hffffffffffffffff;
  bitmask = bitmask << len;
  bitmask = ~bitmask;
  instream = instream & bitmask;  // Cut off any unwanted excess bits
end
endtask
//-------------------------------------------------------------------//
task adbg_write_module_internal_register;
	input [31:0] regidx; 	// the length of the register index data
	input [7:0] idxlen;
	input [63:0] writedata;
	input [7:0] datalen;  	// the length of the data to write.  
				// We assume the two length combined are 59 or less.
	reg[63:0] streamdata;
begin
  streamdata = 64'h0;  // This will 0 the toplevel/module select bit
  streamdata = streamdata | writedata;
  streamdata = streamdata | (regidx << datalen);
  streamdata = streamdata | (ADBG_WB_CMD_IREG_WR << (idxlen+datalen));

  jtag_write_bit(JTAG_TMS_bit);	// select_dr_scan
  jtag_write_bit(3'h0);		// capture_ir
  jtag_write_bit(3'h0);		// shift_ir
  jtag_write_stream(streamdata, (idxlen+datalen+5), 1);  // write data, exit_1
  jtag_write_bit(JTAG_TMS_bit);	// update_dr
  jtag_write_bit(3'h0);		// idle
end
endtask
//-------------------------------------------------------------------//
task adbg_do_module_burst_read;
	input [5:0] word_size_bytes;
	input [15:0] word_count;
	input [31:0] start_address;

	reg [3:0] opcode;
	reg status;
	reg [63:0] instream;
	integer i;
	integer j;
	reg [31:0] crc_calc_i;
	reg [31:0] crc_calc_o;  // temp signal...
	reg [31:0] crc_read;
	reg [5:0] word_size_bits;
begin
  instream = 64'h0;
  word_size_bits = word_size_bytes << 3;
  crc_calc_i = 32'hffffffff;
  // Send the command
  case (word_size_bytes)
	3'h1: opcode = ADBG_WB_CMD_BREAD8;
	3'h2: opcode = ADBG_WB_CMD_BREAD16;
	3'h4: opcode = ADBG_WB_CMD_BREAD32;
	default: begin
		$display("Tried burst read with invalid word size (%0x), defaulting to 4-byte words", 
			word_size_bytes);
		opcode = ADBG_WB_CMD_BREAD32;
	end
  endcase
  adbg_send_module_burst_command(opcode,start_address, word_count);  // returns to state idle
  #100.0;
  jtag_set_ir(6'b001001); // 0x09 -> IDCODE
  #100.0;
  jtag_set_ir(6'b000011); // 0x03 -> USER2
  #100.0;
  // Get us back to shift_dr mode to read a burst
  jtag_write_bit(JTAG_TMS_bit);	// select_dr_scan
  jtag_write_bit(3'h0);		// capture_ir
  jtag_write_bit(3'h0);		// shift_ir

  // Get 1 status bit, then word_size_bytes*8 bits
  status = 1'b0;
  j = 0;
  while(!status) begin
	jtag_read_write_bit(3'h0, status);
	j = j + 1;
  end
  // Now, repeat...
  for(i = 0; i < word_count; i=i+1) begin
	jtag_read_write_stream(64'h0, {2'h0,(word_size_bytes<<3)},0,instream);
	adbg_compute_crc(crc_calc_i, instream[31:0], word_size_bits, crc_calc_o);
	crc_calc_i = crc_calc_o;
	if(word_size_bytes == 1) adbg_input_data8[i] = instream[7:0];
	else if(word_size_bytes == 2) adbg_input_data16[i] = instream[15:0];
	else adbg_input_data32[i] = instream[31:0];
  end
  // Read the data CRC from the debug module.
  jtag_read_write_stream(64'h0, 6'd32, 1, crc_read);
  if(crc_calc_o != crc_read) 
  	$display("CRC ERROR! Computed 0x%x, read CRC 0x%x", crc_calc_o, crc_read);
  jtag_write_bit(JTAG_TMS_bit);	// update_ir
  jtag_write_bit(3'h0);		// idle
end
endtask
//-------------------------------------------------------------------//
task adbg_do_module_burst_write;
	input [5:0] word_size_bytes;
	input [15:0] word_count;
	input [31:0] start_address;

	reg [3:0] opcode;
	reg status;
	reg [63:0] dataword;
	integer i;
	integer j;
	reg [31:0] crc_calc_i;
	reg [31:0] crc_calc_o;
	reg crc_match;
	reg [5:0] word_size_bits;
begin
  word_size_bits = word_size_bytes << 3;
  crc_calc_i = 32'hffffffff;
  // Send the command
  case (word_size_bytes)
	3'h1: opcode = ADBG_WB_CMD_BWRITE8;
	3'h2: opcode = ADBG_WB_CMD_BWRITE16;
	3'h4: opcode = ADBG_WB_CMD_BWRITE32;
	default: begin
		$display("Tried burst write with invalid word size (%0x), defaulting to 4-byte words", 
			word_size_bytes);
		opcode = ADBG_WB_CMD_BWRITE32;
	end
  endcase
  adbg_send_module_burst_command(opcode, start_address, word_count);  // returns to state idle
  // Get us back to shift_dr mode to write a burst
  jtag_write_bit(JTAG_TMS_bit);  // select_dr_scan
  jtag_write_bit(3'h0);           // capture_ir
  jtag_write_bit(3'h0);           // shift_ir
  // Write a start bit (a 1) so it knows when to start counting
  jtag_write_bit(JTAG_TDO_bit);
  // Now, repeat...
  for(i = 0; i < word_count; i=i+1) begin
  	// Write word_size_bytes*8 bits, then get 1 status bit
	if(word_size_bytes == 4)      dataword = {32'h0, adbg_static_data32[i]};
	else if(word_size_bytes == 2) dataword = {48'h0, adbg_static_data16[i]};
	else                          dataword = {56'h0, adbg_static_data8[i]};
	jtag_write_stream(dataword, {2'h0,(word_size_bytes<<3)},0);
	adbg_compute_crc(crc_calc_i, dataword[31:0], word_size_bits, crc_calc_o);
	crc_calc_i = crc_calc_o;
  end
  // Send the CRC we computed
  jtag_write_stream(crc_calc_o, 6'd32,0);
  // Read the 'CRC match' bit, and go to exit1_dr
  jtag_read_write_bit(JTAG_TMS_bit, crc_match);
  if(!crc_match)
	$display("CRC ERROR! match bit after write is %d (computed CRC 0x%x)", crc_match, crc_calc_o);
  jtag_write_bit(JTAG_TMS_bit);  // update_ir
  jtag_write_bit(3'h0);           // idle
end
endtask

//================ JTAG Low level Tasks ==============================//

///////////////////////////////////////
// Check the TAP's ID code          //
/////////////////////////////////////
task jtag_check_idcode;
	reg [63:0] readdata;
	reg [31:0] idcode;
begin
  $display("...jtag_check_idcode at time %t", $time);
  jtag_set_ir(6'b001001); // 0x09
  // Read the IDCODE in the DR
  jtag_write_bit(JTAG_TMS_bit);  // select_dr_scan
  jtag_write_bit(3'h0);           // capture_ir
  jtag_write_bit(3'h0);           // shift_ir
  jtag_read_write_stream(64'h0, 8'd32, 1, readdata); // write data, exit_1
  jtag_write_bit(JTAG_TMS_bit);  // update_ir
  jtag_write_bit(3'h0);           // idle
  idcode = readdata[31:0];
  $display("...Got TAP IDCODE 0x%x ", idcode);
end
endtask

////////////////////////////////
//  Set Instruction Register //
//////////////////////////////
// Puts a value in the TAP IR, assuming we start in IDLE state.
// Returns to IDLE state when finished
task jtag_set_ir;
	input [5:0] irval;
begin
  jtag_write_bit(JTAG_TMS_bit);        // select_dr_scan
  jtag_write_bit(JTAG_TMS_bit);        // select_ir_scan
  jtag_write_bit(3'h0);                // capture_ir
  jtag_write_bit(3'h0);                // shift_ir
  jtag_write_stream({58'h0,irval}, 8'h6, 1);  // write data, exit_1
  jtag_write_bit(JTAG_TMS_bit);        // update_ir
  jtag_write_bit(3'h0);                // idle
end
endtask
/////////////////////////////////
//  Resets TAP                //
///////////////////////////////
task reset_jtag;
	integer i;
begin
  for(i = 0; i < 8; i=i+1) begin
	jtag_write_bit(JTAG_TMS_bit);  // 5 TMS should put us in test_logic_reset mode
  end
  jtag_write_bit(3'h0);              // idle
end
endtask

////////////////////////////////////////////////////////////////////
//  Tasks to write or read-write a string of data                //
//////////////////////////////////////////////////////////////////
task jtag_write_stream;
	input [63:0] stream;
	input [7:0] len;
	input set_last_bit;
	integer i;
	integer databit;
	reg [2:0] bits;
begin
  for(i = 0; i < (len-1); i=i+1) begin
	databit = (stream >> i) & 1'h1;
	bits = databit << JTAG_TDO;
	jtag_write_bit(bits);
  end
  databit = (stream >> i) & 1'h1;
  bits = databit << JTAG_TDO;
  if(set_last_bit)
	bits = (bits | JTAG_TMS_bit);
  jtag_write_bit(bits);
end
endtask

task jtag_read_write_stream;
	input [63:0] stream;
	input [7:0] len;
	input set_last_bit;
	output [63:0] instream;
	integer i;
	integer databit;
	reg [2:0] bits;
	reg inbit;
begin
  instream = 64'h0;
  for(i = 0; i < (len-1); i=i+1) begin
	databit = (stream >> i) & 1'h1;
	bits = databit << JTAG_TDO;
	jtag_read_write_bit(bits, inbit);
	instream = (instream | (inbit << i));
  end
  databit = (stream >> i) & 1'h1;
  bits = databit << JTAG_TDO;
  if(set_last_bit)
	bits = (bits | JTAG_TMS_bit);
  jtag_read_write_bit(bits, inbit);
  instream = (instream | (inbit << (len-1)));
end
endtask

/////////////////////////////////////////////////////////////////////////
//  Tasks which write or readwrite a single bit (including clocking)  //
///////////////////////////////////////////////////////////////////////
task jtag_write_bit;
	input [2:0] bitvals;
begin
  // Set data
  jtag_out(bitvals & ~(JTAG_TCK_bit));
  #50.0; // Wait 50nsecs
  // Raise clock
  jtag_out(bitvals | JTAG_TCK_bit);
  #50.0; // Wait 50nsecs
  // drop clock (making output available in the SHIFT_xR states)
  jtag_out(bitvals & ~(JTAG_TCK_bit));
  #50.0; // Wait 50nsecs
end
endtask
task jtag_read_write_bit;
	input [2:0] bitvals;
	output l_tdi_val;
begin
  // read bit state
  l_tdi_val <= jtag_tdo_pad_o;
  // Set data
  jtag_out(bitvals & ~(JTAG_TCK_bit));
  #50.0; // Wait 50nsecs
  // Raise clock
  jtag_out(bitvals | JTAG_TCK_bit);
  #50.0; // Wait 50nsecs
  // drop clock (making output available in the SHIFT_xR states)
  jtag_out(bitvals & ~(JTAG_TCK_bit));
  #50.0; // Wait 50nsecs
end
endtask

///////////////////////////////////////
//  BASIS FUNCTIONS FOR JTAG        //
/////////////////////////////////////
task jtag_out;
	input   [2:0]   bitvals;
begin
  jtag_tck_pad_i <= bitvals[JTAG_TCK];
  jtag_tms_pad_i <= bitvals[JTAG_TMS];
  jtag_tdi_pad_i <= bitvals[JTAG_TDO];
end
endtask

task jtag_inout;
        input   [2:0]   bitvals;
	output l_tdi_val;
begin
  jtag_tck_pad_i <= bitvals[JTAG_TCK];
  jtag_tms_pad_i <= bitvals[JTAG_TMS];
  jtag_tdi_pad_i <= bitvals[JTAG_TDO];

  l_tdi_val <= jtag_tdo_pad_o;
end
endtask
////////////////////////////////////////////////
//============= Main Tasks List =============//
//////////////////////////////////////////////
integer i;
reg [7:0] tmp_header;
reg [31:0] tmp_addr;
reg [31:0] tmp_data;

// USER1 : 6'b000010 ( 0x02 )
// USER2 : 6'b000011 ( 0x03 )
// USER3 : 6'b100010 ( 0x22 )
// USER4 : 6'b100011 ( 0x23 )
initial begin
	@ (negedge wb_rst);
	$display("... Wishbone Reset Reached");
	reset_jtag;
	#10.0; // Wait for 10nsec
	jtag_check_idcode;
	#100.0;
	$display("Selecting USER2 ( Wishbone Master ) IR at time %t", $time);
	jtag_set_ir(6'b000011); // 0x03 -> USER2
	jtag_write_bit(JTAG_TMS_bit);	// select_dr_scan
	jtag_write_bit(3'h0);		// capture_dr
	jtag_write_bit(3'h0);		// shift_dr
	// Opcodes ( 4bits ):
	// 4'b1000: Wishbone Write
	// 4'b0111: Wishbone Read
	// 4'b0101: Status Read (not implemented)
	// The whole data is clocked reverse (MSB first)
	jtag_write_stream(64'h1234_5678, 8'd32, 0);	// write data
	jtag_write_stream(64'h0000_CDEF, 8'd32, 0);	// write address
	jtag_write_stream(64'b0100,	 8'd4,  0);	// write mask
	jtag_write_stream(64'b1000, 	 8'd4,  1);	// write opcode, exit_1
	#20000.0; // Wait for Memory to initialize
	jtag_write_bit(JTAG_TMS_bit); 	// update_dr (this is where the command actually executes)
	jtag_write_bit(3'h0);		// idle

	#100000.0;
	$display("Selecting USER3 ( CPU )IR at time %t", $time);
	jtag_set_ir(6'b100010); // 0x22 -> USER3 
	//---> Stall CPU
	jtag_write_bit(JTAG_TMS_bit);   // select_dr_scan
	jtag_write_bit(3'h0);           // capture_dr
	jtag_write_bit(3'h0);           // shift_dr
	jtag_write_stream(64'h1234_5678, 8'd32, 0);     // Data
	jtag_write_stream(64'h1234_CDEF, 8'd32, 0);     // Address
	jtag_write_stream(64'h20, 8'd8,  1);     // Header (Stall )
	jtag_write_bit(JTAG_TMS_bit);   // update_dr (this is where the command actually executes)
	//---> Enable exceptions
	jtag_write_bit(JTAG_TMS_bit);   // select_dr_scan
	jtag_write_bit(3'h0);           // capture_dr
	jtag_write_bit(3'h0);           // shift_dr
	jtag_write_stream(64'h0000_0001, 8'd32, 0);     // Data
	jtag_write_stream(64'h0000_0011, 8'd32, 0);     // Address
	jtag_write_stream(64'hC0, 8'd8,  1);     // Header (Write )
	jtag_write_bit(JTAG_TMS_bit);   // update_dr (this is where the command actually executes)
	//---> Trap causes stall
	jtag_write_bit(JTAG_TMS_bit);   // select_dr_scan
	jtag_write_bit(3'h0);           // capture_dr
	jtag_write_bit(3'h0);           // shift_dr
	jtag_write_stream(64'h0000_2000, 8'd32, 0);     // Data
	jtag_write_stream(64'h0000_3014, 8'd32, 0);     // Address
	jtag_write_stream(64'hC0, 8'd8,  1);     // Header (Write )
	jtag_write_bit(JTAG_TMS_bit);   // update_dr (this is where the command actually executes)
	//---> Set PC to 1230
	jtag_write_bit(JTAG_TMS_bit);   // select_dr_scan
	jtag_write_bit(3'h0);           // capture_dr
	jtag_write_bit(3'h0);           // shift_dr
	jtag_write_stream(64'h0000_1230, 8'd32, 0);     // Data
	jtag_write_stream(64'h0000_0010, 8'd32, 0);     // Address
	jtag_write_stream(64'hC0, 8'd8,  1);     // Header (Write )
	jtag_write_bit(JTAG_TMS_bit);   // update_dr (this is where the command actually executes)
	//---> Set step bit
	jtag_write_bit(JTAG_TMS_bit);   // select_dr_scan
	jtag_write_bit(3'h0);           // capture_dr
	jtag_write_bit(3'h0);           // shift_dr
	jtag_write_stream(64'h0040_0000, 8'd32, 0);     // Data
	jtag_write_stream(64'h0000_3010, 8'd32, 0);     // Address
	jtag_write_stream(64'hC0, 8'd8,  1);     // Header (Write )
	jtag_write_bit(JTAG_TMS_bit);   // update_dr (this is where the command actually executes)
	//===> Reading NPC, PPC and R1
	//---> Reading NPC
	jtag_write_bit(JTAG_TMS_bit);   // select_dr_scan
	jtag_write_bit(3'h0);           // capture_dr
	jtag_write_bit(3'h0);           // shift_dr
	jtag_write_stream(64'hFFFF_FFFF, 8'd32, 0);     // Data
	jtag_write_stream(64'h0000_0010, 8'd32, 0);     // Address
	jtag_write_stream(64'h40, 8'd8,  1);     // Header ( Read )
	jtag_write_bit(JTAG_TMS_bit);   // update_dr (this is where the command actually executes)
	//---> Reading PPC
	jtag_write_bit(JTAG_TMS_bit);   // select_dr_scan
	jtag_write_bit(3'h0);           // capture_dr
	jtag_write_bit(3'h0);           // shift_dr
	jtag_write_stream(64'hFFFF_FFFF, 8'd32, 0);     // Data
	jtag_write_stream(64'h0000_0012, 8'd32, 0);     // Address
	jtag_write_stream(64'h40, 8'd8,  1);     // Header ( Read )
	jtag_write_bit(JTAG_TMS_bit);   // update_dr (this is where the command actually executes)
	//---> Reading R1
	jtag_write_bit(JTAG_TMS_bit);   // select_dr_scan
	jtag_write_bit(3'h0);           // capture_dr
	jtag_write_bit(3'h0);           // shift_dr
	jtag_write_stream(64'hFFFF_FFFF, 8'd32, 0);     // Data
	jtag_write_stream(64'h0000_0401, 8'd32, 0);     // Address
	jtag_write_stream(64'h40, 8'd8,  1);     // Header ( Read )
	jtag_write_bit(JTAG_TMS_bit);   // update_dr (this is where the command actually executes)
	//---> Unstall CPU
	jtag_write_bit(JTAG_TMS_bit);   // select_dr_scan
	jtag_write_bit(3'h0);           // capture_dr
	jtag_write_bit(3'h0);           // shift_dr
	jtag_write_stream(64'h1234_5678, 8'd32, 0);     // Data
	jtag_write_stream(64'h1234_CDEF, 8'd32, 0);     // Address
	jtag_write_stream(64'h00, 8'd8,  1);     // Header (Stall )
	jtag_write_bit(JTAG_TMS_bit);   // update_dr (this is where the command actually executes)
	
	jtag_write_bit(3'h0);           // idle
	#10000.0;
	$display("Selecting USER2 (Wishbone) IR at time %t", $time);
	jtag_set_ir(6'b000011); // 0x03 -> USER2
	jtag_write_bit(JTAG_TMS_bit);   // select_dr_scan
	jtag_write_bit(3'h0);           // capture_dr
	jtag_write_bit(3'h0);           // shift_dr

	jtag_write_stream(64'h1234_5678, 8'd32, 0);     // write data
	jtag_write_stream(64'h0000_CDEF, 8'd32, 0);     // write address
	jtag_write_stream(64'b1000_0101, 8'd8,  1);	// Read

	jtag_write_bit(JTAG_TMS_bit);   // update_dr (this is where the command actually executes)

	#1000.0;
	jtag_write_bit(JTAG_TMS_bit);   // select_dr_scan
	jtag_write_bit(3'h0);           // capture_dr
	jtag_write_bit(3'h0);           // shift_dr

	jtag_read_write_stream(64'h9ABC_DEF0, 8'd32, 0, tmp_data); // write n read data
	jtag_read_write_stream(64'h0000_CDEF, 8'd32, 0, tmp_addr); // write n read addr
	jtag_read_write_stream(64'b0111_1010, 8'd8,  1, tmp_header); // write n read header
		// (MSB is mask, lsb is write(0111) or (1000)read code)
	$display("...Prev Operation: DATA=[%x], ADDR=[%x], HEADER=[%x]",
		tmp_data, tmp_addr, tmp_header);

	jtag_write_bit(JTAG_TMS_bit);   // update_dr (this is where the command actually executes)
	jtag_write_bit(3'h0);		// idle

	#1000.0;
	$finish;
end
endmodule
