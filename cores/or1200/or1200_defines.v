//
////////////////////////////////////////////////////////
///////////////////////////////////////////////////////
////     OR1200     CONFIGURATION FILE            ////
/////////////////////////////////////////////////////
////////////////////////////////////////////////////
// Some missing Things for implementation
`define OR1200_OPERAND_WIDTH	32
//
//  Target FPGA memories
//
//`define OR1200_ALTERA_LPM
//`define OR1200_XILINX_RAMB16
//`define OR1200_XILINX_RAMB4
//`define OR1200_XILINX_RAM32X1D
//`define OR1200_USE_RAM16X1D_FOR_RAM32X1D
// Generic models should infer RAM blocks at synthesis time
`define OR1200_GENERIC
//-------------------------------------------------------------------//
// BOOT Options:
//
//--->Boot from 0xf0000100
// `define OR1200_BOOT_PCREG_DEFAULT 30'h3c00003f
`define OR1200_BOOT_ADR 32'hf0000100
//--->Boot from 0x100
//`define OR1200_BOOT_PCREG_DEFAULT 30'h0000003f
//`define OR1200_BOOT_ADR 32'h00000100
//-------------------------------------------------------------------//
//
// DATA CACHE (if defined: NO Data cache will be generated)
//
`define OR1200_NO_DC
//
// Size and type of DATA CACHE, if implemented
//
//`define OR1200_DC_1W_4KB
`define OR1200_DC_1W_8KB (Default)
//`define OR1200_DC_1W_16KB
//`define OR1200_DC_1W_32KB
//-------------------------------------------------------------------//
//
// INSTRUCTION CACHE (if defined: NO Instruction cache will be generated)
//
`define OR1200_NO_IC
//
// Size and type of  INSTRUCTION CACHE, if implemented
//
//`define OR1200_IC_1W_512B
//`define OR1200_IC_1W_4KB (Default)
`define OR1200_IC_1W_8KB
//`define OR1200_IC_1W_16KB
//`define OR1200_IC_1W_32KB
//-------------------------------------------------------------------//
//
// Data MMU Unit (if defined: NO Data MMU will be generated )
//
//`define OR1200_NO_DMMU
//
// INSTRUCTION MMU Unit (if defined: NO Instruction MMU will be generated) 
//
//`define OR1200_NO_IMMU
//-------------------------------------------------------------------//
//
// Select between ASIC and generic multiplier
//
//`define OR1200_ASIC_MULTP2_32X32
`define OR1200_GENERIC_MULTP2_32X32
//-------------------------------------------------------------------//
// DEFAULT SETTINGS: Do not change, unless you know what you are doing.
// For meaning of these sections look at corresponding or1200_defines.v
// file on www.opencores.org projects (orsocpv2 for example)
//
//--->Reset active low
//`define OR1200_RST_ACT_LOW
//--->Enable RAM BIST
//`define OR1200_BIST
//--->Register OR1200 WISHBONE outputs
`define OR1200_REGISTERED_OUTPUTS
//--->Register OR1200 WISHBONE inputs
`define OR1200_REGISTERED_INPUTS // AVD: Default is disabled
//--->Disable Wishbone bursts
`define OR1200_NO_BURSTS	// AVD: Default is Disabled !
//--->WISHBONE retry counter range
`define OR1200_WB_RETRY 7	// AVD: Enabling it
//--->WISHBONE Consecutive Address Burst
//`define OR1200_WB_CAB
//--->WISHBONE B3 compatible interface
`define OR1200_WB_B3
//--->LOG all WISHBONE accesses
`define OR1200_LOG_WB_ACCESS
//--->Enable additional synthesis directives if using "Synopsys"
//`define OR1200_ADDITIONAL_SYNOPSYS_DIRECTIVES
//--->Enables default statement in some case blocks
`define OR1200_CASE_DEFAULT
//--->Operand width / register file address width (DO NOT CHANGE)
`define OR1200_OPERAND_WIDTH		32
`define OR1200_REGFILE_ADDR_WIDTH	5
//--->l.add/l.addi/l.and and l.addc/l.addic set (compare) flag when result of
//    their operation equals zero
//`define OR1200_ADDITIONAL_FLAG_MODIFIERS
//--->Implement l.addc/l.addic instructions
`define OR1200_IMPL_ADDC
//--->Implement l.sub instruction
`define OR1200_IMPL_SUB
//--->Implement carry bit SR[CY]
`define OR1200_IMPL_CY
//--->Implement carry bit SR[OV]
`define OR1200_IMPL_OV
//--->Implement carry bit SR[OVE]
`define OR1200_IMPL_OVE
//--->Implement rotate in the ALU
`define OR1200_IMPL_ALU_ROTATE	// AVD: Implement this for l.slli instruction
//--->Type of ALU compare to implement
//`define OR1200_IMPL_ALU_COMP1
//`define OR1200_IMPL_ALU_COMP2
`define OR1200_IMPL_ALU_COMP3
//--->Implement Find First/Last '1'
`define OR1200_IMPL_ALU_FFL1
//--->Implement l.cust5 ALU instruction
//`define OR1200_IMPL_ALU_CUST5
//--->Implement l.extXs and l.extXz instructions
`define OR1200_IMPL_ALU_EXT
//--->Implement multiplier
`define OR1200_MULT_IMPLEMENTED
//--->Implement multiply-and-accumulate
`define OR1200_MAC_IMPLEMENTED
//--->Implement l.div/l.divu instructions
`define OR1200_DIV_IMPLEMENTED
//--->Serial multiplier.
//`define OR1200_MULT_SERIAL
//--->Serial divider.
`define OR1200_DIV_SERIAL
//--->Implement HW Single Precision FPU
`define OR1200_FPU_IMPLEMENTED
//---> Implement Power Management
//`define OR1200_PM_IMPLEMENTED
//---> Implement Debug Unit
`define OR1200_DU_IMPLEMENTED
//---> Implement HW Breakpoints in Debug Unit
//`define OR1200_DU_HWBKPTS
//---> Implement TRACE BUFFER in Debug Unit
//`define OR1200_DU_TB_IMPLEMENTED
//---> Implement Programmable Interrupt Controller
`define OR1200_PIC_IMPLEMENTED
//---> Implement Tick Timer
`define OR1200_TT_IMPLEMENTED
//---> Implement Store Buffer
// `define OR1200_SB_IMPLEMENTED // AVD: Uncommented this
//---> Implement Quick Embedded Memory
//`define OR1200_QMEM_IMPLEMENTED
//---> Implement configuration registers
`define OR1200_CFGR_IMPLEMENTED
//---> Clock ratio RISC clock versus WB clock (Uncomment both for 1:4)
//`define OR1200_CLKDIV_2_SUPPORTED
//`define OR1200_CLKDIV_4_SUPPORTED
//--->Type of register file RAM
//`define OR1200_RFRAM_TWOPORT
`define OR1200_RFRAM_DUALPORT
//--->Generic (flip-flop based) register file.
//`define OR1200_RFRAM_GENERIC
`ifdef OR1200_RFRAM_GENERIC
	//`define OR1200_RFRAM_16REG
`endif
//--->Type of mem2reg aligner to implement.
`define OR1200_IMPL_MEM2REG1
//`define OR1200_IMPL_MEM2REG2
//===================================================================//
//  There is even less to modify below this line
//
// Reset value and event
//
`ifdef OR1200_RST_ACT_LOW
	`define OR1200_RST_VALUE	(1'b0)
	`define OR1200_RST_EVENT	negedge
`else
	`define OR1200_RST_VALUE	(1'b1)
	`define OR1200_RST_EVENT	posedge
`endif

//
// ALUOPs
//
`define OR1200_ALUOP_WIDTH	5
`define OR1200_ALUOP_NOP	5'b0_0100
/* LS-nibble encodings correspond to bits [3:0] of instruction */
`define OR1200_ALUOP_ADD	5'b0_0000 // 0
`define OR1200_ALUOP_ADDC	5'b0_0001 // 1
`define OR1200_ALUOP_SUB	5'b0_0010 // 2
`define OR1200_ALUOP_AND	5'b0_0011 // 3
`define OR1200_ALUOP_OR		5'b0_0100 // 4
`define OR1200_ALUOP_XOR	5'b0_0101 // 5
`define OR1200_ALUOP_MUL	5'b0_0110 // 6
`define OR1200_ALUOP_RESERVED	5'b0_0111 // 7
`define OR1200_ALUOP_SHROT	5'b0_1000 // 8
`define OR1200_ALUOP_DIV	5'b0_1001 // 9
`define OR1200_ALUOP_DIVU	5'b0_1010 // a
`define OR1200_ALUOP_MULU	5'b0_1011 // b
`define OR1200_ALUOP_EXTHB	5'b0_1100 // c
`define OR1200_ALUOP_EXTW	5'b0_1101 // d
`define OR1200_ALUOP_CMOV	5'b0_1110 // e
`define OR1200_ALUOP_FFL1	5'b0_1111 // f
/* Values sent to ALU from decode unit - not defined by ISA */
`define OR1200_ALUOP_COMP	5'b1_0000 // Comparison
`define OR1200_ALUOP_MOVHI	5'b1_0001 // Move-high
`define OR1200_ALUOP_CUST5	5'b1_0010 // l.cust5
// ALU instructions second opcode field
`define OR1200_ALUOP2_POS	9:6
`define OR1200_ALUOP2_WIDTH	4

//
// MACOPs
//
`define OR1200_MACOP_WIDTH	3
`define OR1200_MACOP_NOP	3'b000
`define OR1200_MACOP_MAC	3'b001
`define OR1200_MACOP_MSB	3'b010

//
// Shift/rotate ops
//
`define OR1200_SHROTOP_WIDTH	4
`define OR1200_SHROTOP_NOP	4'd0
`define OR1200_SHROTOP_SLL	4'd0
`define OR1200_SHROTOP_SRL	4'd1
`define OR1200_SHROTOP_SRA	4'd2
`define OR1200_SHROTOP_ROR	4'd3

//
// Zero/Sign Extend ops
//
`define OR1200_EXTHBOP_WIDTH	4
`define OR1200_EXTHBOP_BS	4'h1
`define OR1200_EXTHBOP_HS	4'h0
`define OR1200_EXTHBOP_BZ	4'h3
`define OR1200_EXTHBOP_HZ	4'h2

`define OR1200_EXTWOP_WIDTH	4
`define OR1200_EXTWOP_WS	4'h0
`define OR1200_EXTWOP_WZ	4'h1

// Execution cycles per instruction
`define OR1200_MULTICYCLE_WIDTH	3
`define OR1200_ONE_CYCLE	3'd0
`define OR1200_TWO_CYCLES	3'd1

// Execution control which will "wait on" a module to finish
`define OR1200_WAIT_ON_WIDTH	2
`define OR1200_WAIT_ON_NOTHING	`OR1200_WAIT_ON_WIDTH'd0
`define OR1200_WAIT_ON_MULTMAC	`OR1200_WAIT_ON_WIDTH'd1
`define OR1200_WAIT_ON_FPU	`OR1200_WAIT_ON_WIDTH'd2
`define OR1200_WAIT_ON_MTSPR	`OR1200_WAIT_ON_WIDTH'd3

// Operand MUX selects
`define OR1200_SEL_WIDTH	2
`define OR1200_SEL_RF		2'd0
`define OR1200_SEL_IMM		2'd1
`define OR1200_SEL_EX_FORW	2'd2
`define OR1200_SEL_WB_FORW	2'd3

//
// BRANCHOPs
//
`define OR1200_BRANCHOP_WIDTH	3
`define OR1200_BRANCHOP_NOP	3'd0
`define OR1200_BRANCHOP_J	3'd1
`define OR1200_BRANCHOP_JR	3'd2
`define OR1200_BRANCHOP_BAL	3'd3
`define OR1200_BRANCHOP_BF	3'd4
`define OR1200_BRANCHOP_BNF	3'd5
`define OR1200_BRANCHOP_RFE	3'd6

//
// LSUOPs
//
`define OR1200_LSUOP_WIDTH	4
`define OR1200_LSUOP_NOP	4'b0000
`define OR1200_LSUOP_LBZ	4'b0010
`define OR1200_LSUOP_LBS	4'b0011
`define OR1200_LSUOP_LHZ	4'b0100
`define OR1200_LSUOP_LHS	4'b0101
`define OR1200_LSUOP_LWZ	4'b0110
`define OR1200_LSUOP_LWS	4'b0111
`define OR1200_LSUOP_LD		4'b0001
`define OR1200_LSUOP_SD		4'b1000
`define OR1200_LSUOP_SB		4'b1010
`define OR1200_LSUOP_SH		4'b1100
`define OR1200_LSUOP_SW		4'b1110

// Number of bits of load/store EA precalculated in ID stage
`define OR1200_LSUEA_PRECALC	2

//
// FETCHOPs
//
`define OR1200_FETCHOP_WIDTH	1
`define OR1200_FETCHOP_NOP	1'b0
`define OR1200_FETCHOP_LW	1'b1

//
// Register File Write-Back OPs
//
`define OR1200_RFWBOP_WIDTH	4
`define OR1200_RFWBOP_NOP	4'b0000
`define OR1200_RFWBOP_ALU	3'b000
`define OR1200_RFWBOP_LSU	3'b001
`define OR1200_RFWBOP_SPRS	3'b010
`define OR1200_RFWBOP_LR	3'b011
`define OR1200_RFWBOP_FPU	3'b100

//
// Compare instructions
//
`define OR1200_COP_SFEQ		3'b000
`define OR1200_COP_SFNE		3'b001
`define OR1200_COP_SFGT		3'b010
`define OR1200_COP_SFGE		3'b011
`define OR1200_COP_SFLT		3'b100
`define OR1200_COP_SFLE		3'b101
`define OR1200_COP_X		3'b111
`define OR1200_SIGNED_COMPARE	'd3
`define OR1200_COMPOP_WIDTH	4

//
// FP OPs
//
`define OR1200_FPUOP_WIDTH	8
`define OR1200_FPUOP_CYCLES	3'd4
`define OR1200_FPUOP_DOUBLE_BIT	4
`define OR1200_FPUOP_ADD	8'b0000_0000
`define OR1200_FPUOP_SUB	8'b0000_0001
`define OR1200_FPUOP_MUL	8'b0000_0010
`define OR1200_FPUOP_DIV	8'b0000_0011
`define OR1200_FPUOP_ITOF	8'b0000_0100
`define OR1200_FPUOP_FTOI	8'b0000_0101
`define OR1200_FPUOP_REM	8'b0000_0110
`define OR1200_FPUOP_RESERVED	8'b0000_0111

//
// FP Compare instructions
//
`define OR1200_FPCOP_SFEQ	8'b0000_1000
`define OR1200_FPCOP_SFNE	8'b0000_1001
`define OR1200_FPCOP_SFGT	8'b0000_1010
`define OR1200_FPCOP_SFGE	8'b0000_1011
`define OR1200_FPCOP_SFLT	8'b0000_1100
`define OR1200_FPCOP_SFLE	8'b0000_1101

//
// TAGs for instruction bus
//
`define OR1200_ITAG_IDLE	4'h0	// idle bus
`define OR1200_ITAG_NI		4'h1	// normal insn
`define OR1200_ITAG_BE		4'hb	// Bus error exception
`define OR1200_ITAG_PE		4'hc	// Page fault exception
`define OR1200_ITAG_TE		4'hd	// TLB miss exception

//
// TAGs for data bus
//
`define OR1200_DTAG_IDLE	4'h0	// idle bus
`define OR1200_DTAG_ND		4'h1	// normal data
`define OR1200_DTAG_AE		4'ha	// Alignment exception
`define OR1200_DTAG_BE		4'hb	// Bus error exception
`define OR1200_DTAG_PE		4'hc	// Page fault exception
`define OR1200_DTAG_TE		4'hd	// TLB miss exception

//
// ORBIS32 ISA specifics
//
// SHROT_OP position in machine word
`define OR1200_SHROTOP_POS	7:6
// Instruction opcode groups (basic)
`define OR1200_OR32_J		6'b000000
`define OR1200_OR32_JAL		6'b000001
`define OR1200_OR32_BNF		6'b000011
`define OR1200_OR32_BF		6'b000100
`define OR1200_OR32_NOP		6'b000101
`define OR1200_OR32_MOVHI	6'b000110
`define OR1200_OR32_MACRC	6'b000110
`define OR1200_OR32_XSYNC	6'b001000
`define OR1200_OR32_RFE		6'b001001
`define OR1200_OR32_JR		6'b010001
`define OR1200_OR32_JALR	6'b010010
`define OR1200_OR32_MACI	6'b010011
`define OR1200_OR32_LWZ		6'b100001
`define OR1200_OR32_LWS		6'b100010
`define OR1200_OR32_LBZ		6'b100011
`define OR1200_OR32_LBS		6'b100100
`define OR1200_OR32_LHZ		6'b100101
`define OR1200_OR32_LHS		6'b100110
`define OR1200_OR32_ADDI	6'b100111
`define OR1200_OR32_ADDIC	6'b101000
`define OR1200_OR32_ANDI	6'b101001
`define OR1200_OR32_ORI		6'b101010
`define OR1200_OR32_XORI	6'b101011
`define OR1200_OR32_MULI	6'b101100
`define OR1200_OR32_MFSPR	6'b101101
`define OR1200_OR32_SH_ROTI	6'b101110
`define OR1200_OR32_SFXXI	6'b101111
`define OR1200_OR32_MTSPR	6'b110000
`define OR1200_OR32_MACMSB	6'b110001
`define OR1200_OR32_FLOAT	6'b110010
`define OR1200_OR32_SW		6'b110101
`define OR1200_OR32_SB		6'b110110
`define OR1200_OR32_SH		6'b110111
`define OR1200_OR32_ALU		6'b111000
`define OR1200_OR32_SFXX	6'b111001
`define OR1200_OR32_CUST5	6'b111100

//
// Exceptions
//
// PPPPP and VV parts
`define OR1200_EXCEPT_EPH0_P	20'h00000
`define OR1200_EXCEPT_EPH1_P	20'hF0000
`define OR1200_EXCEPT_V		8'h00
// N part width
`define OR1200_EXCEPT_WIDTH	4

// Definition of exception vectors
`define OR1200_EXCEPT_UNUSED	`OR1200_EXCEPT_WIDTH'hf
`define OR1200_EXCEPT_TRAP	`OR1200_EXCEPT_WIDTH'he
`define OR1200_EXCEPT_FLOAT	`OR1200_EXCEPT_WIDTH'hd
`define OR1200_EXCEPT_SYSCALL	`OR1200_EXCEPT_WIDTH'hc
`define OR1200_EXCEPT_RANGE	`OR1200_EXCEPT_WIDTH'hb
`define OR1200_EXCEPT_ITLBMISS	`OR1200_EXCEPT_WIDTH'ha
`define OR1200_EXCEPT_DTLBMISS	`OR1200_EXCEPT_WIDTH'h9
`define OR1200_EXCEPT_INT	`OR1200_EXCEPT_WIDTH'h8
`define OR1200_EXCEPT_ILLEGAL	`OR1200_EXCEPT_WIDTH'h7
`define OR1200_EXCEPT_ALIGN	`OR1200_EXCEPT_WIDTH'h6
`define OR1200_EXCEPT_TICK	`OR1200_EXCEPT_WIDTH'h5
`define OR1200_EXCEPT_IPF	`OR1200_EXCEPT_WIDTH'h4
`define OR1200_EXCEPT_DPF	`OR1200_EXCEPT_WIDTH'h3
`define OR1200_EXCEPT_BUSERR	`OR1200_EXCEPT_WIDTH'h2
`define OR1200_EXCEPT_RESET	`OR1200_EXCEPT_WIDTH'h1
`define OR1200_EXCEPT_NONE	`OR1200_EXCEPT_WIDTH'h0

//
// SPR groups
//
`define OR1200_SPR_GROUP_BITS	15:11
`define OR1200_SPR_GROUP_WIDTH	5
`define OR1200_SPR_OFS_BITS	10:0
// List of groups
`define OR1200_SPR_GROUP_SYS	5'd00
`define OR1200_SPR_GROUP_DMMU	5'd01
`define OR1200_SPR_GROUP_IMMU	5'd02
`define OR1200_SPR_GROUP_DC	5'd03
`define OR1200_SPR_GROUP_IC	5'd04
`define OR1200_SPR_GROUP_MAC	5'd05
`define OR1200_SPR_GROUP_DU	5'd06
`define OR1200_SPR_GROUP_PM	5'd08
`define OR1200_SPR_GROUP_PIC	5'd09
`define OR1200_SPR_GROUP_TT	5'd10
`define OR1200_SPR_GROUP_FPU	5'd11

//
// System group
//
// System registers
`define OR1200_SPR_CFGR		7'd0
`define OR1200_SPR_RF		6'd32
`define OR1200_SPR_NPC		11'd16
`define OR1200_SPR_SR		11'd17
`define OR1200_SPR_PPC		11'd18
`define OR1200_SPR_FPCSR	11'd20
`define OR1200_SPR_EPCR		11'd32
`define OR1200_SPR_EEAR		11'd48
`define OR1200_SPR_ESR		11'd64
// SR bits
`define OR1200_SR_WIDTH		17
`define OR1200_SR_SM		0
`define OR1200_SR_TEE		1
`define OR1200_SR_IEE		2
`define OR1200_SR_DCE		3
`define OR1200_SR_ICE		4
`define OR1200_SR_DME		5
`define OR1200_SR_IME		6
`define OR1200_SR_LEE		7
`define OR1200_SR_CE		8
`define OR1200_SR_F		9
`define OR1200_SR_CY		10
`define OR1200_SR_OV		11
`define OR1200_SR_OVE		12
`define OR1200_SR_DSX		13
`define OR1200_SR_EPH		14
`define OR1200_SR_FO		15
`define OR1200_SR_TED		16
`define OR1200_SR_CID		31:28
// Bits that define offset inside the group
`define OR1200_SPROFS_BITS	10:0
// Default Exception Prefix
`define OR1200_SR_EPH_DEF	1'b0
// FPCSR bits
`define OR1200_FPCSR_WIDTH	12
`define OR1200_FPCSR_FPEE	0
`define OR1200_FPCSR_RM		2:1
`define OR1200_FPCSR_OVF	3
`define OR1200_FPCSR_UNF	4
`define OR1200_FPCSR_SNF	5
`define OR1200_FPCSR_QNF	6
`define OR1200_FPCSR_ZF		7
`define OR1200_FPCSR_IXF	8
`define OR1200_FPCSR_IVF	9
`define OR1200_FPCSR_INF	10
`define OR1200_FPCSR_DZF	11
`define OR1200_FPCSR_RES	31:12
//-------------------------------------------------------------------//
// Power Management (PM)
//
// Bit positions inside PMR (don't change)
`define OR1200_PM_PMR_SDF	3:0
`define OR1200_PM_PMR_DME	4
`define OR1200_PM_PMR_SME	5
`define OR1200_PM_PMR_DCGE	6
`define OR1200_PM_PMR_UNUSED	31:7
// PMR offset inside PM group of registers
`define OR1200_PM_OFS_PMR	11'b0
// PM group
`define OR1200_SPRGRP_PM	5'd8
// Define if PMR can be read/written at any address inside PM group
`define OR1200_PM_PARTIAL_DECODING
// Define if reading PMR is allowed
`define OR1200_PM_READREGS
// Define if unused PMR bits should be zero
`define OR1200_PM_UNUSED_ZERO
//-------------------------------------------------------------------//
//
// Debug Unit (DU)
//
// Number of DVR/DCR pairs if HW breakpoints enabled
`define OR1200_DU_DVRDCR_PAIRS	8
// Address offsets of DU registers inside DU group
`ifdef OR1200_DU_HWBKPTS
	`define OR1200_DU_DVR0	11'd0
	`define OR1200_DU_DVR1	11'd1
	`define OR1200_DU_DVR2	11'd2
	`define OR1200_DU_DVR3	11'd3
	`define OR1200_DU_DVR4	11'd4
	`define OR1200_DU_DVR5	11'd5
	`define OR1200_DU_DVR6	11'd6
	`define OR1200_DU_DVR7	11'd7
	`define OR1200_DU_DCR0	11'd8
	`define OR1200_DU_DCR1	11'd9
	`define OR1200_DU_DCR2	11'd10
	`define OR1200_DU_DCR3	11'd11
	`define OR1200_DU_DCR4	11'd12
	`define OR1200_DU_DCR5	11'd13
	`define OR1200_DU_DCR6	11'd14
	`define OR1200_DU_DCR7	11'd15
`endif
`define OR1200_DU_DMR1		11'd16
`ifdef OR1200_DU_HWBKPTS
	`define OR1200_DU_DMR2	11'd17
	`define OR1200_DU_DWCR0	11'd18
	`define OR1200_DU_DWCR1	11'd19
`endif
`define OR1200_DU_DSR		11'd20
`define OR1200_DU_DRR		11'd21
`ifdef OR1200_DU_TB_IMPLEMENTED
	`define OR1200_DU_TBADR	11'h0ff
	`define OR1200_DU_TBIA	11'h1??
	`define OR1200_DU_TBIM	11'h2??
	`define OR1200_DU_TBAR	11'h3??
	`define OR1200_DU_TBTS	11'h4??
`endif
// Position of offset bits inside SPR address
`define OR1200_DUOFS_BITS	10:0
// DCR bits
`define OR1200_DU_DCR_DP	0
`define OR1200_DU_DCR_CC	3:1
`define OR1200_DU_DCR_SC	4
`define OR1200_DU_DCR_CT	7:5
// DMR1 bits
`define OR1200_DU_DMR1_CW0	1:0
`define OR1200_DU_DMR1_CW1	3:2
`define OR1200_DU_DMR1_CW2	5:4
`define OR1200_DU_DMR1_CW3	7:6
`define OR1200_DU_DMR1_CW4	9:8
`define OR1200_DU_DMR1_CW5	11:10
`define OR1200_DU_DMR1_CW6	13:12
`define OR1200_DU_DMR1_CW7	15:14
`define OR1200_DU_DMR1_CW8	17:16
`define OR1200_DU_DMR1_CW9	19:18
`define OR1200_DU_DMR1_CW10	21:20
`define OR1200_DU_DMR1_ST	22
`define OR1200_DU_DMR1_BT	23
`define OR1200_DU_DMR1_DXFW	24
`define OR1200_DU_DMR1_ETE	25
// DMR2 bits
`define OR1200_DU_DMR2_WCE0	0
`define OR1200_DU_DMR2_WCE1	1
`define OR1200_DU_DMR2_AWTC	12:2
`define OR1200_DU_DMR2_WGB	23:13
// DWCR bits
`define OR1200_DU_DWCR_COUNT	15:0
`define OR1200_DU_DWCR_MATCH	31:16
// DSR bits
`define OR1200_DU_DSR_WIDTH	14
`define OR1200_DU_DSR_RSTE	0
`define OR1200_DU_DSR_BUSEE	1
`define OR1200_DU_DSR_DPFE	2
`define OR1200_DU_DSR_IPFE	3
`define OR1200_DU_DSR_TTE	4
`define OR1200_DU_DSR_AE	5
`define OR1200_DU_DSR_IIE	6
`define OR1200_DU_DSR_IE	7
`define OR1200_DU_DSR_DME	8
`define OR1200_DU_DSR_IME	9
`define OR1200_DU_DSR_RE	10
`define OR1200_DU_DSR_SCE	11
`define OR1200_DU_DSR_FPE	12
`define OR1200_DU_DSR_TE	13
// DRR bits
`define OR1200_DU_DRR_RSTE	0
`define OR1200_DU_DRR_BUSEE	1
`define OR1200_DU_DRR_DPFE	2
`define OR1200_DU_DRR_IPFE	3
`define OR1200_DU_DRR_TTE	4
`define OR1200_DU_DRR_AE	5
`define OR1200_DU_DRR_IIE	6
`define OR1200_DU_DRR_IE	7
`define OR1200_DU_DRR_DME	8
`define OR1200_DU_DRR_IME	9
`define OR1200_DU_DRR_RE	10
`define OR1200_DU_DRR_SCE	11
`define OR1200_DU_DRR_FPE	12
`define OR1200_DU_DRR_TE	13
// Define if reading DU regs is allowed
`define OR1200_DU_READREGS
// Define if unused DU registers bits should be zero
`define OR1200_DU_UNUSED_ZERO
// Define if IF/LSU status is not needed by devel i/f
`define OR1200_DU_STATUS_UNIMPLEMENTED
//-------------------------------------------------------------------//
//
// Programmable Interrupt Controller (PIC)
//
// Define number of interrupt inputs (2-31)
`define OR1200_PIC_INTS		20
// Address offsets of PIC registers inside PIC group
`define OR1200_PIC_OFS_PICMR	2'd0
`define OR1200_PIC_OFS_PICSR	2'd2
// Position of offset bits inside SPR address
`define OR1200_PICOFS_BITS 1:0
// Define if you want these PIC registers to be implemented
`define OR1200_PIC_PICMR
`define OR1200_PIC_PICSR
// Define if reading PIC registers is allowed
`define OR1200_PIC_READREGS
// Define if unused PIC register bits should be zero
`define OR1200_PIC_UNUSED_ZERO
//-------------------------------------------------------------------//
//
// Tick Timer (TT)
//
// Address offsets of TT registers inside TT group
`define OR1200_TT_OFS_TTMR	1'd0
`define OR1200_TT_OFS_TTCR	1'd1
// Position of offset bits inside SPR group
`define OR1200_TTOFS_BITS 0
// Define if you want these TT registers to be implemented
`define OR1200_TT_TTMR
`define OR1200_TT_TTCR
// TTMR bits
`define OR1200_TT_TTMR_TP	27:0
`define OR1200_TT_TTMR_IP	28
`define OR1200_TT_TTMR_IE	29
`define OR1200_TT_TTMR_M	31:30
// Define if reading TT registers is allowed
`define OR1200_TT_READREGS

//-------------------------------------------------------------------//
//
// Multiply-ACcumulate (MAC) Unit
//
`define OR1200_MAC_ADDR		0
`define OR1200_MAC_SPR_WE
`define OR1200_MAC_SHIFTBY	0

//-------------------------------------------------------------------//
//
// Data MMU (DMMU)
//
// Address that selects between TLB TR and MR
`define OR1200_DTLB_TM_ADDR	7
// DTLBMR bits
`define OR1200_DTLBMR_V_BITS	0
`define OR1200_DTLBMR_CID_BITS	4:1
`define OR1200_DTLBMR_RES_BITS	11:5
`define OR1200_DTLBMR_VPN_BITS	31:13
// DTLBTR bits
`define OR1200_DTLBTR_CC_BITS	0
`define OR1200_DTLBTR_CI_BITS	1
`define OR1200_DTLBTR_WBC_BITS	2
`define OR1200_DTLBTR_WOM_BITS	3
`define OR1200_DTLBTR_A_BITS	4
`define OR1200_DTLBTR_D_BITS	5
`define OR1200_DTLBTR_URE_BITS	6
`define OR1200_DTLBTR_UWE_BITS	7
`define OR1200_DTLBTR_SRE_BITS	8
`define OR1200_DTLBTR_SWE_BITS	9
`define OR1200_DTLBTR_RES_BITS	11:10
`define OR1200_DTLBTR_PPN_BITS	31:13
// DTLB configuration
`define OR1200_DMMU_PS		13
`define OR1200_DTLB_INDXW	6	// for 64 entry DTLB
//`define OR1200_DTLB_INDXW	7	// for 128 entry DTBL
`define OR1200_DTLB_INDXL	`OR1200_DMMU_PS
`define OR1200_DTLB_INDXH	`OR1200_DMMU_PS+`OR1200_DTLB_INDXW-1
`define OR1200_DTLB_INDX	`OR1200_DTLB_INDXH:`OR1200_DTLB_INDXL
`define OR1200_DTLB_TAGW	32-`OR1200_DTLB_INDXW-`OR1200_DMMU_PS
`define OR1200_DTLB_TAGL	`OR1200_DTLB_INDXH+1
`define OR1200_DTLB_TAG		31:`OR1200_DTLB_TAGL
`define OR1200_DTLBMRW		`OR1200_DTLB_TAGW+1
`define OR1200_DTLBTRW		32-`OR1200_DMMU_PS+5
// Cache inhibit while DMMU is not enabled/implemented
//`define OR1200_DMMU_CI	1'b1 		// cache inhibited 0GB-4GB
//`define OR1200_DMMU_CI	!dcpu_adr_i[31]	// cache inhibited 0GB-2GB
//`define OR1200_DMMU_CI	!dcpu_adr_i[30]	// cache inhibited 0GB-1GB 2GB-3GB
//`define OR1200_DMMU_CI 	dcpu_adr_i[30]	// cache inhibited 1GB-2GB 3GB-4GB
`define OR1200_DMMU_CI 		dcpu_adr_i[31]	// cache inhibited 2GB-4GB (default)
//`define OR1200_DMMU_CI 	1'b0		// cached 0GB-4GB
//-------------------------------------------------------------------//
// Instruction MMU
//
// Address that selects between TLB TR and MR
`define OR1200_ITLB_TM_ADDR	7
// ITLBMR bits
`define OR1200_ITLBMR_V_BITS	0
`define OR1200_ITLBMR_CID_BITS	4:1
`define OR1200_ITLBMR_RES_BITS	11:5
`define OR1200_ITLBMR_VPN_BITS	31:13
// ITLBTR bits
`define OR1200_ITLBTR_CC_BITS	0
`define OR1200_ITLBTR_CI_BITS	1
`define OR1200_ITLBTR_WBC_BITS	2
`define OR1200_ITLBTR_WOM_BITS	3
`define OR1200_ITLBTR_A_BITS	4
`define OR1200_ITLBTR_D_BITS	5
`define OR1200_ITLBTR_SXE_BITS	6
`define OR1200_ITLBTR_UXE_BITS	7
`define OR1200_ITLBTR_RES_BITS	11:8
`define OR1200_ITLBTR_PPN_BITS	31:13
// ITLB configuration
`define OR1200_IMMU_PS		13
`define OR1200_ITLB_INDXW	6	// 64 entry ITLB
//`define OR1200_ITLB_INDXW	7	// 128 entry ITLB
`define OR1200_ITLB_INDXL	`OR1200_IMMU_PS
`define OR1200_ITLB_INDXH	`OR1200_IMMU_PS+`OR1200_ITLB_INDXW-1
`define OR1200_ITLB_INDX	`OR1200_ITLB_INDXH:`OR1200_ITLB_INDXL
`define OR1200_ITLB_TAGW	32-`OR1200_ITLB_INDXW-`OR1200_IMMU_PS
`define OR1200_ITLB_TAGL	`OR1200_ITLB_INDXH+1
`define OR1200_ITLB_TAG		31:`OR1200_ITLB_TAGL
`define OR1200_ITLBMRW		`OR1200_ITLB_TAGW+1
`define OR1200_ITLBTRW		32-`OR1200_IMMU_PS+3
// Cache inhibit while IMMU is not enabled/implemented
//`define OR1200_IMMU_CI	1'b1		// cache inhibited 0GB-4GB
//`define OR1200_IMMU_CI	!icpu_adr_i[31]	// cache inhibited 0GB-2GB
//`define OR1200_IMMU_CI	!icpu_adr_i[30]	// cache inhibited 0GB-1GB 2GB-3GB
//`define OR1200_IMMU_CI	icpu_adr_i[30]	// cache inhibited 1GB-2GB 3GB-4GB
//`define OR1200_IMMU_CI	icpu_adr_i[31]	// cache inhibited 2GB-4GB
`define OR1200_IMMU_CI		1'b0		// cached 0GB-4GB (default)
//-------------------------------------------------------------------//
// Instruction Cache
//
`ifdef OR1200_IC_1W_32KB
	`define OR1200_ICLS	5	// 32 byte line.
`else
	`define OR1200_ICLS	4	// 16 byte line.
`endif
// IC configurations
`ifdef OR1200_IC_1W_512B
	`define OR1200_ICSIZE	9
	`define OR1200_ICINDX	`OR1200_ICSIZE-2
	`define OR1200_ICINDXH	`OR1200_ICSIZE-1
	`define OR1200_ICTAGL	`OR1200_ICINDXH+1
	`define OR1200_ICTAG	`OR1200_ICSIZE-`OR1200_ICLS
	`define OR1200_ICTAG_W	24
`endif
`ifdef OR1200_IC_1W_4KB
	`define OR1200_ICSIZE	12
	`define OR1200_ICINDX	`OR1200_ICSIZE-2
	`define OR1200_ICINDXH	`OR1200_ICSIZE-1
	`define OR1200_ICTAGL	`OR1200_ICINDXH+1
	`define OR1200_ICTAG	`OR1200_ICSIZE-`OR1200_ICLS
	`define OR1200_ICTAG_W	21
`endif
`ifdef OR1200_IC_1W_8KB
	`define OR1200_ICSIZE	13
	`define OR1200_ICINDX	`OR1200_ICSIZE-2
	`define OR1200_ICINDXH	`OR1200_ICSIZE-1
	`define OR1200_ICTAGL	`OR1200_ICINDXH+1
	`define OR1200_ICTAG	`OR1200_ICSIZE-`OR1200_ICLS
	`define OR1200_ICTAG_W	20
`endif
`ifdef OR1200_IC_1W_16KB
	`define OR1200_ICSIZE	14
	`define OR1200_ICINDX	`OR1200_ICSIZE-2
	`define OR1200_ICINDXH	`OR1200_ICSIZE-1
	`define OR1200_ICTAGL	`OR1200_ICINDXH+1
	`define OR1200_ICTAG	`OR1200_ICSIZE-`OR1200_ICLS
	`define OR1200_ICTAG_W	19
`endif
`ifdef OR1200_IC_1W_32KB
	`define OR1200_ICSIZE	15
	`define OR1200_ICINDX	`OR1200_ICSIZE-2
	`define OR1200_ICINDXH	`OR1200_ICSIZE-1
	`define OR1200_ICTAGL	`OR1200_ICINDXH+1
	`define OR1200_ICTAG	`OR1200_ICSIZE-`OR1200_ICLS
	`define OR1200_ICTAG_W	18
`endif
//-------------------------------------------------------------------//
// Data Cache
//
`ifdef OR1200_DC_1W_32KB
	`define OR1200_DCLS	5	// 32 bytes
`else
	`define OR1200_DCLS	4	// 16 bytes
`endif
// Define to enable default behavior of cache as write through
// Turning this off enabled write back statergy
`define OR1200_DC_WRITETHROUGH
// Define to enable stores from the stack not doing writethrough.
//`define OR1200_DC_NOSTACKWRITETHROUGH
// Data cache SPR definitions
`define OR1200_SPRGRP_DC_ADR_WIDTH	3
// Data cache group SPR addresses
`define OR1200_SPRGRP_DC_DCCR	3'd0
`define OR1200_SPRGRP_DC_DCBPR	3'd1
`define OR1200_SPRGRP_DC_DCBFR	3'd2
`define OR1200_SPRGRP_DC_DCBIR	3'd3
`define OR1200_SPRGRP_DC_DCBWR	3'd4
`define OR1200_SPRGRP_DC_DCBLR	3'd5
// Data cache configurations
`ifdef OR1200_DC_1W_4KB
	`define OR1200_DCSIZE	12
	`define OR1200_DCINDX	`OR1200_DCSIZE-2
	`define OR1200_DCINDXH	`OR1200_DCSIZE-1
	`define OR1200_DCTAGL	`OR1200_DCINDXH+1
	`define OR1200_DCTAG	`OR1200_DCSIZE-`OR1200_DCLS
	`define OR1200_DCTAG_W	21
`endif
`ifdef OR1200_DC_1W_8KB
	`define OR1200_DCSIZE	13
	`define OR1200_DCINDX	`OR1200_DCSIZE-2
	`define OR1200_DCINDXH	`OR1200_DCSIZE-1
	`define OR1200_DCTAGL	`OR1200_DCINDXH+1
	`define OR1200_DCTAG	`OR1200_DCSIZE-`OR1200_DCLS
	`define OR1200_DCTAG_W	20
`endif
`ifdef OR1200_DC_1W_16KB
	`define OR1200_DCSIZE	14
	`define OR1200_DCINDX	`OR1200_DCSIZE-2
	`define OR1200_DCINDXH	`OR1200_DCSIZE-1
	`define OR1200_DCTAGL	`OR1200_DCINDXH+1
	`define OR1200_DCTAG	`OR1200_DCSIZE-`OR1200_DCLS
	`define OR1200_DCTAG_W	19
`endif
`ifdef OR1200_DC_1W_32KB
	`define OR1200_DCSIZE	15
	`define OR1200_DCINDX	`OR1200_DCSIZE-2
	`define OR1200_DCINDXH	`OR1200_DCSIZE-1
	`define OR1200_DCTAGL	`OR1200_DCINDXH+1
	`define OR1200_DCTAG	`OR1200_DCSIZE-`OR1200_DCLS
	`define OR1200_DCTAG_W	18
`endif
//-------------------------------------------------------------------//
// Store Buffer (SB)
//
// Number of store buffer entries
`define OR1200_SB_LOG		2	// 2 or 3
`define OR1200_SB_ENTRIES	4	// 4 or 8

//-------------------------------------------------------------------//
// Quick Embedded Memory (QMEM)
//
// Base address and mask of QMEM
`define OR1200_QMEM_IADDR	32'h0080_0000
`define OR1200_QMEM_IMASK	32'hfff0_0000 // Max QMEM size 1MB
`define OR1200_QMEM_DADDR	32'h0080_0000
`define OR1200_QMEM_DMASK	32'hfff0_0000 // Max QMEM size 1MB
// To enable qmem_sel* ports, define this macro.
//`define OR1200_QMEM_BSEL
// To enable qmem_ack port, define this macro.
//`define OR1200_QMEM_ACK

//-------------------------------------------------------------------//
// VR, UPR and Configuration Registers (Should be the last in config file!!!)
//
// Define if you want full address decode inside SYS group
`define OR1200_SYS_FULL_DECODE
// Offsets of VR, UPR and CFGR registers
`define OR1200_SPRGRP_SYS_VR		4'h0
`define OR1200_SPRGRP_SYS_UPR		4'h1
`define OR1200_SPRGRP_SYS_CPUCFGR	4'h2
`define OR1200_SPRGRP_SYS_DMMUCFGR	4'h3
`define OR1200_SPRGRP_SYS_IMMUCFGR	4'h4
`define OR1200_SPRGRP_SYS_DCCFGR	4'h5
`define OR1200_SPRGRP_SYS_ICCFGR	4'h6
`define OR1200_SPRGRP_SYS_DCFGR		4'h7
// VR bits
`define OR1200_VR_REV_BITS	5:0
`define OR1200_VR_RES1_BITS	15:6
`define OR1200_VR_CFG_BITS	23:16
`define OR1200_VR_VER_BITS	31:24
// VR values
`define OR1200_VR_REV		6'h08
`define OR1200_VR_RES1		10'h000
`define OR1200_VR_CFG		8'h00
`define OR1200_VR_VER		8'h12
// UPR bits
`define OR1200_UPR_UP_BITS	0
`define OR1200_UPR_DCP_BITS	1
`define OR1200_UPR_ICP_BITS	2
`define OR1200_UPR_DMP_BITS	3
`define OR1200_UPR_IMP_BITS	4
`define OR1200_UPR_MP_BITS	5
`define OR1200_UPR_DUP_BITS	6
`define OR1200_UPR_PCUP_BITS	7
`define OR1200_UPR_PMP_BITS	8
`define OR1200_UPR_PICP_BITS	9
`define OR1200_UPR_TTP_BITS	10
`define OR1200_UPR_FPP_BITS	11
`define OR1200_UPR_RES1_BITS	23:12
`define OR1200_UPR_CUP_BITS	31:24
// UPR values
`define OR1200_UPR_UP		1'b1
`ifdef OR1200_NO_DC
	`define OR1200_UPR_DCP	1'b0
`else
	`define OR1200_UPR_DCP	1'b1
`endif
`ifdef OR1200_NO_IC
	`define OR1200_UPR_ICP	1'b0
`else
	`define OR1200_UPR_ICP	1'b1
`endif
`ifdef OR1200_NO_DMMU
	`define OR1200_UPR_DMP	1'b0
`else
	`define OR1200_UPR_DMP	1'b1
`endif
`ifdef OR1200_NO_IMMU
	`define OR1200_UPR_IMP	1'b0
`else
	`define OR1200_UPR_IMP	1'b1
`endif
`ifdef OR1200_MAC_IMPLEMENTED
	`define OR1200_UPR_MP	1'b1
`else
	`define OR1200_UPR_MP	1'b0
`endif
`ifdef OR1200_DU_IMPLEMENTED
	`define OR1200_UPR_DUP	1'b1
`else
	`define OR1200_UPR_DUP	1'b0
`endif
`define OR1200_UPR_PCUP		1'b0    // Performance counters not present
`ifdef OR1200_PM_IMPLEMENTED
	`define OR1200_UPR_PMP	1'b1
`else
	`define OR1200_UPR_PMP	1'b0
`endif
`ifdef OR1200_PIC_IMPLEMENTED
	`define OR1200_UPR_PICP	1'b1
`else
	`define OR1200_UPR_PICP	1'b0
`endif
`ifdef OR1200_TT_IMPLEMENTED
	`define OR1200_UPR_TTP	1'b1
`else
	`define OR1200_UPR_TTP	1'b0
`endif
`ifdef OR1200_FPU_IMPLEMENTED
	`define OR1200_UPR_FPP	1'b1
`else
	`define OR1200_UPR_FPP	1'b0
`endif
`define OR1200_UPR_RES1		12'h000
`define OR1200_UPR_CUP		8'h00
// CPUCFGR bits
`define OR1200_CPUCFGR_NSGF_BITS	3:0
`define OR1200_CPUCFGR_HGF_BITS		4
`define OR1200_CPUCFGR_OB32S_BITS	5
`define OR1200_CPUCFGR_OB64S_BITS	6
`define OR1200_CPUCFGR_OF32S_BITS	7
`define OR1200_CPUCFGR_OF64S_BITS	8
`define OR1200_CPUCFGR_OV64S_BITS	9
`define OR1200_CPUCFGR_RES1_BITS	31:10
// CPUCFGR values
`define OR1200_CPUCFGR_NSGF		4'h0
`ifdef OR1200_RFRAM_16REG
	`define OR1200_CPUCFGR_HGF	1'b1
`else
	`define OR1200_CPUCFGR_HGF	1'b0
`endif
`define OR1200_CPUCFGR_OB32S		1'b1
`define OR1200_CPUCFGR_OB64S		1'b0
`ifdef OR1200_FPU_IMPLEMENTED
	`define OR1200_CPUCFGR_OF32S	1'b1
`else
	`define OR1200_CPUCFGR_OF32S	1'b0
`endif
`define OR1200_CPUCFGR_OF64S		1'b0
`define OR1200_CPUCFGR_OV64S		1'b0
`define OR1200_CPUCFGR_RES1		22'h000000
// DMMUCFGR bits
`define OR1200_DMMUCFGR_NTW_BITS	1:0
`define OR1200_DMMUCFGR_NTS_BITS	4:2
`define OR1200_DMMUCFGR_NAE_BITS	7:5
`define OR1200_DMMUCFGR_CRI_BITS	8
`define OR1200_DMMUCFGR_PRI_BITS	9
`define OR1200_DMMUCFGR_TEIRI_BITS	10
`define OR1200_DMMUCFGR_HTR_BITS	11
`define OR1200_DMMUCFGR_RES1_BITS	31:12
// DMMUCFGR values
`ifdef OR1200_NO_DMMU
	`define OR1200_DMMUCFGR_NTW	2'h0	// Irrelevant
	`define OR1200_DMMUCFGR_NTS	3'h0	// Irrelevant
	`define OR1200_DMMUCFGR_NAE	3'h0	// Irrelevant
	`define OR1200_DMMUCFGR_CRI	1'b0	// Irrelevant
	`define OR1200_DMMUCFGR_PRI	1'b0	// Irrelevant
	`define OR1200_DMMUCFGR_TEIRI	1'b0	// Irrelevant
	`define OR1200_DMMUCFGR_HTR	1'b0	// Irrelevant
	`define OR1200_DMMUCFGR_RES1	20'h00000
`else
	`define OR1200_DMMUCFGR_NTW	2'h0	// 1 TLB way
	`define OR1200_DMMUCFGR_NTS	3'h`OR1200_DTLB_INDXW	// Num TLB sets
	`define OR1200_DMMUCFGR_NAE	3'h0	// No ATB entries
	`define OR1200_DMMUCFGR_CRI	1'b0	// No control register
	`define OR1200_DMMUCFGR_PRI	1'b0	// No protection reg
	`define OR1200_DMMUCFGR_TEIRI	1'b0	// TLB entry inv reg NOT impl.
	`define OR1200_DMMUCFGR_HTR	1'b0	// No HW TLB reload
	`define OR1200_DMMUCFGR_RES1	20'h00000
`endif
// IMMUCFGR bits
`define OR1200_IMMUCFGR_NTW_BITS	1:0
`define OR1200_IMMUCFGR_NTS_BITS	4:2
`define OR1200_IMMUCFGR_NAE_BITS	7:5
`define OR1200_IMMUCFGR_CRI_BITS	8
`define OR1200_IMMUCFGR_PRI_BITS	9
`define OR1200_IMMUCFGR_TEIRI_BITS	10
`define OR1200_IMMUCFGR_HTR_BITS	11
`define OR1200_IMMUCFGR_RES1_BITS	31:12
// IMMUCFGR values
`ifdef OR1200_NO_IMMU
	`define OR1200_IMMUCFGR_NTW	2'h0	// Irrelevant
	`define OR1200_IMMUCFGR_NTS	3'h0	// Irrelevant
	`define OR1200_IMMUCFGR_NAE	3'h0	// Irrelevant
	`define OR1200_IMMUCFGR_CRI	1'b0	// Irrelevant
	`define OR1200_IMMUCFGR_PRI	1'b0	// Irrelevant
	`define OR1200_IMMUCFGR_TEIRI	1'b0	// Irrelevant
	`define OR1200_IMMUCFGR_HTR	1'b0	// Irrelevant
	`define OR1200_IMMUCFGR_RES1	20'h00000
`else
	`define OR1200_IMMUCFGR_NTW	2'h0	// 1 TLB way
	`define OR1200_IMMUCFGR_NTS	3'h`OR1200_ITLB_INDXW	// Num TLB sets
	`define OR1200_IMMUCFGR_NAE	3'h0	// No ATB entry
	`define OR1200_IMMUCFGR_CRI	1'b0	// No control reg
	`define OR1200_IMMUCFGR_PRI	1'b0	// No protection reg
	`define OR1200_IMMUCFGR_TEIRI	1'b0	// TLB entry inv reg NOT impl
	`define OR1200_IMMUCFGR_HTR	1'b0	// No HW TLB reload
	`define OR1200_IMMUCFGR_RES1	20'h00000
`endif
// DCCFGR bits
`define OR1200_DCCFGR_NCW_BITS		2:0
`define OR1200_DCCFGR_NCS_BITS		6:3
`define OR1200_DCCFGR_CBS_BITS		7
`define OR1200_DCCFGR_CWS_BITS		8
`define OR1200_DCCFGR_CCRI_BITS		9
`define OR1200_DCCFGR_CBIRI_BITS	10
`define OR1200_DCCFGR_CBPRI_BITS	11
`define OR1200_DCCFGR_CBLRI_BITS	12
`define OR1200_DCCFGR_CBFRI_BITS	13
`define OR1200_DCCFGR_CBWBRI_BITS	14
`define OR1200_DCCFGR_RES1_BITS		31:15
// DCCFGR values
`ifdef OR1200_NO_DC
	`define OR1200_DCCFGR_NCW	3'h0	// Irrelevant
	`define OR1200_DCCFGR_NCS	4'h0	// Irrelevant
	`define OR1200_DCCFGR_CBS	1'b0	// Irrelevant
	`define OR1200_DCCFGR_CWS	1'b0	// Irrelevant
	`define OR1200_DCCFGR_CCRI	1'b0	// Irrelevant
	`define OR1200_DCCFGR_CBIRI	1'b0	// Irrelevant
	`define OR1200_DCCFGR_CBPRI	1'b0	// Irrelevant
	`define OR1200_DCCFGR_CBLRI	1'b0	// Irrelevant
	`define OR1200_DCCFGR_CBFRI	1'b0	// Irrelevant
	`define OR1200_DCCFGR_CBWBRI	1'b0	// Irrelevant
	`define OR1200_DCCFGR_RES1	17'h00000
`else
	`define OR1200_DCCFGR_NCW	3'h0	// 1 cache way
	`define OR1200_DCCFGR_NCS 	(`OR1200_DCTAG)		// Num cache sets
	`define OR1200_DCCFGR_CBS	`OR1200_DCLS==4 ? 1'b0 : 1'b1	// 16 byte cache block
	`ifdef OR1200_DC_WRITETHROUGH
		`define OR1200_DCCFGR_CWS	1'b0	// Write-through strategy
	`else
		`define OR1200_DCCFGR_CWS	1'b1	// Write-back strategy
	`endif
	`define OR1200_DCCFGR_CCRI	1'b1	// Cache control reg impl.
	`define OR1200_DCCFGR_CBIRI	1'b1	// Cache block inv reg impl.
	`define OR1200_DCCFGR_CBPRI	1'b0	// Cache block prefetch reg not impl.
	`define OR1200_DCCFGR_CBLRI	1'b0	// Cache block lock reg not impl.
	`define OR1200_DCCFGR_CBFRI	1'b1	// Cache block flush reg impl.
	`ifdef OR1200_DC_WRITETHROUGH
		`define OR1200_DCCFGR_CBWBRI	1'b0	// Cache block WB reg not impl.
	`else
		`define OR1200_DCCFGR_CBWBRI	1'b1	// Cache block WB reg impl.
	`endif
	`define OR1200_DCCFGR_RES1	17'h00000
`endif
// ICCFGR bits
`define OR1200_ICCFGR_NCW_BITS		2:0
`define OR1200_ICCFGR_NCS_BITS		6:3
`define OR1200_ICCFGR_CBS_BITS		7
`define OR1200_ICCFGR_CWS_BITS		8
`define OR1200_ICCFGR_CCRI_BITS		9
`define OR1200_ICCFGR_CBIRI_BITS	10
`define OR1200_ICCFGR_CBPRI_BITS	11
`define OR1200_ICCFGR_CBLRI_BITS	12
`define OR1200_ICCFGR_CBFRI_BITS	13
`define OR1200_ICCFGR_CBWBRI_BITS	14
`define OR1200_ICCFGR_RES1_BITS		31:15
// ICCFGR values
`ifdef OR1200_NO_IC
	`define OR1200_ICCFGR_NCW	3'h0	// Irrelevant
	`define OR1200_ICCFGR_NCS	4'h0	// Irrelevant
	`define OR1200_ICCFGR_CBS	1'b0	// Irrelevant
	`define OR1200_ICCFGR_CWS	1'b0	// Irrelevant
	`define OR1200_ICCFGR_CCRI	1'b0	// Irrelevant
	`define OR1200_ICCFGR_CBIRI	1'b0	// Irrelevant
	`define OR1200_ICCFGR_CBPRI	1'b0	// Irrelevant
	`define OR1200_ICCFGR_CBLRI	1'b0	// Irrelevant
	`define OR1200_ICCFGR_CBFRI	1'b0	// Irrelevant
	`define OR1200_ICCFGR_CBWBRI	1'b0	// Irrelevant
	`define OR1200_ICCFGR_RES1	17'h00000
`else
	`define OR1200_ICCFGR_NCW	3'h0	// 1 cache way
	`define OR1200_ICCFGR_NCS	(`OR1200_ICTAG)	// Num cache sets
	`define OR1200_ICCFGR_CBS 	`OR1200_ICLS==4 ? 1'b0: 1'b1	// 16 byte cache block
	`define OR1200_ICCFGR_CWS	1'b0	// Irrelevant
	`define OR1200_ICCFGR_CCRI	1'b1	// Cache control reg impl.
	`define OR1200_ICCFGR_CBIRI	1'b1	// Cache block inv reg impl.
	`define OR1200_ICCFGR_CBPRI	1'b0	// Cache block prefetch reg not impl.
	`define OR1200_ICCFGR_CBLRI	1'b0	// Cache block lock reg not impl.
	`define OR1200_ICCFGR_CBFRI	1'b1	// Cache block flush reg impl.
	`define OR1200_ICCFGR_CBWBRI	1'b0	// Irrelevant
	`define OR1200_ICCFGR_RES1	17'h00000
`endif
// DCFGR bits
`define OR1200_DCFGR_NDP_BITS	3:0
`define OR1200_DCFGR_WPCI_BITS	4
`define OR1200_DCFGR_RES1_BITS	31:5
// DCFGR values
`ifdef OR1200_DU_HWBKPTS
	`define OR1200_DCFGR_NDP	4'h`OR1200_DU_DVRDCR_PAIRS	// # of DVR/DCR pairs
	`ifdef OR1200_DU_DWCR0
		`define OR1200_DCFGR_WPCI	1'b1
	`else
		`define OR1200_DCFGR_WPCI	1'b0	// WP counters not impl.
`endif
`else
	`define OR1200_DCFGR_NDP	4'h0	// Zero DVR/DCR pairs
	`define OR1200_DCFGR_WPCI	1'b0	// WP counters not impl.
`endif
`define OR1200_DCFGR_RES1	27'd0
