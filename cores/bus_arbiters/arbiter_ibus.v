`include "arbiter_defines.v"
// One master, 3 slaves.
module arbiter_ibus (
	//---> Master 0 
	input wire  [31:0]	wbm0_adr_i,
	input wire  [31:0]	wbm0_dat_i,
	input wire  [3:0]	wbm0_sel_i,
	input wire 		wbm0_we_i,
	input wire		wbm0_cyc_i,
	input wire		wbm0_stb_i,
	input wire  [2:0]	wbm0_cti_i,
	input wire  [1:0]	wbm0_bte_i,
	output wire [31:0]	wbm0_dat_o,
	output wire		wbm0_ack_o,
	output wire		wbm0_err_o,
	output wire		wbm0_rty_o,
	//---> Slave 0
	output wire [31:0]	wbs0_adr_o,
	output wire [31:0]	wbs0_dat_o,
	output wire [3:0]	wbs0_sel_o,
	output wire		wbs0_we_o,
	output wire		wbs0_cyc_o,
	output wire		wbs0_stb_o,
	output wire [2:0]	wbs0_cti_o,
	output wire [1:0]	wbs0_bte_o,
	input wire  [31:0]	wbs0_dat_i,
	input wire		wbs0_ack_i,
	input wire		wbs0_err_i,
	input wire		wbs0_rty_i,
	//---> Slave 1
	output wire [31:0]      wbs1_adr_o,
	output wire [31:0]      wbs1_dat_o,
	output wire [3:0]       wbs1_sel_o,
	output wire             wbs1_we_o,
	output wire             wbs1_cyc_o,
	output wire             wbs1_stb_o,
	output wire [2:0]       wbs1_cti_o,
	output wire [1:0]       wbs1_bte_o,
	input wire  [31:0]	wbs1_dat_i,
	input wire              wbs1_ack_i,
	input wire              wbs1_err_i,
	input wire              wbs1_rty_i,
	//---> Slave 2
	output wire [31:0]	wbs2_adr_o,
	output wire [31:0]	wbs2_dat_o,
	output wire [3:0]	wbs2_sel_o,
	output wire		wbs2_we_o,
	output wire		wbs2_cyc_o,
	output wire		wbs2_stb_o,
	output wire [2:0]	wbs2_cti_o,
	output wire [1:0]	wbs2_bte_o,
	input wire  [31:0]	wbs2_dat_i,
	input wire		wbs2_ack_i,
	input wire		wbs2_err_i,
	input wire		wbs2_rty_i,
	//---> Clock, reset
	input wire 	wb_clk_i,
	input wire	wb_rst_i

);
	parameter wb_addr_match_width = 8;
	parameter slave0_adr = 		8'hf0; // Usual Boot ROM
	parameter slave1_adr = 		8'h00; // Usual Main memory (SDRAM/FPGA SRAM)
	parameter slave2_adr = 		8'h10; // Usual Main ROM 

	`define WB_ARB_ADDR_MATCH_SEL	31:32-wb_addr_match_width
  	wire [2:0]	slave_sel;	// One bit per slave
	reg 		watchdog_err;
   
`ifdef ARBITER_IBUS_WATCHDOG
	reg [`ARBITER_IBUS_WATCHDOG_TIMER_WIDTH:0]  watchdog_timer;
	reg 	wbm0_stb_r; // Register strobe
	wire	wbm0_stb_edge; // Detect its edge
	reg 	wbm0_stb_edge_r, wbm0_ack_o_r; // Reg these, better timing

	always @(posedge wb_clk_i)
 		wbm0_stb_r <= wbm0_stb_i;

	assign wbm0_stb_edge = (wbm0_stb_i & !wbm0_stb_r);

	always @(posedge wb_clk_i)
		wbm0_stb_edge_r <= wbm0_stb_edge;

	always @(posedge wb_clk_i)
		wbm0_ack_o_r <= wbm0_ack_o;

   	// Counter logic
	always @(posedge wb_clk_i)
	if (wb_rst_i) 
		watchdog_timer <= 0;
	else if (wbm0_ack_o_r) // When we see an ack, turn off timer
		watchdog_timer <= 0;
	else if (wbm0_stb_edge_r) // New access means start timer again
		watchdog_timer <= 1;
	else if (|watchdog_timer) // Continue counting if counter > 0
		watchdog_timer <= watchdog_timer + 1'b1;

	always @(posedge wb_clk_i) 
		watchdog_err <= (&watchdog_timer);
`else // !`ifdef ARBITER_IBUS_WATCHDOG
	always @(posedge wb_clk_i) 
		watchdog_err <= 0;
`endif // !`ifdef ARBITER_IBUS_WATCHDOG
   
   

`ifdef ARBITER_IBUS_REGISTERING
   
	// Master input registers
	reg [31:0]	wbm0_adr_i_r;
	reg [31:0]	wbm0_dat_i_r;
	reg [3:0] 	wbm0_sel_i_r;   
	reg 		wbm0_we_i_r;
	reg 		wbm0_cyc_i_r;
	reg 		wbm0_stb_i_r;
	reg [2:0] 	wbm0_cti_i_r;
	reg [1:0] 	wbm0_bte_i_r;
	// Slave output registers
	reg [31:0]	wbs0_dat_i_r;   
	reg 		wbs0_ack_i_r;
	reg		wbs0_err_i_r;
	reg 		wbs0_rty_i_r;

	reg [31:0]	wbs1_dat_i_r;   
	reg 		wbs1_ack_i_r;
	reg		wbs1_err_i_r;
	reg		wbs1_rty_i_r;

	reg [31:0]      wbs2_dat_i_r;
	reg		wbs2_ack_i_r;
	reg		wbs2_err_i_r;
	reg		wbs2_rty_i_r;

	wire		wbm0_ack_o_pre_reg;

   
	// Register master input signals
	always @(posedge wb_clk_i) begin
		wbm0_adr_i_r <= wbm0_adr_i;
		wbm0_dat_i_r <= wbm0_dat_i;
		wbm0_sel_i_r <= wbm0_sel_i;
		wbm0_we_i_r  <= wbm0_we_i;
		wbm0_cyc_i_r <= wbm0_cyc_i;
		// Classic access only ?
		wbm0_stb_i_r <= wbm0_stb_i & !wbm0_ack_o_pre_reg & !wbm0_ack_o;
		wbm0_cti_i_r <= wbm0_cti_i;
		wbm0_bte_i_r <= wbm0_bte_i;

		// Slave 0 signals
		wbs0_dat_i_r <= wbs0_dat_i;
		wbs0_ack_i_r <= wbs0_ack_i;
		wbs0_err_i_r <= wbs0_err_i;
		wbs0_rty_i_r <= wbs0_rty_i;
		// Slave 1 signals
		wbs1_dat_i_r <= wbs1_dat_i;
		wbs1_ack_i_r <= wbs1_ack_i;
		wbs1_err_i_r <= wbs1_err_i;
		wbs1_rty_i_r <= wbs1_rty_i;
		// Slave 2 signals
		wbs2_dat_i_r <= wbs2_dat_i;
		wbs2_ack_i_r <= wbs2_ack_i;
		wbs2_err_i_r <= wbs2_err_i;
		wbs2_rty_i_r <= wbs2_rty_i;
	end 
	// Slave select
 	assign slave_sel[0] = wbm0_adr_i_r[`WB_ARB_ADDR_MATCH_SEL] == slave0_adr;
	assign slave_sel[1] = wbm0_adr_i_r[`WB_ARB_ADDR_MATCH_SEL] == slave1_adr;
	assign slave_sel[2] = wbm0_adr_i_r[`WB_ARB_ADDR_MATCH_SEL] == slave2_adr;
   	// Slave 0 output assigns
	assign wbs0_adr_o = wbm0_adr_i_r;
	assign wbs0_dat_o = wbm0_dat_i_r;
	assign wbs0_we_o  = wbm0_we_i_r;
	assign wbs0_sel_o = wbm0_sel_i_r;
	assign wbs0_cti_o = wbm0_cti_i_r;
   	assign wbs0_bte_o = wbm0_bte_i_r;
   	assign wbs0_cyc_o = wbm0_cyc_i_r & slave_sel[0];
   	assign wbs0_stb_o = wbm0_stb_i_r & slave_sel[0];
	// Slave 1 output assigns
	assign wbs1_adr_o = wbm0_adr_i_r;
   	assign wbs1_dat_o = wbm0_dat_i_r;
	assign wbs1_we_o  = wbm0_dat_i_r;
	assign wbs1_sel_o = wbm0_sel_i_r;
	assign wbs1_cti_o = wbm0_cti_i_r;
	assign wbs1_bte_o = wbm0_bte_i_r;
	assign wbs1_cyc_o = wbm0_cyc_i_r & slave_sel[1];
	assign wbs1_stb_o = wbm0_stb_i_r & slave_sel[1];
	// Slave 2 output assigns
	assign wbs2_adr_o = wbm0_adr_i_r;
	assign wbs2_dat_o = wbm0_dat_i_r;
	assign wbs2_we_o  = wbm0_dat_i_r;
	assign wbs2_sel_o = wbm0_sel_i_r;
	assign wbs2_cti_o = wbm0_cti_i_r;
	assign wbs2_bte_o = wbm0_bte_i_r;
	assign wbs2_cyc_o = wbm0_cyc_i_r & slave_sel[2];
	assign wbs2_stb_o = wbm0_stb_i_r & slave_sel[2];

	// Master output assigns
	assign wbm0_dat_o = slave_sel[2] ? 
			wbs2_dat_i_r: slave_sel[1] ?
			wbs1_dat_i_r:
			wbs0_dat_i_r;
   	assign wbm0_ack_o = 	(slave_sel[0] & wbs0_ack_i_r) |
				(slave_sel[1] & wbs1_ack_i_r) |
				(slave_sel[2] & wbs2_ack_i_r);
   	assign wbm0_err_o = 	(slave_sel[0] & wbs0_err_i_r) |
				(slave_sel[1] & wbs1_err_i_r) |
				(slave_sel[2] & wbs2_err_i_r) |
				watchdog_err;
	assign wbm0_rty_o = 	(slave_sel[0] & wbs0_rty_i_r) |
				(slave_sel[1] & wbs1_rty_i_r) |
				(slave_sel[2] & wbs2_rty_i_r);
	// Non-registered ack
	assign wbm0_ack_o_pre_reg = (slave_sel[0] & wbs0_ack_i) |
				(slave_sel[1] & wbs1_ack_i) |
				(slave_sel[2] & wbs2_ack_i);
   
`else // !`ifdef ARBITER_IBUS_REGISTERING
	// Slave select
	assign slave_sel[0] = wbm0_adr_i[`WB_ARB_ADDR_MATCH_SEL] == slave0_adr;
	assign slave_sel[1] = wbm0_adr_i[`WB_ARB_ADDR_MATCH_SEL] == slave1_adr;
	assign slave_sel[2] = wbm0_adr_i[`WB_ARB_ADDR_MATCH_SEL] == slave2_adr;
	// Slave 0 output assigns
	assign wbs0_adr_o = wbm0_adr_i;
	assign wbs0_dat_o = wbm0_dat_i;
	assign wbs0_we_o  = wbm0_we_i;
	assign wbs0_sel_o = wbm0_sel_i;
	assign wbs0_cti_o = wbm0_cti_i;
	assign wbs0_bte_o = wbm0_bte_i;
	assign wbs0_cyc_o = wbm0_cyc_i & slave_sel[0];
	assign wbs0_stb_o = wbm0_stb_i & slave_sel[0];
	// Slave 1 output assigns
	assign wbs1_adr_o = wbm0_adr_i;
	assign wbs1_dat_o = wbm0_dat_i;
	assign wbs1_we_o  = wbm0_we_i;
	assign wbs1_sel_o = wbm0_sel_i;
	assign wbs1_cti_o = wbm0_cti_i;
	assign wbs1_bte_o = wbm0_bte_i;
	assign wbs1_cyc_o = wbm0_cyc_i & slave_sel[1];
	assign wbs1_stb_o = wbm0_stb_i & slave_sel[1];
	// Slave 2 output assigns
	assign wbs2_adr_o = wbm0_adr_i;
	assign wbs2_dat_o = wbm0_dat_i;
	assign wbs2_we_o  = wbm0_we_i;
	assign wbs2_sel_o = wbm0_sel_i;
	assign wbs2_cti_o = wbm0_cti_i;
	assign wbs2_bte_o = wbm0_bte_i;
	assign wbs2_cyc_o = wbm0_cyc_i & slave_sel[2];
	assign wbs2_stb_o = wbm0_stb_i & slave_sel[2];
	// Master output assigns
	assign wbm0_dat_o = slave_sel[2] ? 
			wbs2_dat_i : slave_sel[1] ?
			wbs1_dat_i :
			wbs0_dat_i ;
   	assign wbm0_ack_o = 	(slave_sel[0] & wbs0_ack_i) |
				(slave_sel[1] & wbs1_ack_i) |
				(slave_sel[2] & wbs2_ack_i);
	assign wbm0_err_o = 	(slave_sel[0] & wbs0_err_i) |
				(slave_sel[1] & wbs1_err_i) | 
				(slave_sel[2] & wbs2_err_i) | watchdog_err;
	assign wbm0_rty_o = 	(slave_sel[0] & wbs0_rty_i) |
				(slave_sel[1] & wbs1_rty_i) |
				(slave_sel[2] & wbs2_rty_i);
`endif // !`ifdef ARBITER_IBUS_REGISTERING
endmodule
