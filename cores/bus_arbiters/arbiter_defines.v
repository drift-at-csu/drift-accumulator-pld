//===================================================================//
//             Arbiters defines
//===================================================================//

///////////////////////////////////
//  Instruction Bus (IBUS)      //
/////////////////////////////////

// Instruction bus arbiter registering (good wb master design is already doing it)
//`define ARBITER_IBUS_REGISTERING
`define ARBITER_IBUS_WATCHDOG
// Watchdog timeout: 2^(ARBITER_IBUS_WATCHDOG_TIMER_WIDTH+1) cycles
`define ARBITER_IBUS_WATCHDOG_TIMER_WIDTH 24

/////////////////////////////////
// Data Bus   (DBUS)          //
///////////////////////////////
// Don't register for the same reason
//`define ARBITER_DBUS_REGISTERING
`define ARBITER_DBUS_WATCHDOG
// Watchdog timeout: 2^(ARBITER_DBUS_WATCHDOG_TIMER_WIDTH+1) cycles
`define ARBITER_DBUS_WATCHDOG_TIMER_WIDTH 24

//////////////////////////////
//  ADCs Bus (ABUS)        //
////////////////////////////
//`define ARBITER_ABUS_REGISTERING
//`define ARBITER_ABUS_WATCHDOG
// Watchdog timeout: 2^(ARBITER_ABUS_WATCHDOG_TIMER_WIDTH+1) cycles
// `define ARBITER_ABUS_WATCHDOG_TIMER_WIDTH 20

//////////////////////////////
// Byte Bus (BBUS)         //
////////////////////////////
// Don't really need the watchdog here - the databus will pick it up
// `define ARBITER_BBUS_WATCHDOG
// Watchdog timeout: 2^(ARBITER_BYTEBUS_WATCHDOG_TIMER_WIDTH+1) cycles
`define ARBITER_BBUS_WATCHDOG_TIMER_WIDTH 24

