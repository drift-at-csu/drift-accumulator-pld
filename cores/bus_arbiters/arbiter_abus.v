`include "arbiter_defines.v"
module arbiter_abus (
	//---> Master 0
	input wire  [31:0]	wbm0_adr_i,
	input wire  [31:0] 	wbm0_dat_i,
	input wire  [3:0]	wbm0_sel_i,
	input wire		wbm0_we_i,
	input wire		wbm0_cyc_i,
	input wire		wbm0_stb_i,
	input wire  [2:0]	wbm0_cti_i,
	input wire  [1:0]	wbm0_bte_i,
	output wire [31:0]	wbm0_dat_o,
	output wire		wbm0_ack_o,
	output wire		wbm0_err_o,
	output wire		wbm0_rty_o,
	//---> Master 1
	input wire  [31:0]	wbm1_adr_i,
	input wire  [31:0]	wbm1_dat_i,
	input wire  [3:0]	wbm1_sel_i,
	input wire		wbm1_we_i,
	input wire		wbm1_cyc_i,
	input wire		wbm1_stb_i,
	input wire  [2:0]	wbm1_cti_i,
	input wire  [1:0]	wbm1_bte_i,
	output wire [31:0]	wbm1_dat_o,
	output wire		wbm1_ack_o,
	output wire		wbm1_err_o,
	output wire		wbm1_rty_o,
	//---> Master 2
	input wire  [31:0]	wbm2_adr_i,
	input wire  [31:0]	wbm2_dat_i,
	input wire  [3:0]	wbm2_sel_i,
	input wire		wbm2_we_i,
	input wire		wbm2_cyc_i,
	input wire		wbm2_stb_i,
	input wire  [2:0]	wbm2_cti_i,
	input wire  [1:0]	wbm2_bte_i,
	output wire [31:0]	wbm2_dat_o,
	output wire		wbm2_ack_o,
	output wire		wbm2_err_o,
	output wire		wbm2_rty_o,
	//---> Master 3
	input wire  [31:0]	wbm3_adr_i,
	input wire  [31:0]	wbm3_dat_i,
	input wire  [3:0]	wbm3_sel_i,
	input wire		wbm3_we_i,
	input wire		wbm3_cyc_i,
	input wire		wbm3_stb_i,
	input wire  [2:0]	wbm3_cti_i,
	input wire  [1:0]	wbm3_bte_i,
	output wire [31:0]	wbm3_dat_o,
	output wire		wbm3_ack_o,
	output wire		wbm3_err_o,
	output wire		wbm3_rty_o,
	//---> Master 4
	input wire  [31:0]	wbm4_adr_i,
	input wire  [31:0]	wbm4_dat_i,
	input wire  [3:0]	wbm4_sel_i,
	input wire		wbm4_we_i,
	input wire		wbm4_cyc_i,
	input wire		wbm4_stb_i,
	input wire  [2:0]	wbm4_cti_i,
	input wire  [1:0]	wbm4_bte_i,
	output wire [31:0]	wbm4_dat_o,
	output wire		wbm4_ack_o,
	output wire		wbm4_err_o,
	output wire		wbm4_rty_o,
	//---> Master 5
	input wire  [31:0]	wbm5_adr_i,
	input wire  [31:0]	wbm5_dat_i,
	input wire  [3:0]	wbm5_sel_i,
	input wire		wbm5_we_i,
	input wire		wbm5_cyc_i,
	input wire		wbm5_stb_i,
	input wire  [2:0]	wbm5_cti_i,
	input wire  [1:0]	wbm5_bte_i,
	output wire [31:0]	wbm5_dat_o,
	output wire		wbm5_ack_o,
	output wire		wbm5_err_o,
	output wire		wbm5_rty_o,
	//---> Master 6
	input wire  [31:0]	wbm6_adr_i,
	input wire  [31:0]	wbm6_dat_i,
	input wire  [3:0]	wbm6_sel_i,
	input wire		wbm6_we_i,
	input wire		wbm6_cyc_i,
	input wire		wbm6_stb_i,
	input wire  [2:0]	wbm6_cti_i,
	input wire  [1:0]	wbm6_bte_i,
	output wire [31:0]	wbm6_dat_o,
	output wire		wbm6_ack_o,
	output wire		wbm6_err_o,
	output wire		wbm6_rty_o,
	//---> Master 7
	input wire  [31:0]	wbm7_adr_i,
	input wire  [31:0]	wbm7_dat_i,
	input wire  [3:0]	wbm7_sel_i,
	input wire		wbm7_we_i,
	input wire		wbm7_cyc_i,
	input wire		wbm7_stb_i,
	input wire  [2:0]	wbm7_cti_i,
	input wire  [1:0]	wbm7_bte_i,
	output wire [31:0]	wbm7_dat_o,
	output wire		wbm7_ack_o,
	output wire		wbm7_err_o,
	output wire		wbm7_rty_o,
	//---> Slave 0
	output wire [31:0]	wbs0_adr_o,
	output wire [31:0] 	wbs0_dat_o,
	output wire  [3:0]      wbs0_sel_o,
	output wire		wbs0_we_o,
	output wire		wbs0_cyc_o,
	output wire		wbs0_stb_o,
	output wire  [2:0]	wbs0_cti_o,
	output wire  [1:0]	wbs0_bte_o,
	input wire  [31:0]	wbs0_dat_i,
	input wire		wbs0_ack_i,
	input wire		wbs0_err_i,
	input wire		wbs0_rty_i,
	//---> Clock and reset
   	input wire	wb_clk_i, 
	input wire	wb_rst_i
);
// TODO: Make this bus Wishbone b4 compartible (with stalls)

reg [7:0]	input_select, last_selected;
wire	arb_for_wbm0, arb_for_wbm1, arb_for_wbm2, arb_for_wbm3;
wire    arb_for_wbm4, arb_for_wbm5, arb_for_wbm6, arb_for_wbm7;

// Wires allowing selection of new input
assign arb_for_wbm0 = (	last_selected[1] | 
			last_selected[2] |
			last_selected[3] |
			last_selected[4] |
			last_selected[5] |
			last_selected[6] |
			last_selected[7] |
                        !wbm1_cyc_i |
			!wbm2_cyc_i |
			!wbm3_cyc_i |
			!wbm4_cyc_i |
			!wbm5_cyc_i |
			!wbm6_cyc_i |
			!wbm7_cyc_i ) & !(|input_select);
assign arb_for_wbm1 = ( last_selected[0] |
                        last_selected[2] |
                        last_selected[3] |
                        last_selected[4] |
                        last_selected[5] |
                        last_selected[6] |
                        last_selected[7] |
                        !wbm0_cyc_i |
                        !wbm2_cyc_i |
                        !wbm3_cyc_i |
                        !wbm4_cyc_i |
                        !wbm5_cyc_i |
                        !wbm6_cyc_i |
                        !wbm7_cyc_i ) & !(|input_select);
assign arb_for_wbm2 = ( last_selected[0] |
                        last_selected[1] |
                        last_selected[3] |
                        last_selected[4] |
                        last_selected[5] |
                        last_selected[6] |
                        last_selected[7] |
                        !wbm0_cyc_i |
                        !wbm1_cyc_i |
                        !wbm3_cyc_i |
                        !wbm4_cyc_i |
                        !wbm5_cyc_i |
                        !wbm6_cyc_i |
                        !wbm7_cyc_i ) & !(|input_select);
assign arb_for_wbm3 = ( last_selected[0] |
                        last_selected[1] |
                        last_selected[2] |
                        last_selected[4] |
                        last_selected[5] |
                        last_selected[6] |
                        last_selected[7] |
                        !wbm0_cyc_i |
                        !wbm1_cyc_i |
                        !wbm2_cyc_i |
                        !wbm4_cyc_i |
                        !wbm5_cyc_i |
                        !wbm6_cyc_i |
                        !wbm7_cyc_i ) & !(|input_select);
assign arb_for_wbm4 = ( last_selected[0] |
                        last_selected[1] |
                        last_selected[2] |
                        last_selected[3] |
                        last_selected[5] |
                        last_selected[6] |
                        last_selected[7] |
                        !wbm0_cyc_i |
                        !wbm1_cyc_i |
                        !wbm2_cyc_i |
                        !wbm3_cyc_i |
                        !wbm5_cyc_i |
                        !wbm6_cyc_i |
                        !wbm7_cyc_i ) & !(|input_select);
assign arb_for_wbm5 = ( last_selected[0] |
                        last_selected[1] |
                        last_selected[2] |
                        last_selected[3] |
                        last_selected[4] |
                        last_selected[6] |
                        last_selected[7] |
                        !wbm0_cyc_i |
                        !wbm1_cyc_i |
                        !wbm2_cyc_i |
                        !wbm3_cyc_i |
                        !wbm4_cyc_i |
                        !wbm6_cyc_i |
                        !wbm7_cyc_i ) & !(|input_select);
assign arb_for_wbm6 = ( last_selected[0] |
                        last_selected[1] |
                        last_selected[2] |
                        last_selected[3] |
                        last_selected[4] |
                        last_selected[5] |
                        last_selected[7] |
                        !wbm0_cyc_i |
                        !wbm1_cyc_i |
                        !wbm2_cyc_i |
                        !wbm3_cyc_i |
                        !wbm4_cyc_i |
                        !wbm5_cyc_i |
                        !wbm7_cyc_i ) & !(|input_select);
assign arb_for_wbm7 = ( last_selected[0] |
                        last_selected[1] |
                        last_selected[2] |
                        last_selected[3] |
                        last_selected[4] |
                        last_selected[5] |
                        last_selected[6] |
                        !wbm0_cyc_i |
                        !wbm1_cyc_i |
                        !wbm2_cyc_i |
                        !wbm3_cyc_i |
                        !wbm4_cyc_i |
                        !wbm5_cyc_i |
                        !wbm6_cyc_i ) & !(|input_select);
// Master select logic
always @(posedge wb_clk_i)
     if (wb_rst_i)
	input_select <= 0;
     else if (	(input_select[0] & !wbm0_cyc_i) | 
     		(input_select[1] & !wbm1_cyc_i) |
		(input_select[2] & !wbm2_cyc_i) |
		(input_select[3] & !wbm3_cyc_i) |
		(input_select[4] & !wbm4_cyc_i) |
		(input_select[5] & !wbm5_cyc_i) |
		(input_select[6] & !wbm6_cyc_i) |
		(input_select[7] & !wbm7_cyc_i)
	     )
	input_select <= 0;
     else if (!(&input_select) & wbm0_cyc_i & arb_for_wbm0)
	input_select <= 8'b00000001;
     else if (!(&input_select) & wbm1_cyc_i & arb_for_wbm1)
	input_select <= 8'b00000010;
     else if (!(&input_select) & wbm2_cyc_i & arb_for_wbm2)
	input_select <= 8'b00000100;
     else if (!(&input_select) & wbm3_cyc_i & arb_for_wbm3)
	input_select <= 8'b00001000;
     else if (!(&input_select) & wbm4_cyc_i & arb_for_wbm4)
	input_select <= 8'b00010000;
     else if (!(&input_select) & wbm5_cyc_i & arb_for_wbm5)
	input_select <= 8'b00100000;
     else if (!(&input_select) & wbm6_cyc_i & arb_for_wbm6)
	input_select <= 8'b01000000;
     else if (!(&input_select) & wbm7_cyc_i & arb_for_wbm7)
	input_select <= 8'b10000000;

always @(posedge wb_clk_i)
     if (wb_rst_i)
	last_selected <= 0;
     else if (!(&input_select) & wbm0_cyc_i & arb_for_wbm0)
	last_selected <= 8'b00000001;
     else if (!(&input_select) & wbm1_cyc_i & arb_for_wbm1)
	last_selected <= 8'b00000010;
     else if (!(&input_select) & wbm2_cyc_i & arb_for_wbm2)
	last_selected <= 8'b00000100;
     else if (!(&input_select) & wbm3_cyc_i & arb_for_wbm3)
	last_selected <= 8'b00001000;
     else if (!(&input_select) & wbm4_cyc_i & arb_for_wbm4)
	last_selected <= 8'b00010000;
     else if (!(&input_select) & wbm5_cyc_i & arb_for_wbm5)
	last_selected <= 8'b00100000;
     else if (!(&input_select) & wbm6_cyc_i & arb_for_wbm6)
	last_selected <= 8'b01000000;
     else if (!(&input_select) & wbm7_cyc_i & arb_for_wbm7)
	last_selected <= 8'b10000000;

// Mux input signals
assign wbs0_adr_o = 	(input_select[7]) ? wbm7_adr_i :
			(input_select[6]) ? wbm6_adr_i :
			(input_select[5]) ? wbm5_adr_i :
			(input_select[4]) ? wbm4_adr_i :
			(input_select[3]) ? wbm3_adr_i :
			(input_select[2]) ? wbm2_adr_i :
			(input_select[1]) ? wbm1_adr_i :
			(input_select[0]) ? wbm0_adr_i : 0;

assign wbs0_dat_o =	(input_select[7]) ? wbm7_dat_i :
                        (input_select[6]) ? wbm6_dat_i :
			(input_select[5]) ? wbm5_dat_i :
			(input_select[4]) ? wbm4_dat_i :
			(input_select[3]) ? wbm3_dat_i :
			(input_select[2]) ? wbm2_dat_i :
			(input_select[1]) ? wbm1_dat_i :
			(input_select[0]) ? wbm0_dat_i : 0;
assign wbs0_sel_o =	(input_select[7]) ? wbm7_sel_i :
                        (input_select[6]) ? wbm6_sel_i :
			(input_select[5]) ? wbm5_sel_i :
			(input_select[4]) ? wbm4_sel_i :
			(input_select[3]) ? wbm3_sel_i :
			(input_select[2]) ? wbm2_sel_i :
			(input_select[1]) ? wbm1_sel_i :
			(input_select[0]) ? wbm0_sel_i : 0;
assign wbs0_we_o  =	(input_select[7]) ? wbm7_we_i :
                        (input_select[6]) ? wbm6_we_i :
			(input_select[5]) ? wbm5_we_i :
			(input_select[4]) ? wbm4_we_i :
			(input_select[3]) ? wbm3_we_i :
			(input_select[2]) ? wbm2_we_i :
			(input_select[1]) ? wbm1_we_i :
			(input_select[0]) ? wbm0_we_i : 0;
assign wbs0_cyc_o =	(input_select[7]) ? wbm7_cyc_i :
			(input_select[6]) ? wbm6_cyc_i :
			(input_select[5]) ? wbm5_cyc_i :
			(input_select[4]) ? wbm4_cyc_i :
			(input_select[3]) ? wbm3_cyc_i :
			(input_select[2]) ? wbm2_cyc_i :
			(input_select[1]) ? wbm1_cyc_i :
			(input_select[0]) ? wbm0_cyc_i : 0;
assign wbs0_stb_o =	(input_select[7]) ? wbm7_stb_i :
                        (input_select[6]) ? wbm6_stb_i :
			(input_select[5]) ? wbm5_stb_i :
			(input_select[4]) ? wbm4_stb_i :
			(input_select[3]) ? wbm3_stb_i :
			(input_select[2]) ? wbm2_stb_i :
			(input_select[1]) ? wbm1_stb_i :
			(input_select[0]) ? wbm0_stb_i : 0;
assign wbs0_cti_o =	(input_select[7]) ? wbm7_cti_i :
                        (input_select[6]) ? wbm6_cti_i :
			(input_select[5]) ? wbm5_cti_i :
			(input_select[4]) ? wbm4_cti_i :
			(input_select[3]) ? wbm3_cti_i :
			(input_select[2]) ? wbm2_cti_i :
			(input_select[1]) ? wbm1_cti_i :
			(input_select[0]) ? wbm0_cti_i : 0;
assign wbs0_bte_o =	(input_select[7]) ? wbm7_bte_i :
			(input_select[6]) ? wbm6_bte_i :
			(input_select[5]) ? wbm5_bte_i :
			(input_select[4]) ? wbm4_bte_i :
			(input_select[3]) ? wbm3_bte_i :
			(input_select[2]) ? wbm2_bte_i :
			(input_select[1]) ? wbm1_bte_i :
			(input_select[0]) ? wbm0_bte_i : 0;

// Outputs to masters
assign wbm0_dat_o = wbs0_dat_i;
assign wbm0_ack_o = wbs0_ack_i & input_select[0];
assign wbm0_err_o = wbs0_err_i & input_select[0];
assign wbm0_rty_o = wbs0_rty_i & input_select[0];

assign wbm1_dat_o = wbs0_dat_i;
assign wbm1_ack_o = wbs0_ack_i & input_select[1];
assign wbm1_err_o = wbs0_err_i & input_select[1];
assign wbm1_rty_o = wbs0_rty_i & input_select[1];

assign wbm2_dat_o = wbs0_dat_i;
assign wbm2_ack_o = wbs0_ack_i & input_select[2];
assign wbm2_err_o = wbs0_err_i & input_select[2];
assign wbm2_rty_o = wbs0_rty_i & input_select[2];

assign wbm3_dat_o = wbs0_dat_i;
assign wbm3_ack_o = wbs0_ack_i & input_select[3];
assign wbm3_err_o = wbs0_err_i & input_select[3];
assign wbm3_rty_o = wbs0_rty_i & input_select[3];

assign wbm4_dat_o = wbs0_dat_i;
assign wbm4_ack_o = wbs0_ack_i & input_select[4];
assign wbm4_err_o = wbs0_err_i & input_select[4];
assign wbm4_rty_o = wbs0_rty_i & input_select[4];

assign wbm5_dat_o = wbs0_dat_i;
assign wbm5_ack_o = wbs0_ack_i & input_select[5];
assign wbm5_err_o = wbs0_err_i & input_select[5];
assign wbm5_rty_o = wbs0_rty_i & input_select[5];

assign wbm6_dat_o = wbs0_dat_i;
assign wbm6_ack_o = wbs0_ack_i & input_select[6];
assign wbm6_err_o = wbs0_err_i & input_select[6];
assign wbm6_rty_o = wbs0_rty_i & input_select[6];

assign wbm7_dat_o = wbs0_dat_i;
assign wbm7_ack_o = wbs0_ack_i & input_select[7];
assign wbm7_err_o = wbs0_err_i & input_select[7];
assign wbm7_rty_o = wbs0_rty_i & input_select[7];

endmodule // arbiter_abus
