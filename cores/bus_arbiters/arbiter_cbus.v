`include "arbiter_defines.v"
module arbiter_cbus (
	//---> Master 0
	input wire  [31:0]	wbm0_adr_i,
	input wire  [31:0] 	wbm0_dat_i,
	input wire   [3:0]	wbm0_sel_i,
	input wire		wbm0_we_i,
	input wire		wbm0_cyc_i,
	input wire		wbm0_stb_i,
	input wire   [2:0]	wbm0_cti_i,
	input wire   [1:0]	wbm0_bte_i,
	output wire [31:0]	wbm0_dat_o,
	output wire		wbm0_ack_o,
	output wire		wbm0_err_o,
	output wire		wbm0_rty_o,

	//---> Slave 0
	output wire [31:0]	wbs0_adr_o,
	output wire  [3:0]	wbs0_sel_o,
	output wire [31:0] 	wbs0_dat_o,
	output wire		wbs0_we_o,
	output wire		wbs0_cyc_o,
	output wire		wbs0_stb_o,
	output wire  [2:0]	wbs0_cti_o,
	output wire  [1:0]	wbs0_bte_o,
	input wire  [31:0]	wbs0_dat_i,
	input wire		wbs0_ack_i,
	input wire		wbs0_err_i,
	input wire		wbs0_rty_i,
	//---> Slave 1
	output wire [31:0]	wbs1_adr_o,
	output wire  [3:0]	wbs1_sel_o,
	output wire [31:0]	wbs1_dat_o,
	output wire		wbs1_we_o,
	output wire		wbs1_cyc_o,
	output wire		wbs1_stb_o,
	output wire  [2:0]	wbs1_cti_o,
	output wire  [1:0]	wbs1_bte_o,
	input wire  [31:0]	wbs1_dat_i,
	input wire		wbs1_ack_i,
	input wire		wbs1_err_i,
	input wire		wbs1_rty_i,
	//---> Slave 2
	output wire [31:0]	wbs2_adr_o,
	output wire  [3:0]	wbs2_sel_o,
	output wire [31:0]	wbs2_dat_o,
	output wire		wbs2_we_o,
	output wire		wbs2_cyc_o,
	output wire		wbs2_stb_o,
	output wire  [2:0]	wbs2_cti_o,
	output wire  [1:0]	wbs2_bte_o,
	input wire  [31:0]	wbs2_dat_i,
	input wire		wbs2_ack_i,
	input wire		wbs2_err_i,
	input wire		wbs2_rty_i,
	//---> Slave 3
	output wire [31:0]	wbs3_adr_o,
	output wire  [3:0]	wbs3_sel_o,
	output wire [31:0]	wbs3_dat_o,
	output wire		wbs3_we_o,
	output wire		wbs3_cyc_o,
	output wire		wbs3_stb_o,
	output wire  [2:0]	wbs3_cti_o,
	output wire  [1:0]	wbs3_bte_o,
	input wire  [31:0]	wbs3_dat_i,
	input wire		wbs3_ack_i,
	input wire		wbs3_err_i,
	input wire		wbs3_rty_i,
	//---> Slave 4
	output wire [31:0]	wbs4_adr_o,
	output wire  [3:0]	wbs4_sel_o,
	output wire [31:0]	wbs4_dat_o,
	output wire		wbs4_we_o,
	output wire		wbs4_cyc_o,
	output wire		wbs4_stb_o,
	output wire  [2:0]	wbs4_cti_o,
	output wire  [1:0]	wbs4_bte_o,
	input wire  [31:0]	wbs4_dat_i,
	input wire		wbs4_ack_i,
	input wire		wbs4_err_i,
	input wire		wbs4_rty_i,
	//---> Slave 5
	output wire [31:0]	wbs5_adr_o,
	output wire  [3:0]	wbs5_sel_o,
	output wire [31:0]	wbs5_dat_o,
	output wire		wbs5_we_o,
	output wire		wbs5_cyc_o,
	output wire		wbs5_stb_o,
	output wire  [2:0]	wbs5_cti_o,
	output wire  [1:0]	wbs5_bte_o,
	input wire  [31:0]	wbs5_dat_i,
	input wire		wbs5_ack_i,
	input wire		wbs5_err_i,
	input wire		wbs5_rty_i,
	//---> Slave 6
	output wire [31:0]	wbs6_adr_o,
	output wire  [3:0]	wbs6_sel_o,
	output wire [31:0]	wbs6_dat_o,
	output wire		wbs6_we_o,
	output wire		wbs6_cyc_o,
	output wire		wbs6_stb_o,
	output wire  [2:0]	wbs6_cti_o,
	output wire  [1:0]	wbs6_bte_o,
	input wire  [31:0]	wbs6_dat_i,
	input wire		wbs6_ack_i,
	input wire		wbs6_err_i,
	input wire		wbs6_rty_i,
	//---> Slave 7
	output wire [31:0]	wbs7_adr_o,
	output wire  [3:0]	wbs7_sel_o,
	output wire [31:0]	wbs7_dat_o,
	output wire		wbs7_we_o,
	output wire		wbs7_cyc_o,
	output wire		wbs7_stb_o,
	output wire  [2:0]	wbs7_cti_o,
	output wire  [1:0]	wbs7_bte_o,
	input wire  [31:0]	wbs7_dat_i,
	input wire		wbs7_ack_i,
	input wire		wbs7_err_i,
	input wire		wbs7_rty_i,
	//---> Clock and reset
   	input wire	wb_clk_i, 
	input wire	wb_rst_i
);
	parameter wb_addr_match_width = 8;
	// Slave addresses
	parameter slave0_adr =  8'h00; 
	parameter slave1_adr =  8'h00; 
	parameter slave2_adr =  8'h00; 
	parameter slave3_adr =  8'h00; 
	parameter slave4_adr =  8'h00; 
	parameter slave5_adr =  8'h00; 
	parameter slave6_adr =  8'h00; 
	parameter slave7_adr =  8'h00; 
   
`define WB_ARB_ADDR_MATCH_SEL 31:32-wb_addr_match_width

// Internal wires and registers
  reg	watchdog_err;   
   // Master input mux output wires
  wire		wbm_ack_o;
  wire		wbm_err_o;
  wire		wbm_rty_o;   

   
  // Master output mux
  assign wbm0_ack_o = wbm_ack_o;
  assign wbm0_err_o = wbm_err_o;
  assign wbm0_rty_o = wbm_rty_o;

`ifdef ARBITER_BBUS_WATCHDOG
	reg [`ARBITER_BBUS_WATCHDOG_TIMER_WIDTH:0] watchdog_timer;
	reg	wbm_stb_r; // Register strobe
	wire wbm_stb_edge; // Detect its edge

	always @(posedge wb_clk_i)
		wbm_stb_r <= wbm0_stb_i;

	assign wbm_stb_edge = (wbm0_stb_i & !wbm_stb_r);
   
	// Counter logic for watchdog
	always @(posedge wb_clk_i)
		if (wb_rst_i) 
			watchdog_timer <= 0;
		else if (wbm_ack_o) // When we see an ack, turn off timer
			watchdog_timer <= 0;
		else if (wbm_stb_edge) // New access means start timer again
			watchdog_timer <= 1;
		else if (|watchdog_timer) // Continue counting if counter > 0
			watchdog_timer <= watchdog_timer + 1;

	// Set the error if watchdog timer expires
	always @(posedge wb_clk_i) 
		watchdog_err <= (&watchdog_timer);
   
`else // !`ifdef ARBITER_BYTEBUS_WATCHDOG
	always @(posedge wb_clk_i) 
		watchdog_err <= 0;
`endif // !`ifdef ARBITER_BYTEBUS_WATCHDOG

   
  // Wishbone slave mux out wires
  wire [31:0]	wbs_adr_o;
  wire  [7:0]	wbs_dat_o;
  wire		wbs_we_o;
  wire		wbs_cyc_o;
  wire		wbs_stb_o;
  wire [2:0]	wbs_cti_o;
  wire [1:0]	wbs_bte_o;

  wire  [7:0]	wbs_dat_i;
  wire		wbs_ack_i;
  wire		wbs_err_i;
  wire		wbs_rty_i;

  // Slave select wire
  wire [7:0]  wb_slave_sel; 
  // Slave out mux in wires   
  wire [31:0]	wbs_dat_o_mux_i [0:7]; // Seven at the end is number of slaves
  wire		wbs_ack_o_mux_i [0:7];
  wire		wbs_err_o_mux_i [0:7];
  wire		wbs_rty_o_mux_i [0:7];

  // Slave selects
  assign wb_slave_sel[0] = wbm0_adr_i[`WB_ARB_ADDR_MATCH_SEL] == slave0_adr;
  assign wb_slave_sel[1] = wbm0_adr_i[`WB_ARB_ADDR_MATCH_SEL] == slave1_adr;
  assign wb_slave_sel[2] = wbm0_adr_i[`WB_ARB_ADDR_MATCH_SEL] == slave2_adr;
  assign wb_slave_sel[3] = wbm0_adr_i[`WB_ARB_ADDR_MATCH_SEL] == slave3_adr;
  assign wb_slave_sel[4] = wbm0_adr_i[`WB_ARB_ADDR_MATCH_SEL] == slave4_adr;
  assign wb_slave_sel[5] = wbm0_adr_i[`WB_ARB_ADDR_MATCH_SEL] == slave5_adr;
  assign wb_slave_sel[6] = wbm0_adr_i[`WB_ARB_ADDR_MATCH_SEL] == slave6_adr;
  assign wb_slave_sel[7] = wbm0_adr_i[`WB_ARB_ADDR_MATCH_SEL] == slave7_adr;

  // Assign master inputs to slaves and slave inputs for MUXing  back to master
  //---> Slave 0
  assign wbs0_adr_o = wbm0_adr_i;
  assign wbs0_sel_o = wbm0_sel_i;
  assign wbs0_dat_o = wbm0_dat_i;
  assign wbs0_cyc_o = wbm0_cyc_i & wb_slave_sel[0];
  assign wbs0_stb_o = wbm0_stb_i & wb_slave_sel[0];   
  assign wbs0_we_o  = wbm0_we_i;
  assign wbs0_cti_o = wbm0_cti_i;
  assign wbs0_bte_o = wbm0_bte_i;
  assign wbs_dat_o_mux_i[0] = wbs0_dat_i;
  assign wbs_ack_o_mux_i[0] = wbs0_ack_i & wb_slave_sel[0];
  assign wbs_err_o_mux_i[0] = wbs0_err_i & wb_slave_sel[0];
  assign wbs_rty_o_mux_i[0] = wbs0_rty_i & wb_slave_sel[0];
  //---> Slave 1
  assign wbs1_adr_o = wbm0_adr_i;
  assign wbs1_sel_o = wbm0_sel_i;
  assign wbs1_dat_o = wbm0_dat_i;
  assign wbs1_cyc_o = wbm0_cyc_i & wb_slave_sel[1];
  assign wbs1_stb_o = wbm0_stb_i & wb_slave_sel[1];
  assign wbs1_we_o  = wbm0_we_i;
  assign wbs1_cti_o = wbm0_cti_i;
  assign wbs1_bte_o = wbm0_bte_i;
  assign wbs_dat_o_mux_i[1] = wbs1_dat_i;
  assign wbs_ack_o_mux_i[1] = wbs1_ack_i & wb_slave_sel[1];
  assign wbs_err_o_mux_i[1] = wbs1_err_i & wb_slave_sel[1];
  assign wbs_rty_o_mux_i[1] = wbs1_rty_i & wb_slave_sel[1];
  //---> Slave 2
  assign wbs2_adr_o = wbm0_adr_i;
  assign wbs2_sel_o = wbm0_sel_i;
  assign wbs2_dat_o = wbm0_dat_i;
  assign wbs2_cyc_o = wbm0_cyc_i & wb_slave_sel[2];
  assign wbs2_stb_o = wbm0_stb_i & wb_slave_sel[2];
  assign wbs2_we_o  = wbm0_we_i;
  assign wbs2_cti_o = wbm0_cti_i;
  assign wbs2_bte_o = wbm0_bte_i;
  assign wbs_dat_o_mux_i[2] = wbs2_dat_i;
  assign wbs_ack_o_mux_i[2] = wbs2_ack_i & wb_slave_sel[2];
  assign wbs_err_o_mux_i[2] = wbs2_err_i & wb_slave_sel[2];
  assign wbs_rty_o_mux_i[2] = wbs2_rty_i & wb_slave_sel[2];
  //---> Slave 3
  assign wbs3_adr_o = wbm0_adr_i;
  assign wbs3_sel_o = wbm0_sel_i;
  assign wbs3_dat_o = wbm0_dat_i;
  assign wbs3_cyc_o = wbm0_cyc_i & wb_slave_sel[3];
  assign wbs3_stb_o = wbm0_stb_i & wb_slave_sel[3];
  assign wbs3_we_o  = wbm0_we_i;
  assign wbs3_cti_o = wbm0_cti_i;
  assign wbs3_bte_o = wbm0_bte_i;
  assign wbs_dat_o_mux_i[3] = wbs3_dat_i;
  assign wbs_ack_o_mux_i[3] = wbs3_ack_i & wb_slave_sel[3];
  assign wbs_err_o_mux_i[3] = wbs3_err_i & wb_slave_sel[3];
  assign wbs_rty_o_mux_i[3] = wbs3_rty_i & wb_slave_sel[3];
  //---> Slave 4
  assign wbs4_adr_o = wbm0_adr_i;
  assign wbs4_sel_o = wbm0_sel_i;
  assign wbs4_dat_o = wbm0_dat_i;
  assign wbs4_cyc_o = wbm0_cyc_i & wb_slave_sel[4];
  assign wbs4_stb_o = wbm0_stb_i & wb_slave_sel[4];
  assign wbs4_we_o  = wbm0_we_i;
  assign wbs4_cti_o = wbm0_cti_i;
  assign wbs4_bte_o = wbm0_bte_i;
  assign wbs_dat_o_mux_i[4] = wbs4_dat_i;
  assign wbs_ack_o_mux_i[4] = wbs4_ack_i & wb_slave_sel[4];
  assign wbs_err_o_mux_i[4] = wbs4_err_i & wb_slave_sel[4];
  assign wbs_rty_o_mux_i[4] = wbs4_rty_i & wb_slave_sel[4];
  //---> Slave 5
  assign wbs5_adr_o = wbm0_adr_i;
  assign wbs5_sel_o = wbm0_sel_i;
  assign wbs5_dat_o = wbm0_dat_i;
  assign wbs5_cyc_o = wbm0_cyc_i & wb_slave_sel[5];
  assign wbs5_stb_o = wbm0_stb_i & wb_slave_sel[5];
  assign wbs5_we_o  = wbm0_we_i;
  assign wbs5_cti_o = wbm0_cti_i;
  assign wbs5_bte_o = wbm0_bte_i;
  assign wbs_dat_o_mux_i[5] = wbs5_dat_i;
  assign wbs_ack_o_mux_i[5] = wbs5_ack_i & wb_slave_sel[5];
  assign wbs_err_o_mux_i[5] = wbs5_err_i & wb_slave_sel[5];
  assign wbs_rty_o_mux_i[5] = wbs5_rty_i & wb_slave_sel[5];
  //---> Slave 6
  assign wbs6_adr_o = wbm0_adr_i;
  assign wbs6_sel_o = wbm0_sel_i;
  assign wbs6_dat_o = wbm0_dat_i;
  assign wbs6_cyc_o = wbm0_cyc_i & wb_slave_sel[6];
  assign wbs6_stb_o = wbm0_stb_i & wb_slave_sel[6];
  assign wbs6_we_o  = wbm0_we_i;
  assign wbs6_cti_o = wbm0_cti_i;
  assign wbs6_bte_o = wbm0_bte_i;
  assign wbs_dat_o_mux_i[6] = wbs6_dat_i;
  assign wbs_ack_o_mux_i[6] = wbs6_ack_i & wb_slave_sel[6];
  assign wbs_err_o_mux_i[6] = wbs6_err_i & wb_slave_sel[6];
  assign wbs_rty_o_mux_i[6] = wbs6_rty_i & wb_slave_sel[6];
  //---> Slave 7
  assign wbs7_adr_o = wbm0_adr_i;
  assign wbs7_sel_o = wbm0_sel_i;
  assign wbs7_dat_o = wbm0_dat_i;
  assign wbs7_cyc_o = wbm0_cyc_i & wb_slave_sel[7];
  assign wbs7_stb_o = wbm0_stb_i & wb_slave_sel[7];
  assign wbs7_we_o  = wbm0_we_i;
  assign wbs7_cti_o = wbm0_cti_i;
  assign wbs7_bte_o = wbm0_bte_i;
  assign wbs_dat_o_mux_i[7] = wbs7_dat_i;
  assign wbs_ack_o_mux_i[7] = wbs7_ack_i & wb_slave_sel[7];
  assign wbs_err_o_mux_i[7] = wbs7_err_i & wb_slave_sel[7];
  assign wbs_rty_o_mux_i[7] = wbs7_rty_i & wb_slave_sel[7];
//-------------------------------------------------------------------//
  // Master out mux from slave in data
  assign wbm0_dat_o = 
		wb_slave_sel[0] ? wbs_dat_o_mux_i[0] :
		wb_slave_sel[1] ? wbs_dat_o_mux_i[1] :
		wb_slave_sel[2] ? wbs_dat_o_mux_i[2] :
		wb_slave_sel[3] ? wbs_dat_o_mux_i[3] :
		wb_slave_sel[4] ? wbs_dat_o_mux_i[4] :
		wb_slave_sel[5] ? wbs_dat_o_mux_i[5] :
		wb_slave_sel[6] ? wbs_dat_o_mux_i[6] :
		wb_slave_sel[7] ? wbs_dat_o_mux_i[7] :
				  wbs_dat_o_mux_i[0]; // Default is slave 0
  // Master out acks, or together
  assign wbm_ack_o = 	  wbs_ack_o_mux_i[0]  
			| wbs_ack_o_mux_i[1]  
			| wbs_ack_o_mux_i[2]
			| wbs_ack_o_mux_i[3]
			| wbs_ack_o_mux_i[4]
			| wbs_ack_o_mux_i[5]
			| wbs_ack_o_mux_i[6]
			| wbs_ack_o_mux_i[7]
			;
  // Error
  assign wbm_err_o =	  wbs_err_o_mux_i[0] 
   			| wbs_err_o_mux_i[1]
			| wbs_err_o_mux_i[2]
			| wbs_err_o_mux_i[3]
			| wbs_err_o_mux_i[4]
			| wbs_err_o_mux_i[5]
			| wbs_err_o_mux_i[6]
			| wbs_err_o_mux_i[7]
			| watchdog_err;
  // Retry
  assign wbm_rty_o = 	  wbs_rty_o_mux_i[0]
   			| wbs_rty_o_mux_i[1]
			| wbs_rty_o_mux_i[2]
			| wbs_rty_o_mux_i[3]
			| wbs_rty_o_mux_i[4]
			| wbs_rty_o_mux_i[5]
			| wbs_rty_o_mux_i[6]
			| wbs_rty_o_mux_i[7]
			;
endmodule // arbiter_bbus
